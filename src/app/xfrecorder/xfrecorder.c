#include "stdint.h"
#include "stddef.h"
#include "cpu_peri.h"
#include "stdint.h"
#include "stddef.h"
#include "cpu_peri.h"
#include "ring.h"
#include "app_ws_client.h"
#include "app_recorder.h"
#include <heap.h> // psram malloc
#include "cJSON.h"
#include "ghttp.h"

#define MEMORY_FILE_SIZE   (30*1024) // 内存文件最大大小
static struct HeapCB *my_heap = 0;
unsigned char *memFile;
u16 xRecorderEvtt = CN_EVTT_ID_INVALID; // recorder事件类型
int memFileIdx = 0; // 内存文件读写位置

extern int gKeyRecordPressDown;

void encode(unsigned char *data,u32 len)
{
    printf("len      len:%d\r\n",len);
    char *out = malloc(len*4/3);
    printf("out     out:%d\r\n",strlen(out));
    base64_encode(data,len,out);
    FILE *wfile = fopen("/SD/audio.txt", "wb");
    fwrite(out,strlen(out),1,wfile);
    fclose(wfile);
//    printf("out     out:%s\r\n",out);
    // char *ur = url_encode(out);
    // printf("url_encode len:%d\r\n",strlen(ur));
    // char *tt = "%E4%BD%A0%E5%A5%BD";
    char *tt = "你好";
    // urlencode(tt);
    getEvRequest(out,tt);
    printf("base64::::::%s\r\n",tt);
}

ptu32_t xfrecorder_event_main(void)
{
    unsigned char *pnew = (unsigned char*)malloc(1024);
    int ret=0;
    djy_linein_adc_open(3200, 1, audio_sample_rate_16000, AUD_ADC_LINEIN_DETECT_PIN);
    while(1)
    {

        if(gKeyRecordPressDown)
        {
            my_heap = M_FindHeap("PSRAM");
            memFile = M_MallocHeap(30*1024,my_heap,0);
            audio_adc_clear(); //清除DMA数据
            ret = djy_audio_adc_read(pnew, 1024);
            if (memFileIdx < MEMORY_FILE_SIZE) // 128Byte为了容下Tail
            {
                memcpy(&memFile[memFileIdx], pnew, ret);
                memFileIdx += ret;
                printf("========1======:%d\r\n",memFileIdx);
            }
            else
            {
                FILE *wfile = fopen("/SD/voice.pcm", "wb");
                fwrite(memFile,memFileIdx,1,wfile);
                fclose(wfile);
                transform_pcm_to_wave("/SD/voice.pcm", 1, 16000, "/SD/voice.wav");
                printf("========2======:%d\r\n",memFileIdx);
                encode(memFile,memFileIdx);
//                printf("memFile      memFile:%d\r\n",strlen(memFile));
                M_FreeHeap(memFile,my_heap);
//                printf("memFile      memFile:%d\r\n",strlen(memFile));
                memFileIdx = 0;
                gKeyRecordPressDown = 0;
            }
        }
        Djy_EventDelay(10000);
    }

    return 0;
}
void upload_file_bmob(unsigned char* buf,int len)
{
    char *uri="http://api2.bmob.cn/2/files/test.pcm";
    ghttp_request *request = NULL;
    ghttp_status status;
    request = ghttp_request_new();
    if (NULL == request)
    {
        return ;
    }
    if (ghttp_set_uri(request, uri) == -1)
    {
        ghttp_request_destroy(request);
        printf("设置uri失败");
        return;
    }
    if (ghttp_set_type(request, ghttp_type_post) == -1)     //post
    {
        ghttp_request_destroy(request);
        printf("设置请求方式失败");
        return;
    }

    ghttp_set_header(request, "X-Bmob-Application-Id", "97068bc5100ab12f3bffe50c9bfdb35e");
    ghttp_set_header(request, "X-Bmob-REST-API-Key", "527b7bc8ef105f4bd9ae77f9d359c1f6");
    ghttp_set_header(request, http_hdr_Connection, "close");
    ghttp_set_sync(request, ghttp_sync); //set sync
    ghttp_set_body(request, buf, len);
    ghttp_prepare(request);
    status = ghttp_process(request);
    if (status == ghttp_error)
    {
        ghttp_request_destroy(request);
        printf("请求失败，失败的具体的情况%s",  ghttp_get_error(request));
        return;
    }
    printf("长度为%d\n",len);
    printf("返回的内容为%s\n",ghttp_get_body(request));

    //解析返回的内容
    cJSON *root = cJSON_Parse(ghttp_get_body(request));//将字符串转为cJSON对象
    cJSON *item = cJSON_GetObjectItem(root, "url");//获取url节点
    char* url=item->valuestring;//将url值提取出来

    ghttp_request_destroy(request);

    printf("获取到的url为%s\n",url);
}
void xfrecorderEventInit(void)
{
    xRecorderEvtt = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_REAL, 0, 0,
            xfrecorder_event_main, NULL, 0x1000, "xrecorder function");

    if (CN_EVTT_ID_INVALID != xRecorderEvtt)
    {
        Djy_EventPop(xRecorderEvtt, NULL, 0, 0, 0, 0);
    }
    else
    {
        printf("[DEBUG] Djy_EvttRegist-recorder_event_main error\r\n");
    }
}

int transform_pcm_to_wave(const char *pcmpath, int channels, int sample_rate, const char *wavepath)
{
    typedef struct WAVE_HEADER{
        char    fccID[4];       //内容为"RIFF"
        unsigned int dwSize;   //最后填写，WAVE格式音频的大小
        char    fccType[4];     //内容为"WAVE"
    }WAVE_HEADER;

    typedef struct WAVE_FMT{
        char    fccID[4];          //内容为"fmt "
        unsigned int  dwSize;     //内容为WAVE_FMT占的字节数，为16
        short int wFormatTag; //如果为PCM，改值为 1
        short int wChannels;  //通道数，单通道=1，双通道=2
        unsigned int  dwSamplesPerSec;//采样频率
        unsigned int  dwAvgBytesPerSec;/* ==dwSamplesPerSec*wChannels*uiBitsPerSample/8 */
        short int wBlockAlign;//==wChannels*uiBitsPerSample/8
        short int uiBitsPerSample;//每个采样点的bit数，8bits=8, 16bits=16
    }WAVE_FMT;

    typedef struct WAVE_DATA{
        char    fccID[4];       //内容为"data"
        unsigned int dwSize;   //==NumSamples*wChannels*uiBitsPerSample/8
    }WAVE_DATA;

    if(channels==2 || sample_rate==0)
    {
        channels = 2;
        sample_rate = 44100;
    }

    WAVE_HEADER pcmHEADER;
    WAVE_FMT    pcmFMT;
    WAVE_DATA   pcmDATA;

    short int m_pcmData;
    FILE *fp, *fpout;

    fp = fopen(pcmpath, "rb+");
    if(fp==NULL)
    {
        printf("Open pcm file error.\n");
        return -1;
    }
    fpout = fopen(wavepath, "wb+");
    if(fpout==NULL)
    {
        printf("Create wav file error.\n");
        return -1;
    }

    /* WAVE_HEADER */
    memcpy(pcmHEADER.fccID, "RIFF", 4);
    memcpy(pcmHEADER.fccType, "WAVE", 4);
    fseek(fpout, sizeof(WAVE_HEADER), 1);   //1=SEEK_CUR
    /* WAVE_FMT */
    memcpy(pcmFMT.fccID, "fmt ", 4);
    pcmFMT.dwSize = 16;
    pcmFMT.wFormatTag = 0x0001;
    pcmFMT.wChannels = 1;
    pcmFMT.dwSamplesPerSec = 16000;
    pcmFMT.uiBitsPerSample = 16;
    /* ==dwSamplesPerSec*wChannels*uiBitsPerSample/8 */
    pcmFMT.dwAvgBytesPerSec = pcmFMT.dwSamplesPerSec*pcmFMT.wChannels*pcmFMT.uiBitsPerSample/8;
    /* ==wChannels*uiBitsPerSample/8 */
    pcmFMT.wBlockAlign = pcmFMT.wChannels*pcmFMT.uiBitsPerSample/8;

    fwrite(&pcmFMT, sizeof(WAVE_FMT), 1, fpout);

    /* WAVE_DATA */
    memcpy(pcmDATA.fccID, "data", 4);
    pcmDATA.dwSize = 0;
    fseek(fpout, sizeof(WAVE_DATA), 1);

    fread(&m_pcmData, sizeof(short int), 1, fp);
    while(!feof(fp))
    {
        pcmDATA.dwSize += sizeof(short int);
        fwrite(&m_pcmData, sizeof(short int), 1, fpout);
        fread(&m_pcmData, sizeof(short int), 1, fp);
    }
    //
    // pcmDATA.dwSize 表示pcm文件的大小，单位是字节，http://soundfile.sapp.org/doc/WaveFormat/ 中给出的计算方法是NumSamples * NumChannels * BitsPerSample/8
    // 试了一下不行，只能播出大概一秒的时间，我觉得上面那个公式 * 秒数就能表示pcm中数据的字节数了。
    // pcmDATA.dwSize = (unsigned int)(pcmFMT.dwSamplesPerSec * (unsigned int)pcmFMT.wChannels * (unsigned int)pcmFMT.uiBitsPerSample / 8);
    pcmHEADER.dwSize = 36 + pcmDATA.dwSize;

    rewind(fpout);
    fwrite(&pcmHEADER, sizeof(WAVE_HEADER), 1, fpout);
    fseek(fpout, sizeof(WAVE_FMT), SEEK_CUR);
    fwrite(&pcmDATA, sizeof(WAVE_DATA), 1, fpout);

    fclose(fp);
    fclose(fpout);

    return 0;
}
