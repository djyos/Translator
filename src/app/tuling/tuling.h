/*
 * VoicePrompt.h
 *
 *  Created on: 2019��5��13��
 *      Author: c
 */

#ifndef _TULING_H_
#define _TULING_H_

void getuid();
void gettoken(char token[], int number);
void tts_result(char *s);
int url_encode(char const *s,char* result);
int tuling_tts(char *text);
int tuling_ainlp(char *text);
int tuling_ainlp_twice(char *text);
int tuling_asr(unsigned char* buf,int len,char *result);
int tuling_nlp(char *text);

#endif /* _TULING_H_ */
