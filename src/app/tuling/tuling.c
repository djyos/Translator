#include "project_config.h"
#include "stdint.h"
#include "stddef.h"
#include "ghttp.h"
#include "algorithm.h"
#include "cJSON.h"
#include "asea.h"
#include "tuling.h"
#include "stdlib.h"
#include "music_control.h"

char tokent[64] = {0};


int playurl(char *str)
{
    int head, end;
    char dest[256];
    char post[256];
    char address[256];

    for (int i = 0; i < strlen(str); i++)
    {
        if (str[i] == '/' && str[i - 1] == '/')
        {
            head = i;
        }
        if (str[i] == '/' && str[i - 1] != '/' && str[i + 1] != '/')
        {
            end = i - 1;
            break;
        }
    }

    printf("sibichi_url:%s\r\n", str);

    strncpy(dest, str + head + 1, strlen(str) - head);
    dest[end - head] = '\0';
    sprintf(post, "%s", dest);

    strncpy(dest, str + end + 2, strlen(str) - end);
    dest[strlen(str) - end] = '\0';
    sprintf(address, "%s", dest);

//    printf("post:%s\r\n", post);
//    printf("address:%s\r\n", address);
    return http_test(post, 80, address);

}
extern int music_delete;
char tlurl[512]={0};
int tuling_tts(char *text)
{
    char *uri = "http://smartdevice.ai.tuling123.com/speech/v2/tts";
    char payload[2048];
    sprintf(payload,"{\"parameters\":{\"ak\":\"1b94b4aff06f4f8ba0c1d7aafe91173f\",\"uid\":\"c829d086e4f81646c8a4bdf8632ea2fb\",\"token\":\"%s\",\"text\":\"%s\",\"tts\":3,\"tone\":22}}",tokent,text);
    printf("BODY:%s\r\n",payload);

    ghttp_request *request = ghttp_request_new();
    if (ghttp_set_uri(request, uri) == -1)
    {
        printf("ghttp_set_uri is fail\r\n");
        ghttp_close(request);
        ghttp_request_destroy(request);
        return -1;
    }
    if (ghttp_set_type(request, ghttp_type_post) == -1)     //post
    {
        printf("ghttp_set_type is fail\r\n");
        ghttp_close(request);
        ghttp_request_destroy(request);
        return -1;
    }
    ghttp_set_header(request,"Content-Type","application/json");
    ghttp_set_body(request, payload, strlen(payload));    //
    ghttp_prepare(request);
    ghttp_status status = ghttp_process(request);
    if (status == ghttp_error)
    {
        printf("ghttp_process is fail:%s",  ghttp_get_error(request));
        ghttp_close(request);
        ghttp_request_destroy(request);
        return -1;
    }

    int http_len=ghttp_get_body_len(request);
    char *buf=malloc(http_len);
    memset(buf,0,http_len);
    buf=ghttp_get_body(request);
    printf("ghttp_get_body is %s\n\r",buf);

    char *tokent_start_point = strstr(buf, "\"token\"");
    if (tokent_start_point)
    {
        memcpy(tokent, tokent_start_point+9, 32);
        tokent[32] = '\0';
       // printf("new tokent is %s\r\n", tokent);
    }
    ghttp_close(request);
    ghttp_request_destroy(request);

    cJSON* cjson = cJSON_Parse(buf);
    if ( cjson )
    {
        cJSON   *url = cJSON_GetObjectItem( cjson, "url" );
        int arr_size =cJSON_GetArraySize(url);
        for(int i = 0;i <arr_size;i++)
        {
             cJSON* arr_item =url->child;
             memset(tlurl,0,512);
             strcpy(tlurl,arr_item->valuestring);
             url = url->next;
             printf("tlurl is %s\r\n",tlurl);
        }
    }
    cJSON_Delete( cjson );
    free(buf);
    if(tlurl[0]!='\0')
    {
        music_delete=0;
        playurl(tlurl);
    }

    return 1;
}

int tuling_ttse(char *text)
{
    char *uri = "http://smartdevice.ai.tuling123.com/speech/v2/tts";
    char payload[2048];
    sprintf(payload,"{\"parameters\":{\"ak\":\"1b94b4aff06f4f8ba0c1d7aafe91173f\",\"uid\":\"c829d086e4f81646c8a4bdf8632ea2fb\",\"token\":\"%s\",\"text\":\"%s\",\"tts\":3,\"tts_lan\":1,\"tone\":22}}",tokent,text);
    printf("BODY:%s\r\n",payload);

    ghttp_request *request = ghttp_request_new();
    if (ghttp_set_uri(request, uri) == -1)
    {
        printf("ghttp_set_uri is fail\r\n");
        ghttp_close(request);
        ghttp_request_destroy(request);
        return -1;
    }
    if (ghttp_set_type(request, ghttp_type_post) == -1)     //post
    {
        printf("ghttp_set_type is fail\r\n");
        ghttp_close(request);
        ghttp_request_destroy(request);
        return -1;
    }
    ghttp_set_header(request,"Content-Type","application/json");
    ghttp_set_body(request, payload, strlen(payload));    //
    ghttp_prepare(request);
    ghttp_status status = ghttp_process(request);
    if (status == ghttp_error)
    {
        printf("ghttp_process is fail:%s",  ghttp_get_error(request));
        ghttp_close(request);
        ghttp_request_destroy(request);
        return -1;
    }

    int http_len=ghttp_get_body_len(request);
    char *buf=malloc(http_len);
    memset(buf,0,http_len);
    buf=ghttp_get_body(request);
    printf("ghttp_get_body is %s\n\r",buf);

    char *tokent_start_point = strstr(buf, "\"token\"");
    if (tokent_start_point)
    {
        memcpy(tokent, tokent_start_point+9, 32);
        tokent[32] = '\0';
        //printf("new tokent is %s\r\n", tokent);
    }

    ghttp_close(request);
    ghttp_request_destroy(request);

    cJSON* cjson = cJSON_Parse(buf);
    if ( cjson )
    {
        cJSON   *url = cJSON_GetObjectItem( cjson, "url" );
        int arr_size =cJSON_GetArraySize(url);
        for(int i = 0;i <arr_size;i++)
        {
            cJSON* arr_item =url->child;
            memset(tlurl,0,512);
            strcpy(tlurl,arr_item->valuestring);
            url = url->next;
            printf("tlurl is %s\r\n",tlurl);
        }
    }
    cJSON_Delete( cjson );
    free(buf);
    if(tlurl[0]!='\0')
    {
        music_delete=0;
        playurl(tlurl);
    }
    return 1;
}



