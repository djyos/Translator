/*
 * XfBroadcast.c
 *
 *  Created on: 2020年3月20日
 *      Author: zxy
 */

#include "ghttp.h"
#include "cJSON.h"
#include "GuiInfo.h"

char ws_recognize_url[512]  = { 0 };
char recognize_language[16] = "zh_cn";                                  //识别的语言
char *recognize_APIKey      = "65b60fe83f24c17f8c2caee4273fcf29";
char *recognize_APISecret   = "1d48ede6135dab27a392c0e1db0435ff";
char *recognize_host        = "iat-api.xfyun.cn";                       //请求主机

/* 获取时间戳,RFC1123 */
//void getGmtTime( char *out )
//{
//    char *uri = "http://api.m.taobao.com/rest/api3.do?api=mtop.common.getTimestamp";
//    ghttp_request   *request    = NULL;
//    /* Allocate a new empty request object */
//    request = ghttp_request_new();
//    if ( NULL == request )
//    {
//        printf( "ghttp_request_new return null \r\n" );
//    }
//    /* Set the URI for the request object */
//    ghttp_set_uri( request, uri );
//    ghttp_set_type( request, ghttp_type_get );
//    /* Close the connection after you are done. */
//    ghttp_set_header( request, http_hdr_Connection, "close" );
//    /* Prepare the connection */
//    ghttp_prepare( request );
//    /* Process the request */
//    ghttp_process( request );
//    sprintf( out, "%s", ghttp_get_header( request, "Date" ) );
//    ghttp_close( request );
//    ghttp_request_destroy( request );
//}

void Recognize_Authorization( char *in )
{
    char recognize_signature_sha[256]        = { 0 };
    char recognize_origin[256]               = { 0 };
    char recognzie_signature[256]            = { 0 };
    char recognzie_authorization_origin[256] = { 0 };
    char url_recognize_date[32]              = { 0 };
    char recognize_authorization[512]        = { 0 };              // 使用base64编码的签名相关信息(签名基于hmac-sha256计算)

    strcpy( url_recognize_date, in );

    sprintf( recognize_origin, "host: %s\ndate: %s\nGET /v2/iat HTTP/1.1", recognize_host, in );

    hmac_sha256( recognize_APISecret, strlen( recognize_APISecret ), recognize_origin, strlen( recognize_origin ), recognize_signature_sha, 32 );

    base64_encode( recognize_signature_sha, strlen( recognize_signature_sha ), recognzie_signature );

    sprintf( recognzie_authorization_origin, "api_key=\"%s\", algorithm=\"hmac-sha256\", headers=\"host date request-line\", signature=\"%s\"", recognize_APIKey, recognzie_signature );

    base64_encode( recognzie_authorization_origin, strlen( recognzie_authorization_origin ), recognize_authorization );

    sprintf( ws_recognize_url, "ws://iat-api.xfyun.cn/v2/iat?authorization=%s&date=%s&host=iat-api.xfyun.cn", recognize_authorization, urlencode( url_recognize_date ) );
}

