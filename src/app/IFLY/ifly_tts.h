/*
 * ifly_util.h
 *
 *  Created on: 2020��3��20��
 *      Author: WangXi
 */

#ifndef APP_TTS_IFLY_UTIL_H_
#define APP_TTS_IFLY_UTIL_H_

/**
 * ifly developer info
 */
struct IflyInfo
{
    const char *ak;
    const char *as;
    const char *app_id;
};

/**
 * ifly url info
 */
struct IflyUrl
{
    const char *host;
    const char *path;
};

void web_gmt_time(char *gmt_time);

/**
 * build ifly web socket uri
 */
void ifly_ws_uri(struct IflyInfo *info, struct IflyUrl *url, char *uri);

/**
 * build ifly http uri
 */
void ifly_http_uri(struct IflyInfo *info, struct IflyUrl *url, char *uri);

/**
 * build format text to translate
 */
void ifly_tts_post_data(const char *app_id, char *text, char *post_data, char *rec_lang);

/**
 * format tts response pcm data
 * @param data tts response json
 * @param out pcm data
 * @return length of pcm
 */
unsigned int ifly_tts_resp_data( char *data, unsigned char *out );

void tts_pcm_play(unsigned char *audio, int audio_len);

void tts_text2pcm(char *text, char *reg_lang);

void tts_play(char *text, char *reg_lang);


#endif /* APP_TTS_IFLY_UTIL_H_ */
