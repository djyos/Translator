/*
 * XfEvaluate.h
 *
 *  Created on: 2020��3��4��
 *      Author: Administrator
 */

#ifndef APP_IFLY_XFEVALUATE_H_
#define APP_IFLY_XFEVALUATE_H_

void getTbTimeStamp(void);
void getParam(void);
void getEvaluateResult(void);
void getCheckSum(void);
void XfUrlEncode(unsigned char *buf);
void getEvRequest(unsigned char *audio,unsigned char *text);
#endif /* APP_IFLY_XFEVALUATE_H_ */
