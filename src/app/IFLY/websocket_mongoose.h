/*
 * websocket_mongoose.h
 *
 *  Created on: 2020��3��20��
 *      Author: Magic
 */

#ifndef APP_TTS_WEBSOCKET_MONGOOSE_H_
#define APP_TTS_WEBSOCKET_MONGOOSE_H_

#include "mongoose.h"

#define WS_SYNC_TIME_OUT 10 * 1000 * 1000

struct mg_ws_request;

typedef struct websocket_message mg_ws_response;

bool_t ws_model_init();

void *ws_conn(const char *url, const char *protocol, const char *extra_headers, void (*ws_call)(mg_ws_response *));

bool_t ws_send(struct mg_ws_request *ws_request, char *send_buff);

void ws_close(struct mg_ws_request *ws_request);

#endif /* APP_TTS_WEBSOCKET_MONGOOSE_H_ */
