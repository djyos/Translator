/*
 * UrlEncode.h
 *
 *  Created on: 2020��3��6��
 *      Author: Administrator
 */

#ifndef APP_IFLY_URLENCODE_H_
#define APP_IFLY_URLENCODE_H_

int hex2dec(char c);
char dec2hex(short int c);
void urlencode(char url[]);
void urldecode(char url[]);


#endif /* APP_IFLY_URLENCODE_H_ */
