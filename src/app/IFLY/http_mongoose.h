/*
 * http_mongooser.h
 *
 *  Created on: 2020��3��22��
 *      Author: Magic
 */

#ifndef APP_MG_CLIENT_HTTP_MONGOOSE_H_
#define APP_MG_CLIENT_HTTP_MONGOOSE_H_

#include "mongoose.h"

#define HTTP_SYNC_TIME_OUT 20*1000*1000

typedef struct http_message mg_http_response;

struct mg_http_request
{
    char *url;
    char *header;
    char *post_data;
    void (*http_call)(mg_http_response *hm);
    bool_t exit_flag;
    bool_t success;
};

bool_t http_model_init();

bool_t http_request(const char *url, const char *header, const char *post_data,
        void *resp_data, void (*http_call)(mg_http_response *hm));

#endif /* APP_MG_CLIENT_HTTP_MONGOOSE_H_ */
