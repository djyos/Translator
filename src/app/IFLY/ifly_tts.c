/*
 * ifly_util.c
 *
 *  Created on: 2020年3月20日
 *      Author: WangXi
 */
#include "ifly_tts.h"
#include "ghttp.h"
#include <stddef.h>
#include "cJSON.h"
#include "WebSocket_Client.h"
#include "websocket_mongoose.h"

struct IflyInfo info = {
    "65b60fe83f24c17f8c2caee4273fcf29",     /* app key */
    "1d48ede6135dab27a392c0e1db0435ff",     /* app secret */
    "5e572f0f"                              /* app id */
};

struct IflyUrl tts_url = {
    "tts-api.xfyun.cn",
    "/v2/tts"
};

char tts_uri[0x200];                    /* 512 */

char ifly_post_data[0xFF];   /* 255 */

extern int  recvlen;

extern unsigned char word_storage_result_from[512];

extern u32 playing_status;

/* gmt time */

void web_gmt_time( char *gmt_time )
{
    char        *uri        = "http://api.m.taobao.com/rest/api3.do?api=mtop.common.getTimestamp";
    ghttp_request   *request    = NULL;
    /* Allocate a new empty request object */
    request = ghttp_request_new();
    if ( NULL == request )
    {
        printf( "ghttp_request_new return null \r\n" );
    }
    /* Set the URI for the request object */
    ghttp_set_uri( request, uri );
    ghttp_set_type( request, ghttp_type_get );
    /* Close the connection after you are done. */
    ghttp_set_header( request, http_hdr_Connection, "close" );
    /* Prepare the connection */
    ghttp_prepare( request );
    /* Process the request */
    ghttp_process( request );
    sprintf( gmt_time, "%s", ghttp_get_header( request, "Date" ) );
    ghttp_request_destroy( request );
}


void ifly_signature( const char *host, char *date, const char *request_line, const char *as, char *signature )
{
    const char *sign_origin_fmt = "host: %s\n"
                      "date: %s\n"
                      "GET %s HTTP/1.1";
    unsigned char *signature_origin = malloc( strlen( sign_origin_fmt ) + strlen( host ) +
                          strlen( date ) + strlen( request_line ) );
    sprintf( signature_origin, sign_origin_fmt, host, date, request_line );

    unsigned char signature_sha[32];
    hmac_sha256( as, strlen( as ), signature_origin, strlen( signature_origin ), signature_sha, 32 );

    base64_encode( signature_sha, 32, signature );

    free( signature_origin );
}


void ifly_authorization( const char *ak, char *signature, char *authorization )
{
    /* hmac username/api_key */
    const char *auth_fmt = "api_key=\"%s\", "
                   "algorithm=\"hmac-sha256\", "
                   "headers=\"host date request-line\", "
                   "signature=\"%s\"";
    unsigned char *authorization_origin = malloc( strlen( ak ) + strlen( signature ) + strlen( auth_fmt ) );
    sprintf( authorization_origin, auth_fmt, ak, signature );
    base64_encode( authorization_origin, strlen( authorization_origin ), authorization );
    free( authorization_origin );
}


void ifly_ws_url( const char *host, const char *path, char *auth, char *date, char *url )
{
    char date_urlencode[0x40]; /* 64 */
    strcpy( date_urlencode, date );
    urlencode( date_urlencode );
    sprintf( url, "ws://%s%s?authorization=%s&date=%s&host=%s", host, path, auth, date_urlencode, host );
}


void ifly_ws_uri( struct IflyInfo *info, struct IflyUrl *url, char *uri )
{
    char date[0x20];        /* 32 */
    web_gmt_time( date );

    char sign[0x40];        /* 64 */
    ifly_signature( url->host, date, url->path, info->as, sign );

    char auth[0x120];       /* 288 */
    ifly_authorization( info->ak, sign, auth );

    ifly_ws_url( url->host, url->path, auth, date, uri );
}

void ifly_tts_post_data( const char *app_id, char *text, char *post_data, char *rec_lang )
{
    const char *post_fmt =
        "{"
        "\"common\":{"
        "\"app_id\":\"%s\""
        "},"
        "\"business\":{"
        "\"ent\":\"%s\","
        "\"vcn\":\"%s\","
        "\"aue\":\"%s\","
        "\"speed\":%d,"
        "\"tte\":\"%s\""
        "},"
        "\"data\":{"
        "\"status\":%d,"
//        "\"encoding\":\"%s\","
        "\"text\":\"%s\""
        "}"
        "}";
    char *text_base64 = malloc( strlen( text ) * 2 );
    base64_encode( text, strlen( text ), text_base64 );
    sprintf( post_data, post_fmt, app_id, rec_lang,
         "xiaoyan", "raw", 50, "UTF8",
         2, text_base64 );
//    printf( "[INFO POST_DATA] %s\r\n", post_data );
    free( text_base64 );
}


unsigned int ifly_tts_resp_data( char *data, unsigned char *out )
{
    cJSON *json = cJSON_Parse( data );
    /* cJSON *msg = cJSON_GetObjectItem(json, "message"); */
    cJSON   *j_data     = cJSON_GetObjectItem( json, "data" );
    cJSON   *j_audio    = cJSON_GetObjectItem( j_data, "audio" );
    u32  len  = base64_decode( j_audio->valuestring, strlen( j_audio->valuestring ), out );
    cJSON_Delete( json );
    return(len);
}

void tts_pcm_play( unsigned char *audio, int audio_len )
{
    int ret = 0;                         //单次累加长度
    int pos = 0;                         //累计播放长度
    while ( true )
    {
        if ( pos >= audio_len )
        {
            break;
        }
        ret = WavPlayData( audio + pos, audio_len - pos, 5000 );
        if ( ret > 0 )
        {
//            printf("\r\n[DEBUG pos] %d\r\n",pos);
//            printf("\r\n[DEBUG ret] %d\r\n",ret);
            pos += ret;
        }
        else
        {
            Djy_EventDelay( 300 * mS );
        }
    }
}

void tts_text2pcm( char *text, char *reg_lang )
{
    int wsfd = -1;
    int ret = 0;
    WebsocketData_Type  recvWsDataType;
    memset( tts_uri, 0, 0x200 );
    memset( ifly_post_data, 0, 0xFF );

    ifly_ws_uri( &info, &tts_url, tts_uri );

    printf( "\r\n tts_uri is %s\r\n", tts_uri );

    if ( (wsfd = WebSocket_LinkServer( tts_uri ) ) <= 0 )
    {
        wsfd = -1;
        printf( "WebSocket_LinkServer fail!\r\n" );
    }
    else
    {
        printf( "\r\n wsfd is %d\r\n", wsfd );
        printf( "\r\n WebSocket_LinkServer ok!\r\n" );
        ifly_tts_post_data( info.app_id, text, ifly_post_data, reg_lang );
        WebSocket_send( wsfd, ifly_post_data, strlen( ifly_post_data ), true, WDT_TXTDATA );

        /* 4. 接收WS数据  */
        while ( true )
        {
            long long timebak2 = DjyGetSysTime()/1000; // WebSocket_recv time

            printf( "Ready to exec WebSocket_recv\r\n" );

            uint8_t *DataRecvBuf;
            uint8_t *wsBuf = malloc( 4 );

            memset( wsBuf, 0, 4 );
            WebSocket_recv( wsfd, wsBuf, 4, &recvWsDataType );

            printf("WebSocket_recv time is %dMs\r\n",(int)(DjyGetSysTime()/1000-timebak2));

            long long timebak3 = DjyGetSysTime()/1000; // getrecvbuf time

            printf("recvlen is %d\r\n",recvlen);

            DataRecvBuf = malloc( recvlen );
            memset( DataRecvBuf, 0, recvlen );
            getrecvbuf( DataRecvBuf, wsfd, recvlen );

            printf("getrecvbuf time is %dMs\r\n",(int)(DjyGetSysTime()/1000-timebak3));

//            printf( "DataRecvBuf Result:\r\n %s\r\n", DataRecvBuf );

            cJSON * cjson = cJSON_Parse( DataRecvBuf );
            cJSON * data = cJSON_GetObjectItem( cjson, "data" );
            cJSON * audio = cJSON_GetObjectItem( data, "audio" );
            cJSON * status = cJSON_GetObjectItem( data, "status" );  //status值  1=分包, 2=尾包

//            free( wsBuf );
            free( DataRecvBuf );
#if 1
            printf( "\r\n[INFO status] %d\r\n", status->valueint );
#endif
            unsigned char *tts_audio = malloc(strlen(audio->valuestring));

            u32 a_l = base64_decode(audio->valuestring,strlen(audio->valuestring),tts_audio);

            if(a_l == 0)
            {
                playing_status = 0;
                break;
            }

            tts_audio[a_l]='\0';

            long long timebak = DjyGetSysTime()/1000; // tts_pcm_play time

            tts_pcm_play(tts_audio, a_l);

            printf("tts_pcm_play time is %dMs\r\n",(int)(DjyGetSysTime()/1000-timebak));


//            FILE *fp=fopen("/SD/des.pcm","a+");
//            fwrite(fuck, 1, a_l , fp);
 //           fclose(fp);

//            printf("\r\n[DEBUG a_l] %d\r\n",a_l);

            free(tts_audio);

            if( status->valueint >=2 || status->valueint <=0 ){
                cJSON_Delete( cjson );
                playing_status = 0;

#if 0
                printf("\r\n[DEBUG playing_status DONE] %d\r\n",playing_status);
#endif

                break;
            }
            cJSON_Delete( cjson );
        }
    }
}


void tts_play( char *text, char *reg_lang )
{
    tts_text2pcm( text, reg_lang );
}


