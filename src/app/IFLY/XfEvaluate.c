#include "XfEvaluate.h"
#include "md5.h"
#include "project_config.h"
#include "stdint.h"
#include "stddef.h"
#include "ghttp.h"
#include "cJSON.h"
#include "stdio.h"

const char Xapikey[]="a6f78bfa99658e7a7ff8e63e73c1975e";
const char Xappid[]="5e572f0f";       //应用ID(appid)
char  XcurTime[16];                   //当前UTC时间戳
char  result_level[]="entirety";        //评测结果等级
char  language[5]="zh_cn";            //评测语种
char  category[16]="read_word";       //评测题型
char  Xparam[256];                    //相关参数JSON串经Base64编码后的字符串
unsigned char XcheckSum[256];         //令牌

//获取时间戳
void getTbTimeStamp(void)
{
   char *uri = "http://api.m.taobao.com/rest/api3.do?api=mtop.common.getTimestamp";
   ghttp_request *request = NULL;
   /* Allocate a new empty request object */
   request = ghttp_request_new();
   if (NULL == request) {
       printf("ghttp_request_new return null \r\n");
   }
   /* Set the URI for the request object */
   ghttp_set_uri(request, uri);
   ghttp_set_type(request, ghttp_type_get);
   /* Close the connection after you are done. */
   ghttp_set_header(request, http_hdr_Connection, "close");
   /* Prepare the connection */
   ghttp_prepare(request);
   /* Process the request */
   ghttp_process(request);
   char *json_string = ghttp_get_body(request);
   printf("=========json_string is %s\r\n",json_string);
   if (json_string) {
          cJSON* cjson = cJSON_Parse(json_string);
          if (cjson) {
              cJSON* Time_tb = cJSON_GetObjectItem(cjson, "data");
              cJSON* t = cJSON_GetObjectItem(Time_tb, "t");
              strncpy(XcurTime,t->valuestring,10);
              XcurTime[10]='\0';
              printf("info : evaluate : XcurTime : %s\r\n",XcurTime);
     }
   }
   ghttp_request_destroy(request);
}

//获取参数
void getParam(void)
{
    char param[128];
    sprintf(param,"{\"aue\": \"raw\",\"result_level\": \"%s\",\"language\": \"%s\",\"category\": \"%s\"}",result_level,language,category);
    base64_encode(param,strlen(param),Xparam);
    printf("info : evaluate : param : %s\r\n",param);
    printf("info : evaluate : Xparam : %s\r\n",Xparam);
}

//获取令牌
void getCheckSum(void)
{
    char checkSum[256];
    sprintf(checkSum,"%s%s%s",Xapikey,XcurTime,Xparam);
    printf("info : evaluate : checkSum : %s\r\n",checkSum);
//    printf("info : evaluate : Xappid : %s\r\n",Xappid);
//    printf("info : evaluate : XcurTime : %s\r\n",XcurTime);
//    printf("info : evaluate : Xparam : %s\r\n",Xparam);
    md5(XcheckSum,checkSum);
    printf("info : evaluate : XcheckSum : %s\r\n",XcheckSum);
}

//md5加密
void md5(char *sign, char* encrypt)
{
    char temp[2];
    MD5_CTX md5;
    MD5Init(&md5);
    int i;
    unsigned char decrypt[16];
    MD5Update(&md5,encrypt,strlen((char *)encrypt));
    MD5Final(decrypt,&md5);
    //printf("加密前:%s\n加密后16位:%s",encrypt,decrypt);
    for (i = 0; i<16; i++)
    {
       //printf("%d:%02x", i,decrypt[i]);
       sprintf(temp,"%02x",decrypt[i]);
       //printf(temp);
       sign[(2*i)]=temp[0];
       sign[(2*i+1)]=temp[1];
    }
    sign[32]='\0';
    //printf("字符串为:%s",sign);
}

//XfUrlEncode
void XfUrlEncode(unsigned char *buf){
    unsigned char base64MemFile[10240];
    base64_encode(buf,strlen(buf),base64MemFile);
    unsigned char Evaluate_text[50]="你好";
//    printf("----------------strlen(buf)-----------------%d\r\n",strlen(buf));
//    printf("----------------strlen(base64MemFile)-----------------%d\r\n",strlen(base64MemFile));
//    printf("----------------base64MemFile-----------------%s\r\n",base64MemFile);
    urlencode(base64MemFile); //编码后
    urlencode(Evaluate_text);
    printf("info : evaluate base64MemFile : %s --------------\r\n", base64MemFile);
    printf("info : evaluate Evaluate_text : %s\r\n", Evaluate_text);
    getEvRequest(base64MemFile,Evaluate_text);
}

void getEvRequest(unsigned char *audio,unsigned char *text){
   char *XfBody=malloc(strlen(audio)+strlen(text)+200);
   sprintf(XfBody,"audio=%s&text=%s",audio,text);
   printf("XfBody     len:%d\r\n",strlen(XfBody));
   FILE *wfile = fopen("/SD/Body.txt", "wb");
   fwrite(XfBody,strlen(XfBody),1,wfile);
   fclose(wfile);
   char *uri = "http://api.xfyun.cn/v1/service/v1/ise";
   ghttp_request *request = NULL;
   /* Allocate a new empty request object */
   request = ghttp_request_new();
   if (NULL == request) {
       printf("ghttp_request_new return null \r\n");
   }
   /* Set the URI for the request object */
   ghttp_set_uri(request, uri);
   ghttp_set_type(request, ghttp_type_post);
   /* Close the connection after you are done. */
   ghttp_set_header(request, "X-Appid",Xappid);
   ghttp_set_header(request, "X-CurTime",XcurTime);
   ghttp_set_header(request, "X-Param",Xparam);
   ghttp_set_header(request, "X-CheckSum",XcheckSum);
   ghttp_set_header(request, "Content-Type","application/x-www-form-urlencoded;charset=utf-8");
   ghttp_set_body(request, XfBody,strlen(XfBody));
   free(XfBody);
   /* Prepare the connection */
   ghttp_prepare(request);
   /* Process the request */
   ghttp_process(request);
   char *json_string = ghttp_get_body(request);
   printf("=========json_string is %s\r\n",json_string);
   ghttp_request_destroy(request);
}

//获取结果
void getEvaluateResult(void){
    getTbTimeStamp();
    getParam();
    getCheckSum();
}


