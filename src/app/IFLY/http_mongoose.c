/*
 * http_mongoose.c
 *
 *  Created on: 2020��3��22��
 *      Author: Magic
 */

#include "../IFLY/http_mongoose.h"

#include <djyos.h>

#include "mongoose.h"

static void http_ev_handler(struct mg_connection *nc, int ev, void *ev_data) {
  struct http_message *hm = (struct http_message *) ev_data;

  struct mg_http_request *request;
  void *user_data;
  Djy_GetEventPara(&request, &user_data);

  switch (ev) {
    case MG_EV_CONNECT:
      if (*(int *) ev_data != 0)
      {
        fprintf(stderr, "http connect() failed: %s\r\n", strerror(*(int *) ev_data));
        request->exit_flag = true;
        request->success = false;
      }
      break;
    case MG_EV_HTTP_REPLY:
      nc->flags |= MG_F_CLOSE_IMMEDIATELY;
      /*
      if (s_show_headers) {
        fwrite(hm->message.p, 1, hm->message.len, stdout);
      } else {
        fwrite(hm->body.p, 1, hm->body.len, stdout);
      }
      putchar('\n');
      */
      if(request->http_call)
      {
          request->http_call(ev_data);
      }
      if(user_data)
      {
          memcpy(user_data, hm, sizeof(struct http_message));
      }
      request->exit_flag = true;
      break;
    case MG_EV_CLOSE:
      if (!request->exit_flag) {
        printf("Server closed connection\n");
        request->exit_flag = true;
      }
      break;
    default:
      break;
  }
}

bool_t http_handler()
{
    struct mg_http_request *request;
    Djy_GetEventPara(&request, NULL);

    request->exit_flag = false;

    if(request->post_data == NULL)
    {
        request->post_data = "\0";
    }

    struct mg_mgr mgr;

    mg_mgr_init(&mgr, NULL);

    mg_connect_http(&mgr, http_ev_handler,
            request->url, request->header, request->post_data);
    request->success = true;

    s64 start_t =  DjyGetSysTime();

    while (!request->exit_flag)
    {
        s64 start_c = DjyGetSysTime();
        if(start_c - start_t > HTTP_SYNC_TIME_OUT)
        {
            printf("mg_http over time\r\n");
            return false;
        }
        mg_mgr_poll(&mgr, 1000);
        Djy_EventDelay(10 * 1000);
    }

    if(!request->success)
    {
        return false;
    }

    mg_mgr_free(&mgr);

    return true;
}

u16 http_evtt_id = 0;

bool_t http_model_init()
{
    if(http_evtt_id > 0)
    {
        return false;
    }

    http_evtt_id = Djy_EvttRegist(EN_INDEPENDENCE, CN_PRIO_RRS, 0, 0,
            http_handler, NULL, 0x2000, "http_mongoose");

    return http_evtt_id != CN_EVENT_ID_INVALID;
}

bool_t http_request(const char *url, const char *header, const char *post_data,
                  void *resp_data, void (*http_call)(mg_http_response *))
{
    if(http_evtt_id == 0)
    {
        printf("http evtt not init!/r/n");
    }
    if(http_evtt_id != CN_EVENT_ID_INVALID)
    {
        struct mg_http_request request = {url, header, post_data, http_call, false, true};
        Djy_EventPop(http_evtt_id, 0, HTTP_SYNC_TIME_OUT, &request, resp_data, 0);
    }
    return Djy_GetEventResult();
}
