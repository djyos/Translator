/*
 * websocket_mongoose.c
 *
 *  Created on: 2020��3��20��
 *      Author: Magic
 */

#include "../IFLY/websocket_mongoose.h"

struct mg_ws_request
{
    const char *url;
    const char *protocol;
    const char *extra_headers;
    void (*ws_call)(mg_ws_response *wm);
    struct mg_connection *nc;
    bool_t exit_flag;
    bool_t conn_flag;
};

u16 evtt_ws = 0;

u16 evtt_ws_send = 0;

static void ws_ev_handler(struct mg_connection *nc, int ev, void *ev_data) {

    struct mg_ws_request *request = nc->user_data;
//    Djy_GetEventPara(&request, NULL);

  (void) nc;
  switch (ev) {
    case MG_EV_CONNECT: {
      int status = *((int *) ev_data);
      if (status != 0) {
        printf("-- Connection error: %d\n", status);
        request->conn_flag = false;
      }
      break;
    }
    case MG_EV_WEBSOCKET_HANDSHAKE_DONE: {
      struct http_message *hm = (struct http_message *) ev_data;
      if (hm->resp_code == 101) {
        printf("-- Connected\n");
        request->conn_flag = true;
      } else {
        printf("-- Connection failed! HTTP code %d\n", hm->resp_code);
        request->conn_flag = false;
        /* Connection will be closed after this. */
      }
      break;
    }
    case MG_EV_POLL: {
      char msg[500]; // FIXME
      int n = 0;

      fd_set read_set, write_set, err_set;
      struct timeval timeout = {0, 0};
      FD_ZERO(&read_set);
      FD_ZERO(&write_set);
      FD_ZERO(&err_set);
      FD_SET(0 /* stdin */, &read_set);
      if (select(1, &read_set, &write_set, &err_set, &timeout) == 1) {
        n = read(0, msg, sizeof(msg));
      }

      if (n <= 0) break;
      while (msg[n - 1] == '\r' || msg[n - 1] == '\n') n--;
      mg_send_websocket_frame(nc, WEBSOCKET_OP_TEXT, msg, n);
      break;
    }
    case MG_EV_WEBSOCKET_FRAME: {
      struct websocket_message *wm = (struct websocket_message *) ev_data;
      // printf("MG_EV_WEBSOCKET_FRAME --> %.*s\n", (int) wm->size, wm->data);
      request->ws_call(wm);
      break;
    }
    case MG_EV_CLOSE: {
      if (request->conn_flag) printf("-- Disconnected\n");
      request->exit_flag = true;
      break;
    }
  }
}

bool_t is_ws_conn(struct mg_ws_request *request)
{
    return request->conn_flag;
}

void ws_handler()
{
    struct mg_ws_request *request;
    Djy_GetEventPara(&request, NULL);

    struct mg_mgr mgr;
    mg_mgr_init(&mgr, NULL);

    request->nc = mg_connect_ws(&mgr, ws_ev_handler, request->url, request->protocol, request->extra_headers);
    if (request->nc == NULL) {
        fprintf(stderr, "Invalid address\n");
        goto end_ws_handler;
    }
    request->nc->user_data = request;

    while (!request->exit_flag) {
        mg_mgr_poll(&mgr, 100); // FIXME
    }

end_ws_handler:
    mg_mgr_free(&mgr);
    free(request);
}

void ws_send_handler()
{
    struct mg_ws_request *request;
    char *send_data;
    Djy_GetEventPara(&request, &send_data);
    if(request == NULL){
        printf("mg_socket_request is NULL!\r\n");
        return;
    } else if(request->nc == NULL) {
        printf("mg_socket maybe not connect!\r\n");
        return;
    } else if(send_data == NULL) {
        printf("send_data is NULL!\r\n");
        return;
    }

    // FIXME
    s64 start_t =  DjyGetSysTime();
    while(!request->conn_flag)
    {
        s64 start_c = DjyGetSysTime();
        if(start_c - start_t > WS_SYNC_TIME_OUT)
        {
            printf("wait web socket conn over time / end ! \r\n");
            return;
        }
        printf("wait web socket .. \r\n");
        Djy_EventDelay(1000 * 1000);
    }
    mg_send_websocket_frame(request->nc, WEBSOCKET_OP_TEXT, send_data, strlen(send_data));
}

bool_t ws_model_init()
{
    if(evtt_ws > 0)
    {
        return false;
    }

    if(evtt_ws == 0)
    {
        evtt_ws = Djy_EvttRegist(EN_INDEPENDENCE, CN_PRIO_RRS, 0, 0,
                ws_handler, NULL, 0x2000, "ws_mongoose");
    }
    if(evtt_ws_send == 0)
    {
        evtt_ws_send = Djy_EvttRegist(EN_INDEPENDENCE, CN_PRIO_RRS, 0, 0,
                ws_send_handler, NULL, 0x1800, "ws_mg_send");
    }
    return ws_handler != CN_EVENT_ID_INVALID;
}

void *ws_conn(const char *url, const char *protocol, const char *extra_headers, void (*ws_call)(mg_ws_response *))
{
    struct mg_ws_request *ws_request = malloc(sizeof(struct mg_ws_request));
    ws_request->url = url;
    ws_request->protocol = protocol;
    ws_request->extra_headers = extra_headers;
    ws_request->ws_call = ws_call;
    ws_request->exit_flag = false;
    ws_request->conn_flag = false;

    if(evtt_ws != CN_EVENT_ID_INVALID)
    {
       Djy_EventPop(evtt_ws, 0, 0, ws_request, 0, 0);
       return (void *)ws_request;
    }
    return NULL;
}

bool_t ws_send(struct mg_ws_request *ws_request, char *send_buff)
{
    if(evtt_ws_send != CN_EVENT_ID_INVALID)
    {
       Djy_EventPop(evtt_ws_send, 0, 0, ws_request, send_buff, 0);
       return true;
    }
    return false;
}

void ws_close(struct mg_ws_request *ws_request)
{
    if(ws_request)
    {
        if(ws_request->nc && ws_request->conn_flag)
        {
            mg_send_websocket_frame(ws_request->nc, WEBSOCKET_OP_CLOSE, NULL, 0);
        }
        free(ws_request);
    }
}
