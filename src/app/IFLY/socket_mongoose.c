/*
 * socket_mongoose.c
 *
 *  Created on: 2020年3月23日
 *      Author: Magic
 */

#include "../IFLY/socket_mongoose.h"

struct mg_socket_request
{
    const char *url;
    void (*socket_call)(char *buf, size_t len);
    struct mg_connection *nc;
    bool_t socket_flag;
};

u16 evtt_socket = 0;

u16 evtt_socket_send = 0;

bool_t is_socket_tcp_conn(struct mg_socket_request *request)
{
    return request->socket_flag;
}

void socket_tcp_ev_handler(struct mg_connection *nc, int ev, void *ev_data)
{
    struct mg_socket_request *request;
    Djy_GetEventPara(&request, NULL);

    unsigned char *cm = (unsigned char *) ev_data;
    switch (ev) {
    case MG_EV_CONNECT:
        if (*(int *) ev_data != 0) {
            fprintf(stderr, "socket connect() failed: %s\n", strerror(*(int *) ev_data));
            request->socket_flag = false;
            break;
        }
        request->socket_flag = true;
        request->nc->user_data = true;
        printf("MG_SOCKET_EV_CONNECT\r\n");
        //  mg_printf(nc, "%c", 0xFF);  /// connect成功随即发送FF
        break;
    // case MG_EV_POLL:
    //     break;
    case MG_EV_RECV:
        printf("MG_EV_RECV\r\n");
        request->socket_call(nc->recv_mbuf.buf, nc->recv_mbuf.len);
        nc->recv_mbuf.len = 0;
        // mbuf_remove();
        break;
    case MG_EV_CLOSE:
        printf("MG_EV_CLOSE\r\n");
        request->socket_flag = false;
        // Djy_EventDelay(500 * 1000);
        // mg_connect(nc->mgr, url, socket_ev_handler);
        break;
    default:
        break;
    }

}

void socket_tcp_handler()
{
    struct mg_socket_request *request;
    Djy_GetEventPara(&request, NULL);

    if(request == NULL)
    {
        printf("mg_socket_request is NULL!\r\n");
        return;
    }

    struct mg_mgr mgr;

    mg_mgr_init(&mgr, NULL);
    request->socket_flag = false;
    printf("request->url:[%s]\r\n", request->url);
    request->nc = mg_connect(&mgr, request->url, socket_tcp_ev_handler);

    if(request->nc == NULL)
        goto end_socket_tcp_handler;

    // request->nc->user_data = request->socket_call;
    request->nc->user_data = NULL;


    while (request->socket_flag || request->nc->user_data == NULL)
    {
        mg_mgr_poll(&mgr, 500);
        Djy_EventDelay(10 * 1000);
    }

end_socket_tcp_handler:
    mg_mgr_free(&mgr);
    free(request);
}

void socket_tcp_send_handler()
{
    struct mg_socket_request *request;
    char *send_data;
    Djy_GetEventPara(&request, &send_data);
    if(request == NULL){
        printf("mg_socket_request is NULL!\r\n");
        return;
    } else if(request->nc == NULL) {
        printf("mg_socket maybe not connect!\r\n");
        return;
    } else if(send_data == NULL) {
        printf("send_data is NULL!\r\n");
        return;
    }

    // FIXME
    s64 start_t =  DjyGetSysTime();
    while(!request->socket_flag)
    {
        s64 start_c = DjyGetSysTime();
        if(start_c - start_t > SOCKET_SYNC_TIME_OUT)
        {
            printf("wait socket over time / end ! \r\n");
            return;
        }
        printf("wait socket .. \r\n");
        Djy_EventDelay(1000 * 1000);
    }

    // FIXME send data type or length
    printf("socket_tcp_send_handler \r\n");
    mg_send(request->nc, send_data, strlen(send_data));
}

bool_t socket_tcp_model_init()
{
    if(evtt_socket == CN_EVENT_ID_INVALID)
    {
        printf("evtt_socket_send regist error!\r\n");
        return false;
    }

    if(evtt_socket == 0)
    {
        evtt_socket = Djy_EvttRegist(EN_INDEPENDENCE, CN_PRIO_RRS, 0, 0,
                socket_tcp_handler, NULL, 0x2000, "socket_tcp_mongoose");
    }

    if(evtt_socket_send == 0)
    {
        evtt_socket_send = Djy_EvttRegist(EN_INDEPENDENCE, CN_PRIO_RRS, 0, 0,
                socket_tcp_send_handler, NULL, 0x1800, "socket_tcp_mg_send");
    }

    return evtt_socket != CN_EVENT_ID_INVALID;
}

void *socket_tcp_conn(const char *url, void (*socket_call)(char *buf, size_t len))
{
    struct mg_socket_request *socket_request = malloc(sizeof(struct mg_socket_request));
    socket_request->url = url;
    socket_request->socket_call = socket_call;

    if(evtt_socket != CN_EVENT_ID_INVALID)
    {
       Djy_EventPop(evtt_socket, 0, 0, socket_request, 0, 0);
       return (void *)socket_request;
    }
    return NULL;
}

bool_t socket_tcp_send(struct mg_socket_request *socket_request, char *send_buff)
{
    printf("socket_tcp_send \r\n");
    if(evtt_socket_send != CN_EVENT_ID_INVALID)
    {
       Djy_EventPop(evtt_socket_send, 0, 0, socket_request, send_buff, 0);
       return true;
    }
    return false;
}

void socket_tcp_close(struct mg_socket_request *socket_request)
{
    if(socket_request)
    {
        socket_request->socket_flag = false;
        free(socket_request);
    }
}
