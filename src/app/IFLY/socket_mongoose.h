/*
 * socket_mongoos.h
 *
 *  Created on: 2020��3��23��
 *      Author: Magic
 */

#ifndef APP_MG_CLIENT_SOCKET_MONGOOSE_H_
#define APP_MG_CLIENT_SOCKET_MONGOOSE_H_

#include "mongoose.h"

#define SOCKET_SYNC_TIME_OUT 10 * 1000 * 1000

struct mg_socket_request;

bool_t socket_tcp_model_init();

void *socket_tcp_conn(const char *url, void (*socket_call)(char *buf, size_t len));

bool_t socket_tcp_send(struct mg_socket_request *socket_request, char *send_buff);

void socket_tcp_close(struct mg_socket_request *socket_request);

#endif /* APP_MG_CLIENT_SOCKET_MONGOOSE_H_ */
