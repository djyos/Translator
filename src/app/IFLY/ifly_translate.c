/*
 * XfTranslation.c
 *
 *  Created on: 2020年2月27日
 *      Author: zxy
 */
#include "ifly_translate.h"
#include "project_config.h"
#include "stdint.h"
#include "stddef.h"
#include "ghttp.h"
#include "cJSON.h"
#include "hmac_sha1.h"
#include "systime.h"
#include "../IFLY/sha256.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"

char            *request_line  = "POST /v2/ots HTTP/1.1";
char            *appid         = "5e572f0f";
char            *apikey        = "65b60fe83f24c17f8c2caee4273fcf29";
char            *apiSecret     = "1d48ede6135dab27a392c0e1db0435ff";
char            *XfHost        = "ntrans.xfyun.cn";                  // 请求主机
char            from_language[16] = "cn";                            // 输出语言
char            to_language[16]   = "en";                            // 输入语言

unsigned char   sha256body[0x400];                          // sha265加密
unsigned char   sha256signature_sha[0x400];
unsigned char   base64[0x480];                              // base64加密
unsigned char   XfBody[0x400];                              // 请求BODY
unsigned char   XfDigest[0x500];          //1280            // 加密请求body SHA-256=Base64(SHA256(请求body))
unsigned char   XfAuthorization[0x400];   //1024            // 使用base64编码的签名相关信息
unsigned char   XfDate[32];                                 // 当前时间戳，RFC1123格式
unsigned char   word_storage_result_to[0x200];              // 存储翻译结果
unsigned char   word_storage_result_from[0x200];            // 存储录音结果

size_t          hmac_sha256_size = 32;
int             translate_suceess = 1;                      //判断是否翻译成功 0表示失败

extern u32 reverse__result;                //逆向翻译保存结果开关 0代表关闭 1代表开启

/* 获取body */

void getSnBody( char *in, int in_len)
{
    unsigned char *base64body = malloc( in_len*4/3 );

    base64_encode( in, in_len, base64body );

    sprintf( XfBody, "{\"common\":{\"app_id\":\"%s\"},\"business\":{\"from\":\"%s\",\"to\":\"%s\"},\"data\":{\"text\":\"%s\"}}", appid,from_language,to_language,base64body );

#if 1
    printf("\r\n[DEBUG XfBody] %s\r\n",XfBody);
#endif

    free( base64body );
}


/*服务器获得时间 */

void getSnTime( void )
{
    char        *uri        = "http://api.m.taobao.com/rest/api3.do?api=mtop.common.getTimestamp";
    ghttp_request   *request    = NULL;
    /* Allocate a new empty request object */
    request = ghttp_request_new();
    if ( NULL == request )
    {
        printf( "ghttp_request_new return null \r\n" );
    }
    /* Set the URI for the request object */
    ghttp_set_uri( request, uri );
    ghttp_set_type( request, ghttp_type_get );
    /* Close the connection after you are done. */
    ghttp_set_header( request, http_hdr_Connection, "close" );
    /* Prepare the connection */
    ghttp_prepare( request );
    /* Process the request */
    ghttp_process( request );
    sprintf( XfDate, "%s", ghttp_get_header( request, "Date" ) );
    ghttp_request_destroy( request );

    StrSHA256Base64( XfBody, strlen( XfBody ), sha256body );

    base64_encode( sha256body, strlen( sha256body ), base64 );

    sprintf( XfDigest, "SHA-256=%s", base64 );

    size_t  signature_origin_size   = strlen( XfDigest ) + strlen( XfHost ) + strlen( request_line ) + strlen( XfDate ) + 40;

    char    *signature_origin   = malloc( signature_origin_size );

    sprintf( signature_origin, "host: %s\ndate: %s\nPOST /v2/ots HTTP/1.1\ndigest: %s", XfHost, XfDate, XfDigest );

    hmac_sha256( apiSecret, strlen( apiSecret ), signature_origin, strlen( signature_origin ), sha256signature_sha, 32 );

    size_t  signature_size      = hmac_sha256_size * 4 / 3;

    char    *base64signature    = malloc( signature_size );

    base64_encode( sha256signature_sha, strlen( sha256signature_sha ), base64signature );

    sprintf( XfAuthorization, "api_key=\"%s\", algorithm=\"hmac-sha256\", headers=\"host date request-line digest\", signature=\"%s\"", apikey, base64signature );

    free( base64signature );
}

/* 返回结果 */

void getTranslationResult( char *in, int in_len)
{
    getSnBody(in, in_len);

    getSnTime();

    if(reverse__result == 0)
    {
        memset(word_storage_result_from,'\0',sizeof(word_storage_result_from));
        strcpy(word_storage_result_from, in);
    }
    else
    {
        memset(word_storage_result_to,'\0',sizeof(word_storage_result_to));
        strcpy( word_storage_result_to, in);
    }
    char *uri = "http://ntrans.xfyun.cn/v2/ots";
    ghttp_request   *request    = NULL;
    /* Allocate a new empty request object */
    request = ghttp_request_new();
    if ( NULL == request )
    {
        printf( "ghttp_request_new return null \r\n" );
        /* return ret; */
    }
    /* Set the URI for the request object */
    ghttp_set_uri( request, uri );
    ghttp_set_type( request, ghttp_type_post );
    /* Close the connection after you are done. */
    /* ghttp_set_header(request, http_hdr_Connection, "close"); */
    ghttp_set_header( request, "Content-Type", "application/json" );
    ghttp_set_header( request, "Accept", "application/json,version=1.0" );
    ghttp_set_header( request, "Host", XfHost );
    ghttp_set_header( request, "Date", XfDate );
    ghttp_set_header( request, "Digest", XfDigest );
    ghttp_set_header( request, "Authorization", XfAuthorization );
    ghttp_set_body( request, XfBody, strlen( XfBody ) );
    /* Prepare the connection */
    ghttp_prepare( request );
    /* Process the request */
    ghttp_process( request );
    char *json_string = ghttp_get_body( request );
    printf("\r\n[DEBUG json_string] %s\r\n",json_string);
    cJSON* cjson = cJSON_Parse( json_string );
    if (strstr(json_string,"HMAC signature does not match"))
    {
        translate_suceess = 0;
    }
    else
    {
        translate_suceess = 1;
    }
    printf("\r\n[DEBUG translate_suceess] %d\r\n",translate_suceess);
    if ( cjson )
    {
        cJSON   * data      = cJSON_GetObjectItem( cjson, "data" );
        cJSON   * result    = cJSON_GetObjectItem( data, "result" );
        cJSON   * trans_result  = cJSON_GetObjectItem( result, "trans_result" );
        cJSON   * dst       = cJSON_GetObjectItem( trans_result, "dst" );
        if(reverse__result == 0)
        {
            memset(word_storage_result_to,'\0',sizeof(word_storage_result_to));
            strcpy(word_storage_result_to, dst->valuestring);
        }
        else
        {
            memset(word_storage_result_from,'\0',sizeof(word_storage_result_from));
            strcpy(word_storage_result_from, dst->valuestring);
        }

#if 1
        printf( "\r\n[INFO reverse__result] %d\r\n",reverse__result);
        printf( "\r\n[INFO word_storage_result_from] %s\r\n", word_storage_result_from );
        printf( "\r\n[INFO word_storage_result_to] %s\r\n", word_storage_result_to );
#endif

        Refresh_GuiWin();
    }
    cJSON_Delete( cjson );

    ghttp_request_destroy( request );
}


