#include "mongoose.h"
#include "recorder_cache.h"

#define DEBUG 0
#define SELF_TEST 0
#define DJYOS 1


#if SELF_TEST
char *GetRecordCacheAddrTEST();
int GetRecordCacheSizeTEST();
#else
char *GetRecordCacheAddr();
int GetRecordCacheSize();
#endif

typedef struct StRecCache {
    char url[256];
    unsigned int id;//记录创建时id，用来区分同一个录音不同时间录制
    int type; //0：录音按键追加数据； 1: 网络下载追加数据
    int no_used;
    int readed;
    int rlen; //已经读的长度， readed - base
    int writed;
    int wlen; //base <-> writed
    int base;
    int totals; //记录录音完整的长度，这里提供给网上下载一半，又回来缓冲区把剩下一半找回来。
    int status; //0: 不完整(没尾巴，需要等待)； 1： 完整;  2: 不完整（没头），因为缓冲不够，这个base更新为readed
    int is_fin; //录音是否已经结束，0:录音还在进行中，1:录音结束
}StRecCache;

typedef struct StRecCacheMgr {
    char *pmem; // pmem, Bytes array.
    int mem_max; //mem_max of pmem
    int mem_head;
    int mem_tail;
    int mem_cnts;

    struct StRecCache *parr_cache;
    int arr_max;//StRecCache环形数组总个数
    int arr_head;//StRecCache环形数组的头下标
    int arr_tail;//StRecCache环形数组的尾下标
    int arr_cnts; //StRecCache环形数组,有效数据个数，区分head_index==tail_index满或空两种情况

    int init_flag;

    unsigned int gid; //全局id

    int is_recording; //这个状态在网络下载添加数据到录音缓冲和用户点击录音按钮添加数据到缓冲，如果录音点击了，必须停止网络添加数据。

}StRecCacheMgr;

int RecCachePrintInfo();

static struct SemaphoreLCB* gPSRCache = 0;
struct StRecCacheMgr gRecCacheMgr;

#if DJYOS
#define RECORD_MGR_CACHE_LOCK()        do { if (gPSRCache) Lock_SempPend(gPSRCache, 0xFFFFFFFF); } while (0);
#define RECORD_MGR_CACHE_UNLOCK()      do { if (gPSRCache) Lock_SempPost(gPSRCache); } while (0);
#else
#define RECORD_MGR_CACHE_LOCK()
#define RECORD_MGR_CACHE_UNLOCK()
#endif

int RecCacheInit(int max_nums)
{
    static int sem_flag = 0;
    int ret = 0;

    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    if (pMgr->init_flag == 1) return 0;

    if (gPSRCache == 0) {
#if DJYOS
        gPSRCache = semp_init(1, 1, 0);
        if (gPSRCache == 0) {
            printf("error: pRecordCacheMgr->sem_cache failed!\r\n");
            ret = ERC_PARAMRAM;
            goto FUN_RET;
        }
#else
        gPSRCache = 1;
#endif
    }

    RECORD_MGR_CACHE_LOCK();
    memset(pMgr, 0, sizeof(struct StRecCacheMgr));

#if SELF_TEST
    pMgr->pmem = GetRecordCacheAddrTEST();
    pMgr->mem_max = GetRecordCacheSizeTEST();
#else
    pMgr->pmem = GetRecordCacheAddr();
    pMgr->mem_max = GetRecordCacheSize();
#endif
    pMgr->arr_max = max_nums;
    pMgr->parr_cache = (struct StRecCache*)malloc(pMgr->arr_max * sizeof(struct StRecCache));
    if (pMgr->parr_cache == 0) {
        printf("error: pMgr->parr_cache failed!\r\n");
        ret = ERC_PARAMRAM;
        goto FUN_RET;
    }
    memset(pMgr->parr_cache, 0, pMgr->arr_max * sizeof(struct StRecCache));
    pMgr->init_flag = 1;

FUN_RET:
    RECORD_MGR_CACHE_UNLOCK();
    return ret;
}


int RecCacheDeInit()
{
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;

    if (pMgr->init_flag == 0) return 0;

    RECORD_MGR_CACHE_LOCK();
    if (pMgr->parr_cache) {
        free(pMgr->parr_cache);
        pMgr->arr_max = 0;//StRecCache环形数组总个数
        pMgr->arr_head = 0;//StRecCache环形数组的头下标
        pMgr->arr_tail = 0;//StRecCache环形数组的尾下标
        pMgr->arr_cnts = 0; //StRecCache环形数组,有效数据个数，区分head_index==tail_index满或空两种情况
    }
    memset(pMgr, 0, sizeof(struct StRecCacheMgr));
    pMgr->init_flag = 0;
    RECORD_MGR_CACHE_UNLOCK();

    return 0;
}

int FreeRecordCache()
{
    RecCacheDeInit();
}


static int RecCacheRemove(char *url)
{
    int ret = 0;
    int idx = 0;
    int maxs = 0;
    int cnts = 0;
    int mark_tail = 0;
    int mark_head = 0;
    int tmp_memcnts = 0;
    int tmp_arrcnts = 0;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    struct StRecCache *parr = 0;

    if (url == 0 || pMgr->parr_cache == 0 || pMgr->arr_max <= 0) return -1;

    parr = pMgr->parr_cache;
    maxs = pMgr->arr_max;
    cnts = pMgr->arr_cnts;
    idx = pMgr->arr_head;

    if (cnts == 0) goto FUN_RET;

    //标志no_used元素
    while (cnts) {
        if (strcmp(parr[idx].url, url) == 0) {
            parr[idx].no_used = 1;
        }
        idx++;
        idx = idx % maxs;
        cnts--;
    }
    //如果删除了头尾，同时要处理头尾相邻的no_used的元素
    idx = pMgr->arr_head;
    cnts = pMgr->arr_cnts;
    //从头到尾处理no_used元素
    mark_head = -1;
    mark_tail = -1;
    tmp_memcnts = 0;
    tmp_arrcnts = 0;
    while (cnts) {
        if (parr[idx].no_used == 0) {
            if (mark_head == -1) {
                pMgr->mem_head = parr[idx].base;//更新头位置
                pMgr->mem_cnts = 0; //从头计算mem_cnts;
                mark_head = idx;
                pMgr->arr_head = mark_head;//更新数组头索引
                pMgr->arr_cnts = 0; //从头计算数组个数
                tmp_memcnts = 0; //这里清空中间有no_used标志的bytes
                tmp_arrcnts = 0; //这里清空中间有no_used标志的arr cnts
            }
            mark_tail = idx;
            pMgr->mem_tail = parr[idx].writed;//更新尾位置
            pMgr->arr_tail = (mark_tail + 1) % maxs; //更新数组尾索引

            pMgr->mem_cnts += parr[idx].wlen + tmp_memcnts;//加tmp_memcnts就是前面为no_used的数据
            pMgr->arr_cnts = pMgr->arr_cnts+ 1 + tmp_arrcnts;//加tmp_arrcnts就是前面为no_used的数据

            tmp_memcnts = 0; //这里清空中间有no_used标志的bytes
            tmp_arrcnts = 0; //这里清空中间有no_used标志的arr cnts
        }
        else { //no_used
            tmp_memcnts += parr[idx].wlen;
            tmp_arrcnts++;
        }

        idx++;
        idx = idx % maxs;
        cnts--;
    }
    if (mark_head == -1) {
        pMgr->mem_head = 0;
        pMgr->mem_tail = 0;
        pMgr->mem_cnts = 0;

        pMgr->arr_head = 0;
        pMgr->arr_tail = 0;
        pMgr->arr_cnts = 0;
    }


FUN_RET:
    return ret;
}

//flag==0: 只有元素的数据被网络读取完成（readed==writed）或标志no_used, 才能remove
//flag==1: 强制remove
//注意：这个函数可以删除所有的项目，因为网络完全读完所有数据
static int RecCacheRemoveHead(int flag)
{
    int ret = 0;
    struct StRecCache *parr = 0;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    if (pMgr->parr_cache == 0 || pMgr->arr_max <= 0) return -1;
    parr = pMgr->parr_cache;
    if (flag == 0 && pMgr->arr_cnts > 0) {
        if (parr[pMgr->arr_head].rlen == parr[pMgr->arr_head].wlen || parr[pMgr->arr_head].no_used) {
            ret = RecCacheRemove(parr[pMgr->arr_head].url);
        }
    }
    else {
        ret = RecCacheRemove(parr[pMgr->arr_head].url);
    }
    return ret;
}

//如果足够，返回足够就可以，如果不足够，返回最大的空闲空间
// flag == 0: 释放全部，包括自己项的数据，这样导致损坏自己的头部。
// flag == 1: 释放全部（但不能释放自己的任何数据，保证自己数据头部完整
static int Try2FreeSpace(int need_bytes, int flag)
{
    int ret = 0;
    int i = 0;
    int base_free = 0;
    int cut_size = 0;
    int idx = 0;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    struct StRecCache *parr = pMgr->parr_cache;

    base_free = pMgr->mem_max - pMgr->mem_cnts;

    if (base_free >= need_bytes) { //原本就有足够空间，不需要释放空间。
        return base_free;
    }

    while (pMgr->arr_cnts) {
        idx = pMgr->arr_head;
        if (flag == 1) { //不能释放自己的任何数据。
            if ((idx + 1) % pMgr->arr_max == pMgr->arr_tail) {//最后一项，不能释放，跳出。
                break;
            }
        }

        if ((parr[idx].rlen == parr[idx].wlen && parr[idx].is_fin) //结束且完整读完才能释放
            || parr[idx].no_used) { //可以完全释放
            RecCacheRemoveHead(1); //强制删除
                                   //计算mem_max
            base_free = pMgr->mem_max - pMgr->mem_cnts;
            if (base_free >= need_bytes) {
                break;
            }
        }
        else if (parr[idx].rlen <= parr[idx].wlen && parr[idx].rlen != 0) {//最后不能释放，因为网络没读取数据或读取完还没结束，所以只能释放部分
            if (parr[idx].base < parr[idx].readed) {
                cut_size = parr[idx].readed - parr[idx].base;
            }
            else {
                cut_size = pMgr->mem_max - parr[idx].base + parr[idx].readed;
            }
            parr[idx].base = parr[idx].readed;
            parr[idx].wlen = parr[idx].wlen - cut_size;
            parr[idx].rlen = 0;
            parr[idx].status = ERS_NO_HEAD;


            //更新pMgr数据接口
            pMgr->mem_cnts = pMgr->mem_cnts - cut_size;
            pMgr->mem_head = parr[idx].base;

            base_free = pMgr->mem_max - pMgr->mem_cnts;
            break;
        }
        else {//缓冲慢，没数据读出
            break;
        }
    }
    return base_free;
}

static struct StRecCache * RecCacheCreateItem(char *url)
{
    int ret = 0;
    struct StRecCache *parr = 0;
    struct StRecCache *ptmp = 0;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    if (pMgr->parr_cache == 0 || pMgr->arr_max <= 0) return 0;
    parr = pMgr->parr_cache;

    RecCacheRemove(url);//创建之前删除存在的同名的项目
    if (pMgr->arr_cnts == pMgr->arr_max) {//arr 满数据，需要删除，但必须要判断writed == readed才能删除，否则返回等待读才能继续分配。
        RecCacheRemoveHead(0);
        if (pMgr->arr_cnts == pMgr->arr_max) {
            return 0;
        }
    }
    ptmp = &parr[pMgr->arr_tail];
    memset(ptmp, 0, sizeof(struct StRecCache));
    strcpy(ptmp->url, url);
    pMgr->gid++;
    ptmp->id = pMgr->gid;
    pMgr->arr_cnts++;
    pMgr->arr_tail++;
    pMgr->arr_tail = pMgr->arr_tail % pMgr->arr_max;
    ptmp->writed = ptmp->readed = ptmp->base = pMgr->mem_tail;
    return ptmp;
}


// return : 添加新记录失败，需要等待读空一些数据才能再添加新项目
int RecCacheAppendTail(char *url, unsigned char *data, int len, int flag)
{
    int ret = 0;
    int i = 0;
    int last = -1;
    int is_new = 0;
    struct StRecCache *parr = 0;
    struct StRecCache *ptmp = 0;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    if (pMgr->parr_cache == 0 || pMgr->arr_max <= 0 || url == 0 || url[0] == 0) return ERC_PARAMRAM;

    RECORD_MGR_CACHE_LOCK();

    if (pMgr->init_flag == 0) goto FUN_RET;

    pMgr->is_recording = 1; //用户点击录音按钮，这是网络如果同时添加数据到缓冲区必须停止。

    parr = pMgr->parr_cache;
    //判断url是否跟最后操作的一致。
    if (pMgr->arr_cnts > 0) {
        if (pMgr->arr_tail == 0) {
            last = pMgr->arr_max - 1;
        }
        else {
            last = pMgr->arr_tail - 1;
        }
    }
    if (last >= 0 &&
        parr[last].id == pMgr->gid && ////当前相同才能追加
        strcmp(pMgr->parr_cache[last].url, url) == 0) {//the same url
        ptmp = &parr[last];
        is_new = 0;
    }
    else {
        is_new = 1;
    }

    //创建新项时，需设置上一次结束标志
    if (is_new || flag == E_APPEND_HEAD) {
        if (last >= 0 &&
            pMgr->parr_cache[last].status == ERS_NO_TAIL &&
            pMgr->parr_cache[last].type == 0) { //这里不能type=0, 录音按键添加数据只能设置录音按键类型的完全标志
            pMgr->parr_cache[last].status = ERS_COMPLETE;
        }
    }

    //create item
    if (is_new || flag == E_APPEND_HEAD) {//url diff or E_APPEND_HEAD will remove the old url, and then create one
                                          //开始创建项
        ptmp = RecCacheCreateItem(url);
    }
    if (ptmp == 0) {//没空间创建新项
        ret = ERC_CREATE_ITEM;
        goto FUN_RET;
    }
    //append data
    //计算空闲空间，如果空间足够就直接存储，如果空间不够，就释放部分
    int space_free = Try2FreeSpace(len, 0);
    if (pMgr->arr_cnts == 0) {//Try2FreeSpace(len, 0); 当完全读完，而且申请的len大于整个缓冲，会删除全部。
        ptmp = RecCacheCreateItem(url);//重新创建一个
        if (flag == E_APPEND_TAIL) {//如果是append tail，则这个音频是不完整的，播放时不直接播放。
            ptmp->status = ERS_NO_HEAD;
        }
    }

    if (space_free <= 0) {
        ret = ERC_WRITE_FULL;
        goto FUN_RET;
    }

    i = 0;
    while (len--) {
        pMgr->pmem[ptmp->writed] = data[i];
        ptmp->wlen++;
        ptmp->writed++;
        ptmp->totals++; //记录完整的录音大小，可能比缓冲区大小还要大，可能大几倍。
        ptmp->writed = ptmp->writed % pMgr->mem_max;
        if (ptmp->writed == pMgr->mem_head) {//不可以覆盖写，需要释放
            i++;
            break;
        }
        i++;
    }
    ret = i;
    if (ret > 0) {
        pMgr->mem_cnts += ret;
        pMgr->mem_tail += ret;
        pMgr->mem_tail = pMgr->mem_tail % pMgr->mem_max;
    }

FUN_RET:
    RECORD_MGR_CACHE_UNLOCK();

    return ret;
}
// 当第一次开机点击播放录音，此时没任何录音缓冲，只能从网络下载，此时需要同时把
// 网络下载的数据备份一份到录音缓冲区，这样再次播放就不用从网络重新下载。
// 但是这样添加数据必须设置rlen == wlen, 这样才不会又被重新发送到网络，造成死循环。
// 这跟正常录音数据添加到缓冲差别是，readed == writed, rlen == wlen,
// return : 添加新记录失败，需要等待读空一些数据才能再添加新项目
int RecCacheAppendTailFromNet(char *url, unsigned char *data, int len, int flag)
{
    int ret = 0;
    int i = 0;
    int last = -1;
    int is_new = 0;
    int maxs = 0;
    int cnts = 0;
    int idx = 0;
    struct StRecCache *parr = 0;
    struct StRecCache *ptmp = 0;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    if (pMgr->parr_cache == 0 || pMgr->arr_max <= 0 || url == 0 || url[0] == 0) return ERC_PARAMRAM;

    RECORD_MGR_CACHE_LOCK();

    if (pMgr->init_flag == 0) goto FUN_RET;

    if (pMgr->is_recording) {//用户点击录音按钮添加数据到缓冲，此时需要停止网络下载数据到缓冲
        ret = ERC_NET_STOP;
        goto FUN_RET;
    }

    parr = pMgr->parr_cache;
    maxs = pMgr->arr_max;
    cnts = pMgr->arr_cnts;
    idx = pMgr->arr_head;

    //判断url是否跟最后操作的一致。
    if (pMgr->arr_cnts > 0) {
        if (pMgr->arr_tail == 0) {
            last = pMgr->arr_max - 1;
        }
        else {
            last = pMgr->arr_tail - 1;
        }
    }
    if (last >= 0 && strcmp(pMgr->parr_cache[last].url, url) == 0) {//the same url
        ptmp = &parr[last];
        is_new = 0;
    }
    else {
        is_new = 1;
    }

    //创建新项时，需设置上一次结束标志
    if (is_new || flag == E_APPEND_HEAD) {
        if (last >= 0 &&
            pMgr->parr_cache[last].status == ERS_NO_TAIL &&
            pMgr->parr_cache[last].type == 1) { //这里不能type=0, 网络添加数据只能设置网络类型的完全标志
            pMgr->parr_cache[last].status = ERS_COMPLETE;
        }
    }

    //create item
    if (is_new || flag == E_APPEND_HEAD) {//url diff or E_APPEND_HEAD will remove the old url, and then create one
                                          //开始创建项
        ptmp = RecCacheCreateItem(url);
    }
    if (ptmp == 0) {//没空间创建新项

        ret = ERC_CREATE_ITEM;
        goto FUN_RET;
    }
    ptmp->type = 1; //网络追加数据需要设置。

                    //append data
                    //计算空闲空间，如果空间足够就直接存储，不释放自己，这样保证自己头部不释放
    int space_free = Try2FreeSpace(len, 1);

    if (space_free <= 0) {
        ret = ERC_WRITE_FULL;
        goto FUN_RET;
    }

    i = 0;
    while (len--) {
        pMgr->pmem[ptmp->writed] = data[i];
        ptmp->wlen++;
        ptmp->writed++;
        ptmp->writed = ptmp->writed % pMgr->mem_max;
        if (ptmp->writed == pMgr->mem_head) {//不可以覆盖写，需要释放
            i++;
            break;
        }
        i++;
    }

    ret = i;
    //网络下载数据添加到缓冲，必须设置下面参数，这样读任务就不会再把这些数据添加到网络，导致死循环。
    ptmp->rlen = ptmp->wlen;
    ptmp->readed = ptmp->writed;
    ptmp->is_fin = 1;
    ptmp->status = ERS_NO_TAIL;

    if (ret > 0) {
        pMgr->mem_cnts += ret;
        pMgr->mem_tail += ret;
        pMgr->mem_tail = pMgr->mem_tail % pMgr->mem_max;
    }

FUN_RET:
    RECORD_MGR_CACHE_UNLOCK();

    return ret;
}

//注意：如果最后一项已经读完且设置了结束标志，则返回最后一项的下一个位置（即arr_tail)，否则返回最后一项，但数据已经读完
static int RecCacheGetReadIdx()
{
    int ret = 0;
    int idx = 0;
    int cnts = 0;
    int maxs = 0;
    struct StRecCache *parr = 0;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    if (pMgr->parr_cache == 0 || pMgr->arr_max <= 0 || pMgr->arr_cnts == 0) return ERC_PARAMRAM;

    parr = pMgr->parr_cache;
    maxs = pMgr->arr_max;
    idx = pMgr->arr_head;
    cnts = pMgr->arr_cnts;
    //查找第一个rlen != wlen, 还有一种情况整个mem都占用了
    while (cnts) {
        if (parr[idx].no_used == 0) {
            if (parr[idx].rlen != parr[idx].wlen) {//rlen != wlen, 已经网络读完此项
                break;
            }
            if ((idx + 1) % maxs == pMgr->arr_tail) {//最后一项，但是rlen=wlen, 这时要判断结束标志
                if (parr[idx].is_fin == 0) {//结束标志如果没设定，那么返回当前项，可能这项还有数据添加
                    break;
                }
            }
        }
        idx++;
        idx = idx % maxs;

        cnts--;
    }
    //当cnts==0,完全读完，还要返回最后一个元素的下个元素（arr_tail）
    return idx;

}

static int strxcpy(char *out, int olen, char *in)
{
    if (out == 0 || in == 0) {
        return ERC_PARAMRAM;
    }
    int min = strlen(in) + 1;
    min = min < olen ? min : olen;
    memcpy(out, in, min);
    out[min - 1] = 0;
    return 0;
}


int RecCacheDelItem(char *url)
{
    int ret = 0;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    if (url == 0) return -1;

    RECORD_MGR_CACHE_LOCK();
    if (pMgr->init_flag == 0) goto FUN_RET;
    ret = RecCacheRemove(url);

FUN_RET:
    RECORD_MGR_CACHE_UNLOCK();
    return ret;
}
//item, 记录数组的大小
int RecCacheReset(int items)
{
    int ret = 0;
    RecCacheDeInit();
    RecCacheInit(items);
    return ret;
}


//从环形数组读出数据
//读出数据不会删除数据，但会改变数组每个元素的readed的位置。（这主要是为了播放时能从这里获取数据，而不用从网络获取）
//返回：
// >= 0:读取的数据长度。
// <  0:错误

int RecCacheReadHead(char *out_url, int *out_id, int ulen, unsigned char *out_data, int dlen, int *pstatus)
{
    int ret = 0;
    int i = 0;
    int cur_idx = 0;
    struct StRecCache *parr = 0;
    struct StRecCache *ptmp = 0;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    if (pMgr->parr_cache == 0 || pMgr->arr_max <= 0 || pMgr->arr_cnts == 0) {
        ret = ERC_PARAMRAM;
        goto FUN_RET;
    }

    RECORD_MGR_CACHE_LOCK();
    if (pMgr->init_flag == 0) goto FUN_RET;

    parr = pMgr->parr_cache;

    cur_idx = RecCacheGetReadIdx();

    if (cur_idx < 0) {
        ret = ERC_PARAMRAM;
        goto FUN_RET;
    }

    if (cur_idx == pMgr->arr_tail && pMgr->arr_cnts > 0) {//完全读完，且最后一项有结束标志，则返回负数
        ret = ERC_NO_NEW_ITEM;
        goto FUN_RET;
    }
    ptmp = &parr[cur_idx];
    strxcpy(out_url, ulen, ptmp->url);

    i = 0;
    while (dlen--) {
        if (ptmp->rlen == ptmp->wlen) {
            break;
        }
        out_data[i] = pMgr->pmem[ptmp->readed];
        ptmp->readed++;
        ptmp->readed = ptmp->readed % pMgr->mem_max;
        ptmp->rlen++;
        i++;
    }
    if (out_id && ptmp) *out_id = ptmp->id;
    //注意，这里有可能是返回0，如果最后一项读取完成且没有结束标志，可能要等待更多的数据。
    ret = i;
    if (pstatus) { //设置录音结束标志
        *pstatus = ptmp->is_fin;
    }

FUN_RET:
    RECORD_MGR_CACHE_UNLOCK();
    return ret;
}
//设置最后一个录音结束标志
//type=0: 录音按键类型
//type=1: 网络下载类型
int RecCacheSetItemDone(int type)
{
    int last = -1;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    if (pMgr->parr_cache == 0 || pMgr->arr_max <= 0 || pMgr->arr_cnts == 0) {
        return ERC_PARAMRAM;
    }

    RECORD_MGR_CACHE_LOCK();
    if (pMgr->init_flag == 0) goto FUN_RET;

    if (pMgr->arr_cnts > 0) {
        if (pMgr->arr_tail == 0) {
            last = pMgr->arr_max - 1;
        }
        else {
            last = pMgr->arr_tail - 1;
        }
        if (pMgr->parr_cache[last].status == ERS_NO_TAIL && pMgr->parr_cache[last].type == type) { //只有最后一个没被破坏头，才能设置完整标志。
            pMgr->parr_cache[last].status = ERS_COMPLETE;
        }
        pMgr->parr_cache[last].is_fin = 1; //结束标志。
        if (pMgr->parr_cache[last].type == 0) {
            pMgr->is_recording = 0; //录音按键停止，允许网络下载到缓冲区。
        }
    }
FUN_RET:

    RECORD_MGR_CACHE_UNLOCK();
    return 0;
}
//检查读取录音数据状态， 如果读完数据和结束标志设置则返回1，
//否则返回没读完数据或没置结束位返回0
//错误返回负数
int RecCacheCheckReadStatus(char *url)
{
    int status = -1;
    int maxs = 0;
    int idx = 0;
    int cnts = 0;
    struct StRecCache *parr = 0;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    if (url == 0 || url[0] == 0 || pMgr->parr_cache == 0 || pMgr->arr_max <= 0 || pMgr->arr_cnts == 0) {
        return ERC_PARAMRAM;
    }
    RECORD_MGR_CACHE_LOCK();
    if (pMgr->init_flag == 0) goto FUN_RET;

    parr = pMgr->parr_cache;
    maxs = pMgr->arr_max;
    idx = pMgr->arr_head;
    cnts = pMgr->arr_cnts;
    while (cnts) {
        if (parr[idx].no_used == 0) {
            if (strcmp(parr[idx].url, url) == 0) { //找到url这项
                if (parr[idx].rlen == parr[idx].wlen && //读写长度一致，已经读完
                    parr[idx].is_fin == 1) {//完成标志结束设置
                    status = 1; //完整的读完该项
                    break;
                }
                else {
                    status = 0; //该项存在，但没完成。
                    break;
                }
            }
        }
        idx++;
        idx = idx % maxs;
        cnts--;
    }
FUN_RET:

    RECORD_MGR_CACHE_UNLOCK();
    return status;
}

//return:
// ret == -1: find none, 如果缓冲音频头部被破坏也返回-1
// ret == 0: 完整的音频，不用网络下载
// ret == 1: 部分音频，可以播放前半段，后半段从网络下载
// ppdata, plen : 环型缓冲区尾巴
// ppdata1, plen1: 环形缓冲区头部， 这个有可能没头部，所以可以为0
int RecCacheFindCache(char *url, unsigned char **ppdata, unsigned int *plen,
    unsigned char **ppdata1, unsigned int *plen1)
{
    int ret = -1;
    int idx = 0;
    int maxs = 0;
    int cnts = 0;
    int base = -1;
    struct StRecCache *parr = 0;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;

    if (url == 0 || pMgr->parr_cache == 0 || pMgr->arr_max <= 0) return -1;

    RECORD_MGR_CACHE_LOCK();

    if (pMgr->init_flag == 0) goto FUN_RET;

    parr = pMgr->parr_cache;
    maxs = pMgr->arr_max;
    cnts = pMgr->arr_cnts;
    idx = pMgr->arr_head;

    //标志no_used元素
    while (cnts) {
        if (strcmp(parr[idx].url, url) == 0 && parr[idx].no_used == 0) {
            if (parr[idx].status == ERS_COMPLETE) {
                base = pMgr->parr_cache[idx].base;
                *ppdata = &pMgr->pmem[base];
                if (pMgr->parr_cache[idx].wlen > 0 &&
                    pMgr->parr_cache[idx].base >= pMgr->parr_cache[idx].writed) {//反转
                    *plen = pMgr->mem_max - pMgr->parr_cache[idx].base;
                    *ppdata1 = pMgr->pmem;
                    *plen1 = pMgr->parr_cache[idx].writed;
                }
                else {//没反转
                    *plen = pMgr->parr_cache[idx].wlen;
                    *ppdata1 = 0;
                    *plen1 = 0;
                }

                ret = 0;//完整的音频，不用下载
                goto FUN_RET;
            }
            else if (parr[idx].status == ERS_NO_TAIL) {
                base = pMgr->parr_cache[idx].base;
                *ppdata = &pMgr->pmem[base];
                if (pMgr->parr_cache[idx].wlen > 0 &&
                    pMgr->parr_cache[idx].base >= pMgr->parr_cache[idx].writed) {//反转
                    *plen = pMgr->mem_max - pMgr->parr_cache[idx].base;
                    *ppdata1 = pMgr->pmem;
                    *plen1 = pMgr->parr_cache[idx].writed;
                }
                else {//没反转
                    *plen = pMgr->parr_cache[idx].wlen;
                    *ppdata1 = 0;
                    *plen1 = 0;
                }
                ret = 1;//部分音频，要继续下载下半部分
                goto FUN_RET;
            }
            else  /* (parr[idx].status == ERS_NO_HEAD) */ {
                ret = -1;//头部不完整的音频，需要在网络从头下载
                goto FUN_RET;
            }
        }
        idx++;
        idx = idx % maxs;
        cnts--;
    }

FUN_RET:
    RECORD_MGR_CACHE_UNLOCK();

    return ret; //查找不到需要在网络从头下载
}

/* 网上播放了一段录音，然后录音还没上传，部分还在本地缓冲区， 从本地缓冲区获取数据 */
/* ret = 0: 正常返回 */
/* ret < 0: 错误返回 */
int RecCacheFindCacheTail(char *url, int pos, unsigned char **ppdata, unsigned int *plen,
    unsigned char **ppdata1, unsigned int *plen1)
{
    int ret = -1;
    int idx = 0;
    int maxs = 0;
    int cnts = 0;
    int base = -1;
    int pos_start = 0;
    int left = 0;
    struct StRecCache *parr = 0;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;

    if (url == 0 || pMgr->parr_cache == 0 || pMgr->arr_max <= 0) return -1;

    RECORD_MGR_CACHE_LOCK();

    if (pMgr->init_flag == 0) goto FUN_RET;

    parr = pMgr->parr_cache;
    maxs = pMgr->arr_max;
    cnts = pMgr->arr_cnts;
    idx = pMgr->arr_head;

    printf("info: RecCacheFindCacheTail, url=%s, pos=%d!\r\n", url, pos);
    //标志no_used元素
    while (cnts) {
        if (strcmp(parr[idx].url, url) == 0 && parr[idx].no_used == 0) {
            left = parr[idx].totals - pos;
            if (left <= 0) {
                //printf("----2--left=%d, parr[%d].totals=%d---\r\n", left, idx, parr[idx].totals);
                goto FUN_RET;
            }
            //printf("----3--left=%d, wlen=%d, totals=%d---\r\n", left, parr[idx].wlen, parr[idx].totals);
            if (left <= parr[idx].wlen) {//网络剩余没上传的数据 少于 本地缓冲， 可以成功找到
                printf("info: RecCacheFindCacheTail, matched, totals=%d, left=%d\r\n", parr[idx].totals, left);
                pos_start = (parr[idx].base + parr[idx].wlen - left) % pMgr->mem_max;
                *ppdata = &pMgr->pmem[pos_start];
                if (pos_start < parr[idx].writed) {
                    *plen = parr[idx].writed - pos_start;
                    *ppdata1 = 0;
                    *plen1 = 0;
                }
                else {//反转
                    *plen = pMgr->mem_max - pos_start;
                    *ppdata1 = pMgr->pmem;
                    *plen1 = parr[idx].writed;
                }
                ret = 0;
            }
            goto FUN_RET;
        }
        idx++;
        idx = idx % maxs;
        cnts--;
    }

FUN_RET:
    RECORD_MGR_CACHE_UNLOCK();

    return ret; //查找不到需要在网络从头下载
}

/*
* 获取环形缓冲区的最大数值和剩余空余数值（字节）
*/
int RecCacheGetSizeInfo(int *free_size, int *total_size)
{
    int ret = 0;
    int rd_pos = 0;
    int wt_pos = 0;
    int cur_idx = 0;
    struct StRecCache *parr = 0;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;

    if (free_size == 0 || total_size == 0) return -1;

    parr = pMgr->parr_cache;

    RECORD_MGR_CACHE_LOCK();

    if (pMgr->init_flag == 0) goto FUN_RET;

    cur_idx = RecCacheGetReadIdx();

    if (cur_idx < 0 ||
        (cur_idx == pMgr->arr_tail && pMgr->arr_cnts > 0)) {
        if (free_size && total_size) {
            *free_size = *total_size = pMgr->mem_max;
            goto FUN_RET;
        }
    }
    rd_pos = parr[cur_idx].readed;
    wt_pos = pMgr->mem_tail;
    if (rd_pos == wt_pos && parr[cur_idx].rlen == parr[cur_idx].wlen) {//全部读完
        if (free_size && total_size) {
            *free_size = *total_size = pMgr->mem_max;
            goto FUN_RET;
        }
    }
    else if (rd_pos < wt_pos) {
        if (free_size)
            *free_size = pMgr->mem_max - (wt_pos - rd_pos);
        if (total_size)
            *total_size = pMgr->mem_max;
    }
    else {
        if (free_size) {
            *free_size = rd_pos - wt_pos;
        }
        if (total_size)
            *total_size = pMgr->mem_max;
    }
FUN_RET:
    RECORD_MGR_CACHE_UNLOCK();
    return ret;
}

int RecCachePrintInfo()
{
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    char buf[200];
    char subinfo[200 * 4] = "";
    int maxs = pMgr->arr_max;
    int idx = pMgr->arr_head;
    int cnts = pMgr->arr_cnts;
    struct StRecCache *parr = pMgr->parr_cache;
    strcat(subinfo, "[");
    while (cnts) {
        sprintf(buf, "(i:%d,u:%s,n:%d,r:%d,rl:%d,w:%d,wl:%d,b:%d,s:%d,f:%d)",
            idx,
            parr[idx].url,
            parr[idx].no_used,
            parr[idx].readed,
            parr[idx].rlen,
            parr[idx].writed,
            parr[idx].wlen,
            parr[idx].base,
            parr[idx].status,
            parr[idx].is_fin);
        strcat(subinfo, buf);
        idx++;
        idx = idx % maxs;
        cnts--;
    }
    strcat(subinfo, "]\r\n");
    printf("mem[m:%d,h:%d,t:%d,c:%d] arr[m:%d,h:%d,t:%d,c:%d]\r\n",
        pMgr->mem_max, pMgr->mem_head, pMgr->mem_tail, pMgr->mem_cnts,
        pMgr->arr_max, pMgr->arr_head, pMgr->arr_tail, pMgr->arr_cnts);
    printf("%s", subinfo);
    return 0;
}


#if SELF_TEST

char malloc_arr[20];
char *GetRecordCacheAddrTEST()
{
    return malloc_arr;
}
int GetRecordCacheSizeTEST()
{
    return 20;
}

/***************************************************/
// 下面是测试用例
/***************************************************/

char *arr_url[] = {
    "devtemp/0000.wav",
    "devtemp/0001.wav",
    "devtemp/0002.wav",
    "devtemp/0003.wav",
    "devtemp/0004.wav",
    "devtemp/0005.wav",
    "devtemp/0006.wav",
    "devtemp/0007.wav",
    "devtemp/0008.wav",
    "devtemp/0009.wav",
};

#define ASSERT_TEST(t,s) do {if(t) {/*printf("info[%d]: %s test ok!!!\r\n", __LINE__, (s));*/}else{printf("error[%d]: %s test failed!!!\r\n", __LINE__, s); while(1);}}while(0)

#define BOUNDARY_ERROR() do {printf("BOUNDARY CHECK ERROR: code(%d)\r\n", __LINE__);goto ERROR_RET;} while(0)
int BoudaryCheck()
{
    int ret = 0;
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    if (pMgr->parr_cache == 0 || pMgr->arr_max <= 0 || pMgr->arr_cnts == 0) return ERC_PARAMRAM;

    struct StRecCache *parr = pMgr->parr_cache;
    int maxs = pMgr->arr_max;
    int idx = pMgr->arr_head;
    int cnts = pMgr->arr_cnts;

    //判断pMgr有没有越界
    if (pMgr->arr_cnts > pMgr->arr_max || pMgr->arr_cnts < 0) BOUNDARY_ERROR();
    if (pMgr->arr_head > pMgr->arr_max || pMgr->arr_head < 0) BOUNDARY_ERROR();
    if (pMgr->arr_tail > pMgr->arr_max || pMgr->arr_tail < 0) BOUNDARY_ERROR();
    //检查arr_cnts == arr_head 和 arr_tail之间差距
    if (pMgr->arr_head < pMgr->arr_tail) {
        if (pMgr->arr_cnts != pMgr->arr_tail - pMgr->arr_head) BOUNDARY_ERROR();
    }
    if (pMgr->arr_head > pMgr->arr_tail) {
        if (pMgr->arr_cnts != pMgr->arr_max - pMgr->arr_head + pMgr->arr_tail) BOUNDARY_ERROR();
    }
    if (pMgr->arr_cnts == 0 || pMgr->arr_cnts == pMgr->arr_max) {
        if (pMgr->arr_head != pMgr->arr_tail) BOUNDARY_ERROR();
    }

    //检查边界
    if (pMgr->mem_cnts > pMgr->mem_max || pMgr->mem_cnts < 0) BOUNDARY_ERROR();
    if (pMgr->mem_head > pMgr->mem_max || pMgr->mem_head < 0) BOUNDARY_ERROR();
    if (pMgr->mem_tail > pMgr->mem_max || pMgr->mem_tail < 0) BOUNDARY_ERROR();
    //检查mem_cnts == mem_head 和 mem_tail之间差距
    if (pMgr->mem_head < pMgr->mem_tail) {
        if (pMgr->mem_cnts != pMgr->mem_tail - pMgr->mem_head) {
            RecCachePrintInfo();
            BOUNDARY_ERROR();
        }
    }
    if (pMgr->mem_head > pMgr->mem_tail) {
        if (pMgr->mem_cnts != pMgr->mem_max - pMgr->mem_head + pMgr->mem_tail) BOUNDARY_ERROR();
    }
    if (pMgr->mem_cnts == 0 || pMgr->mem_cnts == pMgr->mem_max) {
        if (pMgr->mem_head != pMgr->mem_tail) BOUNDARY_ERROR();
    }

    //遍历数组元素是否越界
    int totals = 0;
    int mark = -1;
    while (cnts) {
        if (mark == -1) {//第一个元素，检查base==pMgr->mem_head
            mark = 1;
            if (pMgr->mem_head != parr[idx].base) BOUNDARY_ERROR();
        }
        if (cnts - 1 == 0) {//最后一个元素
            if (pMgr->mem_tail != parr[idx].writed) BOUNDARY_ERROR();
        }
        //readed 必须在base和writed之间，可以等于

        if (parr[idx].base == parr[idx].writed) {
            if (!(parr[idx].wlen == 0 || parr[idx].wlen == pMgr->mem_max))  BOUNDARY_ERROR();
        }
        else if (parr[idx].base < parr[idx].writed) {
            if (parr[idx].readed < parr[idx].base || parr[idx].readed > parr[idx].writed) BOUNDARY_ERROR();
        }
        else if (parr[idx].base > parr[idx].writed) {
            if (parr[idx].readed < parr[idx].base && parr[idx].readed > parr[idx].writed) BOUNDARY_ERROR();
        }
        //检查有效数组总和 == pMgr->mem_cnts
        totals += parr[idx].wlen;
        //检查rlen <= wlen
        if (parr[idx].rlen > parr[idx].wlen) BOUNDARY_ERROR();

        idx++;
        idx = idx % maxs;
        cnts--;
    }
    //检查有效数组总和 == pMgr->mem_cnts
    if (totals != pMgr->mem_cnts) BOUNDARY_ERROR();

    return 0;

ERROR_RET:
    while (1);
    return 0;
}

int DebugRecordError()
{
    struct StRecCacheMgr *pMgr = (struct StRecCacheMgr *)&gRecCacheMgr;
    pMgr->mem_max = 20;
    pMgr->mem_head = 13;
    pMgr->mem_tail = 13;
    pMgr->mem_cnts = 20;

    pMgr->arr_max = 4;//StRecCache环形数组总个数
    pMgr->arr_head = 3;//StRecCache环形数组的头下标
    pMgr->arr_tail = 2;//StRecCache环形数组的尾下标
    pMgr->arr_cnts = 3; //StRecCache环形数组,有效数据个数，区分head_index==tail_index满或空两种情况

    strcpy(pMgr->parr_cache[3].url, "devtemp/0003.wav");
    pMgr->parr_cache[3].type = 0; //0：录音按键追加数据； 1: 网络下载追加数据
    pMgr->parr_cache[3].no_used = 0;
    pMgr->parr_cache[3].readed = 13;
    pMgr->parr_cache[3].rlen = 0; //已经读的长度， readed - base
    pMgr->parr_cache[3].writed = 19;
    pMgr->parr_cache[3].wlen = 6; //base <-> writed
    pMgr->parr_cache[3].base = 13;
    pMgr->parr_cache[3].totals = 0; //记录录音完整的长度，这里提供给网上下载一半，又回来缓冲区把剩下一半找回来。
    pMgr->parr_cache[3].status = 1; //0: 不完整(没尾巴，需要等待)； 1： 完整;  2: 不完整（没头），因为缓冲不够，这个base更新为readed
    pMgr->parr_cache[3].is_fin = 1; //录音是否已经结束，0:录音还在进行中，1:录音结束

    strcpy(pMgr->parr_cache[0].url, "devtemp/0001.wav");
    pMgr->parr_cache[0].type = 0; //0：录音按键追加数据； 1: 网络下载追加数据
    pMgr->parr_cache[0].no_used = 0;
    pMgr->parr_cache[0].readed = 19;
    pMgr->parr_cache[0].rlen = 0; //已经读的长度， readed - base
    pMgr->parr_cache[0].writed = 4;
    pMgr->parr_cache[0].wlen = 5; //base <-> writed
    pMgr->parr_cache[0].base = 19;
    pMgr->parr_cache[0].totals = 0; //记录录音完整的长度，这里提供给网上下载一半，又回来缓冲区把剩下一半找回来。
    pMgr->parr_cache[0].status = 1; //0: 不完整(没尾巴，需要等待)； 1： 完整;  2: 不完整（没头），因为缓冲不够，这个base更新为readed
    pMgr->parr_cache[0].is_fin = 1; //录音是否已经结束，0:录音还在进行中，1:录音结束


    strcpy(pMgr->parr_cache[1].url, "devtemp/0000.wav");
    pMgr->parr_cache[1].type = 0; //0：录音按键追加数据； 1: 网络下载追加数据
    pMgr->parr_cache[1].no_used = 0;
    pMgr->parr_cache[1].readed = 4;
    pMgr->parr_cache[1].rlen = 0; //已经读的长度， readed - base
    pMgr->parr_cache[1].writed = 13;
    pMgr->parr_cache[1].wlen = 9; //base <-> writed
    pMgr->parr_cache[1].base = 4;
    pMgr->parr_cache[1].totals = 0; //记录录音完整的长度，这里提供给网上下载一半，又回来缓冲区把剩下一半找回来。
    pMgr->parr_cache[1].status = 1; //0: 不完整(没尾巴，需要等待)； 1： 完整;  2: 不完整（没头），因为缓冲不够，这个base更新为readed
    pMgr->parr_cache[1].is_fin = 1; //录音是否已经结束，0:录音还在进行中，1:录音结束

    char buf[1000];
    RecCachePrintInfo();
    int ret = RecCacheAppendTail("devtemp/0001.wav", buf, 5, 1);
    RecCachePrintInfo();
    BoudaryCheck();
    return 0;
}

int RecordCacheTest()
{
    int ret = 0;
    char url_rd[100];
    char buf[1024];
    unsigned char temp[100];
    char *pAddr = 0;
    int length = 0;
    char *pAddr1 = 0;
    int length1 = 0;
    char cmp[] = "\x22\x22\x22\x22\x22\x22\x22\x22\x22\x22";

    /*************************************************************/
    /* 测试用例1：
    /* 操作顺序：录音1, 录音1
    /* 正常结果：
    /* 删除第一次录音1，尾部添加第二次录音1。
    **************************************************************/
    RecCacheInit(4);
    //第一个录音
    memset(buf, 0x11, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[1], buf, 4, E_APPEND_HEAD);
    ASSERT_TEST(ret == 4, "testcase 1");
    BoudaryCheck();
    //再返回第一个录音
    memset(buf, 0x11, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[1], buf, 4, E_APPEND_HEAD);
    ASSERT_TEST(ret == 4, "testcase 1");
    BoudaryCheck();
    RecCacheDeInit();

    /*************************************************************/
    /* 测试用例2：
    /* 操作顺序：录音1，录音2，录音1
    /* 正常结果：最后第一次删除录音1，然后在尾部添加录音1
    **************************************************************/
    RecCacheInit(4);
    //第一个录音
    memset(buf, 0x11, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[1], buf, 4, E_APPEND_HEAD);
    ASSERT_TEST(ret == 4, "testcase 2");
    BoudaryCheck();
    //第二个录音
    memset(buf, 0x22, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[2], buf, 4, E_APPEND_HEAD);
    ASSERT_TEST(ret == 4, "testcase 2");
    BoudaryCheck();
    //再返回第一个录音
    memset(buf, 0x11, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[1], buf, 4, E_APPEND_HEAD);
    ASSERT_TEST(ret == 4, "testcase 2");
    BoudaryCheck();
    RecCacheDeInit();

    /*************************************************************/
    /* 测试用例3：
    /* 操作顺序：录音1，录音2，录音2
    /* 正常结果：删除第一次录音2，在删除的录音2位置添加录音2
    **************************************************************/
    RecCacheInit(4);
    //第一个录音
    memset(buf, 0x11, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[1], buf, 4, E_APPEND_HEAD);
    ASSERT_TEST(ret == 4, "testcase 3");
    BoudaryCheck();
    //第二个录音
    memset(buf, 0x22, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[2], buf, 4, E_APPEND_HEAD);
    ASSERT_TEST(ret == 4, "testcase 3");
    BoudaryCheck();
    //第二个录音
    memset(buf, 0x22, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[2], buf, 4, E_APPEND_HEAD);
    ASSERT_TEST(ret == 4, "testcase 3");
    BoudaryCheck();
    RecCacheDeInit();

    /*************************************************************/
    /* 测试用例4：
    /* 操作顺序：录音1，录音2，录音3，录音4
    /* 正常结果：测试录音个数大于总数，因为没读操作，录音4返回失败
    **************************************************************/
    RecCacheInit(3);//3个录音
                    //第1个录音
    memset(buf, 0x11, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[1], buf, 4, E_APPEND_HEAD);
    ASSERT_TEST(ret == 4, "testcase 4");
    BoudaryCheck();
    //第2个录音
    memset(buf, 0x22, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[2], buf, 4, E_APPEND_HEAD);
    ASSERT_TEST(ret == 4, "testcase 4");
    BoudaryCheck();
    //第3个录音
    memset(buf, 0x33, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[3], buf, 4, E_APPEND_HEAD);
    ASSERT_TEST(ret == 4, "testcase 4");
    BoudaryCheck();
    //第4个录音
    memset(buf, 0x44, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[4], buf, 4, E_APPEND_HEAD);
    ASSERT_TEST(ret == ERC_CREATE_ITEM, "testcase 4");
    BoudaryCheck();
    RecCacheDeInit();


    /*************************************************************/
    /* 测试用例5：
    /* 操作顺序：录音1， 录音2， 录音3
    /* 正常结果：综合测试
    **************************************************************/
    RecCacheInit(4);
    //第1个录音
    memset(buf, 0x11, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[1], buf, 20, E_APPEND_HEAD);
    ASSERT_TEST(ret == 20, "testcase 5");
    BoudaryCheck();
    //第1个录音，添加1个字节
    ret = RecCacheAppendTail(arr_url[1], buf, 1, E_APPEND_TAIL);
    RecCacheSetItemDone(0);//结束
    ASSERT_TEST(ret == 0, "testcase 5");
    BoudaryCheck();
    //第2个录音，注意第2个录音mem为空，但项数据是存在的
    memset(buf, 0x22, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[2], buf, 4, E_APPEND_HEAD);
    RecCacheSetItemDone(0);//结束
    ASSERT_TEST(ret == 0, "testcase 5");
    BoudaryCheck();
    //第3个录音。
    memset(buf, 0x33, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[3], buf, 4, E_APPEND_HEAD);
    RecCacheSetItemDone(0);//结束
    ASSERT_TEST(ret == 0, "testcase 5");
    BoudaryCheck();
    //读录音数据，释放空间4
    ret = RecCacheReadHead(url_rd, 0, sizeof(url_rd), temp, 4, 0);
    ASSERT_TEST(ret == 4, "testcase 5");
    ASSERT_TEST(strcmp(url_rd, arr_url[1]) == 0, "testcase 5");
    BoudaryCheck();
    //第3个录音,追加5个，返回4个
    memset(buf, 0x33, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[3], buf, 5, E_APPEND_TAIL);
    RecCacheSetItemDone(0);//结束
    ASSERT_TEST(ret == 4, "testcase 5");
    BoudaryCheck();
    //测试读查找音频1,返回破坏了头部的音频，从头网络下载
    ret = RecCacheFindCache(arr_url[1], &pAddr, &length, &pAddr1, &length1);
    ASSERT_TEST(ret == -1, "testcase 5");
    //读录音数据，读取全部，不过返回第1个录音的剩下16个字节
    ret = RecCacheReadHead(url_rd, 0, sizeof(url_rd), temp, 21, 0);
    ASSERT_TEST(ret == 16, "testcase 5");
    ASSERT_TEST(strcmp(url_rd, arr_url[1]) == 0, "testcase 5");
    BoudaryCheck();
    //读录音数据，再读取全部，不过返回第3个录音的4字节
    //注意：第2个录音表项存在，不过mem==0, 通过RecCacheGetReadIdx()直接返回有数据的下一个录音3
    ret = RecCacheReadHead(url_rd, 0, sizeof(url_rd), temp, 21, 0);
    ASSERT_TEST(ret == 4, "testcase 5");
    ASSERT_TEST(strcmp(url_rd, arr_url[3]) == 0, "testcase 5");
    BoudaryCheck();
    //创建录音4(当前所有数据读空),写入满数据
    memset(buf, 0x44, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[4], buf, 20, E_APPEND_HEAD);
    RecCacheSetItemDone(0);//结束
    ASSERT_TEST(ret == 20, "testcase 5");
    BoudaryCheck();
    //读录音数据，读取录音4的全部数据。
    ret = RecCacheReadHead(url_rd, 0, sizeof(url_rd), temp, 21, 0);
    ASSERT_TEST(ret == 20, "testcase 5");
    ASSERT_TEST(strcmp(url_rd, arr_url[4]) == 0, "testcase 5");
    BoudaryCheck();
    //第2个录音
    memset(buf, 0x22, sizeof(buf));
    ret = RecCacheAppendTail(arr_url[2], buf, 4, E_APPEND_HEAD);
    ASSERT_TEST(ret == 4, "testcase 5");
    BoudaryCheck();
    ret = RecCacheAppendTail(arr_url[2], buf, 17, E_APPEND_TAIL);
    RecCacheSetItemDone(0);//结束
    ASSERT_TEST(ret == 16, "testcase 5");
    BoudaryCheck();
    //读录音数据，读取录音2的全部数据。
    ret = RecCacheReadHead(url_rd, 0, sizeof(url_rd), temp, 21, 0);
    ASSERT_TEST(ret == 20, "testcase 5");
    ASSERT_TEST(strcmp(url_rd, arr_url[2]) == 0, "testcase 5");
    BoudaryCheck();
    //测试读查找音频2,返回没结束，继续网络下载下半部
    ret = RecCacheFindCache(arr_url[2], &pAddr, &length, &pAddr1, &length1);
    ASSERT_TEST(ret == 0, "testcase 5");
    ASSERT_TEST(length+length1 == 20, "testcase 5");
    //测试读查找音频2,返回完整音频，不用任何网络下载
    RecCacheSetItemDone(0);//设置结束标志，证明该录音结束且是完整的。
    ret = RecCacheFindCache(arr_url[2], &pAddr, &length, &pAddr1, &length1);
    ASSERT_TEST(ret == 0, "testcase 5");
    ASSERT_TEST(length+ length1 == 20, "testcase 5");
    //测试读查找音频6,返回-1，需要从头网上下载音频。
    ret = RecCacheFindCache(arr_url[6], &pAddr, &length, &pAddr1, &length1);
    ASSERT_TEST(ret == -1, "testcase 5");
    RecCacheDeInit();

    /*************************************************************/
    /* 测试用例6：
    /* 操作顺序：
    /* 正常结果：随机测试，边界检查
    **************************************************************/

    while (1) {
        //随机数组个数
        RecCacheInit(rand() % 10 + 1);

        while (rand() % 100) {
            //BoudaryCheck();
            int num = rand() % 4;
            switch (num) {
            case 0:
                memset(buf, 0x00, sizeof(buf));
                break;
            case 1:
                memset(buf, 0x11, sizeof(buf));
                break;
            case 2:
                memset(buf, 0x22, sizeof(buf));
                break;
            case 3:
                memset(buf, 0x33, sizeof(buf));
                break;
            default:
                memset(buf, 0x00, sizeof(buf));
                break;
            }
            int wlen;
            int rlen;
            int hort;
            int counts = 100;

            int rand_rn = 0;
            int rand_wn = 0;
            //RecCachePrintInfo();
            BoudaryCheck();
            printf("STEP 1: %d TIMES READ AND WRITED!\r\n", counts);
            while (counts--) {
                wlen = rand() % 30 + 1;
                rlen = rand() % 30 + 1;
                hort = rand() % 2;
                num = rand() % 4;

                //随机次读
                rand_rn == rand() % 4;
                while (rand_rn) {
                    ret = RecCacheAppendTail(arr_url[num], buf, wlen, hort);
                    printf("info: [WRITE] url=%s, write data[%d:%d], %s\r\n", arr_url[num], wlen, ret, (hort ? "TAIL" : "HEAD"));
                    BoudaryCheck();
                    rand_rn--;
                }
                //随机次写
                rand_wn == rand() % 10;
                while (rand_wn) {
                    ret = RecCacheReadHead(url_rd, 0, sizeof(url_rd), temp, rlen, 0);
                    RecCacheSetItemDone(0);//设置结束标志
                    printf("info: [READ] url=%s, read data[%d:%d]\r\n", url_rd, rlen, ret);
                    BoudaryCheck();
                    rand_wn--;
                }
            }


            //读清全部
            printf("STEP 2: READ OUT ALL DATA!\r\n");
            while (1) {
                rlen = rand() % 30 + 1;
                ret = RecCacheReadHead(url_rd, 0, sizeof(url_rd), temp, rlen, 0);
                BoudaryCheck();
                if (ret > 0) {
                    printf("info: [READ] url=%s, read data[%d:%d]\r\n", url_rd, rlen, ret);
                }
                if (ret == ERC_READ_EMPTY) {
                    printf("info: [READ] DONE, EMPTY!!!\r\n");
                    break;
                }
                if (ret == ERC_PARAMRAM) {
                    printf("info: [READ] PARAMRAM ERROR!!!\r\n");
                    break;
                }
                if (ret == ERC_NO_NEW_ITEM) {
                    break;
                }
            }

            //写满全部
            printf("STEP 3: WRITE FULL ALL DATA!\r\n");
            rlen = rand() % 30 + 1;
            while (1) {
                wlen = rand() % 30 + 1;
                rlen = rand() % 30 + 1;
                hort = rand() % 2;
                num = rand() % 4;
                ret = RecCacheAppendTail(arr_url[num], buf, wlen, hort);
                RecCacheSetItemDone(0);//设置结束标志
                BoudaryCheck();
                if (ret > 0) {
                    printf("info: [WRITE] url=%s, write data[%d:%d]   \r\n", arr_url[num], wlen, ret);
                }
                if (ret == ERC_CREATE_ITEM) {
                    printf("warnning: [WRITE] code: ERC_CREATE_ITEM!\r\n");
                }
                if (ret == ERC_WRITE_FULL) {
                    printf("info: [WRITE] DONE, FULL!!!\r\n");
                    break;
                }
            }

            //读清全部
            printf("STEP 4: READ OUT ALL DATA!\r\n");
            while (1) {
                rlen = rand() % 30 + 1;
                ret = RecCacheReadHead(url_rd, 0, sizeof(url_rd), temp, rlen, 0);
                BoudaryCheck();
                if (ret > 0) {
                    printf("info: [READ] url=%s, read data[%d:%d]\r\n", url_rd, rlen, ret);
                }
                if (ret == ERC_READ_EMPTY) {
                    printf("info: [READ] DONE, EMPTY!!!\r\n");
                    break;
                }
                if (ret == ERC_NO_NEW_ITEM) {
                    break;
                }
            }
        }
        RecCacheDeInit();
    }
    return ret;
}
#endif

