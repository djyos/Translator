#include "stdint.h"
#include "stddef.h"
#include "stdlib.h"
#include <sys/socket.h>
#include <sys/dhcp.h>
#include <sys/dhcp.h>
#include <app_flash.h>
#include "wlan_ui_pub.h"
#include "project_config.h"
#include "board.h"
#include "djyos.h"
#include "int.h"
#include "app_flash.h"

typedef struct StWiFiSWRefs {
    volatile int refs_wifi_on_off;
    volatile unsigned int refs_timemark;
}StWiFiSWRefs;

StWiFiSWRefs gWiFiRefs;

extern  WIFI_CFG_T  LoadWifi;

extern void Int_SaveAsynSignal(void);
extern void Int_RestoreAsynSignal(void);
extern bool_t DhcpIp_InfoSave(char *info ,u32 len);
extern int is_wifi_connected();

//volatile int dhcp_db_flag = 0;

//
//u32 Get_DhcpInitflag(void)
//{
//#if RC_DEBUG_OFF
//    if(dhcp_db_flag == 0) {
//        if(!WifiLoad())
//        {
//            return 1<<1;
//        }
//        return (1<<0);
//    }
//    else
//    {
//        return 1;
//    }
//#else
//    return 1;
//#endif
//}

char gip_addr[30] = {0};

char *DevGetIpAddr()
{
    return gip_addr;
}
char *inet_ntoa(struct in_addr addr);
int cb_ip_get(u32 *ip)
{
    struct in_addr ipaddr;
    if (DhcpIp_InfoLoad(ip)) {
        ipaddr.s_addr = *ip;
        char *ip_str = inet_ntoa(ipaddr);
        if (ip_str && strcmp(ip_str,"0.0.0.0")!=0 && strcmp(ip_str,"255.255.255.255")!=0) {
            printf("info: dhcp ip address load from flash: %s !\r\n", ip_str);
            return 1;
        }
    }
    printf("info: dhcp ip address loaded from flash failed !\r\n");
    return 0;
}

int cb_ip_set(u32 ip)
{
    struct in_addr ipaddr;
    ipaddr.s_addr = ip;
    char *ip_str = inet_ntoa(ipaddr);
    u32 ip_temp;
    if (ip_str) {
        memset(gip_addr, 0, sizeof(gip_addr));
        if (ip_str && strcmp(ip_str,"0.0.0.0")!=0 && strcmp(ip_str,"255.255.255.255")!=0) {
            if (strlen(ip_str)<sizeof(gip_addr)) {
                strcpy(gip_addr, ip_str);
            }
            printf("info: Get IP Address: %s !\r\n", gip_addr);
        }
    }
    if (DhcpIp_InfoLoad(&ip_temp)) {
        if (ip_temp == ip) {
            printf("info: the same IP address!\r\n");
            return 0;
        }
    }
    printf("info: write flash the new IP address!\r\n");
    DhcpIp_InfoSave((char*)&ip,(u32) sizeof(ip));
    return 1;
}

int SetWiFiStrength(int level)
{
    static int wifi_level = 0;
    if (wifi_level != level) {
        wifi_level = level;
//        printf ("info: wifi signal level %d!\r\n", level);
    }
    return 0;
}


void DelayCloseWiFi(int delay_ms)
{
    if (    is_wifi_connected() &&
            gWiFiRefs.refs_wifi_on_off <= 0 &&
            gWiFiRefs.refs_timemark != 0 &&
            DjyGetSysTime()/1000 - gWiFiRefs.refs_timemark > delay_ms)
    {
        printf("info: WiFi PowerDown!\r\n");
        DjyWifi_StaDisConnect();
    }
}

void WiFiPowerUp()
{
    int flag = 0;
    Int_SaveAsynSignal();
    gWiFiRefs.refs_wifi_on_off++;
    gWiFiRefs.refs_timemark = DjyGetSysTime()/1000;
    if (gWiFiRefs.refs_wifi_on_off == 1)
        flag = 1;
    Int_RestoreAsynSignal();

    printf("\r\n<<<<<<<<<%d>>>>>>>>>>\r\n", gWiFiRefs.refs_wifi_on_off);
    if (flag) {
        printf("info: WiFi PowerUp, ssid=%s, pwd=%s!\r\n", LoadWifi.WifiSsid, LoadWifi.WifiPassWd);
        DjyWifi_StaAdvancedConnect(LoadWifi.WifiSsid, LoadWifi.WifiPassWd);
    }
}
void DjyWifi_StaDisConnect(void);
void WiFiPowerDown()
{
    Int_SaveAsynSignal();
    gWiFiRefs.refs_wifi_on_off--;
    gWiFiRefs.refs_timemark = DjyGetSysTime()/1000;
    Int_RestoreAsynSignal();

    printf("\r\n<<<<<<<<<%d>>>>>>>>>>\r\n", gWiFiRefs.refs_wifi_on_off);

//    ��ʱ����ر�WiFi
//    if (gWiFiRefs.refs_wifi_on_off==0) {
//        printf("info: WiFi PowerDown!\r\n");
//        DjyWifi_StaDisConnect();
//    }
}
int WiFiOpen(unsigned int timeout_ms)
{
    return 1;
    int ret = 1;
    WiFiPowerUp();
    unsigned int timemark = DjyGetSysTime()/1000;
    while (!is_wifi_connected()) {
        Djy_EventDelay(100*1000);
        if (DjyGetSysTime()/1000 - timemark > timeout_ms) {
            ret = -1;
            break;
        }
    }
    return ret;
}

int WiFiClose(unsigned int timeout_ms)
{
    return 1;
    int ret = 1;

    WiFiPowerDown();
    unsigned int timemark = DjyGetSysTime()/1000;
    while (is_wifi_connected()) {
        Djy_EventDelay(100*1000);
        if (DjyGetSysTime()/1000 - timemark > timeout_ms) {
            ret = -1;
            break;
        }
    }
    return ret;
}
