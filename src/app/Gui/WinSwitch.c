#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "inc/GuiInfo.h"
#include "inc/WinSwitch.h"
#include "align.h"

static struct GuiWinCb winCtrl[WIN_Max];
/*static*/ enum WinType eng_CurrentWindow = WIN_Main_WIN;
/*static*/ enum WinType eng_CurrentWindowbak = WIN_Main_WIN;


enum WinType Get_SelectionWinType()
{
    return eng_CurrentWindow;
}

extern struct WinTimer *tg_pPowerDownTimer;


//绘制图标
void Draw_Icon(HDC hdc,s32 x,s32 y,const RECT *rc,const char *bmp)
{
    struct RectBitmap bm;
    struct RopGroup RopCode = (struct RopGroup){ 0, 0, 0, CN_R2_COPYPEN, 0, 1, 0  };

    bm.reversal = true;
    bm.PixelFormat =CN_SYS_PF_RGB565;
    bm.width  =rc->right - rc->left;
    bm.height =rc->bottom- rc->top;
    bm.linebytes =align_up(4,bm.width*2);
    bm.ExColor =(ptu32_t)0;
    bm.bm_bits =(u8*)bmp + 70;

//    printf("width = %d,height = %d,linebytes = %d\n\r",bm.width,bm.height,bm.linebytes);
    DrawBitmap(hdc,x,y,&bm,RGB(255,223,115),RopCode);
}

//绘制图标
void Draw_Icon_2(HDC hdc,s32 x,s32 y,s32 width,s32 height,const char *bmp)
{
    struct RectBitmap bm;
    struct RopGroup RopCode = (struct RopGroup){ 0, 0, 0, CN_R2_COPYPEN, 0, 1, 0  };

    bm.reversal = true;
    bm.PixelFormat =CN_SYS_PF_RGB565;
    bm.width  =width;
    bm.height =height;
    bm.linebytes =align_up(4,bm.width*2);
    bm.ExColor =(ptu32_t)0;
    bm.bm_bits =(u8*)bmp + 70;

//    printf("width = %d,height = %d,linebytes = %d\n\r",bm.width,bm.height,bm.linebytes);
    DrawBitmap(hdc,x,y,&bm,RGB(197,223,189),RopCode);
}

void Draw_Circle_Button(HDC hdc,RECT *rc,s32 r,u32 color)
{
    RECT rc_rect;

    rc_rect = *rc;
    SetFillColor(hdc,color);

    rc_rect.left += r;
    rc_rect.right =rc_rect.right-r-2;
    FillRect(hdc,&rc_rect);

    FillCircle(hdc, rc->left+r, rc->top+r,r);
    FillCircle(hdc, rc->right-r-2,rc->top+r,r);
}

//主窗口消息分发
bool_t HmiNotify_EasyTalk(struct WindowMsg *pMsg)
{
    enum WinType NextWin;
    static u8 left_flag=0;
    static s64 timebak;

    GDD_ResetTimer(tg_pPowerDownTimer,600000);

    if((eng_CurrentWindow >= WIN_Max)||(winCtrl[eng_CurrentWindow].DoMsg==NULL))
    {
        eng_CurrentWindow = WIN_Main_WIN;
        return false;
    }
//=============================屏蔽误触发=======================================
    u16 event = HI16(pMsg->Param1);
    if(event==MSG_BTN_PEN_MOVE)
    {
        left_flag = 1;
    }

    if(left_flag && event==MSG_BTN_UP)
    {
        timebak =  DjyGetSysTime();
        left_flag = 0;
        return true;
    }
    //move 消息之后短时间不响应按下弹起消息
    if(((DjyGetSysTime() - timebak) < 300*mS)&&((event==MSG_BTN_UP)||(event==MSG_BTN_DOWN)))
    {
        return true;
    }
//==============================================================================
    NextWin = winCtrl[eng_CurrentWindow].DoMsg(pMsg);
    if(NextWin != WIN_NotChange)
    {
        eng_CurrentWindowbak  = NextWin;
        DestroyAllChild(pMsg->hwnd);      //删除当前控件。
        PostMessage(pMsg->hwnd, MSG_REFRESH_UI, NextWin, 0);
    }
    return true;
}

//主窗口界面更新
bool_t HmiRefresh(struct WindowMsg *pMsg)
{
    HWND hwnd;
    u32 recreat;
    const char * bmp ;

    hwnd =pMsg->hwnd;
    HDC hdc =BeginPaint(hwnd);
    recreat = pMsg->Param2;

    eng_CurrentWindow = (enum WinType)pMsg->Param1;

    if(!((recreat == CN_RECREAT_WIDGET)&&(winCtrl[eng_CurrentWindow].HmiCreate != NULL)))//容错
    {
        eng_CurrentWindow = WIN_Main_WIN;
    }

    if(Get_SelectionWinType() != WIN_Tip )
    {
        bmp = Get_BmpBuf(BMP_Background_bmp);
        if(bmp != NULL)
        {
            DrawBMP(hdc,0,0,bmp);
        }
    }



    if((recreat == CN_RECREAT_WIDGET)&&(winCtrl[eng_CurrentWindow].HmiCreate != NULL))
        winCtrl[eng_CurrentWindow].HmiCreate(pMsg);//创建本界面各控件

    if(winCtrl[eng_CurrentWindow].HmiPaint != NULL)
        winCtrl[eng_CurrentWindow].HmiPaint(pMsg); //绘制各控件

    EndPaint(hwnd,hdc);
//  UpdateDisplay(hwnd);
    return true;
}

//界面刷新
bool_t Refresh_SwitchWIn(HWND hwnd)
{
    DestroyAllChild(hwnd);      //删除当前控件。
    PostMessage(hwnd, MSG_REFRESH_UI, eng_CurrentWindow, 0);
    return true;

}

//界面跳转
bool_t  Jump_Interface(HWND hwnd,enum WinType nextInterface)
{
    DestroyAllChild(hwnd);      //删除当前控件。
    PostMessage(hwnd, MSG_REFRESH_UI, nextInterface, 0);
    return true;
}


//注册所有的界面
int Init_WinSwitch()
{
    Register_Main_WIN();             //翻译主页面
    Register_SpeedTranslation();     //录音速译
    Register_DialogueTranslation();  //对话翻译
    Register_LangSel();              //语种选择
    Register_LangSel2();             //语种选择2
    Register_Wifi_Connecting();      //WIFI正在连接界面
    Register_Wifi_ConnectSucess();   //WIFI连接成功界面
    Register_Wifi_ConnectError();    //WIFI连接失败界面
    Register_Tip();                  //录音提示
    Register_Settings();             //设置
    Register_Settings_WifiConnect(); //wifi设置
    Register_FriendlyReminders();    //配网提示
    Register_Settings_TurnOff();     //关机页面
    return 0;
}


//注册新的界面
int Register_NewWin(enum WinType id,T_HmiCreate HmiCreate,T_HmiPaint HmiPaint, T_DoMsg DoMsg)
{
    winCtrl[id].HmiCreate = HmiCreate;
    winCtrl[id].HmiPaint = HmiPaint;
    winCtrl[id].DoMsg = DoMsg;
    return 0;
}

int print_Wininfo()
{
    int flag = 0;
    printf("eng_CurrentWindow %d  eng_CurrentWindowbak %d \n\r",eng_CurrentWindow,eng_CurrentWindowbak);
    for(int i=0;i<WIN_Max;i++)
    {
        flag = 0;
        if((winCtrl[i].HmiCreate != NULL))
            flag |= 1;
        if(winCtrl[i].HmiPaint != NULL)
            flag |=2;
        if(winCtrl[i].DoMsg != NULL)
            flag |=2;

        printf("%d : flag: %d   \n\r",i,flag);
    }
    return 0;

}
