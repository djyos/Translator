/*
该文件格式务必保存为utf8格式否则显示会乱码
*/
#include <stddef.h>
#include "inc/utf8_text.h"

const char * Get_Utf8Text(enum Utf8_Text enumtext)
{
    switch (enumtext)
    {
    case utf8_text_TeacherComments:
        return "教师评语：";
    case utf8_text_HomeWorkType:
        return "作业类型：";
        default:
            break;
    }
    return NULL;
}


/*==============================================================================
 * 功能：将一行UTF8字符串添加换行符输出
 * 参数：instr输入字符串
 *       outstr 字符串输出缓冲区
 *       outlen 输出字符串最大长度
 *       bytenum 每行字节数
 * 返回：格式化后的行数
 *=========================================================================== */

int Utf8_Typesetting(const char *instr, char *outstr, int outlen, int bytenum)
{
    int cnt, num = 0;
    int datacnt = 1;
    int ret = 1;

    while (*instr != '\0')
    {
        if (0 == (*instr & (char)(0x80 >> 0)))
        {
            cnt = 1;
        }
        else if (0 == (*instr & (char)(0x80 >> 1)))
        {
            return -1;
        }
        else if (0 == (*instr & (char)(0x80 >> 2)))
        {
            cnt = 2;
        }
        else if (0 == (*instr & (char)(0x80 >> 3)))
        {
            cnt = 3;
        }
        else if (0 == (*instr & (char)(0x80 >> 4)))
        {
            cnt = 4;
        }
        else if (0 == (*instr & (char)(0x80 >> 5)))
        {
            cnt = 5;
        }
        else if (0 == (*instr & (char)(0x80 >> 6)))
        {
            cnt = 6;
        }
        datacnt += cnt;
        if (datacnt > outlen)
            return -2;

        for (int i = 0; i < cnt; i++)
        {
            *outstr++ = *instr++;
        }

        num++;
        if (num == bytenum)
        {
            datacnt += 2;
            if (datacnt > outlen)
                return -2;
            *outstr++ = '\n';
            *outstr++ = '\r';
            num = 0;
            ret ++;
        }
    }
    *outstr++ = '\0';

    return ret;
}

int Utf8_Typesetting_2(const char *instr, char *outstr, int outlen, int bytenum)
{
    int cnt, num = 0;
    int datacnt = 1;
    int ret = 1;

    while (*instr != '\0')
    {
        if (0 == (*instr & (char)(0x80 >> 0)))
        {
            cnt = 1;
        }
        else if (0 == (*instr & (char)(0x80 >> 1)))
        {
            return -1;
        }
        else if (0 == (*instr & (char)(0x80 >> 2)))
        {
            cnt = 2;
        }
        else if (0 == (*instr & (char)(0x80 >> 3)))
        {
            cnt = 3;
        }
        else if (0 == (*instr & (char)(0x80 >> 4)))
        {
            cnt = 4;
        }
        else if (0 == (*instr & (char)(0x80 >> 5)))
        {
            cnt = 5;
        }
        else if (0 == (*instr & (char)(0x80 >> 6)))
        {
            cnt = 6;
        }
        datacnt += cnt;
        if (datacnt > outlen)
            return -2;

        for (int i = 0; i < cnt; i++)
        {
            *outstr++ = *instr++;
        }

        num++;
        if (num == bytenum)
        {
            datacnt += 2;
            if (datacnt > outlen)
                return -2;
            *outstr++ = '.';
            *outstr++ = '.';
            *outstr++ = '.';
            num = 0;
            ret ++;
            break;
        }
    }
    *outstr++ = '\0';

    return ret;
}
