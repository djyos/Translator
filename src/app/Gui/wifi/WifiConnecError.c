//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * MainWin.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *      过往家庭作业界面
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"


enum WifiConnectErrorId{

    ENUM_BACKGROUND, //背景
    ENUM_WIFI,
    ENUM_TIME,
    ENUM_CLOSE,
    ENUM_WORD,
    ENUM_CONFIGUREWIFI,

    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO WifiWinConnectError[ENUM_MAXNUM] =
{
    [ENUM_BACKGROUND] = {
        .position = {0,0,240,320},
        .name = "MainWin",
        .type = type_background,
        .userParam = BMP_Background_bmp,
    },

};

//按钮控件创建函数
static bool_t BmpButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    const  struct GUIINFO *buttoninfo = GetWindowPrivateData(hwnd);
    hdc =BeginPaint(hwnd);
    GetClientRect(hwnd,&rc);


    bmp = Get_BmpBuf(BMP_Close_bmp);
    if(bmp != NULL)
    {
        Draw_Icon_2(hdc,93,100,50,50,bmp);
    }

    SetTextColor(hdc,RGB(100,100,100));
    rc.top+=200;
    DrawText(hdc,"网络连接失败",-1,&rc,DT_CENTER|DT_TOP);


    EndPaint(hwnd,hdc);
    return true;
}


/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/

static bool_t HmiCreate_WifieWin(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;

    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
             {MSG_PAINT,BmpButtonPaint},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (WifiWinConnectError[i].type)
        {
            case  type_background :
            {

                HWND tmpHwnd =  CreateButton(WifiWinConnectError[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                        WifiWinConnectError[i].position.left, WifiWinConnectError[i].position.top,\
                                 RectW(&WifiWinConnectError[i].position),RectH(&WifiWinConnectError[i].position), //按钮位置和大小
                                 hwnd,i,(void*)&WifiWinConnectError[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                if(tmpHwnd == NULL)
                {
                    printf("CreateButton Mainhwnd error \n\r");
                }
            }
            break;

            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_WE(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;

    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);

    //主界面背景
    bmp = Get_BmpBuf(WifiWinConnectError[ENUM_BACKGROUND].userParam);
    if(bmp != NULL)
    {
        DrawBMP(hdc,WifiWinConnectError[ENUM_BACKGROUND].position.left,\
                WifiWinConnectError[ENUM_BACKGROUND].position.top,bmp);
    }

//    //Wifi 状态
//    bmp = Get_BmpBuf(WifiWinConnectError[ENUM_WIFI].userParam);
//    if(bmp != NULL)
//    {
//        Draw_Icon(hdc,WifiWinConnectError[ENUM_WIFI].position.left,\
//                WifiWinConnectError[ENUM_WIFI].position.top,&WifiWinConnectError[ENUM_WIFI].position,bmp);
//    }

    //    //时间
    //    char* time =  Win_GetTime();
    //    DrawText(hdc,time,-1,&MusicMain[ENUM_TIME].position,DT_CENTER|DT_VCENTER);



    EndPaint(hwnd,hdc);
    return true;

}


static enum WinType HmiNotify_WifiError(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;

    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);


    return NextUi;
}


int Register_Wifi_ConnectError()
{
    return Register_NewWin(WIN_Wifi_ConnectError,HmiCreate_WifieWin,HmiPaint_WE,HmiNotify_WifiError);
}
