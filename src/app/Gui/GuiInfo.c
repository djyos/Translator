//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * BmpInfo.c
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 *
 *  功能:对要显示的Bmp文件集中管理，提供获取获取相应Bmp文件的接口
 *
 */
#define  GUI_INFO_FILE_C_
#include "inc/GuiInfo.h"
#include "inc/WinSwitch.h"
#include <gdd_widget.h>
#include <stddef.h>
#include "stdio.h"
#include "cJSON.h"
#include "string.h"
#include "stdlib.h"
#include "device_user.h"
#include "board.h"
#include <app_flash.h>
#include "recorder.h"
#include <dbug.h>
#include <time.h>
#include "ghttp.h"
//============================Bmp 文件数组======================================

static const char MainInterface_bmp[] = {//开机界面图
#include "Picture/start_page_bmp"
};
static const char Background_bmp[] = {//背景Bmp格式文件
#include "Picture/Background_bmp"
};
static const char Background1_bmp[] = {//背景Bmp1格式文件
#include "Picture/Background1_bmp"
};
static const char CheckFileLogo_bmp[] = {
#include "Picture/CheckFileLogo_bmp"
};
static const char Close_bmp[] = {
#include "Picture/Close_bmp"
};

static const char WifiConnected_bmp[] = {
#include "Picture/WifiConnected_bmp"
};
static const char WifiDisconnected_bmp[] = {
#include "Picture/WifiDisconnected_bmp"
};
static const char Temperature_bmp[] = {
#include "Picture/Temperature_bmp"
};
static const char Weather_bmp[] = {
#include "Picture/Weather_bmp"
};
static const char Aql_bmp[] = {
#include "Picture/Aql_bmp"
};
static const char Dark_bmp[] = {
#include "Picture/Dark_bmp"
};
static const char LightUp_bmp[] = {
#include "Picture/LightUp_bmp"
};
static const char Dot_bmp[] = {
#include "Picture/Dot_bmp"
};
static const char time_0_bmp[] = {
#include "Picture/time_0_bmp"
};
static const char time_1_bmp[] = {
#include "Picture/time_1_bmp"
};
static const char time_2_bmp[] = {
#include "Picture/time_2_bmp"
};
static const char time_3_bmp[] = {
#include "Picture/time_3_bmp"
};
static const char time_4_bmp[] = {
#include "Picture/time_4_bmp"
};
static const char time_5_bmp[] = {
#include "Picture/time_5_bmp"
};
static const char time_6_bmp[] = {
#include "Picture/time_6_bmp"
};
static const char time_7_bmp[] = {
#include "Picture/time_7_bmp"
};
static const char time_8_bmp[] = {
#include "Picture/time_8_bmp"
};
static const char time_9_bmp[] = {
#include "Picture/time_9_bmp"
};
static const char Wind_bmp[] = {
#include "Picture/Wind_bmp"
};
static const char Doing_bmp[] = {
#include "Picture/Doing_bmp"
};
static const char SystemSet_bmp[] = {
#include "Picture/SystemSet_bmp"
};
static const char FileSet_bmp[] = {
#include "Picture/FileSet_bmp"
};
static const char CalendarSet_bmp[] = {
#include "Picture/CalendarSet_bmp"
};
static const char VideoSet_bmp[] = {
#include "Picture/VideoSet_bmp"
};
static const char ClockSet_bmp[] = {
#include "Picture/ClockSet_bmp"
};
static const char PhotoSet_bmp[] = {
#include "Picture/PhotoSet_bmp"
};
static const char MusicMain_bmp[] = {
#include "Picture/MusicMain_bmp"
};
static const char Recorder_bmp[] = {
#include "Picture/Recorder_bmp"
};
static const char TranslationSetting_bmp[] = {
#include "Picture/TranslationSetting_bmp"
};
static const char SpeedTranslation_bmp[] = {
#include "Picture/SpeedTranslation_bmp"
};
static const char DialogueTranslation_bmp[] = {
#include "Picture/DialogueTranslation_bmp"
};
static const char Exchange_bmp[] = {
#include "Picture/Exchange_bmp"
};
static const char rerurn_bmp[] = {
#include "Picture/return_bmp"
};
static const char TranslationChange_bmp[] = {
#include "Picture/TranslationChange_bmp"
};
static const char TranslationPlay_bmp[] = {
#include "Picture/TranslationPlay_bmp"
};
static const char TranslationPause_bmp[] = {
#include "Picture/TranslationPause_bmp"
};
static const char MicroPhone_bmp[] = {
#include "Picture/MicroPhone_bmp"
};
static const char CN_bmp[] = {
#include "Picture/CN_bmp"
};
static const char arEG_bmp[] = {
#include "Picture/ar-EG_bmp"
};
static const char DE_bmp[] = {
#include "Picture/DE_bmp"
};
static const char ES_bmp[] = {
#include "Picture/ES_bmp"
};
static const char JP_bmp[] = {
#include "Picture/JP_bmp"
};
static const char KR_bmp[] = {
#include "Picture/KR_bmp"
};
static const char RU_bmp[] = {
#include "Picture/RU_bmp"
};
static const char UK_bmp[] = {
#include "Picture/UK_bmp"
};
static const char US_bmp[] = {
#include "Picture/US_bmp"
};
static const char Volume_bmp[] = {
#include "Picture/Volume_bmp"
};
static const char WifiSetLogo_bmp[] = {
#include "Picture/WifiSetLogo_bmp"
};
static const char UpgradeSetLogo_bmp[] = {
#include "Picture/UpgradeSetLogo_bmp"
};
static const char Device_BindingLogo_bmp[] = {
#include "Picture/Device_BindingLogo_bmp"
};
static const char TermsOfService_bmp[] = {
#include "Picture/TermsOfService_bmp"
};
static const char AboutDevices_bmp[] = {
#include "Picture/AboutDevices_bmp"
};
static const char Round_bmp[] = {
#include "Picture/Round_bmp"
};
static const char RoundDot_bmp[] = {
#include "Picture/RoundDot_bmp"
};
static const char Background_bmp_kouyuji[] = {
#include "Picture/Background_bmp_kouyuji"
};

void DispMainInterface(unsigned char *pic)
{
    struct RopGroup RopCode = (struct RopGroup){ 0, 0, 0, CN_R2_COPYPEN, 0, 0, 0  };
       struct GkWinObj *desktop;
       struct RectBitmap   bitmap;
       desktop = GK_GetDesktop(CFG_DISPLAY_NAME);
       bitmap.bm_bits = pic+70;
       bitmap.linebytes = CFG_LCD_XSIZE*2;
       bitmap.PixelFormat = CN_SYS_PF_RGB565;
       bitmap.ExColor = 0;
       bitmap.height=CFG_LCD_YSIZE;
       bitmap.width=CFG_LCD_XSIZE;
       bitmap.reversal = true;
       GK_DrawBitMap(desktop,&bitmap,0,0,0,RopCode,100000);
       GK_SyncShow(100000);
}

//#define PICTURES_TOTAL_NUMBER   18         //所有要显示的图片总数
//#define UNCOMPRESS_PICTURES_BUF_SIZE   1*1024*1024     //除开机界面外，解压后图片存在PSRAM中需要的缓冲区大小,根据图片大小自行调节
////#define MAININTERFACE_UNCOMPRESS_BUF_SIZE   200*1024//开机界面，解压后图片存在PSRAM中需要的缓冲区大小,根据图片大小自行调节
//u32 PictureDateAddr[PICTURES_TOTAL_NUMBER]; //存所有解压后图片在PSRAM中的地址

int GetStatusHandle = 0;                                    // 更新网络时间
static bool_t WifiConnectedflag = false;                    // wifi链接标志
static char Powertxtbuf[8] = {'2','5','%',0,0,0,0,0};       // 电量
char recognize_date[32] = { 0 };                            // 当前时间戳，RFC1123格式

struct WinTime GuiInfoTime = {
        .year = 2019,
        .mon = 10,
        .mday = 10,
        .weeks = 4,
        .hour = 12,
        .minute = 20,
        .sec = 30,
};

int GetStatus(int status)
{
    if(status == 1)
    {
        GetStatusHandle = 1;
        return true;
    }
    else if(status == -1)
    {
        GetStatusHandle = 0;
        return false;
    }
}

static char Timetxtbuf[16] = {'1','2',':','1','0',0,0,0,0,0,0,0,0,0,0,0};//时间
PROGRESSBAR_DATA PowerProgressbar = //电量进度条
{
    .Flag    =PBF_SHOWTEXT|PBF_ORG_LEFT,
    .Range   =100,
    .Pos     = 25,
    .FGColor = CN_COLOR_GREEN,
    .BGColor = CN_COLOR_WHITE,
    .TextColor =CN_COLOR_WHITE,
    .DrawTextFlag =DT_VCENTER|DT_CENTER,
    .text = Powertxtbuf,
};
HWND Powerhwnd;

//extern void deep_sleep(void);
extern  WIFI_CFG_T  LoadWifi;
unsigned int gRtcTickSecOffset = 0;
unsigned int gRtcTickSecTimestamp = 0;
char Datebuf[64] = {0};//日期
extern char *lang_pic;
extern char *lang_pic2;
/*****************************************************************************
 *  功能：更新wifi状态
 *  参数：flag ：false wifi未连接  true wifi已连接
 *  返回：
 *****************************************************************************/
bool_t Wifi_Connectedflag(u8 flag)
{
    WifiConnectedflag = flag;
    Refresh_GuiWin();
    return true;
}
bool_t Get_Wifi_Connectedflag()
{
    return WifiConnectedflag;
}


const char * Get_BmpBuf(enum Bmptype type)
{
    switch (type)
    {
        case BMP_Close_bmp   :
          return Close_bmp;
       case BMP_MainInterface_bmp   :
          return MainInterface_bmp;
       case BMP_CheckFileLogo_bmp   :
          return CheckFileLogo_bmp;
        case BMP_Background_bmp   :
            return Background_bmp;
        case BMP_Background1_bmp   :
            return Background1_bmp;
        case BMP_WIFI             :
            if(WifiConnectedflag)
            {
                return WifiConnected_bmp;
            }
            else
            {
                return WifiDisconnected_bmp;
            }
        case BMP_Temperature_bmp   :
            return Temperature_bmp;
        case BMP_Dot_bmp   :
            return Dot_bmp;
         case BMP_time_0_bmp   :
            return time_0_bmp;
         case BMP_time_1_bmp   :
            return time_1_bmp;
         case BMP_time_2_bmp   :
            return time_2_bmp;
         case BMP_time_3_bmp   :
            return time_3_bmp;
         case BMP_time_4_bmp   :
            return time_4_bmp;
         case BMP_time_5_bmp   :
            return time_5_bmp;
         case BMP_time_6_bmp   :
            return time_6_bmp;
         case BMP_time_7_bmp   :
            return time_7_bmp;
         case BMP_time_8_bmp   :
            return time_8_bmp;
         case BMP_time_9_bmp   :
            return time_9_bmp;
        case BMP_Weather_bmp   :
            return Weather_bmp;
        case BMP_Aql_bmp   :
            return Aql_bmp ;
        case BMP_Wind_bmp   :
            return Wind_bmp;
        case BMP_Dark_bmp   :
            return Dark_bmp;
        case BMP_LightUp_bmp   :
            return LightUp_bmp;
        case BMP_Doing_bmp   :
            return Doing_bmp;
        case BMP_SystemSet_bmp   :
            return SystemSet_bmp;
        case BMP_FileSet_bmp   :
            return FileSet_bmp;
        case BMP_CalenderSet_bmp :
             return CalendarSet_bmp;
        case BMP_VideoSet_bmp:
            return VideoSet_bmp;
        case BMP_CLockSet_bmp  :
            return ClockSet_bmp;
        case BMP_PhotoSet_bmp  :
            return PhotoSet_bmp;
        case BMP_Recorder_bmp   :
            return Recorder_bmp;
        case BMP_Translation_setting_bmp :
            return TranslationSetting_bmp;
        case BMP_SpeedTranslation_bmp :
            return SpeedTranslation_bmp;
        case BMP_DialogueTranslation_bmp :
            return DialogueTranslation_bmp;
        case BMP_Exchange_bmp :
            return Exchange_bmp;
        case BMP_return_bmp :
            return rerurn_bmp;
        case BMP_TranslationChange_bmp :
            return TranslationChange_bmp;
        case BMP_TranslationPlay_bmp :
            return TranslationPlay_bmp;
        case BMP_TranslationPause_bmp :
            return TranslationPause_bmp;
        case BMP_MicroPhone_bmp :
            return MicroPhone_bmp;
        case BMP_WifiSetLogo_bmp    :
            return WifiSetLogo_bmp;
        case BMP_UpgradeSetLogo_bmp  :
            return UpgradeSetLogo_bmp;
        case BMP_Device_BindingLogo_bmp :
            return Device_BindingLogo_bmp;
        case BMP_TermsOfService_bmp :
            return TermsOfService_bmp;
        case BMP_AboutDevices_bmp :
            return AboutDevices_bmp;
        case BMP_Background_bmp_kouyuji :
            return Background_bmp_kouyuji;
        case BMP_Round_bmp :
            return Round_bmp;
        case BMP_RoundDot_bmp :
            return RoundDot_bmp;
        case BMP_CN_bmp :
            return CN_bmp;
        case BMP_arEG_bmp  :
            return arEG_bmp;
        case BMP_DE_bmp  :
            return DE_bmp;
        case BMP_ES_bmp  :
            return ES_bmp;
        case BMP_JP_bmp  :
            return JP_bmp;
        case BMP_KR_bmp  :
            return KR_bmp;
        case BMP_RU_bmp  :
            return RU_bmp;
        case BMP_UK_bmp  :
            return UK_bmp;
        case BMP_US_bmp  :
            return US_bmp;
        case BMP_LANG_bmp :
            if(strcmp(lang_pic,"BMP_CN_bmp") == 0 )
            {
                return CN_bmp;
            }
            else if(strcmp(lang_pic,"BMP_US_bmp") == 0)
            {
                return US_bmp;
            }
            else if(strcmp(lang_pic,"BMP_UK_bmp") == 0)
            {
                return UK_bmp;
            }
            else if(strcmp(lang_pic,"BMP_JP_bmp") == 0)
            {
                return JP_bmp;
            }
            else if(strcmp(lang_pic,"BMP_DE_bmp") == 0)
            {
                return DE_bmp;
            }
            else if(strcmp(lang_pic,"BMP_arEG_bmp") == 0)
            {
                return arEG_bmp;
            }
            else if(strcmp(lang_pic,"BMP_ES_bmp") == 0)
            {
                return ES_bmp;
            }
            else if(strcmp(lang_pic,"BMP_RU_bmp") == 0)
            {
                return RU_bmp;
            }
            else if(strcmp(lang_pic,"BMP_KR_bmp") == 0)
            {
                return KR_bmp;
            }
        case BMP_LANG2_bmp :
            if(strcmp(lang_pic2,"BMP_CN_bmp") == 0 )
            {
               return CN_bmp;
            }
            else if(strcmp(lang_pic2,"BMP_US_bmp") == 0)
            {
               return US_bmp;
            }
            else if(strcmp(lang_pic2,"BMP_UK_bmp") == 0)
            {
               return UK_bmp;
            }
            else if(strcmp(lang_pic2,"BMP_JP_bmp") == 0)
            {
               return JP_bmp;
            }
            else if(strcmp(lang_pic2,"BMP_DE_bmp") == 0)
            {
               return DE_bmp;
            }
            else if(strcmp(lang_pic2,"BMP_arEG_bmp") == 0)
            {
               return arEG_bmp;
            }
            else if(strcmp(lang_pic2,"BMP_ES_bmp") == 0)
            {
               return ES_bmp;
            }
            else if(strcmp(lang_pic2,"BMP_RU_bmp") == 0)
            {
               return RU_bmp;
            }
            else if(strcmp(lang_pic2,"BMP_KR_bmp") == 0)
            {
               return KR_bmp;
            }
        case BMP_Volume_bmp :
           return Volume_bmp;
        default:      break;
    }
    return NULL;
}

static u8 is_leap_year(int year)
{
    if(
            (((year%4) == 0) && ((year%100)!=0))
        ||  ((year%400)==0)
      )
    {
        return 1;
    }

    return 0;
}

unsigned int calc_sec1970(int Y, u8 M, u8 D, u8 h, u8 m, u8 s)
{
    int i = 0;
    unsigned int sec = 0;
    int days[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    for(i = 1970; i < Y; i++)
    {
        if(is_leap_year(i))
            sec += 366 * 24 * 60 * 60;
        else
            sec += 365 * 24 * 60 * 60;
    }
    for(i = 1; i < M; i++)
    {
        if(i == 2 && is_leap_year(Y))
        {
            sec += 29 * 24 * 60 * 60;
        }else{
            sec += days[i] * 24 * 60 * 60;
        }
    }
    sec += (D-1) * 24 * 60 * 60;
    sec += h * 60 * 60 + m * 60 + s;
    sec = sec - (8 * 60 * 60);
    return sec;
}

void MainInterface(void)
{
   DispMainInterface((unsigned char *)Get_BmpBuf(BMP_MainInterface_bmp));
}

//获取窗口界面时间
struct WinTime Win_GetTime()
{
    return GuiInfoTime;
}
/**
 * 设置界面时间
 */
void Win_SetTime(void)
{
        GuiInfoTime.minute = GuiInfoTime.minute+1;
        if(60==GuiInfoTime.minute)
        {
            GuiInfoTime.hour = GuiInfoTime.hour+1;
            GuiInfoTime.minute = 0;
        }
        if(25==GuiInfoTime.hour)
        {
            GuiInfoTime.hour = 0;
            GuiInfoTime.minute = 0;
        }

        Refresh_GuiWin();
}

void SetClock(int hour, int min)
{
    GuiInfoTime.hour = hour;
    if(GuiInfoTime.minute!=min)
    {
        GuiInfoTime.minute = min;
        Refresh_GuiWin();
    }
}

/******************************************/

int getSuNingTime(void)
{
    char *uri = "http://quan.suning.com/getSysTime.do";
     ghttp_request *request = ghttp_request_new();
    if (NULL == request)
    {
        printf("ghttp_request_new return null \r\n");
        ghttp_close(request);
        ghttp_request_destroy(request);
        return -1;
    }
    ghttp_set_uri(request, uri);
    if(-1==ghttp_set_type(request, ghttp_type_get))
    {
        printf("ghttp_set_type is fail\r\n");
        ghttp_close(request);
        ghttp_request_destroy(request);
        return -1;
    }
    ghttp_prepare(request);
    ghttp_status status = ghttp_process(request);
    sprintf( recognize_date, "%s", ghttp_get_header( request, "Date" ) );    //获取当前GMT时间;
    GetStatus(status);
#if 1
    printf("[DEBUG ghttp status is %d]\r\n",status);
#endif
    if (status == ghttp_error)
    {
        printf("ghttp_process is fail:%s",  ghttp_get_error(request));
        ghttp_close(request);
        ghttp_request_destroy(request);
        return -1;
    }
    char *json_string = ghttp_get_body(request);

#if 0
    printf("[DEBUG time] %s\r\n",json_string);
#endif

    ghttp_close(request);
    ghttp_request_destroy(request);

    char str[5] = {0};

    memset(str, 0, strlen(str));
    memcpy(str, json_string+46,4);
    GuiInfoTime.year=atoi(str);


    memset(str, 0, strlen(str));
    memcpy(str, json_string+50,2);
    GuiInfoTime.mon=atoi(str);


    memset(str, 0, strlen(str));
    memcpy(str, json_string+52,2);
    GuiInfoTime.mday=atoi(str);


    memset(str, 0, strlen(str));
    memcpy(str, json_string+54,2);
    GuiInfoTime.hour=atoi(str);


    memset(str, 0, strlen(str));
    memcpy(str, json_string+56,2);
    GuiInfoTime.minute=atoi(str);


    memset(str, 0, strlen(str));
    memcpy(str, json_string+58,2);
    GuiInfoTime.sec=atoi(str);

//    printf("year:%d\r\n",GuiInfoTime.year);
//    printf("mon:%d\r\n",GuiInfoTime.mon);
//    printf("mday:%d\r\n",GuiInfoTime.mday);
//    printf("hour:%d\r\n",GuiInfoTime.hour);
//    printf("minute:%d\r\n",GuiInfoTime.minute);
//    printf("sec:%d\r\n",GuiInfoTime.sec);


    gRtcTickSecOffset = DjyGetSysTime()/1000000;
    gRtcTickSecTimestamp=calc_sec1970(GuiInfoTime.year,GuiInfoTime.mon,GuiInfoTime.mday,GuiInfoTime.hour,GuiInfoTime.minute,GuiInfoTime.sec);

    return 1;
}

int GetTimeHourMinute(int *hour, int *min ,int *sec)
{
    unsigned int timestamp_out = 0;
    unsigned int cur_tick_sec  = 0;
    struct tm *ptm = 0;
    cur_tick_sec = DjyGetSysTime()/1000000;
    timestamp_out  = gRtcTickSecTimestamp + cur_tick_sec - gRtcTickSecOffset;
    time_t t = timestamp_out;
    ptm = oss_gmtime(&t, 8);
    *hour = ptm->tm_hour;
    *min = ptm->tm_min;
    *sec = ptm->tm_sec;
    //printf("---->hour=%d, min=%d!\r\n", *hour, *min);
    //strftime(GMT, sizeof(GMT), "%a, %d %b %Y %H:%M:%S GMT", oss_gmtime(&t, 0));
    //printf("GetTimeHourMinute: %d, TimeStamp: %d, RTC TIME: %d!\r\n", gRtcTickSecOffset, gRtcTickSecTimestamp, timestamp_out);
    return 0;
}

//获取窗口界面时间
char*  Win_GetDate()
{
    sprintf(Datebuf, "%d.%d.%d", GuiInfoTime.year,GuiInfoTime.mon,GuiInfoTime.mday);
    return Datebuf;
}

int findDate(char *s)
{
   cJSON* cjson = cJSON_Parse(s);
   char str[5] = {0};
   if (NULL == cjson)
   {
       return 0;
   }

   cJSON* dm = cJSON_GetObjectItem(cjson,"dm");
   if (dm==NULL)
   {
       cJSON_Delete(cjson);
       return 0;
   }

    cJSON* widget=cJSON_GetObjectItem(dm,"widget");
    if(widget==NULL)
    {
        cJSON_Delete(cjson);
        return 0;
    }

    cJSON* extra=cJSON_GetObjectItem(widget,"extra");
    if(extra==NULL)
    {
        cJSON_Delete(cjson);
        return 0;
    }

    cJSON* years =cJSON_GetObjectItem(extra,"year");
    cJSON* weekdays =cJSON_GetObjectItem(extra,"weekday");
    cJSON* months =cJSON_GetObjectItem(extra,"month");
    cJSON* days =cJSON_GetObjectItem(extra,"day");
    cJSON* times =cJSON_GetObjectItem(extra,"time");

    GuiInfoTime.year=atoi(years->valuestring);
    printf("year:%d\r\n", GuiInfoTime.year);

    strcpy(GuiInfoTime.weeks,weekdays->valuestring);
    printf("weekday:%s\r\n",GuiInfoTime.weeks);

    GuiInfoTime.mon=atoi(months->valuestring);
    printf("month:%d\r\n",GuiInfoTime.mon);

    GuiInfoTime.mday=atoi(days->valuestring);
    printf("day:%d\r\n",GuiInfoTime.mday);


    memset(str, 0, strlen(str));
    memcpy(str, times->valuestring, 2);
    GuiInfoTime.hour=atoi(str);
    printf("hour:%d\r\n",GuiInfoTime.hour);
    memset(str, 0, strlen(str));
    memcpy(str, times->valuestring+3, 2);
    GuiInfoTime.minute=atoi(str);
    printf("minute:%d\r\n",GuiInfoTime.minute);
    memset(str, 0, strlen(str));
    memcpy(str, times->valuestring+6, 2);
    GuiInfoTime.sec=atoi(str);
    printf("sec:%d\r\n",GuiInfoTime.sec);

    gRtcTickSecOffset = DjyGetSysTime()/1000000;
    gRtcTickSecTimestamp=calc_sec1970(GuiInfoTime.year,GuiInfoTime.mon,GuiInfoTime.mday,GuiInfoTime.hour,GuiInfoTime.minute,GuiInfoTime.sec);

    cJSON_Delete(cjson);
    return 1;
}






/**********************************************/

void deep_sleep(void)
{
    sdcard_power_off();
    CloseSpeaker();
    Djy_DelayUs(100000);
    __LP_BSP_EntrySleepL4( );
//    LP_DeepSleep();
}

//关机
int Set_ShoutDown()
{
    printf(">power down\r\n");
    if(usb_is_pluged() == 0)
        deep_sleep();
    return 0;
}

////*****************************************************************************
////  以下解压图片相关代码
////  增加或者减少图片时有修改以下地方
////  1、PICTURES_TOTAL_NUMBER、UNCOMPRESS_PICTURES_BUF_SIZE根据实际图片大小个数进行修改
////  2、压缩图片的文件数组、Get_BmpBuf和Get_CompressBuf根据需要增加和删除。
////  3、增加和删除某些图片时CompressPicturetype这里也需要相应的增加和删除
//
////==============================================================================
////功能：获取图片的压缩数据。
////参数：type：压缩图片的枚举名,len：压缩图片的长度
////返回：NULL：失败；其它：成功。
////说明：switch中的每个枚举对应一张图片，要增加和删除某些图片时CompressPicturetype这里也需要相应的增加和删除
////==============================================================================
//const lzo_bytep * Get_CompressBuf(enum CompressPicturetype type, lzo_uint *len)
//{
//    switch (type)
//    {
//        case Compress_MainInterface_bmp   :
//            *len = sizeof(MainInterface_bmp);
//            return (const lzo_bytep *)MainInterface_bmp;
//        case Compress_Background_bmp   :
//            *len = sizeof(Background_bmp);
//            return (const lzo_bytep *)Background_bmp;
//        case Compress_Lights_bmp   :
//            *len = sizeof(Lights_bmp);
//            return (const lzo_bytep *)Lights_bmp;
//        case Compress_SetUp_bmp   :
//            *len = sizeof(SetUp_bmp);
//            return (const lzo_bytep *)SetUp_bmp;
//        case Compress_Music_bmp   :
//            *len = sizeof(Music_bmp);
//            return (const lzo_bytep *)Music_bmp;
//        case Compress_Luminance_bmp   :
//            *len = sizeof(Luminance_bmp);
//            return (const lzo_bytep *)Luminance_bmp;
//        case Compress_Close_bmp   :
//            *len = sizeof(Close_bmp);
//            return (const lzo_bytep *)Close_bmp;
//        case Compress_Dark_bmp   :
//            *len = sizeof(Dark_bmp);
//            return (const lzo_bytep *)Dark_bmp;
//        case Compress_LightUp_bmp   :
//            *len = sizeof(LightUp_bmp);
//            return (const lzo_bytep *)LightUp_bmp;
//        case Compress_LastSong_bmp   :
//            *len = sizeof(LastSong_bmp);
//            return (const lzo_bytep *)LastSong_bmp;
//        case Compress_Play_bmp   :
//            *len = sizeof(Play_bmp);
//            return (const lzo_bytep *)Play_bmp;
//        case Compress_Pause_bmp   :
//            *len = sizeof(Pause_bmp);
//            return (const lzo_bytep *)Pause_bmp;
//        case Compress_NextSong_bmp   :
//            *len = sizeof(NextSong_bmp);
//            return (const lzo_bytep *)NextSong_bmp;
//        case Compress_Recording_bmp   :
//            *len = sizeof(Recording_bmp);
//            return (const lzo_bytep *)Recording_bmp;
//        case Compress_CheckFileLogo_bmp   :
//            *len = sizeof(CheckFileLogo_bmp);
//            return (const lzo_bytep *)CheckFileLogo_bmp;
//        case Compress_NetLoading_bmp   :
//            *len = sizeof(NetLoading_bmp);
//            return (const lzo_bytep *)NetLoading_bmp;
//        case Compress_WifiConnected_bmp   :
//            *len = sizeof(WifiDisconnected_bmp);
//            return (const lzo_bytep *)WifiDisconnected_bmp;
//        case Compress_WifiDisconnected_bmp   :
//            *len = sizeof(WifiDisconnected_bmp);
//            return (const lzo_bytep *)WifiDisconnected_bmp;
//        default:      break;
//    }
//    return NULL;
//}
//
//extern void *psram_malloc (unsigned int size);
//static lzo_bytep *PSRAM_DateAddr = NULL;
//lzo_uint MainInterface_DateLen = 0;
////==============================================================================
////功能：解压开机界面数据到PSRAM中。
////参数：
////返回：0：成功；-1：失败
////==============================================================================
//int Uncompress_MainInterface(void)
//{
//    const lzo_bytep *MainInterfaceDate;
//    lzo_uint compress_len = 0;
//    s32 ret;
//    if (PSRAM_DateAddr == 0)
//    {
//        PSRAM_DateAddr = (lzo_bytep *)psram_malloc (UNCOMPRESS_PICTURES_BUF_SIZE);
//        printf("info: get 1M uncompress picture buffer ok   addr = %x!\r\n",(u32)PSRAM_DateAddr);
//    }
//    if (PSRAM_DateAddr)
//    {
//        MainInterfaceDate = Get_CompressBuf(Compress_MainInterface_bmp, &compress_len);
//
//        if(MainInterfaceDate == NULL)
//        {
//            error_printf("uncompress"," No found MainInterface.\r\n");
//        }
//        else
//        {
//            ret = lzo1x_decompress((lzo_bytep)MainInterfaceDate, compress_len, (lzo_bytep)PSRAM_DateAddr, &MainInterface_DateLen, NULL);
//            if(ret != LZO_E_OK)
//            {
//                error_printf("uncompress"," uncompress fail MainInterface.\r\n");
//                return -1;
//            }
//            else
//            {
//                PictureDateAddr[Compress_MainInterface_bmp] = (u32)PSRAM_DateAddr;
//                return 0;
//            }
//        }
//    }
//    return -1;
//}
//
////==============================================================================
////功能：解压所有图片数据到PSRAM中。
////参数：compress_date：待解压的数据；compress_len：待解压的数据长度,umcompress_date：解压后的数据，
////      uncompress_len：解压后的数据长度
////返回：0：成功；-1：失败,-2：内存不够存所有图片的解压数据，-3：有图片解压失败。
////==============================================================================
//int UncompressPicture_to_PSRAM(void)
//{
//    static lzo_bytep *Uncompress_DateAddr = NULL;    //所有解压后图片在PSRAM中的起始地址
//    lzo_bytep *TempPSRAM_DateAddr;
//    const lzo_bytep *CompressDate;
//    enum CompressPicturetype i;
//    lzo_uint compress_len = 0,uncompress_len = 0;
//    s32 res = 0,ret,PSRAMDateLen = 0;
//    if (PSRAM_DateAddr == 0)
//    {
//        PSRAM_DateAddr = (lzo_bytep *)psram_malloc (UNCOMPRESS_PICTURES_BUF_SIZE);
//        printf("info: get 1M uncompress picture buffer ok   addr = %x!\r\n",(u32)PSRAM_DateAddr);
//    }
//    if (PSRAM_DateAddr)
//    {
////        Uncompress_DateAddr = PSRAM_DateAddr + MainInterface_DateLen;
//        Uncompress_DateAddr = (lzo_bytep *)((lzo_uint)PSRAM_DateAddr + MainInterface_DateLen);
//        TempPSRAM_DateAddr = Uncompress_DateAddr;
//        //所有压缩图片的枚举那里改了的话，这里也要做相应修改i从第一个到倒数第二个，最后一个是开机界面，开机界面先单独解压，这里不做解压了，
//        for(i = Compress_Background_bmp; i <= Compress_WifiDisconnected_bmp; i++)
//        {
////            printf("uncompress, uncompress Picture \"%d\".\r\n",i);
//            CompressDate = Get_CompressBuf(i, &compress_len);
//            if(CompressDate == NULL)
//            {
//                error_printf("uncompress"," No picture found \"%d\".\r\n",i);
//                res = -3;
//            }
//            else
//            {
//                ret = lzo1x_decompress((lzo_bytep)CompressDate, compress_len, (lzo_bytep)TempPSRAM_DateAddr, &uncompress_len, NULL);
//                if(ret != LZO_E_OK)
//                {
//                    error_printf("uncompress"," uncompress fail \"%d\".\r\n",i);
//                    res = -3;
//                }
//                else
//                {
//                    if(((u32)TempPSRAM_DateAddr - (u32)Uncompress_DateAddr) + uncompress_len > UNCOMPRESS_PICTURES_BUF_SIZE)
//                     {
//                         error_printf("uncompress"," Out of memory space %x.\r\n",TempPSRAM_DateAddr);
//                         return -2;
//                     }
////                    printf("TempPSRAM_DateAddr - PSRAM_DateAddr    = %d\r\n",((u32)TempPSRAM_DateAddr - (u32)PSRAM_DateAddr));
//                    PSRAMDateLen += uncompress_len;
//                    PictureDateAddr[i] = (u32)TempPSRAM_DateAddr;
////                    printf("TempPSRAM_DateAddr \"%x\" ， %ld .\r\n",(u32)TempPSRAM_DateAddr,uncompress_len);
//                    TempPSRAM_DateAddr = (lzo_bytep *)((lzo_uint)TempPSRAM_DateAddr + uncompress_len);
//                }
//            }
//
//        }
//        return res;
//    }
//    else
//    {
//        printf("error: get 1m uncompress picture buffer failed!\r\n");
//    }
//    return -1;
//}
