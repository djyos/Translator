/*
 * SpeedTranslation.c
 *
 *  Created on: 2020年3月17日
 *      Author: zxy
 */
/*
 * ----------------------------------------------------
 * Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list2 of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list2 of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * -----------------------------------------------------------------------------
 * Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
 *
 * 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
 * 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
 *
 * 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
 *    及下述的免责声明。
 * 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
 *    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
 *    的免责声明。
 */

/*
 * 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
 * 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
 * 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
 * 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
 * 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
 * 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
 * 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
 * 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
 * -----------------------------------------------------------------------------
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "music_player.h"
#include "chain_table.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"

enum InformationId {
    ENUM_BACKGROUND,        /* 背景 */
    ENUM_WIFI,
    ENUM_TIME,
    ENUM_LANG1,
    ENUM_LANG2,
    ENUM_LANG3,
    ENUM_LANG4,
    ENUM_MAXNUM,
};

extern char  from_language[16];                            /* 输出语言 */
extern char  to_language[16];                              /* 输入语言 */
extern char  LANGUAGE2[16];
extern unsigned char    word_storage_result_to[512];     /* 翻译结果 */
extern unsigned char    word_storage_result_from[512];   /* 录音结果*/

/* ==================================config====================================== */
static const struct GUIINFO LangSel2[ENUM_MAXNUM] =
{
    [ENUM_BACKGROUND] = {
        .position = {0,0,240,320},
        .name = "Background",
        .type = type_background,
        .userParam = BMP_Background_bmp,
    },
    [ENUM_WIFI] = {
        .position = {5,5,25+5,25+5},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
    },
    [ENUM_TIME] = {
        .position = {100,10,100+40,30+10},
        .name = "ACTIME",
        .type = widget_type_time,
        .userParam = Bmp_NULL,
    },
    [ENUM_LANG1] =  {
        .position = {0,42,240,111},
        .name      = "LANG1",
        .type      = widget_type_button,
        .userParam = 0,
    },
    [ENUM_LANG2] =  {
        .position  = {0,111,240,180},
        .name      = "LANG2",
        .type      = widget_type_button,
        .userParam = 1,
    },
    [ENUM_LANG3] = {
        .position  = {0,180,240,249},
        .name      = "LANG3",
        .type      = widget_type_button,
        .userParam = 2,
    },
    [ENUM_LANG4] = {
        .position  = {0,249,240,320},
        .name      = "LANG4",
        .type      = widget_type_button,
        .userParam = 3,
    },

};

struct Lang_list2
{
   u32 courseBase;
   u32 coursemax;
   HWND hwnd[4];

};

struct Lang_list2 list2= {0,8,{NULL,NULL,NULL,NULL},};
char  *lang_pic2;
static char  *lang_picture2[]={"BMP_CN_bmp","BMP_UK_bmp","BMP_JP_bmp","BMP_DE_bmp","BMP_arEG_bmp","BMP_ES_bmp","BMP_RU_bmp","BMP_KR_bmp"};
static char  *lang_name2[]={"中文","英语","日语","德语","阿拉伯语","西班牙语","俄语","韩语"};
//按钮控件创建函数
static bool_t BmpButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char *course;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    GetClientRect(hwnd,&rc);
    SetTextColor(hdc,RGB(100,100,100));
    const  struct GUIINFO *buttoninfo = GetWindowPrivateData(hwnd);

    if(hdc)
    {

        if(widget_type_button == buttoninfo->type)
        {
            course=lang_name2[list2.courseBase+buttoninfo->userParam];
            lang_pic2=lang_picture2[list2.courseBase+buttoninfo->userParam];
            SetFillColor(hdc,RGB(255,255,255));
            FillRect(hdc,&rc);
            if(list2.courseBase+buttoninfo->userParam < 8)
            {
                bmp = Get_BmpBuf(BMP_LANG2_bmp);
                if(bmp != NULL)
                {
                    Draw_Icon_2(hdc,5,0,80,60,bmp);
                }
//                char *txtbuf = malloc( 800 );
//                struct Charset  * myCharset = Charset_NlsSearchCharset( CN_NLS_CHARSET_UTF8 );
//                struct Charset  * Charsetbak    = SetCharset( hdc, myCharset );
//                Utf8_Typesetting( course, txtbuf, 800, 10 );
                rc.left+=90;
                rc.bottom-=5;
                DrawText(hdc,course,-1,&rc,DT_VCENTER|DT_CENTER);
//                SetCharset( hdc, Charsetbak );
//                free( txtbuf );
            }
        }
    }
    EndPaint(hwnd,hdc);
    return true;
}


/*---------------------------------------------------------------------------
*  功能：  窗口创建函数
*   输入   :tagWindowMsg *pMsg
*   输出 :false或true
*  ---------------------------------------------------------------------------*/

static bool_t HmiCreate_LangSel2( struct WindowMsg *pMsg )
{
    RECT    rc0;
    HWND    hwnd = pMsg->hwnd;
    HWND hwndButton;
    /* 消息处理函数表 */
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
        { MSG_PAINT, BmpButtonPaint },
    };

    static struct MsgTableLink s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum  = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *) &s_gBmpMsgTablebutton;

    GetClientRect( hwnd, &rc0 );

    for(int i = 0; i < ENUM_MAXNUM; i++)
    {
        switch (LangSel2[i].type)
        {
            case  widget_type_button :
            {
                hwndButton =  CreateButton(LangSel2[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                                LangSel2[i].position.left, LangSel2[i].position.top,\
                                RectW(&LangSel2[i].position),RectH(&LangSel2[i].position), //按钮位置和大小
                                hwnd,i,(ptu32_t)&LangSel2[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                if(hwndButton != NULL)
                {
                    list2.hwnd[LangSel2[i].userParam] = hwndButton;
                }
            }
                break;
            default:
                break;
        }
    }
    return(true);
}


/* 绘制消息处函数 */
static bool_t HmiPaint_LangSel2( struct WindowMsg *pMsg )
{
    HWND        hwnd;
    HDC     hdc;
    const char  * bmp;

    hwnd    = pMsg->hwnd;
    hdc = BeginPaint( hwnd );

//   主界面背景
//   SetFillColor(hdc,LangSel[ENUM_BACKGROUND].userParam);
//   FillRect(hdc,&LangSel[ENUM_BACKGROUND].position);

     //Wifi 状态
     bmp = Get_BmpBuf(LangSel2[ENUM_WIFI].userParam);
     if(bmp != NULL)
     {
         Draw_Icon(hdc,LangSel2[ENUM_WIFI].position.left,
                 LangSel2[ENUM_WIFI].position.top,&LangSel2[ENUM_WIFI].position,bmp);
     }




    //时间
    struct WinTime time =  Win_GetTime();
    char timeing[16]={0};
    SetTextColor(hdc,RGB(100,100,100));
    sprintf(timeing,"%02d:%02d",time.hour,time.minute);
    DrawText(hdc,timeing,-1,&LangSel2[ENUM_TIME].position,DT_TOP|DT_LEFT);
    EndPaint( hwnd, hdc );
    return(true);
}


static enum WinType HmiNotify_LangSel2( struct WindowMsg *pMsg )
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    s16 x,y;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_PEN_MOVE)
    {
        x= LO16(pMsg->Param2);
        y= HI16(pMsg->Param2);

        if(abs(y) > abs(x))
        {
            if((y >0))
            {
                printf("this is up\r\n");
                if(list2.coursemax > (list2.courseBase+sizeof(list2.hwnd)/sizeof(HWND)))
                {
                    list2.courseBase+=sizeof(list2.hwnd)/sizeof(HWND);
                }
            }
            else if(y <0)
            {
                if(list2.courseBase>sizeof(list2.hwnd)/sizeof(HWND))
                {
                    list2.courseBase-=sizeof(list2.hwnd)/sizeof(HWND);
                }
                else
                {
                    list2.courseBase=0;
                }
            }
            Refresh_GuiWin();
        }

        else if(abs(x) > abs(y))
        {
            //左滑
            if(x <0)
            {
                printf("左滑\r\n");
            }
            //右滑
            else
            {
                printf("右滑\r\n");
            }
        }

    }


    else if ( event == MSG_BTN_UP )
    {
        switch ( id )
        {
        case ENUM_LANG1:
            if(strcmp(lang_name2[list2.courseBase+0],"中文") == 0)
            {
             sprintf(to_language,"cn");
             sprintf(LANGUAGE2,"中文");
            }
            else if(strcmp(lang_name2[list2.courseBase+0],"阿拉伯语") == 0)
            {
             sprintf(to_language,"ar");
             sprintf(LANGUAGE2,"阿拉伯语");
            }
            printf("lang_name : %s\r\n",lang_name2[list2.courseBase+0]);
            break;
        case ENUM_LANG2:
            if(strcmp(lang_name2[list2.courseBase+1],"英语") == 0)
            {
             sprintf(to_language,"en");
             sprintf(LANGUAGE2,"英语");
            }
            else if(strcmp(lang_name2[list2.courseBase+1],"西班牙语") == 0)
            {
             sprintf(to_language,"es");
             sprintf(LANGUAGE2,"西班牙语");
            }
            printf("lang_name : %s\r\n",lang_name2[list2.courseBase+1]);
            break;
        case ENUM_LANG3:
            if(strcmp(lang_name2[list2.courseBase+2],"日语") == 0)
            {
             sprintf(to_language,"ja");
             sprintf(LANGUAGE2,"日语");
            }
            else if(strcmp(lang_name2[list2.courseBase+2],"俄语") == 0)
            {
             sprintf(to_language,"ru");
             sprintf(LANGUAGE2,"俄语");
            }
            printf("lang_name : %s\r\n",lang_name2[list2.courseBase+2]);
            break;
        case ENUM_LANG4:
            if(strcmp(lang_name2[list2.courseBase+3],"德语") == 0)
            {
             sprintf(to_language,"de");
             sprintf(LANGUAGE2,"德语");

            }
            else if(strcmp(lang_name2[list2.courseBase+3],"韩语") == 0)
            {
             sprintf(to_language,"ko");
             sprintf(LANGUAGE2,"韩语");
            }
            printf("lang_name : %s\r\n",lang_name2[list2.courseBase+2]);
            break;
        }
        printf("1LANGUAGE is %s\r\n",LANGUAGE2);
        printf("2from_language is %s\r\n",to_language);
        memset( word_storage_result_to, 0, strlen( word_storage_result_to ) );
        memset( word_storage_result_from, 0, strlen( word_storage_result_from ) );
        Jump_GuiWin(WIN_DialogueTranslation);
    }
    return NextUi;
}


int Register_LangSel2()
{
    return(Register_NewWin( WIN_LangSel2, HmiCreate_LangSel2, HmiPaint_LangSel2, HmiNotify_LangSel2) );
}

