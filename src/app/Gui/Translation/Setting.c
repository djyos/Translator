//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include <upgrade.h>

//界面元素ID编号
enum SetListsId{
    ENUM_BACKGROUND,
    ENUM_WORD1,
    ENUM_WIFI,
    ENUM_RETURN,
    ENUM_TIME,
    CheckUpdates,
    WifiSetting,
    NetLoading,
    Restart,
    Device_Binding,
    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO SetListsCfgTab[ENUM_MAXNUM] =
{
    [ENUM_BACKGROUND] = {
        .position   = { 0, 0, 240,   20       },
        .name       = "MainWin",
        .type       = type_background,
        .userParam  = BMP_Background1_bmp,
    },
    [ENUM_WORD1] =    {
        .position  = {27,42,95,60},
        .name      = "WORD1",
        .type      = widget_type_button,
        .userParam = ENUM_WORD1,
    },
    [ENUM_WIFI] =       {
        .position   = {5,5,25+5,25+5},
        .name       = "wifi",
        .type       = widget_type_picture,
        .userParam  = BMP_WIFI,
    },
    [ENUM_TIME] =       {
        .position   = { 100, 10, 100 + 43, 30 + 13 },
        .name       = "ACTIME",
        .type       = widget_type_time,
        .userParam  = Bmp_NULL,
    },
    [ENUM_RETURN] =    {
        .position  = {5,40,27,62},
        .name      = "return",
        .type      = widget_type_button,
        .userParam = ENUM_RETURN,
    },
    [WifiSetting] = {
        .position = {0,71+12-4,240,105+12-4},
        .name = "WiFi连接",
        .type = widget_type_Setting,
        .userParam = BMP_WifiSetLogo_bmp,
    },
    [CheckUpdates] = {
        .position = {0,132-1,240,166-1}, //行宽34  间距15
        .name = "检查更新",
        .type = widget_type_Setting,
        .userParam = BMP_UpgradeSetLogo_bmp,
    },
    [Device_Binding] = {
        .position = {0,181-1,240,215-1},
        .name = "版本信息",
        .type = widget_type_Setting,
        .userParam = BMP_Device_BindingLogo_bmp,
    },
    [NetLoading] = {
        .position = {0,230-1,240,264-1},
        .name = "DJYOS服务条款",
        .type = widget_type_Setting,
        .userParam = BMP_TermsOfService_bmp,
    },
    [Restart] = {
        .position = {0,279-1,240,313-1},
        .name = "关于设备",
        .type = widget_type_Setting,
        .userParam = BMP_AboutDevices_bmp,
    },
};



//按钮控件创建函数
static bool_t  SetListsButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if(widget_type_Setting == buttoninfo->type)
        {
           bmp = Get_BmpBuf(buttoninfo->userParam);
           if(bmp != NULL)
           {
               DrawBMP(hdc,16,0,bmp);

           }
           rc.left+=58;
           SetTextColor(hdc,RGB(28,32,42));
           DrawText(hdc,buttoninfo->name,-1,&rc,DT_LEFT|DT_CENTER);
        }
        else if((ENUM_RETURN == buttoninfo->userParam))
        {
            bmp = Get_BmpBuf(BMP_return_bmp);
                  if(bmp != NULL)
                  {
                      Draw_Icon_2(hdc,0,0,22,22,bmp);
                  }
        }
        else if((ENUM_WORD1 == buttoninfo->userParam))
        {
               SetTextColor(hdc,RGB(28,32,42));
               DrawText(hdc,"设置",-1,&rc,DT_TOP|DT_LEFT);
        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_Setting(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    HWND hwndButton;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, SetListsButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (SetListsCfgTab[i].type)
        {
            case  widget_type_Setting :
                 hwndButton = CreateButton(SetListsCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                              SetListsCfgTab[i].position.left, SetListsCfgTab[i].position.top,\
                              RectW(&SetListsCfgTab[i].position),RectH(&SetListsCfgTab[i].position), //按钮位置和大小
                              hwnd,i,(ptu32_t)&SetListsCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                if ( hwndButton == NULL )
                {
                    printf( "CreateButton widget_type_Setting error \n\r" );
                }
                break;
            case  widget_type_button:
            {
                CreateButton(SetListsCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                        SetListsCfgTab[i].position.left, SetListsCfgTab[i].position.top,\
                        RectW(&SetListsCfgTab[i].position),RectH(&SetListsCfgTab[i].position), //按钮位置和大小
                        hwnd,i,(ptu32_t)&SetListsCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                break;
            }
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_Setting(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        /* 主界面背景 */
//         bmp = Get_BmpBuf( SetListsCfgTab[ENUM_BACKGROUND].userParam );
//         if ( bmp != NULL )
//         {
//             DrawBMP( hdc, SetListsCfgTab[ENUM_BACKGROUND].position.left,
//                     SetListsCfgTab[ENUM_BACKGROUND].position.top, bmp );
//         }

        /* Wifi 状态 */
        bmp = Get_BmpBuf( SetListsCfgTab[ENUM_WIFI].userParam );
        if ( bmp != NULL )
        {
            Draw_Icon( hdc, SetListsCfgTab[ENUM_WIFI].position.left,
                    SetListsCfgTab[ENUM_WIFI].position.top, &SetListsCfgTab[ENUM_WIFI].position, bmp );
        }

        /* 时间 */
        struct WinTime  time        = Win_GetTime();
        char        timeing[16] = { 0 };
        SetTextColor( hdc, RGB( 100, 100, 100 ) );
        sprintf( timeing, "%02d:%02d", time.hour, time.minute );
        DrawText( hdc, timeing, -1, &SetListsCfgTab[ENUM_TIME].position, DT_TOP | DT_LEFT );

        EndPaint(hwnd,hdc);
        return true;
    }
    return false;

}
bool_t  runapp(char *param);
void Set_UpdateWinType(enum WinType state);
static enum WinType HmiNotify_Settings(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 id =LO16(pMsg->Param1);
    u16 event =HI16(pMsg->Param1);


    if ( event == MSG_BTN_UP )
    {
        switch ( id )
        {
        case ENUM_RETURN:
            NextUi = WIN_Main_WIN;
            break;
        case ENUM_WORD1:
            NextUi = WIN_Main_WIN;
            break;
        case WifiSetting:
            NextUi = WIN_Settings_WifiConnect;
            break;
        default:    break;
        }
    }

    return NextUi;
}


int Register_Settings()
{
    return Register_NewWin(WIN_Settings,HmiCreate_Setting,HmiPaint_Setting,HmiNotify_Settings);
}

















