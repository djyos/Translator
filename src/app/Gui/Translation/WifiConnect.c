//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include <app_flash.h>
//控件ID编号
enum WifiConnectId{
    ENUM_BACKGROUND,
    ENUM_WORD1,
    ENUM_WIFI,
    ENUM_TIME,
    ENUM_RETURN,
    WifiConnect_LOGO,         //过往作业
    WifiConnect_statue,       //wifi状态
    WifiConnect_ssid,         //wifi的ssid
    WifiConnect_ip,           //wifi的ip
    WifiConnect_PreviousPage, //上一页

    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO WifiConnectCfgTab[ENUM_MAXNUM] =
{
    [ENUM_BACKGROUND] = {
        .position   = { 0, 0, 240,   20       },
        .name       = "MainWin",
        .type       = type_background,
        .userParam  = BMP_Background1_bmp,
    },
    [ENUM_WORD1] =    {
        .position  = {27,42,95,60},
        .name      = "WORD1",
        .type      = widget_type_button,
        .userParam = ENUM_WORD1,
    },
    [ENUM_WIFI] =       {
        .position   = {5,5,25+5,25+5},
        .name       = "wifi",
        .type       = widget_type_picture,
        .userParam  = BMP_WIFI,
    },
    [ENUM_TIME] =       {
        .position   = { 100, 10, 100 + 43, 30 + 13 },
        .name       = "ACTIME",
        .type       = widget_type_time,
        .userParam  = Bmp_NULL,
    },
    [ENUM_RETURN] =    {
        .position  = {5,40,27,62},
        .name      = "return",
        .type      = widget_type_button,
        .userParam = ENUM_RETURN,
    },
    [WifiConnect_statue] = {
        .position = {15,65+40*0,240,65+40*1},
        .name = "WiFi状态",
        .type = widget_type_text,
        .userParam = 0,
    },
    [WifiConnect_ssid] = {
        .position = {15,65+40*1,240,65+40*2},
        .name = "Ssid",
        .type = widget_type_text,
        .userParam = 1,
    },

    [WifiConnect_ip] = {
        .position = {15,65+40*2,240,65+40*3},
        .name = "ip",
        .type = widget_type_text,
        .userParam = 3,
    },
    [WifiConnect_PreviousPage] = {
        .position = {8,270,240-8,320-8},
        .name = "重新配网",
        .type = widget_type_button,
        .userParam = RGB(47,43,40),
    },
};

static char wifi_text_buf[100];
extern  WIFI_CFG_T  LoadWifi;
extern u8 *getnetmacaddr();
//按钮控件创建函数
static bool_t  WifiConnectButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        const  struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if((ENUM_RETURN == buttoninfo->userParam))
        {
            bmp = Get_BmpBuf(BMP_return_bmp);
                  if(bmp != NULL)
                  {
                      Draw_Icon_2(hdc,0,0,22,22,bmp);
                  }
        }
        else if((ENUM_WORD1 == buttoninfo->userParam))
        {
            SetTextColor(hdc,RGB(28,32,42));
            DrawText(hdc,"WiFi状态",-1,&rc,DT_TOP|DT_LEFT);

        }
        if((0==strcmp(buttoninfo->name,WifiConnectCfgTab[WifiConnect_PreviousPage].name)))
        {
            Draw_Circle_Button(hdc,&rc,21,RGB(247,192,51));
            SetTextColor(hdc,RGB(255,255,255));

            DrawText(hdc,buttoninfo->name,-1,&rc,DT_VCENTER|DT_CENTER);
        }


        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}

/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_WifiConnect(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;
//    HWND hwndButton;
    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, WifiConnectButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (WifiConnectCfgTab[i].type)
        {
            case  widget_type_button :
                {
                 CreateButton(WifiConnectCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                         WifiConnectCfgTab[i].position.left, WifiConnectCfgTab[i].position.top,\
                         RectW(&WifiConnectCfgTab[i].position),RectH(&WifiConnectCfgTab[i].position), //按钮位置和大小
                         hwnd,i,(ptu32_t)&WifiConnectCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                }
                break;
            default:    break;
        }
    }

    return true;
}
char *DevGetIpAddr();
//绘制消息处函数
static bool_t HmiPaint_WifiConnect(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    char* time;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        /* 主界面背景 */
//         bmp = Get_BmpBuf( WifiConnectCfgTab[ENUM_BACKGROUND].userParam );
//         if ( bmp != NULL )
//         {
//             DrawBMP( hdc, WifiConnectCfgTab[ENUM_BACKGROUND].position.left,
//                     WifiConnectCfgTab[ENUM_BACKGROUND].position.top, bmp );
//         }

        /* Wifi 状态 */
        bmp = Get_BmpBuf( WifiConnectCfgTab[ENUM_WIFI].userParam );
        if ( bmp != NULL )
        {
            Draw_Icon( hdc, WifiConnectCfgTab[ENUM_WIFI].position.left,
                    WifiConnectCfgTab[ENUM_WIFI].position.top, &WifiConnectCfgTab[ENUM_WIFI].position, bmp );
        }

        /* 时间 */
        struct WinTime  time        = Win_GetTime();
        char        timeing[16] = { 0 };
        SetTextColor( hdc, RGB( 100, 100, 100 ) );
        sprintf( timeing, "%02d:%02d", time.hour, time.minute );
        DrawText( hdc, timeing, -1, &WifiConnectCfgTab[ENUM_TIME].position, DT_TOP | DT_LEFT );

        for(int i=0;i<ENUM_MAXNUM;i++)
        {
            switch (WifiConnectCfgTab[i].type)
            {
                case  widget_type_text :
                    SetTextColor(hdc,RGB(28,32,42));
                    if(WifiConnectCfgTab[i].userParam == 0)
                    {
                        if(Get_Wifi_Connectedflag() == 1){
                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name,"已连接");
                        }else{
                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name,"未连接");
                        }
                    }else if(WifiConnectCfgTab[i].userParam == 1){
                        if(LoadWifi.statue == 0x55){
                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name,LoadWifi.WifiSsid);
                        }else{
                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name,"NULL");
                        }
//                    }else if(WifiConnectCfgTab[i].userParam == 2){
//                        if(LoadWifi.statue == 0x55){
//                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name,LoadWifi.WifiPassWd);
//                        }else{
//                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name,"NULL");
//                        }
                    }else if(WifiConnectCfgTab[i].userParam == 3){
                        if(LoadWifi.statue == 0x55){
                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name, DevGetIpAddr());
                        }else{
                            sprintf(wifi_text_buf,"%s:%s",WifiConnectCfgTab[i].name,"NULL");
                        }
                    }
                    DrawText(hdc,wifi_text_buf,-1,&WifiConnectCfgTab[i].position,DT_LEFT|DT_VCENTER);
                    break;

                default:    break;
            }
        }

        EndPaint(hwnd,hdc);
        return true;
    }
    return false;

}
bool_t  runapp(char *param);
static enum WinType HmiNotify_Settings_WifiConnect(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);
    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case ENUM_RETURN:
                NextUi = WIN_Settings;
                break;
            case ENUM_WORD1:
                NextUi = WIN_Settings;
                break;
            case  WifiConnect_PreviousPage:
                NextUi = WIN_FriendlyReminders;
                break;
            default:    break;
        }
    }
    return NextUi;
}

int Register_Settings_WifiConnect()
{
    return Register_NewWin(WIN_Settings_WifiConnect,HmiCreate_WifiConnect,HmiPaint_WifiConnect,HmiNotify_Settings_WifiConnect);
}
