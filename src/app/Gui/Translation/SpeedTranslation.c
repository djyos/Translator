/*
 * SpeedTranslation.c
 *
 *  Created on: 2020年3月17日
 *      Author: zxy
 */
/*
 * ----------------------------------------------------
 * Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * -----------------------------------------------------------------------------
 * Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
 *
 * 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
 * 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
 *
 * 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
 *    及下述的免责声明。
 * 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
 *    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
 *    的免责声明。
 */

/*
 * 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
 * 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
 * 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
 * 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
 * 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
 * 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
 * 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
 * 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
 * -----------------------------------------------------------------------------
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "music_player.h"
#include "chain_table.h"

extern unsigned char    word_storage_result_to[512];        /* 翻译结果 */
extern unsigned char    word_storage_result_from[512];      /* 录音结果*/
int play_status = 0;                                        /* 录音状态 0表示关闭 1表示开启*/
int language_status = 0;                                    /* 语言状态 0,1表示*/
int translation_real_start = 0;                             /* 录音速译开关 */
extern char  from_language[16];
extern char  to_language[16];


enum InformationId {
    ENUM_BACKGROUND,        /* 背景 */
    ENUM_INFORMATION_UI,
    ENUM_WIFI,
    ENUM_TIME,
    ENUM_RETURN,
    ENUM_WORD1,
    ENUM_WORD2,
    ENMU_CHANGE,
    ENMU_PlAY_PAUSE,
    ENMU_STAGE,
    ENUM_WORD3,
    ENUM_WORD4,
    ENUM_MAXNUM,            /* 总数量 */
};
/* ==================================config====================================== */
static const struct GUIINFO InformationMain[ENUM_MAXNUM] =
{
        [ENUM_BACKGROUND] = {
        .position   = { 0, 0, 240, 20 },
        .name       = "MainWin",
        .type       = type_background,
        .userParam  = BMP_Background1_bmp,
        },
        [ENUM_WIFI] = {
        .position = {5,5,25+5,25+5},
        .name = "wifi",
        .type = widget_type_picture,
        .userParam = BMP_WIFI,
        },
        [ENUM_RETURN] =    {
        .position  = {5,40,27,62},
        .name      = "return",
        .type      = widget_type_button,
        .userParam = ENUM_RETURN,
        },
        [ENUM_WORD1] =    {
        .position  = {27,42,95,60},
        .name      = "WORD1",
        .type      = widget_type_button,
        .userParam = ENUM_WORD1,
        },
        [ENUM_WORD2] =    {
        .position  = {190,40,229,60},
        .name      = "WORD2",
        .type      = widget_type_button,
        .userParam = ENUM_WORD2,
        },
        [ENUM_TIME] = {
        .position = {100,10,100+40,30+10},
        .name = "ACTIME",
        .type = widget_type_time,
        .userParam = Bmp_NULL,
        },
        [ENMU_CHANGE] =    {
        .position  = {45,250,95,295}, //45,45
        .name      = "CHANGE",
        .type      = widget_type_button,
        .userParam = ENMU_CHANGE,
        },
        [ENMU_PlAY_PAUSE] =    {
        .position  = {143,257,176,290}, //45,45
        .name      = "PLAY",
        .type      = widget_type_button,
        .userParam = ENMU_PlAY_PAUSE,
        },
        [ENMU_STAGE] =    {
        .position  = {5,70,235,250},
        .name      = "STAGE",
        .type      = widget_type_button,
        .userParam = ENMU_STAGE,
        },
        [ENUM_WORD3] = {
        .position   = { 35,295,100,320},
        .name       = "WORD3",
        .type       = widget_type_msg,
        .userParam  = ENUM_WORD3,
        },
        [ENUM_WORD4] = {
        .position   = { 120,295,205,320},
        .name       = "WORD4",
        .type       = widget_type_msg,
        .userParam  = ENUM_WORD4,
        },
  };

//按钮控件创建函数
static bool_t BmpButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    GetClientRect(hwnd,&rc);
    SetTextColor(hdc,RGB(100,100,100));
    enum Bmptype type = Bmp_NULL;
    const  struct GUIINFO *buttoninfo = GetWindowPrivateData( hwnd );

        if((ENUM_RETURN == buttoninfo->userParam))
        {
            bmp = Get_BmpBuf(BMP_return_bmp);
                  if(bmp != NULL)
                  {
                      Draw_Icon_2(hdc,0,0,22,22,bmp);
                  }
        }
        else if((ENUM_WORD1 == buttoninfo->userParam))
        {
               DrawText(hdc,"录音速译",-1,&rc,DT_TOP|DT_LEFT);

        }
        else if((ENUM_WORD2 == buttoninfo->userParam))
        {

               DrawText(hdc,"清空",-1,&rc,DT_TOP|DT_CENTER);

        }
        else if((ENMU_CHANGE == buttoninfo->userParam))
        {
            bmp = Get_BmpBuf(BMP_TranslationChange_bmp);
                  if(bmp != NULL)
                  {
                      Draw_Icon_2(hdc,0,0,45,45,bmp);
                  }
         }
        else if((ENMU_PlAY_PAUSE == buttoninfo->userParam))
        {
            if(play_status == 0){
               bmp = Get_BmpBuf(BMP_TranslationPlay_bmp);
                if(bmp != NULL)
                {
                    Draw_Icon_2(hdc,0,0,33,33,bmp);
                }
            }
            else{
                bmp = Get_BmpBuf(BMP_TranslationPause_bmp);
                if(bmp != NULL)
                {
                    Draw_Icon_2(hdc,0,0,33,33,bmp);
                }
            }
        }
        else if((ENMU_STAGE == buttoninfo->userParam))
        {
            if(language_status == 0)
            {

                if(strcmp(from_language,"cn") == 0 )
                   {
                    char *txtbuf = malloc( 800 );
                    struct Charset  * myCharset = Charset_NlsSearchCharset( CN_NLS_CHARSET_UTF8 );
                    struct Charset  * Charsetbak    = SetCharset( hdc, myCharset );
                    if ( txtbuf == NULL )
                     {
                         printf( "malloc error \n\r" );
                         return(false);
                     }
                    Utf8_Typesetting( word_storage_result_from, txtbuf, 800, 13 );
                    rc.top  += 10;
                    rc.left += 10;
                    DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
                    SetCharset( hdc, Charsetbak );
                    free( txtbuf );
                   }
               else if(strcmp(from_language,"en") == 0 )
                   {
                   char *txtbuf = malloc( 800 );
                   struct Charset  * myCharset = Charset_NlsSearchCharset( CN_NLS_CHARSET_UTF8 );
                   struct Charset  * Charsetbak    = SetCharset( hdc, myCharset );
                   if ( txtbuf == NULL )
                    {
                        printf( "malloc error \n\r" );
                        return(false);
                    }
                    Utf8_Typesetting( word_storage_result_from, txtbuf, 800, 26 );
                    rc.top  += 10;
                    rc.left += 10;
                    DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
                    SetCharset( hdc, Charsetbak );
                    free( txtbuf );
                   }
             }
            else if(language_status == 1)
            {

                if(strcmp(from_language,"cn") == 0 )
                {
                    char *txtbuf = malloc( 800 );
                    struct Charset  * myCharset = Charset_NlsSearchCharset( CN_NLS_CHARSET_UTF8 );
                    struct Charset  * Charsetbak    = SetCharset( hdc, myCharset );
                    if ( txtbuf == NULL )
                     {
                         printf( "malloc error \n\r" );
                         return(false);
                     }
                    Utf8_Typesetting( word_storage_result_to, txtbuf, 800, 26 );
                    rc.top  += 10;
                    rc.left += 10;
                    DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
                    SetCharset( hdc, Charsetbak );
                    free( txtbuf );
                }
                else if(strcmp(from_language,"en") == 0 )
                {
                    char *txtbuf = malloc( 800 );
                    struct Charset  * myCharset = Charset_NlsSearchCharset( CN_NLS_CHARSET_UTF8 );
                    struct Charset  * Charsetbak    = SetCharset( hdc, myCharset );
                    if ( txtbuf == NULL )
                     {
                         printf( "malloc error \n\r" );
                         return(false);
                     }
                    Utf8_Typesetting( word_storage_result_to, txtbuf, 800, 13 );
                    rc.top  += 10;
                    rc.left += 10;
                    DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
                    SetCharset( hdc, Charsetbak );
                    free( txtbuf );
                }
            }
        }
        EndPaint(hwnd,hdc);
        return true;

}


/*---------------------------------------------------------------------------
*  功能：  窗口创建函数
*   输入   :tagWindowMsg *pMsg
*   输出 :false或true
*  ---------------------------------------------------------------------------*/

static bool_t HmiCreate_SpeedTranslation( struct WindowMsg *pMsg )
{
    RECT    rc0;
    HWND    hwnd = pMsg->hwnd;

    /* 消息处理函数表 */
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
        { MSG_PAINT, BmpButtonPaint },
    };

    static struct MsgTableLink s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum  = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *) &s_gBmpMsgTablebutton;

    GetClientRect( hwnd, &rc0 );

    for ( int i = 0; i < ENUM_MAXNUM; i++ )
    {
        switch ( InformationMain[i].type )
        {
        case  widget_type_button:
        {
            HWND tmpHwnd = CreateButton( InformationMain[i].name, WS_CHILD | BS_NORMAL | WS_UNFILL,                         /*按钮风格 */
                             InformationMain[i].position.left, InformationMain[i].position.top, \
                             RectW( &InformationMain[i].position ), RectH( &InformationMain[i].position ),      /*按钮位置和大小 */
                             hwnd, i, (void *) &InformationMain[i], &s_gBmpDemoMsgLinkBUtton );                 /*按钮所属的父窗口，ID,附加数据 */
            if ( tmpHwnd == NULL )
            {
                printf( "CreateButton Mainhwnd error \n\r" );
            }
        }
        break;

        default:    break;
        }
    }

    return(true);
}


/* 绘制消息处函数 */
static bool_t HmiPaint_SpeedTranslation( struct WindowMsg *pMsg )
{
    HWND        hwnd;
    HDC     hdc;
    const char  * bmp;

    hwnd    = pMsg->hwnd;
    hdc = BeginPaint( hwnd );

    /* 主界面背景 */
//    bmp = Get_BmpBuf( InformationMain[ENUM_BACKGROUND].userParam );
//    if ( bmp != NULL )
//    {
//        DrawBMP( hdc, InformationMain[ENUM_BACKGROUND].position.left,
//             InformationMain[ENUM_BACKGROUND].position.top, bmp );
//    }

     //Wifi 状态
     bmp = Get_BmpBuf(InformationMain[ENUM_WIFI].userParam);
     if(bmp != NULL)
     {
         Draw_Icon(hdc,InformationMain[ENUM_WIFI].position.left,
         InformationMain[ENUM_WIFI].position.top,&InformationMain[ENUM_WIFI].position,bmp);
     }

    //时间
    struct WinTime time =  Win_GetTime();
    char timeing[16]={0};
    SetTextColor(hdc,RGB(100,100,100));
    sprintf(timeing,"%02d:%02d",time.hour,time.minute);
    DrawText(hdc,timeing,-1,&InformationMain[ENUM_TIME].position,DT_TOP|DT_LEFT);

    //word3
    DrawText( hdc, "切换", -1, &InformationMain[ENUM_WORD3].position, DT_TOP | DT_CENTER );

    //word4
    DrawText( hdc, "录音/停止", -1, &InformationMain[ENUM_WORD4].position, DT_TOP | DT_CENTER );

    EndPaint( hwnd, hdc );
    return(true);
}


static enum WinType HmiNotify_SpeedTranslation( struct WindowMsg *pMsg )
{
    enum WinType NextUi = WIN_NotChange;
    u16     event, id;

    event   = HI16( pMsg->Param1 );
    id  = LO16( pMsg->Param1 );

    if ( event == MSG_BTN_UP )
    {
        switch ( id )
        {
        case ENUM_RETURN:
            memset( word_storage_result_to, 0, strlen( word_storage_result_to ) );
            memset( word_storage_result_from, 0, strlen( word_storage_result_from ) );

            translation_real_start = 0;
            play_status = 0;
            language_status = 0;

            NextUi = WIN_Main_WIN;

            Djy_EventDelay(100*1000);
            break;
        case ENUM_WORD1:
            memset( word_storage_result_to, 0, strlen( word_storage_result_to ) );
            memset( word_storage_result_from, 0, strlen( word_storage_result_from ) );

            translation_real_start = 0;
            play_status = 0;
            language_status = 0;

            NextUi = WIN_Main_WIN;

            Djy_EventDelay(100*1000);
            break;
        case ENUM_WORD2:
            memset( word_storage_result_to, 0, strlen( word_storage_result_to ) );
            memset( word_storage_result_from, 0, strlen( word_storage_result_from ) );

            Refresh_GuiWin();
            break;
        case ENMU_PlAY_PAUSE:
            if(play_status == 0)
            {
                strcpy( to_language, "en" );
                strcpy( from_language, "cn" );
                translation_real_start = 1;
                play_status = 1;
            }
            else if(play_status == 1)
            {
                translation_real_start = 0;
                play_status = 0;
                language_status = 0;
            }
            Refresh_GuiWin();
            break;
        case ENMU_CHANGE:
            if(language_status == 0)
            {
                language_status = 1;
            }
            else{
                language_status = 0;
            }
            Refresh_GuiWin();
            break;
        default:    break;
        }
#if 0
        printf("[DEBUG word_storage_result_from] %s\r\n",word_storage_result_from);
        printf("[DEBUG word_storage_result_to] %s\r\n",word_storage_result_to);
        printf("[DEBUG language_status] %d\r\n",language_status);
        printf("[DEBUG from_language] %s\r\n",from_language);
#endif
    }

    return(NextUi);
}


int Register_SpeedTranslation()
{
    return(Register_NewWin( WIN_SpeedTranslation, HmiCreate_SpeedTranslation, HmiPaint_SpeedTranslation, HmiNotify_SpeedTranslation ) );
}




