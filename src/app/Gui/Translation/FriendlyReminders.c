//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"

//控件ID编号
enum FriendlyRemindersId{
    ENUM_BACKGROUND,   //背景
    ENUM_WIFI,         //wifi
    ENUM_TIME,         //时间
    ENUM_RETURN,       //返回
    ENUM_WORD1,
    FriendlyReminders_content,    //提示内容
    FriendlyReminders_exit,    //提示内容
    FriendlyReminders_MAXNUM,     //总数量
};
//==================================config======================================
static const  struct GUIINFO FriendlyRemindersCfgTab[FriendlyReminders_MAXNUM] =
{
   [ENUM_BACKGROUND] = {
           .position   = { 0, 0, 240,   20       },
           .name       = "MainWin",
           .type       = type_background,
           .userParam  = BMP_Background1_bmp,
       },
   [ENUM_WORD1] =    {
       .position  = {27,42,95,60},
       .name      = "WORD1",
       .type      = widget_type_button,
       .userParam = ENUM_WORD1,
   },
   [ENUM_WIFI] =       {
       .position   = {5,5,25+5,25+5},
       .name       = "wifi",
       .type       = widget_type_picture,
       .userParam  = BMP_WIFI,
   },
   [ENUM_TIME] =       {
       .position   = { 100, 10, 100 + 43, 30 + 13 },
       .name       = "ACTIME",
       .type       = widget_type_time,
       .userParam  = Bmp_NULL,
   },
   [ENUM_RETURN] =    {
       .position  = {5,40,27,62},
       .name      = "return",
       .type      = widget_type_button,
       .userParam = ENUM_RETURN,
   },
    [FriendlyReminders_content] = {
        .position = {0,62+12,240,320-50},
        .name = "content",
        .type = widget_type_text,
        .userParam = 0,
    },

    [FriendlyReminders_exit] = {
        .position = {8,320-50,240-8,320-5},
        .name = "exit",
        .type = widget_type_button,
        .userParam = 0,
    },
};

//static enum enumFriendlyReminders SelectionFriendlyReminders = enumFriendlyReminders_null;
static enum WinType winswitchfrom = WIN_Main_WIN;
static int errortopic;
int Set_ErrorTopicCnt(int cnt)
{
    errortopic = cnt;
    return 0;
}
//enum enumFriendlyReminders Get_SelectionFriendly()
//{
//    return SelectionFriendlyReminders;
//}

//int Set_SelectionFriendlyReminders(enum enumFriendlyReminders  Tips,enum WinType switchfrom)
//{
//
//    if(Tips>=enumFriendlyReminders_max)
//        return -1;
//    if(switchfrom>=WIN_Max)
//        return -2;
//    SelectionFriendlyReminders = Tips;
//    winswitchfrom = switchfrom;
//    return 0;
//}

//const char * FriendlyReminderstxttab[enumFriendlyReminders_max] =
//{
//    [enum_Insufficient_buffer]     = " 温馨提示:\r\n    网络快走丢了，快去找找哪\r\n 里网络好。",
//    [enum_buf_overflow]            = " 温馨提示:\r\n    作业跟着网路走丢了，要去\r\n 找个网络好的地方重新做一下\r\n 这题吧。",
//    [enum_warning_Submit_homework] = " 温馨提示:\r\n    网络快走丢了,快去找个网络\r\n 好的地方重新提交吧。",
//    [enum_configure_network]       = " 温馨提示:\r\n    设备将要自行重启，重启后\r\n 请拿出手机,打开wifi连接至设\r\n 备热点 %s\r\n 以进 行网络设置。",
//    [enum_Recording_upload_failed] = " 温馨提示:\r\n    第%d题的录音走丢了，去看看\r\n 是不是忘记了。",
//};

//按钮控件创建函数
static bool_t  FriendlyRemindersButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        struct GUIINFO *buttoninfo = (struct GUIINFO *)GetWindowPrivateData(hwnd);

        if((ENUM_RETURN == buttoninfo->userParam))
        {
            bmp = Get_BmpBuf(BMP_return_bmp);
                  if(bmp != NULL)
                  {
                      Draw_Icon_2(hdc,0,0,22,22,bmp);
                  }
        }
        else if((ENUM_WORD1 == buttoninfo->userParam))
        {
            SetTextColor(hdc,RGB(28,32,42));
            DrawText(hdc,"重新配网",-1,&rc,DT_TOP|DT_LEFT);

        }
        if(0==strcmp(buttoninfo->name,FriendlyRemindersCfgTab[FriendlyReminders_exit].name))
        {
            GetClientRect(hwnd,&rc);
            SetFillColor(hdc,RGB(255,215,107));
            FillRect(hdc,&rc);
            SetTextColor(hdc,RGB(255,255,255));
            DrawText(hdc,"好的",-1,&rc,DT_VCENTER|DT_CENTER);
            EndPaint(hwnd,hdc);
        }
        return true;
    }
    return false;
}
/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/
static bool_t HmiCreate_FriendlyReminders(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;
    HWND hwndButton;

    //消息处理函数表
    static const struct MsgProcTable s_gMsgTablebutton[] =
    {
            {MSG_PAINT, FriendlyRemindersButtonPaint},
    };
    static struct MsgTableLink  s_gDemoMsgLinkBUtton;

    s_gDemoMsgLinkBUtton.MsgNum = sizeof(s_gMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<FriendlyReminders_MAXNUM;i++)
    {
        switch (FriendlyRemindersCfgTab[i].type)
        {
            case  widget_type_button :
//                if((SelectionFriendlyReminders!=enum_configure_network)&&(i == FriendlyReminders_BACK))
//                    break;
                hwndButton = CreateButton(FriendlyRemindersCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                     FriendlyRemindersCfgTab[i].position.left, FriendlyRemindersCfgTab[i].position.top,\
                     RectW(&FriendlyRemindersCfgTab[i].position),RectH(&FriendlyRemindersCfgTab[i].position), //按钮位置和大小
                     hwnd,i,(ptu32_t)&FriendlyRemindersCfgTab[i], &s_gDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                    if(hwndButton == NULL)
                    {
                         printf("error :CreateButtone error %s %d %s \r\n",__FILE__,__LINE__,FriendlyRemindersCfgTab[i].name);
                    }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_FriendlyReminders(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;
    char* time;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    char *tbuf = NULL;

    if(hdc)
    {
        /* Wifi 状态 */
        bmp = Get_BmpBuf( FriendlyRemindersCfgTab[ENUM_WIFI].userParam );
        if ( bmp != NULL )
        {
            Draw_Icon( hdc, FriendlyRemindersCfgTab[ENUM_WIFI].position.left,
                    FriendlyRemindersCfgTab[ENUM_WIFI].position.top, &FriendlyRemindersCfgTab[ENUM_WIFI].position, bmp );
        }

        /* 时间 */
        struct WinTime  time        = Win_GetTime();
        char        timeing[16] = { 0 };
        SetTextColor( hdc, RGB( 100, 100, 100 ) );
        sprintf( timeing, "%02d:%02d", time.hour, time.minute );
        DrawText( hdc, timeing, -1, &FriendlyRemindersCfgTab[ENUM_TIME].position, DT_TOP | DT_LEFT );


        for(int i=0;i<FriendlyReminders_MAXNUM;i++)
        {
            switch (FriendlyRemindersCfgTab[i].type)
            {
                case  widget_type_text :
                    SetTextColor(hdc,RGB(255,81,82));
                    tbuf = malloc(1024);
                    sprintf(tbuf," 温馨提示:\r\n    设备将要自行重启，重启后\r\n 请拿出手机,打开wifi连接至设\r\n 备热点 %s 进行配网。","AI_翻译机");
                    DrawText(hdc,tbuf,-1,&FriendlyRemindersCfgTab[FriendlyReminders_content].position,DT_TOP|DT_LEFT);
                    if(tbuf)
                    {
                        free(tbuf);
                    }
                    break;
                default:    break;
            }
        }
        hdc =BeginPaint(hwnd);
        return true;
    }
    return false;
}



void WifiErase( );
//子控件发来的通知消息
static enum WinType HmiNotify_FriendlyReminders(struct WindowMsg *pMsg)
{
    #define linkbuf_max   10
    enum WinType NextUi = WIN_NotChange;
    u16 event,id;
    event =HI16(pMsg->Param1);
    id =LO16(pMsg->Param1);

    if(event==MSG_BTN_UP)
    {
        switch (id)
        {
            case ENUM_RETURN:
                NextUi = WIN_Settings_WifiConnect;
            break;
                case ENUM_WORD1:
                NextUi = WIN_Settings_WifiConnect;
            break;
            case  FriendlyReminders_exit        :
                WifiErase();
                bool_t  runapp(char *param);
                runapp(NULL);
                break;
            default:   break;
        }
    }
    return NextUi;
}


int Register_FriendlyReminders()
{
    return Register_NewWin(WIN_FriendlyReminders,HmiCreate_FriendlyReminders,HmiPaint_FriendlyReminders,HmiNotify_FriendlyReminders);
}

