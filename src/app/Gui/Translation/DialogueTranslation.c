/*
 * ----------------------------------------------------
 * Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 * -----------------------------------------------------------------------------
 * Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
 *
 * 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
 * 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
 *
 * 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
 *    及下述的免责声明。
 * 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
 *    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
 *    的免责声明。
 */

/*
 * 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
 * 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
 * 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
 * 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
 * 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
 * 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
 * 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
 * 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
 * -----------------------------------------------------------------------------
 */

/*
 * SpeedTranslation.c
 *
 *  Created on: 2020年3月17日
 *      Author: zxy
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "string.h"
#include "../inc/GuiInfo.h"
#include "../inc/WinSwitch.h"
#include "music_player.h"
#include "chain_table.h"

char            LANGUAGE1[16]   = "中文";
char            LANGUAGE2[16]   = "英语";
u32             playing_status  = 0;                    /* 播放状态 0表示未播放，1表示正在播放 */
extern unsigned char    word_storage_result_to[512];    /* 翻译结果 */
extern unsigned char    word_storage_result_from[512];  /* 录音结果*/
extern char     tts_info[256];                  /* 播报内容 */
extern char     from_language[16];              /* 输出语言 */
extern char     to_language[16];                /* 输入语言 */
extern char     recognize_language[16];         /* 识别的语言 */
int             translation_start = 0;          /* 对话翻译开关 */


enum InformationId {
    ENUM_BACKGROUND,                                /* 背景 */
    ENUM_INFORMATION_UI,
    ENUM_WIFI,
    ENUM_TIME,
    ENUM_RETURN,
    ENUM_WORD1,
    ENUM_EXCHANGE,
    ENUM_LANGUAGE1,
    ENUM_LANGUAGE2,
    ENMU_STAGE1,
    ENMU_STAGE2,
    ENUM_MAXNUM,        /* 总数量 */
};
/* ==================================config====================================== */
static const struct GUIINFO InformationMain[ENUM_MAXNUM] =
{
    [ENUM_BACKGROUND] = {
        .position   = { 0, 0, 240,   20       },
        .name       = "MainWin",
        .type       = type_background,
        .userParam  = BMP_Background1_bmp,
    },
    [ENUM_WIFI] =       {
        .position   = {5,5,25+5,25+5},
        .name       = "wifi",
        .type       = widget_type_picture,
        .userParam  = BMP_WIFI,
    },
    [ENUM_RETURN] =    {
        .position  = {5,40,27,62},
        .name      = "return",
        .type      = widget_type_button,
        .userParam = ENUM_RETURN,
    },
    [ENUM_WORD1] =    {
        .position  = {27,42,95,60},
        .name      = "WORD1",
        .type      = widget_type_button,
        .userParam = ENUM_WORD1,
    },
    [ENUM_LANGUAGE1] =  {
        .position   = { 5, 62+8, 100,  88+8       },  //30   26
        .name       = "LANGUAGE1",
        .type       = widget_type_button,
        .userParam  = ENUM_LANGUAGE1,
    },
    [ENUM_EXCHANGE] =   {
        .position   = { 105, 62+5, 135, 88+5          },
        .name       = "exchange",
        .type       = widget_type_button,
        .userParam  = ENUM_EXCHANGE,
    },
    [ENUM_LANGUAGE2] =  {
        .position   = { 140, 62+8, 235, 88+8          },
        .name       = "WORD2",
        .type       = widget_type_button,
        .userParam  = ENUM_LANGUAGE2,
    },
    [ENUM_TIME] =       {
        .position   = { 100, 10, 100 + 43, 30 + 13 },
        .name       = "ACTIME",
        .type       = widget_type_time,
        .userParam  = Bmp_NULL,
    },
    [ENMU_STAGE1] =     {
        .position   = { 0, 92+8, 240,  180+8          },
        .name       = "ENMU_STAGE1",
        .type       = widget_type_button,
        .userParam  = ENMU_STAGE1,
    },
    [ENMU_STAGE2] =     {
        .position   = { 0, 180+8, 240, 320          },
        .name       = "ENMU_STAGE2",
        .type       = widget_type_button,
        .userParam  = ENMU_STAGE2,
    },
};

/*按钮控件创建函数 */
static bool_t BmpButtonPaint( struct WindowMsg *pMsg )
{
    HWND        hwnd;
    HDC     hdc;
    RECT        rc;
    const char  * bmp;
    hwnd    = pMsg->hwnd;
    hdc = BeginPaint( hwnd );
    GetClientRect( hwnd, &rc );
    SetTextColor( hdc, RGB( 100, 100, 100 ) );
    enum Bmptype        type        = Bmp_NULL;
    const struct GUIINFO    *buttoninfo = GetWindowPrivateData( hwnd );

    if((ENUM_RETURN == buttoninfo->userParam))
    {
        bmp = Get_BmpBuf(BMP_return_bmp);
              if(bmp != NULL)
              {
                  Draw_Icon_2(hdc,0,0,22,22,bmp);
              }
    }
    else if((ENUM_WORD1 == buttoninfo->userParam))
    {
           DrawText(hdc,"对话翻译",-1,&rc,DT_TOP|DT_LEFT);

    }
    else if ( (ENUM_LANGUAGE1 == buttoninfo->userParam) )
    {
        Draw_Circle_Button( hdc, &rc, 13, RGB( 0, 0, 0 ) );
        SetTextColor( hdc, RGB( 255, 255, 255 ) );
        DrawText( hdc, LANGUAGE1, -1, &rc, DT_VCENTER | DT_CENTER );
    }else if ( (ENUM_LANGUAGE2 == buttoninfo->userParam) )
    {
        Draw_Circle_Button( hdc, &rc, 13, RGB( 0, 0, 0 ) );
        SetTextColor( hdc, RGB( 255, 255, 255 ) );
        DrawText( hdc, LANGUAGE2, -1, &rc, DT_VCENTER | DT_CENTER );
    }else if ( (ENUM_EXCHANGE == buttoninfo->userParam) )
    {
        bmp = Get_BmpBuf( BMP_Exchange_bmp );
        if ( bmp != NULL )
        {
            Draw_Icon_2( hdc, 0, 0, 30, 26, bmp );
        }
    }else if ( (ENMU_STAGE1 == buttoninfo->userParam) )
    {
        char        *txtbuf     = malloc( 800 );
        struct Charset  * myCharset = Charset_NlsSearchCharset( CN_NLS_CHARSET_UTF8 );
        struct Charset  * Charsetbak    = SetCharset( hdc, myCharset );
        if ( txtbuf == NULL )
        {
            printf( "malloc error \n\r" );
            return(false);
        }
        if ( strcmp( LANGUAGE1, "中文" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_from) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_from, txtbuf, 800, 12 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        else if ( strcmp( LANGUAGE1, "英语" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_from) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_from, txtbuf, 800, 24 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        else if ( strcmp( LANGUAGE1, "日语" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_from) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_from, txtbuf, 800, 12 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        else if ( strcmp( LANGUAGE1, "德语" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_from) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_from, txtbuf, 800, 23 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        else if ( strcmp( LANGUAGE1, "阿拉伯语" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_from) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_from, txtbuf, 800, 11 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        else if ( strcmp( LANGUAGE1, "西班牙语" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_from) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_from, txtbuf, 800, 23 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        else if ( strcmp( LANGUAGE1, "俄语" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_from) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_from, txtbuf, 800, 13 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        else if ( strcmp( LANGUAGE1, "韩语" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_from) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_from, txtbuf, 800, 11 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        }
        else if ( (ENMU_STAGE2 == buttoninfo->userParam) )
        {
            char        *txtbuf     = malloc( 800 );
            struct Charset  * myCharset = Charset_NlsSearchCharset( CN_NLS_CHARSET_UTF8 );
            struct Charset  * Charsetbak    = SetCharset( hdc, myCharset );
            if ( txtbuf == NULL )
            {
                printf( "malloc error \n\r" );
                return(false);
            }
        if ( strcmp( LANGUAGE2, "中文" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_to) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_to, txtbuf, 800, 12 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        else if ( strcmp( LANGUAGE2, "英语" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_to) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_to, txtbuf, 800, 24 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        else if ( strcmp( LANGUAGE2, "日语" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_to) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_to, txtbuf, 800, 12 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        else if ( strcmp( LANGUAGE2, "德语" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_to) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_to, txtbuf, 800, 23 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        else if ( strcmp( LANGUAGE2, "阿拉伯语" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_to) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_to, txtbuf, 800, 11 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        else if ( strcmp( LANGUAGE2, "西班牙语" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_to) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_to, txtbuf, 800, 23 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        else if ( strcmp( LANGUAGE2, "俄语" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_to) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_to, txtbuf, 800, 13 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
        else if ( strcmp( LANGUAGE2, "韩语" ) == 0 )
        {
            bmp = Get_BmpBuf( BMP_Volume_bmp );
               if ( bmp != NULL && strlen(word_storage_result_to) != 0)
               {
                   Draw_Icon_2( hdc, 0, 2, 30, 26, bmp );
               }
            Utf8_Typesetting( word_storage_result_to, txtbuf, 800, 11 );
            rc.left += 31;
            rc.top += 5;
            DrawText( hdc, txtbuf, -1, &rc, DT_LEFT | DT_TOP );
            SetCharset( hdc, Charsetbak );
            free( txtbuf );
        }
    }

    EndPaint( hwnd, hdc );
    return(true);
}

static bool_t HmiTimer(struct WindowMsg *pMsg)
{
    HWND hwnd;
    struct WinTimer *pTmr;
    u16 TmrId;
    hwnd=pMsg->hwnd;
    TmrId=pMsg->Param1;

//    printf("[INFO Tmrid] %s\r\n",TmrId);
//    printf("[INFO hwnd] %s\r\n",hwnd);
//    pTmr=GDD_FindTimer(hwnd,TmrId);
//
////    printf("[INFO pTmr]")
//    GDD_ResetTimer(pTmr,1000);
//    GDD_StopTimer(tg_pCtrlTimer);

    return true;
}

/*---------------------------------------------------------------------------
 *  功能：  窗口创建函数
 *   输入   :tagWindowMsg *pMsg
 *   输出 :false或true
 *  ---------------------------------------------------------------------------*/

static bool_t HmiCreate_DialogueTranslation( struct WindowMsg *pMsg )
{
    RECT    rc0;
    HWND    hwnd = pMsg->hwnd;

    /* 消息处理函数表 */
    static struct MsgProcTable s_gBmpMsgTablebutton[] =
    {
        { MSG_PAINT, BmpButtonPaint },
//        { MSG_TIMER, HmiTimer },                   //定时器消息响应函数
    };

    static struct MsgTableLink s_gBmpDemoMsgLinkBUtton;

    s_gBmpDemoMsgLinkBUtton.MsgNum  = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *) &s_gBmpMsgTablebutton;

    GetClientRect( hwnd, &rc0 );

    for ( int i = 0; i < ENUM_MAXNUM; i++ )
    {
        switch ( InformationMain[i].type )
        {
        case  widget_type_button:
        {
            HWND tmpHwnd = CreateButton( InformationMain[i].name, WS_CHILD | BS_NORMAL | WS_UNFILL,                         /*按钮风格 */
                             InformationMain[i].position.left, InformationMain[i].position.top, \
                             RectW( &InformationMain[i].position ), RectH( &InformationMain[i].position ),      /*按钮位置和大小 */
                             hwnd, i, (void *) &InformationMain[i], &s_gBmpDemoMsgLinkBUtton );                 /*按钮所属的父窗口，ID,附加数据 */
            if ( tmpHwnd == NULL )
            {
                printf( "CreateButton Mainhwnd error \n\r" );
            }
        }
        break;

        default:    break;
        }
    }
    return(true);
}


/* 绘制消息处函数 */
static bool_t HmiPaint_DialogueTranslation( struct WindowMsg *pMsg )
{
    HWND        hwnd;
    HDC     hdc;
    const char  * bmp;

    hwnd    = pMsg->hwnd;
    hdc = BeginPaint( hwnd );

    /* 主界面背景 */

//     bmp = Get_BmpBuf( InformationMain[ENUM_BACKGROUND].userParam );
//     if ( bmp != NULL )
//     {
//         DrawBMP( hdc, InformationMain[ENUM_BACKGROUND].position.left,
//              InformationMain[ENUM_BACKGROUND].position.top, bmp );
//     }


    /* Wifi 状态 */
    bmp = Get_BmpBuf( InformationMain[ENUM_WIFI].userParam );
    if ( bmp != NULL )
    {
        Draw_Icon( hdc, InformationMain[ENUM_WIFI].position.left,
               InformationMain[ENUM_WIFI].position.top, &InformationMain[ENUM_WIFI].position, bmp );
    }

    /* 时间 */
    struct WinTime  time        = Win_GetTime();
    char        timeing[16] = { 0 };
    SetTextColor( hdc, RGB( 100, 100, 100 ) );
    sprintf( timeing, "%02d:%02d", time.hour, time.minute );
    DrawText( hdc, timeing, -1, &InformationMain[ENUM_TIME].position, DT_TOP | DT_LEFT );
    EndPaint( hwnd, hdc );
    return(true);
}


char            ent[12];
static enum WinType HmiNotify_DialogueTranslation( struct WindowMsg *pMsg )
{
    enum WinType    NextUi = WIN_NotChange;
    u16     event, id;
    event   = HI16( pMsg->Param1 );
    id  = LO16( pMsg->Param1 );

    if ( event == MSG_BTN_UP )
    {
        switch ( id )
        {
        case ENUM_RETURN:
            NextUi = WIN_Main_WIN;
            memset( word_storage_result_to, 0, strlen( word_storage_result_to ) );
            memset( word_storage_result_from, 0, strlen( word_storage_result_from ) );
            break;
        case ENUM_WORD1:
            NextUi = WIN_Main_WIN;
            memset( word_storage_result_to, 0, strlen( word_storage_result_to ) );
            memset( word_storage_result_from, 0, strlen( word_storage_result_from ) );
            break;
        case ENUM_EXCHANGE:
            memset( word_storage_result_to, 0, strlen( word_storage_result_to ) );
            memset( word_storage_result_from, 0, strlen( word_storage_result_from ) );
            if ( strcmp( to_language, "cn" ) == 0 )
            {
                char    temp[16];
                char    temp2[16];
                strcpy( temp, to_language );
                strcpy( temp2, LANGUAGE1 );
                strcpy( to_language, from_language );
                strcpy( from_language, temp );
                strcpy( LANGUAGE1, LANGUAGE2 );
                strcpy( LANGUAGE2, temp2 );
                strcpy( recognize_language, "zh_cn" );
            }else if ( strcmp( to_language, "en" ) == 0 )
            {
                char    temp[16];
                char    temp2[16];
                strcpy( temp, to_language );
                strcpy( temp2, LANGUAGE1 );
                strcpy( to_language, from_language );
                strcpy( from_language, temp );
                strcpy( LANGUAGE1, LANGUAGE2 );
                strcpy( LANGUAGE2, temp2 );
                strcpy( recognize_language, "en_us" );
            }else if ( strcmp( to_language, "ja" ) == 0 )
            {
                char    temp[16];
                char    temp2[16];
                strcpy( temp, to_language );
                strcpy( temp2, LANGUAGE1 );
                strcpy( to_language, from_language );
                strcpy( from_language, temp );
                strcpy( LANGUAGE1, LANGUAGE2 );
                strcpy( LANGUAGE2, temp2 );
                strcpy( recognize_language, "ja_jp" );
            }else if ( strcmp( to_language, "ko" ) == 0 )
            {
                char    temp[16];
                char    temp2[16];
                strcpy( temp, to_language );
                strcpy( temp2, LANGUAGE1 );
                strcpy( to_language, from_language );
                strcpy( from_language, temp );
                strcpy( LANGUAGE1, LANGUAGE2 );
                strcpy( LANGUAGE2, temp2 );
                strcpy( recognize_language, "ko_kr" );
            }else if ( strcmp( to_language, "ru" ) == 0 )
            {
                char    temp[16];
                char    temp2[16];
                strcpy( temp, to_language );
                strcpy( temp2, LANGUAGE1 );
                strcpy( to_language, from_language );
                strcpy( from_language, temp );
                strcpy( LANGUAGE1, LANGUAGE2 );
                strcpy( LANGUAGE2, temp2 );
                strcpy( recognize_language, "ru-ru" );
            }else if ( strcmp( to_language, "fr" ) == 0 )
            {
                char    temp[16];
                char    temp2[16];
                strcpy( temp, to_language );
                strcpy( temp2, LANGUAGE1 );
                strcpy( to_language, from_language );
                strcpy( from_language, temp );
                strcpy( LANGUAGE1, LANGUAGE2 );
                strcpy( LANGUAGE2, temp2 );
                strcpy( recognize_language, "fr_fr" );
            }else if ( strcmp( to_language, "es" ) == 0 )
            {
                char    temp[16];
                char    temp2[16];
                strcpy( temp, to_language );
                strcpy( temp2, LANGUAGE1 );
                strcpy( to_language, from_language );
                strcpy( from_language, temp );
                strcpy( LANGUAGE1, LANGUAGE2 );
                strcpy( LANGUAGE2, temp2 );
                strcpy( recognize_language, "es_es" );
            }else if ( strcmp( to_language, "ar" ) == 0 )
            {
                char    temp[16];
                char    temp2[16];
                strcpy( temp, to_language );
                strcpy( temp2, LANGUAGE1 );
                strcpy( to_language, from_language );
                strcpy( from_language, temp );
                strcpy( LANGUAGE1, LANGUAGE2 );
                strcpy( LANGUAGE2, temp2 );
                strcpy( recognize_language, "ar_il" );
            }else if ( strcmp( to_language, "de" ) == 0 )
            {
                char    temp[16];
                char    temp2[16];
                strcpy( temp, to_language );
                strcpy( temp2, LANGUAGE1 );
                strcpy( to_language, from_language );
                strcpy( from_language, temp );
                strcpy( LANGUAGE1, LANGUAGE2 );
                strcpy( LANGUAGE2, temp2 );
                strcpy( recognize_language, "de_DE" );
            }
            Refresh_GuiWin();
            break;

        case ENUM_LANGUAGE1:
            Jump_GuiWin( WIN_LangSel );
            break;
        case ENUM_LANGUAGE2:
            Jump_GuiWin( WIN_LangSel2 );
            break;
        case ENMU_STAGE1:
            /*
             *  引擎类型，可选值：
             *    aisound（普通效果）
             *    intp65（中文）
             *    intp65_en（英文）
             *    mtts（小语种，需配合小语种发音人使用）
             *    xtts（优化效果）
             *  默认为intp65
             */
            if( strlen(word_storage_result_from) != 0 && playing_status == 0)
            {
                if ( strcmp( LANGUAGE1, "中文" ) == 0 )
                {
                    sprintf( ent, "intp65" );
                }else if ( strcmp( LANGUAGE1, "英语" ) == 0 )
                {
                    sprintf( ent, "intp65_en" );
                }else  {
                    sprintf( ent, "xtts" );
                }
#if 0
                printf( "[DEBUG playing_status] %d\r\n", playing_status );
                printf( "[DEBUG ent] %s\r\n", ent );
#endif
                tts_play( word_storage_result_from, ent );
            }
            break;
        case ENMU_STAGE2:
            if( strlen(word_storage_result_to) != 0 && playing_status == 0)
            {
                if ( strcmp( LANGUAGE2, "中文" ) == 0 )
                {
                    sprintf( ent, "intp65" );
                }else if ( strcmp( LANGUAGE2, "英语" ) == 0 )
                {
                    sprintf( ent, "intp65_en" );
                }else  {
                    sprintf( ent, "xtts" );
                }
#if 0
                printf( "[DEBUG playing_status] %d\r\n", playing_status );
                printf( "[DEBUG ent] %s\r\n", ent );
#endif
                tts_play( word_storage_result_to, ent );
            }
            break;
        default:    break;
        }
    }

    return(NextUi);
}


int Register_DialogueTranslation()
{
    return(Register_NewWin( WIN_DialogueTranslation, HmiCreate_DialogueTranslation, HmiPaint_DialogueTranslation, HmiNotify_DialogueTranslation ) );
}


