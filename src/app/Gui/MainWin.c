//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * Translation.c
 *
 *  Created on: 2020年3月17日
 *      Author: zxy
 */

#include <stdint.h>
#include "stdlib.h"
#include "stdio.h"
#include "djyos.h"
#include "gdd.h"
#include <gdd_widget.h>
#include "board.h"
#include "app_flash.h"
#include "inc/GuiInfo.h"
#include "inc/WinSwitch.h"
#include "music_player.h"
#include "chain_table.h"
#include "upgrade.h"

#define CN_SETTING_VOLUME           0       //当前设置音量
#define CN_SETTING_BRIGHT           1       //当前设置亮度
#define CN_TIMER_CLOSE_BRIGHT       10      //关闭背光定时
#define CN_TIMER_POWERDOWN          20      //关闭电源定时
#define CN_TIMER_CTRL               30      //音量、亮度、电源开关控制
#define CN_VOLUMEBAR                10      //也没什么含义

HWND tg_pMainWinHwn,tg_VolumeBar,tg_Record;

u32 u32g_Volume         =  80;                   //当前音量值
u32 u32g_Bright         =  50;                   //当前亮度值
u32 u32g_Setting        =  CN_SETTING_VOLUME;    //"+"、"-"按钮的功能，选择调音量或者背光亮
u32 reverse_translation =  0;                    //逆向翻译开关 0代表关闭 1代表开启
u32 reverse__result     =  0;                    //逆向翻译保存结果开关 0代表关闭 1代表开启

struct WinTimer *tg_pCtrlTimer,*tg_pPowerDownTimer;
static enum     WinType eng_ShutOffWindow = WIN_Main_WIN;
static enum     WinType eng_LastWindow = WIN_Main_WIN;
static u8       ShutOff_Statue = 0;

extern struct WinTime GuiInfoTime;
extern int    translation_real_start;
extern int    play_status;
extern int    language_status;
extern char   recognize_language[16];                 /* 识别的语言 */
extern char   from_language[16];                      /* 输出语言 */
extern char   to_language[16];                        /* 输入语言 */
extern char   LANGUAGE1[16];
extern char   LANGUAGE2[16];

void Set_ShutOff_Statue(u8 statue)
{
    ShutOff_Statue = statue;
}
enum WinType Get_LastWinType()
{
    return eng_LastWindow;
}

PROGRESSBAR_DATA tg_SettingProgressbar = //电量进度条
{
    .Flag    =PBF_SHOWTEXT|PBF_ORG_LEFT,
    .Range   =100,
    .Pos     = 50,
    .FGColor = CN_COLOR_GREEN,
    .BGColor = CN_COLOR_WHITE,
    .TextColor =CN_COLOR_WHITE,
    .DrawTextFlag =DT_VCENTER|DT_CENTER,
    .text = NULL,
};

//按键手动控制定时器的状态机
enum CtrlTimerMachine
{
    CtrlNullOp,
    CtrlUpVolume,
    CtrlDownVolume,
    CtrlRecord,
    CtrlWaitExit,
    CtrlPowerDown,

};

enum CtrlTimerMachine en_StatueMachine;

u32 u32g_BrightStatus = 1;       //表示背光是亮的，开机后确实是亮的

enum widgeId{
    ENUM_BACKGROUND, //背景
    ENUM_WIFI,       //wifi
    ENUM_ACTIME,
    ENUM_DATE,
    ENUM_WEEKDAY,
    ENUM_MAIN_LIST_UI,
    ENUM_weather_ui,
    ENUM_BOTTOM1,
    ENUM_BOTTOM2,
    ENUM_BOTTOM3,

//==============================================================================
    ENUM_MAXNUM,//总数量
};
//==================================config======================================
static const  struct GUIINFO MainWinCfgTab[ENUM_MAXNUM] =
{
    [ENUM_BACKGROUND] = {
        .position = {0,0,240,320},
        .name = "MainWin",
        .type = type_background,
        .userParam = BMP_Background_bmp,
    },
    [ENUM_WIFI] = {
         .position = {5,5,25+5,25+5},
         .name = "wifi",
         .type = widget_type_picture,
         .userParam = BMP_WIFI,
     },
    [ENUM_ACTIME] = {
        .position = {100,10,100+40,30+10},
        .name = "ACTIME",
        .type = widget_type_time,
        .userParam = Bmp_NULL,
    },
    [ENUM_BOTTOM1] = {
        .position = {30,54,110,154},
        .name = "DialogueTranslation",
        .type = widget_type_button,
        .userParam = ENUM_BOTTOM1,
    },
    [ENUM_BOTTOM2] = {
        .position = {127,54,207,154},
        .name = "SpeedTranslation",
        .type = widget_type_button,
        .userParam = ENUM_BOTTOM2,
    },
    [ENUM_BOTTOM3] = {
        .position = {30,184,110,284},
        .name = "TranslationSetting",
        .type = widget_type_button,
        .userParam = ENUM_BOTTOM3,
    },
};

//按钮控件创建函数
static bool_t BmpButtonPaint(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    RECT rc;
    const char * bmp;
    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);
    if(hdc)
    {
        GetClientRect(hwnd,&rc);
        enum Bmptype type = Bmp_NULL;

        const  struct GUIINFO *buttoninfo = GetWindowPrivateData(hwnd);

        bmp = Get_BmpBuf(BMP_Background_bmp);
        if(bmp != NULL)
        {
            DrawBMP(hdc,-buttoninfo->position.left,-buttoninfo->position.top,bmp);
        }

       if((ENUM_BOTTOM1 == buttoninfo->userParam))
        {
            bmp = Get_BmpBuf(BMP_DialogueTranslation_bmp);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,0,0,80,80,bmp);
                SetTextColor(hdc,RGB(100,100,100));
                rc.top+=80;
                DrawText(hdc,"对话翻译",-1,&rc,DT_TOP|DT_CENTER);
            }
        }
        else if((ENUM_BOTTOM2 == buttoninfo->userParam))
        {
            bmp = Get_BmpBuf(BMP_SpeedTranslation_bmp);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,0,0,80,80,bmp);
                SetTextColor(hdc,RGB(100,100,100));
                rc.top+=80;
                DrawText(hdc,"录音速译",-1,&rc,DT_TOP|DT_CENTER);
            }
        }
        else if((ENUM_BOTTOM3 == buttoninfo->userParam))
        {
            bmp = Get_BmpBuf(BMP_Translation_setting_bmp);
            if(bmp != NULL)
            {
                Draw_Icon_2(hdc,0,0,80,80,bmp);
                SetTextColor(hdc,RGB(100,100,100));
                rc.top+=80;
                DrawText(hdc,"网络设置",-1,&rc,DT_TOP|DT_CENTER);
            }
        }
        EndPaint(hwnd,hdc);
        return true;
    }
    return false;
}


/*---------------------------------------------------------------------------
功能：  窗口创建函数
    输入   :tagWindowMsg *pMsg
    输出 :false或true
---------------------------------------------------------------------------*/

static bool_t HmiCreate_MainWin(struct WindowMsg *pMsg)
{
    RECT rc0;
    HWND hwnd =pMsg->hwnd;

    //消息处理函数表
     static struct MsgProcTable s_gBmpMsgTablebutton[] =
     {
             {MSG_PAINT,BmpButtonPaint},
     };

     static struct MsgTableLink  s_gBmpDemoMsgLinkBUtton;

     s_gBmpDemoMsgLinkBUtton.MsgNum = sizeof(s_gBmpMsgTablebutton) / sizeof(struct MsgProcTable);
     s_gBmpDemoMsgLinkBUtton.myTable = (struct MsgProcTable *)&s_gBmpMsgTablebutton;

    GetClientRect(hwnd,&rc0);

    for(int i=0;i<ENUM_MAXNUM;i++)
    {
        switch (MainWinCfgTab[i].type)
        {
            case  widget_type_button :
            {
                HWND tmpHwnd =  CreateButton(MainWinCfgTab[i].name, WS_CHILD|BS_NORMAL | WS_UNFILL,    //按钮风格
                                 MainWinCfgTab[i].position.left, MainWinCfgTab[i].position.top,\
                                 RectW(&MainWinCfgTab[i].position),RectH(&MainWinCfgTab[i].position), //按钮位置和大小
                                 hwnd,i,(void*)&MainWinCfgTab[i], &s_gBmpDemoMsgLinkBUtton);   //按钮所属的父窗口，ID,附加数据
                if(tmpHwnd == NULL)
                {
                    printf("CreateButton Mainhwnd error \n\r");
                }
            }
                break;
            default:    break;
        }
    }

    return true;
}

//绘制消息处函数
static bool_t HmiPaint_MainWin(struct WindowMsg *pMsg)
{
    HWND hwnd;
    HDC  hdc;
    const char * bmp ;

    hwnd =pMsg->hwnd;
    hdc =BeginPaint(hwnd);

    //主界面背景
    bmp = Get_BmpBuf(MainWinCfgTab[ENUM_BACKGROUND].userParam);
    if(bmp != NULL)
    {
        DrawBMP(hdc,MainWinCfgTab[ENUM_BACKGROUND].position.left,
                MainWinCfgTab[ENUM_BACKGROUND].position.top,bmp);
    }

    //Wifi 状态
    bmp = Get_BmpBuf(MainWinCfgTab[ENUM_WIFI].userParam);
    if(bmp != NULL)
    {
       Draw_Icon(hdc,MainWinCfgTab[ENUM_WIFI].position.left,
       MainWinCfgTab[ENUM_WIFI].position.top,&MainWinCfgTab[ENUM_WIFI].position,bmp);
    }

//  日期
//    char* date =  Win_GetDate();
//    SetTextColor(hdc,RGB(100,100,100));
//    DrawText(hdc,date,-1,&MainWinCfgTab[ENUM_DATE].position,DT_TOP|DT_LEFT);

//  时间
    struct WinTime time =  Win_GetTime();
    char timeing[16]={0};
    SetTextColor(hdc,RGB(100,100,100));
    sprintf(timeing,"%02d:%02d",time.hour,time.minute);
    DrawText(hdc,timeing,-1,&MainWinCfgTab[ENUM_ACTIME].position,DT_TOP|DT_LEFT);


//    星期
//    extern char weekday[16];
//    struct Charset*  myCharset = Charset_NlsSearchCharset(CN_NLS_CHARSET_UTF8);
//    struct Charset* Charsetbak = SetCharset(hdc,myCharset);
//    SetTextColor(hdc,RGB(100,100,100));
//    DrawText(hdc,weekday,-1,&MainWinCfgTab[ENUM_WEEKDAY].position,DT_TOP|DT_LEFT);
//    SetCharset(hdc,Charsetbak);

      EndPaint(hwnd,hdc);
      return true;

}

uint32_t gKeyRecordPressDown = 0; // 0表示未在录音状态；1表示在录音中
extern int translation_start;
extern int   up_stage_status;                           /* 上层框语言状态 */
extern unsigned char    word_storage_result_to[512];     /* 翻译结果 */
extern unsigned char    word_storage_result_from[512];   /* 录音结果*/
extern pNODE head;

static enum WinType HmiNotify_MainWin(struct WindowMsg *pMsg)
{
    enum WinType NextUi = WIN_NotChange;
    u16 event =HI16(pMsg->Param1);
    u16 id =LO16(pMsg->Param1);
    s16 x,y;

    if(event == MSG_BTN_UP)
    {
        switch(id)
        {
            case ENUM_BOTTOM1:
                Jump_GuiWin(WIN_DialogueTranslation);
                break;
            case ENUM_BOTTOM2:
                Jump_GuiWin(WIN_SpeedTranslation);
                break;
            case ENUM_BOTTOM3:
                Jump_GuiWin(WIN_Settings);
                break;
            default:
                break;
        }
    }
    return NextUi;
}
static bool_t HmiKeyUp(struct WindowMsg *pMsg)
{
    HWND hwnd;
    u16 KeyValue;
    hwnd =pMsg->hwnd;
    KeyValue = pMsg->Param1;
    HDC hdc =BeginPaint(hwnd);
    //电源
    if(KeyValue == POWER_KEY)
    {
        GDD_StopTimer(tg_pCtrlTimer);
        if(u32g_BrightStatus == 1)
        {
            if(!ShutOff_Statue)
            {
                u32g_BrightStatus = 0;
                CloseScreen();
            }
        }
        else
        {
            u32g_BrightStatus = 1;
            OpenScreen();
        }
    }
    //ADC_KEY1
    else if(KeyValue == PAUSE_PLAY_KEY)
    {
        if(1 == translation_start )
        {
            translation_start = 0;
            Jump_GuiWin(WIN_DialogueTranslation);
            Djy_EventDelay(100*1000);
        }
    }
    else if(KeyValue == COMEBACK_KEY)
    {
        if(1 == translation_start )
        {
            reverse_translation = 0;
            translation_start = 0;
            Jump_GuiWin(WIN_DialogueTranslation);
            Djy_EventDelay(100*1000);
        }
    }
    //ADC_KEY3
    else if(KeyValue == VOL_UP_KEY)
    {
        en_StatueMachine = CtrlWaitExit;
    }
    //ADC_KEY2
    else if(KeyValue == VOL_DOWN_KEY)
    {
        en_StatueMachine = CtrlWaitExit;
    }
    EndPaint(hwnd,hdc);
    return true;
}

char* get_to_language(char *in)
{
    char *out = NULL;
    if ( strcmp( in, "中文" ) == 0 )
    {
        out = "cn";
    }
    else if ( strcmp( in, "英语" ) == 0 )
    {
        out = "en";
    }
    else if ( strcmp( in, "日语" ) == 0 )
    {
        out = "ja";
    }
    else if ( strcmp( in, "韩语" ) == 0 )
    {
        out = "ko";
    }
    else if ( strcmp( in, "俄语" ) == 0 )
    {
        out = "ru";
    }
    else if ( strcmp( in, "西班牙语" ) == 0 )
    {
        out = "es";
    }
    else if ( strcmp( in, "阿拉伯语" ) == 0 )
    {
        out = "ar";
    }
    else if ( strcmp( LANGUAGE1, "德语" ) == 0 )
    {
        out = "de";
    }
    return out;
}
extern struct player_ctrl_info PlayerCtrlInfo;
static bool_t HmiKeyDown(struct WindowMsg *pMsg)
{
    HWND hwnd;
    u16 KeyValue;
    hwnd = pMsg->hwnd;
    HDC hdc =BeginPaint(hwnd);
    KeyValue = pMsg->Param1;
    GDD_ResetTimer(tg_pPowerDownTimer,600000);

   if(KeyValue == PAUSE_PLAY_KEY)
    {
        if(0 == translation_start && Get_SelectionWinType() == WIN_DialogueTranslation)
          {
            if ( strcmp( LANGUAGE1, "中文" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE2) );
                  strcpy( from_language, "cn" );
                  strcpy( recognize_language, "zh_cn" );
            }
            else if ( strcmp( LANGUAGE1, "英语" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE2) );
                  strcpy( from_language, "en" );
                  strcpy( recognize_language, "en_us" );
            }
            else if ( strcmp( LANGUAGE1, "日语" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE2) );
                  strcpy( from_language, "ja" );
                  strcpy( recognize_language, "ja_jp" );
            }
            else if ( strcmp( LANGUAGE1, "韩语" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE2) );
                  strcpy( from_language, "ko" );
                  strcpy( recognize_language, "ko_kr" );
            }
            else if ( strcmp( LANGUAGE1, "俄语" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE2) );
                  strcpy( from_language, "ru" );
                  strcpy( recognize_language, "ru-ru" );
            }
            else if ( strcmp( LANGUAGE1, "西班牙语" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE2) );
                  strcpy( from_language, "es" );
                  strcpy( recognize_language, "es_es" );
            }
            else if ( strcmp( LANGUAGE1, "阿拉伯语" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE2) );
                  strcpy( from_language, "ar" );
                  strcpy( recognize_language, "ar_il" );
            }
            else if ( strcmp( LANGUAGE1, "德语" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE2) );
                  strcpy( from_language, "de" );
                  strcpy( recognize_language, "de_DE" );
            }
#if 0
            printf("[DEBUG after from_language] %s\r\n",from_language);
            printf("[DEBUG after to_language] %s\r\n",to_language);
            printf("[DEBUG after recognize_language] %s\r\n",recognize_language);
#endif
            translation_start = 1;
            Jump_GuiWin(WIN_Tip);
            Djy_EventDelay(100*1000);
          }
    }
   else if(KeyValue == COMEBACK_KEY)
   {
       if(0 == translation_start && Get_SelectionWinType() == WIN_DialogueTranslation)
       {
            if ( strcmp( LANGUAGE2, "中文" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE1) );
                  strcpy( from_language, "cn" );
                  strcpy( recognize_language, "zh_cn" );
              }
            else if ( strcmp( LANGUAGE2, "英语" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE1) );
                  strcpy( from_language, "en" );
                  strcpy( recognize_language, "en_us" );
              }
            else if ( strcmp( LANGUAGE2, "日语" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE1) );
                  strcpy( from_language, "ja" );
                  strcpy( recognize_language, "ja_jp" );
              }
            else if ( strcmp( LANGUAGE2, "韩语" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE1) );
                  strcpy( from_language, "ko" );
                  strcpy( recognize_language, "ko_kr" );
              }
            else if ( strcmp( LANGUAGE2, "俄语" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE1) );
                  strcpy( from_language, "ru" );
                  strcpy( recognize_language, "ru-ru" );
              }
            else if ( strcmp( LANGUAGE2, "西班牙语" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE1) );
                  strcpy( from_language, "es" );
                  strcpy( recognize_language, "es_es" );
              }
            else if ( strcmp( LANGUAGE2, "阿拉伯语" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE1) );
                  strcpy( from_language, "ar" );
                  strcpy( recognize_language, "ar_il" );
              }
            else if ( strcmp( LANGUAGE2, "德语" ) == 0 )
            {
                  strcpy( to_language, get_to_language(LANGUAGE1) );
                  strcpy( from_language, "de" );
                  strcpy( recognize_language, "de_DE" );
            }
#if 0
            printf("[DEBUG after from_language] %s\r\n",from_language);
            printf("[DEBUG after to_language] %s\r\n",to_language);
            printf("[DEBUG after recognize_language] %s\r\n",recognize_language);
#endif
              translation_start = 1;
              reverse_translation = 1;
              reverse__result = 1;
              Jump_GuiWin(WIN_Tip);
              Djy_EventDelay(100*1000);
        }
   }
   else if(KeyValue == VOL_UP_KEY)
           {
               GDD_ResetTimer(tg_pCtrlTimer,500);
               if(u32g_Volume == 0)
               {
                   if(GetSpeakerState() == Speaker_off)
                       OpenSpeaker();  //开喇叭
               }

               if(u32g_Setting == CN_SETTING_VOLUME)
               {
                   if(u32g_Volume <= 90)
                       u32g_Volume +=10;
                   tg_SettingProgressbar.Pos = u32g_Volume;
                   if(en_StatueMachine == CtrlWaitExit)
                   {
                       PostMessage(tg_VolumeBar, MSG_ProcessBar_SETPOS,u32g_Volume,0);
                   }
                   else
                   {
                   tg_VolumeBar = CreateProgressBar("音量",WS_CHILD|PBS_HOR|WS_VISIBLE | WS_UNFILL,
                                                   20,150,200,20,tg_pMainWinHwn,CN_VOLUMEBAR,
                                                   (ptu32_t)&tg_SettingProgressbar,NULL);
                   }
                   en_StatueMachine = CtrlUpVolume;
               }
               else
               {
                   if(u32g_Bright <= 90)
                       u32g_Bright +=10;
                   tg_SettingProgressbar.Pos = u32g_Bright;
                   //TODO：在setting界面创建进度条
               }
               djy_audio_dac_ctrl(AUD_DAC_CMD_SET_VOLUME,&u32g_Volume);
           }
    else if(KeyValue == VOL_DOWN_KEY)
    {
       GDD_ResetTimer(tg_pCtrlTimer,500);
       if(u32g_Setting == CN_SETTING_VOLUME)
       {
           if(u32g_Volume >= 10)
               u32g_Volume -=10;
           tg_SettingProgressbar.Pos = u32g_Volume;
           if(en_StatueMachine == CtrlWaitExit)
           {
               PostMessage(tg_VolumeBar, MSG_ProcessBar_SETPOS,u32g_Volume,0);
           }
           else
           {
           tg_VolumeBar = CreateProgressBar("音量",WS_CHILD|PBS_HOR|WS_VISIBLE | WS_UNFILL,
                                           20,150,200,20,tg_pMainWinHwn,CN_VOLUMEBAR,
                                           (ptu32_t)&tg_SettingProgressbar,NULL);
           }
           en_StatueMachine = CtrlDownVolume;
       }
       else
       {
           if(u32g_Bright >= 10)
               u32g_Bright -=10;
           tg_SettingProgressbar.Pos = u32g_Bright;
           //TODO：在setting界面创建控制条
       }
       if(u32g_Volume < 10)
       {
           if(GetSpeakerState() == Speaker_on)
               CloseSpeaker();    //关喇叭
           u32g_Volume = 0;
       }
       djy_audio_dac_ctrl(AUD_DAC_CMD_SET_VOLUME,&u32g_Volume);
    }
    else if(KeyValue == POWER_KEY)
    {
        en_StatueMachine = CtrlPowerDown;
        GDD_ResetTimer(tg_pCtrlTimer,3000);
    }

    EndPaint(hwnd,hdc);
    return true;
}

static bool_t HmiTimer(struct WindowMsg *pMsg)
{
    HWND hwnd;
    struct WinTimer *pTmr;
    u16 TmrId;
    hwnd=pMsg->hwnd;
    TmrId=pMsg->Param1;
    pTmr=GDD_FindTimer(hwnd,TmrId);
    static bool_t last_charge_flag = false, now_charge_flag;

    if(TmrId == CN_TIMER_CTRL)
    {
        HDC hdc =BeginPaint(hwnd);
        switch(en_StatueMachine)
        {
            //TODO：在以下各case内，加上调音量或亮度代码
            case CtrlUpVolume:
                GDD_ResetTimer(pTmr,200);
                if(u32g_Volume == 0)
                {
                    if(GetSpeakerState() == Speaker_off)
                        OpenSpeaker(); //开喇叭
                }

                if(u32g_Volume <= 90)
                {
                    u32g_Volume += 10;
                    djy_audio_dac_ctrl(AUD_DAC_CMD_SET_VOLUME,&u32g_Volume);
                    PostMessage(tg_VolumeBar, MSG_ProcessBar_SETPOS,u32g_Volume,0);
                }
                break;
            case CtrlDownVolume:
                GDD_ResetTimer(pTmr,200);
                if(u32g_Volume >= 10)
                {
                    u32g_Volume -= 10;
                    if(u32g_Volume < 10)
                    {
                        if(GetSpeakerState() == Speaker_on)
                            CloseSpeaker(); //关喇叭
                        u32g_Volume = 0;
                    }
                    djy_audio_dac_ctrl(AUD_DAC_CMD_SET_VOLUME,&u32g_Volume);
                    PostMessage(tg_VolumeBar, MSG_ProcessBar_SETPOS,u32g_Volume,0);
                }
                break;
            case CtrlPowerDown:
                printf(">ShutOff\r\n");
                ShutOff_Statue = 1;
                eng_LastWindow = Get_SelectionWinType();
                DestroyAllChild(hwnd);      //删除当前控件。
                PostMessage(hwnd, MSG_REFRESH_UI, WIN_Settings_TurnOff, 0);
                GDD_StopTimer(tg_pCtrlTimer);
                break;
            case CtrlWaitExit:
                en_StatueMachine = CtrlNullOp;
                GDD_StopTimer(pTmr);
                DestroyAllChild(hwnd);
                PostMessage(hwnd, MSG_REFRESH_UI, Get_SelectionWinType(), 0);
            default:
                break;
        }
        EndPaint(hwnd,hdc);
    }
    else if(TmrId == CN_TIMER_POWERDOWN)        //表示定时关机定时器到了。
    {
        if(Get_update_flag()!=UpgradInProgress)
        {
            GDD_StopTimer(pTmr);
            Set_ShoutDown();
        }
    }
    return true;
}


static bool_t HmiMove_MainWin(struct WindowMsg *pMsg)
{
    struct WindowMsg Msg;

    Msg.hwnd   = pMsg->hwnd;
    Msg.Code   = MSG_NOTIFY;
    Msg.Param1 = (MSG_BTN_PEN_MOVE<<16|ENUM_MAIN_LIST_UI);
    Msg.Param2 = pMsg->Param1;
    Msg.ExData = NULL;

    HmiNotify_EasyTalk(&Msg);
    return true;
}
bool_t Refresh_GuiWin()
{
    return Refresh_SwitchWIn(tg_pMainWinHwn);
}
bool_t Jump_GuiWin(enum WinType nextInterface)
{
    return Jump_Interface(tg_pMainWinHwn,nextInterface);
}
void Main_GuiWin(void)
{
    Init_WinSwitch();
    //消息处理函数表
    static struct MsgProcTable s_gBmpMsgTable[] =
    {
        {MSG_CREATE,HmiCreate_MainWin},         //主窗口创建消息
        {MSG_PAINT,HmiPaint_MainWin},           //绘制消息
        {MSG_NOTIFY,HmiNotify_EasyTalk},        //子控件发来的通知消息
        {MSG_REFRESH_UI,HmiRefresh},               //刷新窗口消息
        {MSG_KEY_DOWN,HmiKeyDown},              //按键按下消息
        {MSG_KEY_UP,HmiKeyUp},                  //按键按下消息
        {MSG_TIMER,HmiTimer},                   //定时器消息响应函数
        {MSG_TOUCH_MOVE,HmiMove_MainWin},
    };

    static struct MsgTableLink  s_gBmpDemoMsgLink;

    s_gBmpDemoMsgLink.MsgNum = sizeof(s_gBmpMsgTable) / sizeof(struct MsgProcTable);
    s_gBmpDemoMsgLink.myTable = (struct MsgProcTable *)&s_gBmpMsgTable;
    tg_pMainWinHwn = GDD_CreateGuiApp((char*)MainWinCfgTab[ENUM_BACKGROUND].name,
                                &s_gBmpDemoMsgLink, 0x2000, CN_WINBUF_PARENT,0);
    tg_pCtrlTimer = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_CTRL,100);
    tg_pPowerDownTimer = GDD_CreateTimer(tg_pMainWinHwn, CN_TIMER_POWERDOWN,600000);
    GDD_ResetTimer(tg_pPowerDownTimer,600000);
}


int Register_Main_WIN()
{
    return Register_NewWin(WIN_Main_WIN,HmiCreate_MainWin,HmiPaint_MainWin,HmiNotify_MainWin);
}





