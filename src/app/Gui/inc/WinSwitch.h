#ifndef _GUIWIN_WINSWITCH_H_
#define _GUIWIN_WINSWITCH_H_
#include "stddef.h"
#include "gdd.h"
#ifdef __cplusplus
extern "C" {
#endif
enum WinType
{
    WIN_Main_WIN = 0,               //翻译主页面
    WIN_Wifi_Connecting,            //WiFi正在连接
    WIN_Wifi_ConnectSucess,         //Wifi连接成功
    WIN_Wifi_ConnectError,          //Wifi连接失败
    WIN_Recorder,
    WIN_MusicMain,                  //音乐主界面
    WIN_MusicList,                  //音乐列表界面
    WIN_Information,                //播报界面
    WIN_SpeedTranslation,           //录音速译
    WIN_DialogueTranslation,        //对话翻译
    WIN_LangSel,                    //语种选择
    WIN_LangSel2,                   //语种选择2
    WIN_Settings_TurnOff,           //关机
    WIN_Tip,                        //录音提示
    WIN_Settings,                   //设置
    WIN_Settings_WifiConnect,       //wifi设置
    WIN_FriendlyReminders,          //配网提示
    WIN_Max,                        //有效界面总数
    WIN_NotChange,                  //不改变窗口
    WIN_Error,                      //状态错误

};
//========================界面内注册接口定义===================================
int Register_Main_WIN();             //翻译机主页
int Register_DialogueTranslation();  //对话翻译
int Register_SpeedTranslation();     //录音速译
int Register_LangSel();              //语种选择
int Register_LangSel2();             //语种选择2
int Register_Wifi_Connecting();      //WIFI正在连接界面
int Register_Wifi_ConnectSucess();   //WIFI连接成功界面
int Register_Wifi_ConnectError();    //WIFI连接失败界面
int Register_MusicMain();            //音乐主界面
int Register_MusicList();            //音乐列表界面
int Register_InformationMain();      //播报界面
int Register_Settings_TurnOff();     //关机页面
int Init_WinSwitch();
enum WinType Get_SelectionWinType();
bool_t Refresh_SwitchWIn(HWND hwnd);
bool_t  Jump_Interface(HWND hwnd,enum WinType nextInterface);

typedef bool_t (*T_HmiCreate)(struct WindowMsg *pMsg);//界面控件创建
typedef bool_t (*T_HmiPaint)(struct WindowMsg *pMsg);//界面绘制
typedef enum WinType (*T_DoMsg)(struct WindowMsg *pMsg);//界面消息响应

struct GuiWinCb
{
    T_HmiCreate HmiCreate;//界面控件创建
    T_HmiPaint HmiPaint;//界面绘制
    T_DoMsg DoMsg;//界面消息响应
};
int Register_NewWin(enum WinType id,T_HmiCreate HmiCreate,T_HmiPaint HmiPaint, T_DoMsg DoMsg);
bool_t HmiRefresh(struct WindowMsg *pMsg);
bool_t HmiNotify_EasyTalk(struct WindowMsg *pMsg);


#define MSG_REFRESH_UI     (MSG_WM_USER + 0x0001)  //
#define CN_RECREAT_WIDGET       0
#define CN_ONLY_RESHOW          1
bool_t Refresh_GuiWin();
bool_t Jump_GuiWin(enum WinType nextInterface);
int Update_WinType(enum WinType type);

void Draw_Icon(HDC hdc,s32 x,s32 y,const RECT *rc,const char *bmp);
void Draw_Icon_2(HDC hdc,s32 x,s32 y,s32 width,s32 height,const char *bmp);

#ifdef __cplusplus
}
#endif

#endif /* _GUIWIN_WINSWITCH_H_ */
