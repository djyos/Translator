/*
 * BmpInfo.h
 *
 *  Created on: 2019年9月20日
 *      Author: czz
 */

#ifndef IBOOT_GUI_INFO_H_
#define IBOOT_GUI_INFO_H_
#include "stddef.h"
#include "gdd.h"
#ifdef __cplusplus
extern "C" {
#endif
#include "widget/gdd_progressbar.h"
#ifndef GUI_INFO_FILE_C_
extern PROGRESSBAR_DATA PowerProgressbar;
extern HWND Powerhwnd;
#endif

//界面元素枚举
enum widgettype
{
    type_background,
    widget_type_course,
    widget_type_Setting,
    widget_type_button,
    widget_type_msg,
    widget_type_picture,
    widget_type_text,
    widget_type_job,
    widget_type_time,
    widget_type_Line,
    widget_type_Power,
    widget_type_work,
    widget_type_Orencode,
    type_HomeWork_background,//作业背景
    type_MainLogo_background,//主界面logo背景
};

//界面元素信息
struct GUIINFO
{
    RECT position;//坐标，大小信息
    const char* name;
    enum widgettype type;
    int userParam;
} ;

//=================Bmp枚举=======================
enum Bmptype
{
    BMP_MainInterface_bmp,       //开机界面
    BMP_Background_bmp,          //背景图
    BMP_Background1_bmp,         //背景图
    BMP_Temperature_bmp,
    BMP_Weather_bmp,
    BMP_Aql_bmp,
    BMP_Wind_bmp,
    BMP_Close_bmp,

    BMP_Dark_bmp,                 //暗
    BMP_LightUp_bmp,              //亮

    BMP_Dot_bmp,
    BMP_time_0_bmp,
    BMP_time_1_bmp,
    BMP_time_2_bmp,
    BMP_time_3_bmp,
    BMP_time_4_bmp,
    BMP_time_5_bmp,
    BMP_time_6_bmp,
    BMP_time_7_bmp,
    BMP_time_8_bmp,
    BMP_time_9_bmp,

    BMP_Doing_bmp,
    BMP_Recorder_bmp,
    BMP_SystemSet_bmp,           //系统设置
    BMP_FileSet_bmp,             //文件管理
    BMP_CalenderSet_bmp,         //日历
    BMP_VideoSet_bmp,            //视频
    BMP_CLockSet_bmp,            //时钟
    BMP_PhotoSet_bmp,            //图库
    BMP_Translation_setting_bmp, //翻译网络设置
    BMP_SpeedTranslation_bmp,    //录音速译
    BMP_DialogueTranslation_bmp, //对话翻译
    BMP_return_bmp,              //返回按钮
    BMP_Exchange_bmp,            //翻译交换语言按键
    BMP_TranslationChange_bmp,   //翻译语言转换按键
    BMP_TranslationPlay_bmp,     //翻译录音按键
    BMP_TranslationPause_bmp,    //翻译录音暂停键
    BMP_MicroPhone_bmp,          //对话翻译提示键
    BMP_CN_bmp,                  //中国国旗
    BMP_arEG_bmp,                //阿拉伯埃及国旗
    BMP_DE_bmp,                  //德国国旗
    BMP_ES_bmp,                  //西班牙国旗
    BMP_JP_bmp,                  //日本国旗
    BMP_KR_bmp,                  //韩国国旗
    BMP_RU_bmp,                  //俄罗斯国旗
    BMP_UK_bmp,                  //英国国旗
    BMP_US_bmp,                  //美国国旗
    BMP_LANG_bmp,                //国旗选择
    BMP_LANG2_bmp,               //国旗选择2
    BMP_Volume_bmp,              //播放tip
    BMP_WifiSetLogo_bmp,         //wifi logo
    BMP_UpgradeSetLogo_bmp,      //update logo
    BMP_Device_BindingLogo_bmp,  //版本信息
    BMP_TermsOfService_bmp,      //服务条款
    BMP_AboutDevices_bmp,        //关于设备
    BMP_Round_bmp,               //关机
    BMP_RoundDot_bmp,            //取消
    BMP_Background_bmp_kouyuji,  //口语机背景
    BMP_MusicMain_bmp,
    BMP_CheckFileLogo_bmp,
    BMP_NetLoading_bmp,
    BMP_WIFI,
    BMP_MAXNNUM, //图标数量
    Bmp_NULL,
};

////=================所有压缩图片的枚举=======================
//enum CompressPicturetype
//{
//    Compress_Background_bmp,
//    Compress_Lights_bmp,
//    Compress_SetUp_bmp,
//    Compress_Music_bmp,
//    Compress_Luminance_bmp,
//    Compress_Close_bmp,
//    Compress_Dark_bmp,
//    Compress_LightUp_bmp,
//    Compress_LastSong_bmp,
//    Compress_Play_bmp,
//    Compress_Pause_bmp,
//    Compress_NextSong_bmp,
//    Compress_Recording_bmp,
//
//    Compress_CheckFileLogo_bmp,
//    Compress_NetLoading_bmp,
//    Compress_WifiConnected_bmp,
//    Compress_WifiDisconnected_bmp,
//
//
//    Compress_MainInterface_bmp, //开机界面，保证在最后一个就可以了
//};

void MainInterface(void);

const char * Get_BmpBuf(enum Bmptype type);

struct WinTime{
  int  year;
  u8   mon;        // months  - [1,12]
  u8   mday;       // day of the month - [1,31]
  u8   weeks;       //[1,7]
  u8   hour;       // hours since midnight - [0,23]
  u8   minute;        // minutesafter the hour - [0,59]
  u8   sec;
};
bool_t Wifi_Connectedflag(u8 flag);
bool_t Get_Wifi_Connectedflag();

unsigned int calc_sec1970(int Y, u8 M, u8 D, u8 h, u8 m, u8 s);
void Win_SetTime(void);
struct WinTime Win_GetTime();

void DispMainInterface(unsigned char *pic);

bool_t Win_UpdatePower(u8 power);
u32 Win_GetPower();

int findDate(char *s);
char*  Win_GetDate();
int getSuNingTime(void);
int GetTimeHourMinute(int *hour, int *min ,int *sec);

#endif /* IBOOT_GUI_INFO_H_ */
