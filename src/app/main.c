
//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * app_main.c
 *
 *  Created on: 2014-5-28
 *      Author: Administrator
 */
#include "stdint.h"
#include "stddef.h"
#include "stdlib.h"
#include <sys/socket.h>
#include <gpio_pub.h>
#include <app_flash.h>
#include "wlan_ui_pub.h"
#include "project_config.h"
#include "board.h"
#include "djyos.h"
#include "shell.h"
#include "mem/mem_mgr.h"
#include "GuiInfo.h"
#include "WinSwitch.h"
#include <Iboot_info.h>
#include <upgrade.h>
#include <wdt_soft.h>
#include <wdt_pub.h>
#include <qspi_pub.h>
#include <bk_timer_pub.h>

#define OPEN_INTERNAL_CHARGE      0
#define   RC_DEBUG_OFF     1
#define LONG_DOWN_TURN 0
#define ASM_GET_LR(fun)   asm( "mov %0, LR\n" :"=r"(fun):)

char power_percentage = 0;
extern int GetStatusHandle;                                    // 更新网络时间
extern char recognize_date[32];                                // 当前时间戳，RFC1123格式
extern u32 u32g_Volume;
int login_ok = 0;
s64 login_time = 0;
//char start_user_loading = 0;
volatile int flag_connected = 0;

extern  WIFI_CFG_T  LoadWifi;

extern void usb_charge_check_cb(void);
extern void usb_plug_func_open(void);
//extern int vbat_voltage_get(void);
//extern char Get_UpdateRunModet(void);
extern char vol_to_percentage(int assign);
extern enum UpgradeMode GetUpgradeMode(void);
extern void flash_protection_op(UINT8 mode, PROTECT_TYPE type);

extern int wav_play_from_file(char *path);
extern int mp3_play_from_file(char *path);
extern int WavIsBusy();
extern int Mp3IsBusy();
extern int mp3_module_init();
extern int wav_module_init();
extern int record_init();
extern int DevMgrInit();
//extern void MainInterface();
extern bool_t oss_player_event_init(void);
extern bool_t oss_download_event_init(void);
extern int web_upgrade_firmware(int check);
extern int Update_ServerInfo();

extern OSStatus bk_wlan_get_link_status(LinkStatusTypeDef *outStatus);
extern bool_t DhcpModeInit(int dhcp_mode);
void dhcpd_route_add_default();
extern void DelayCloseWiFi(int delay_ms);
extern void WiFiPowerUp();
extern bool_t Wifi_Connectedflag(u8 flag);

extern int SetWiFiStrength(int level);
extern void Set_Time_Ctrl();
extern bool_t Refresh_SoftUpdateWin(void);
extern bool_t Set_UpdateSource(char *param);
extern int vbat_voltage_get(void);
extern void deep_sleep(void);
#if LONG_DOWN_TURN
//typedef enum
//{
//    NO_KEY = 0,
////    PAUSE_PLAY_KEY,
////    VOL_UP_KEY,
////    VOL_DOWN_KEY,
////    RECORD_KEY,
//    POWER_KEY,
//} KEY_ID;

static char key_recognition(void)
{
    char current_key;

    if(djy_gpio_read(13))
        current_key = POWER_KEY;
    else
        current_key = NO_KEY;

//    printf("key = %d\r\n", current_key);
    return current_key;
}
#endif

char current_power(void)
{
    return power_percentage;
}

void set_cell_power(void)
{
    char i = 0, per = -1;

    while(i++ < 3)
    {
        per = vol_to_percentage(0);
        if(per != -1)
            break;
    }

    if(i <= 3)
        power_percentage = per;

//    printf(" ====== 拔充电器是的电量   = %d ======\r\n", power_percentage);
}

int is_wifi_connected()
{
    return flag_connected;
}

bool_t UsbIsCharge = false;
bool_t IsCharge(void)
{
    return UsbIsCharge;
}
void SetChargeFlag(enum ChargeFlag flag)
{
    if(flag == NoCharge)
        UsbIsCharge = false;
    else if(flag == OnCharge)
        UsbIsCharge = true;
}

ptu32_t Key_EventMain(void)
{
    while (1) {

        wav_play_from_file("/SD/16D.wav");
        while (WavIsBusy()) {
            printf("wait wav end ..\r\n");
            Djy_EventDelay(500*1000);
        }
        mp3_play_from_file("/SD/1.mp3");
        while (Mp3IsBusy()) {
            printf("wait mp3 end ..\r\n");
            Djy_EventDelay(500*1000);
        }
    }
    return 0;

}



ptu32_t timer_EventMain(void)
{
    int local_hour = 0;
    int local_min = 0;
    int hour, min, sec;
    while (1)
    {
        GetTimeHourMinute(&hour, &min, &sec);
        if (local_hour != hour || local_min != min)
        {
             local_hour = hour;
             local_min = min;
             SetClock(hour, min);
        }
        Refresh_GuiWin();
        Djy_EventDelay(1000*1000);
    }
}


bool_t timer_event_init(void)
{
    u16 evtt_timer;
    evtt_timer = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS + 1, 0, 0, timer_EventMain, 0, 0x1000, "timer_event");
    if(evtt_timer != CN_EVENT_ID_INVALID)
    {
        Djy_EventPop(evtt_timer, NULL, 0, 0, 0, 0);
    }else
    {
        printf("\r\evtt_timer Event Start Fail!\r\n");
    }

    return true;
}

bool_t Key_event_init(void)
{
    u16 evtt_Key;
    evtt_Key = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS, 0, 0, Key_EventMain, NULL, 0x1000, "Key_evevt");
    if(evtt_Key != CN_EVENT_ID_INVALID)
    {
        Djy_EventPop(evtt_Key, NULL, 0, 0, 0, 0);
    }else
    {
        printf("\r\evtt_recorder Event Start Fail!\r\n");
    }

    return true;
}

//void battery_low_voltage(int vol)
//{
//    char Vbat;
//    Vbat = vol_to_percentage(vol);
//    while(Vbat < 5)
//    {
//        Djy_EventDelay(30000*1000);
//        Vbat = vol_to_percentage(0);
//        printf("percent = %d %%  \r\n", Vbat);    //显示当前的电池电压
//        if(IsCharge() == false)
//        {
//            printf("电量不足5 %%，准备关机\r\n");
//            Djy_EventDelay(3000*1000);
//            deep_sleep();
//        }
//    }
//}

void CheckUpdate(void)
{   //开机时等待1分钟，判断是否连上网，连上网后是否需要进行升级
    Set_update_flag(NoUpdate);
//    for(i = 0; i < 60; i++)
//    {
//        Djy_EventDelay(1000*1000);
        if(flag_connected)
        {
            if((IsCharge()) || (current_power() >= 50))//电量大于50%才可以进行升级
            {
    //            if (WiFiOpen(10*1000) > 0) {
                if(web_upgrade_firmware(1) == 1)
                {
                    Set_update_flag(ReadyUpdate);
                }
                printf("info: download firmware from the network done!\r\n");
    //            }
    //            WiFiClose(10);
//                break;
            }
            else
            {
                printf("info: battery is low and cannot be Update!\r\n");
//                break;
            }
        }
//    }
}

void Power_on_control(void)
{
    int vol = 0;

#if OPEN_INTERNAL_CHARGE
    usb_plug_func_open();   //充电功能开启函数
//    Vbat = vbat_voltage_get();
//    printf("读到的指定的Vbat = %d  \r\n", vol);
    usb_charge_check_cb();      //判断是否开始和结束充电的函数
#endif
    if(usb_is_plug_in() != 0)
    {
        SetChargeFlag(OnCharge);
        vol = vbat_voltage_get();
        vol -= 150;
    }
    else
        SetChargeFlag(NoCharge);
    power_percentage = vol_to_percentage(vol);
    printf("========= percent = %d %%  \r\n", power_percentage);    //显示当前的电池电压
    if(power_percentage < 1)
    {
        if(usb_is_plug_in() == 0)
        {
            if(vbat_voltage_get() < 3470)
            {
                printf("电量不足，准备关机\r\n");
                Djy_EventDelay(3000*1000);
//                deep_sleep();
            }
        }
    }

}

tagWdt *ptWdtHard;
u32 wdtcounter = 0;
u32 WdtYipHook(tagWdt *wdt)
{
    wdtcounter = 0;
    return EN_BLACKBOX_DEAL_IGNORE;
}

bool_t spyk(char *param);
bool_t eventk(char *param);

//void timer0_isr(UINT8 param)
//{
////    CloseBackLight();
//    wdtcounter++;
//    if (wdtcounter > 15)
//    {
//        wdtcounter = 0;
//        printk("------wdt error------\r\n");
//        eventk(NULL);
//        spyk("0");
//        spyk("1");
//        spyk("2");
//        spyk("3");
//        spyk("4");
//        spyk("5");
//        spyk("6");
//        spyk("7");
//        spyk("8");
//        spyk("9");
//        spyk("10");
//        spyk("11");
//        spyk("12");
//        spyk("13");
//        spyk("14");
//        spyk("15");
//        spyk("16");
//        spyk("17");
//        spyk("18");
//        spyk("19");
//        spyk("20");
//        spyk("21");
//        spyk("22");
//        spyk("23");
//        spyk("24");
//        spyk("25");
//    }
////    Djy_DelayUs(1000);
////  printf("******timer interrupt\r\n");
////    OpenBackLight();
//}

//void mcu_init_timer0(void)
//{
//    timer_param_t param;
//    param.channel = 0;
//    param.div = 1;
//    param.period = 1000000;
//    param.t_Int_Handler= timer0_isr;
//
//    sddev_control(TIMER_DEV_NAME, CMD_TIMER_INIT_PARAM_US, &param);
//}
int GetNetInfo(char *ssid, int ssid_len, char *passwd, int pwd_len);
u32 xff8c,xff0c,xff8cnow,xff0cnow;

void wifi_connect()
{
    if(WifiLoad()==1)
    {
        DhcpModeInit(0);//dhcp client
    }
    else
    {
        DhcpModeInit(1);//dhcp server
        dhcpd_route_add_default();
    }
    if(LoadWifi.statue != 0x55)
    {
        DjyWifi_ApOpen("AI翻译机","");
        bool_t ModuleInstall_Portal(char *ssid,u32 ssidmaxlen,char *key,u32 keymaxlen);
        ModuleInstall_Portal(LoadWifi.WifiSsid,sizeof(LoadWifi.WifiSsid),LoadWifi.WifiPassWd,sizeof(LoadWifi.WifiPassWd));
        Djy_EventDelay(100*mS);
        DjyWifi_ApClose();
        WifiSave(LoadWifi.WifiSsid, LoadWifi.WifiPassWd);
        Djy_EventDelay(2000*1000);
        runapp(0);
    }
    DjyWifi_StaAdvancedConnect(LoadWifi.WifiSsid, LoadWifi.WifiPassWd);

    msg_sta_states status;
    while(status != MSG_GOT_IP) {
        printf("wait wifi <%d>... \r\n", status);
        status = mhdr_get_station_status();
        switch(status) {
            case MSG_GOT_IP:
                if (flag_connected != 1) {
                    flag_connected = 1;
                    Wifi_Connectedflag(1);
                    DjyWifi_StaConnectDone();
                }
                break;
            case MSG_IDLE:
            case MSG_CONNECTING:
            case MSG_PASSWD_WRONG:
            case MSG_NO_AP_FOUND:
            case MSG_CONN_FAIL:
            case MSG_CONN_SUCCESS:
            default:
                if (flag_connected != 0) {
                    flag_connected = 0;
                    Wifi_Connectedflag(0);
                }
                break;
        }
        Djy_EventDelay(1000*1000);
    }
}

void wx_wifi_conn()
{
    DhcpModeInit(0);//dhcp client

    const char ssid[] = "Z";
    const char wifi_pw[] = "1234567980";

    memcpy(LoadWifi.WifiSsid, ssid, strlen(ssid));
    memcpy(LoadWifi.WifiPassWd, wifi_pw, strlen(wifi_pw));

    WifiSave(LoadWifi.WifiSsid, LoadWifi.WifiPassWd);
    WiFiPowerUp();

    msg_sta_states status;
    while(status != MSG_GOT_IP) {
        printf("wait wifi <%d>... \r\n", status);
        status = mhdr_get_station_status();
        switch(status) {
            case MSG_GOT_IP:
                if (flag_connected != 1) {
                    flag_connected = 1;
                    Wifi_Connectedflag(1);
                    DjyWifi_StaConnectDone();
                }
                break;
            case MSG_IDLE:
            case MSG_CONNECTING:
            case MSG_PASSWD_WRONG:
            case MSG_NO_AP_FOUND:
            case MSG_CONN_FAIL:
            case MSG_CONN_SUCCESS:
            default:
                if (flag_connected != 0) {
                    flag_connected = 0;
                    Wifi_Connectedflag(0);
                }
                break;
        }
        Djy_EventDelay(1000*1000);
    }
}

ptu32_t djy_main(void)
{
    Heap_Add(QSPI_DCACHE_BASE, 0x7fff80, 64, 0, false, "PSRAM");       //把QSPI接的RAM添加到heap当中

    gpio_config(GPIO13, GMODE_INPUT_PULLDOWN);

    LP_BSP_ResigerGpioToWakeUpL4(0x2000,0);

    OpenScreen();

    DevMgrInit();

    MainInterface();

    Djy_EventDelay(1000*mS);

    power_percentage = vol_to_percentage(0);

    Main_GuiWin();

    wifi_connect();

//    ws_model_init();             // websocket初始化

    timer_event_init();

    recorderEventInit();           // 实时录音上传任务

    wsClientEventInit();           // WS客户端任务（用户上传、下载解析数据）

    wav_module_init();             // WAV播放

//    mp3_module_init();

    OpenSpeaker(); //开喇叭

    djy_audio_dac_ctrl(AUD_DAC_CMD_SET_VOLUME,&u32g_Volume);

    while(1)
    {
        if(flag_connected == 1)
        {
            getSuNingTime();
        }

#if 1
        printf("\r\n GetStatusHandle is %d\r\n",GetStatusHandle);
#endif

        if(GetStatusHandle == 1)
        {
            Recognize_Authorization(recognize_date);        // 语言转写初始化
            Djy_EventDelay(180*1000*mS);                    // 3min
            GetStatusHandle = 0;
        }
        Djy_EventDelay(1000*mS);
    }


    return 0;
}

bool_t electric_quantity(char *param)
{
    (void)param;
    int vol;
    power_percentage = vol_to_percentage(0);
    printf("  percent = %d %%  \r\n", power_percentage);    //显示当前的电池电压
    vol = vbat_voltage_get();
    printf("测量到的Vbat = %d  \r\n", vol);    //显示当前的电池电压

    return true;
}

bool_t PrintInfo(char * p)
{
    (void)p;
//    printf_Infolist();
    return true;
}
//bool_t testupdate(char *param)
//{
//    (void)param;
//    Set_UpdateSource("1");  //设置升级方式为，从ram获取app数据
//    update_flag = 1;
//    return true;
//}
bool_t format_efs(char *param)
{
    (void)param;
    if(Format("/efs"))
    {
        printf("格式化efs文件系统失败\r\n");
        return false;
    }
    else
    {
        printf("格式化efs文件系统成功\r\n");
        return true;
    }
}

bool_t erase_efs(char *param)
{
    u32 i;
    (void)param;
    flash_protection_op(0,FLASH_PROTECT_NONE);
    for(i = CFG_EMBFLASH_EFS_PART_START; i < CFG_EMBFLASH_EFS_PART_END; i++)
    {
        djy_flash_erase(i * 4096);
    }
    flash_protection_op(0,FLASH_PROTECT_ALL);
    return true;
}
extern void stub_debug(void);
bool_t init_jtag(char *param)
{
    (void)param;
    sddev_control(WDT_DEV_NAME, WCMD_POWER_DOWN, 0);
    stub_debug();
    return true;
}

bool_t Show_Wininfo(char *param)
{
    (void) param;
    int print_Wininfo();
    print_Wininfo();
    return true;
}

ADD_TO_ROUTINE_SHELL(initjtag,init_jtag,"重新初始化 :COMMAND:init_jtag+enter");
ADD_TO_ROUTINE_SHELL(eraseefs,erase_efs,"擦除efs文件系统 :COMMAND:erase_efs+enter");
ADD_TO_ROUTINE_SHELL(formatefs,format_efs,"格式化efs文件系统 :COMMAND:formatefs+enter");
//ADD_TO_ROUTINE_SHELL(testupdate,testupdate,"帮助信息格式 :help [cmd]");
ADD_TO_ROUTINE_SHELL(printinfo,PrintInfo,"打印信息");
//ADD_TO_ROUTINE_SHELL(touchclean,touchclean,"帮助信息格式 :help [cmd]");
ADD_TO_ROUTINE_SHELL(battery,electric_quantity,"帮助信息格式 :help [cmd]");
ADD_TO_ROUTINE_SHELL(wininfo,Show_Wininfo,"帮助信息格式 :help [cmd]");
