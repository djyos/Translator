#include "stdint.h"
#include "stddef.h"
#include "cpu_peri.h"
#include "stdint.h"
#include "stddef.h"
#include "cpu_peri.h"
#include "ring.h"
#include "app_ws_client.h"
#include "app_recorder.h"
#include "vad.h"

#define ADC_BUFFER_SIZE 3200

extern int translation_real_start;
extern int translation_start;


u16 gRecorderEvtt = CN_EVTT_ID_INVALID; // recorder事件类型

#if (1 == USE_REC_BUFFER)
/// recorder buffer management - START -
// 每次读128Byte，积攒到512了，写一次到录音序列缓冲区 sRecBuffer 中
static int rbIdx = 0;
static uint8_t tempReadBuffer[REC_BUFFER_A_SIZE] = {0};
static int ssIdx = 0;
static int16_t speechSeg[160] = {0};  // 20 ms in 8 kHz.

// 固定长度，每次写数据512B，每次读数据512B
static uint8_t sRecBuffer[REC_BUFFER_CNT][REC_BUFFER_A_SIZE] = {0};// __attribute__ ((section(".loopbuf"))) = {0};
static int wrIdx = 0; // 写位置，大于REC_BUFFER_CNT后回环
static int rdIdx = 0; // 读位置，大于REC_BUFFER_CNT后回环
void recorderBufferInit()
{
    wrIdx = 0;
    rdIdx = 0;
}

// 512B数据
int recorderBufferWrite(uint8_t *dat)
{
    // 没有空间可写
    if (wrIdx < REC_BUFFER_A_SIZE-1)
    {
        if ((wrIdx+1) == rdIdx)
        {
            return 0;
        }
    }
    else
    {
        if (0 == rdIdx)
        {
            return 0;
        }
    }

//    printf("%06d wr %03d\r\n", (int)(DjyGetSysTime()/1000), wrIdx);
    memcpy(sRecBuffer[wrIdx], dat, REC_BUFFER_A_SIZE);
    wrIdx++;
    if (wrIdx >= REC_BUFFER_CNT)
    {
        wrIdx = 0;
    }

    return REC_BUFFER_A_SIZE;
}

// 512B数据
int recorderBufferRead(uint8_t *dat)
{
    // 没有数据可读
    if (wrIdx == rdIdx)
    {
        return 0;
    }

//    printf("%06d rd %03d\r\n", (int)(DjyGetSysTime()/1000), rdIdx);
    memcpy(dat, sRecBuffer[rdIdx], REC_BUFFER_A_SIZE);
    rdIdx++;
    if (rdIdx >= REC_BUFFER_CNT)
    {
        rdIdx = 0;
    }

    return REC_BUFFER_A_SIZE;
}
/// recorder buffer management - END -
#endif

static int gSpeedchDisapeareTMs = 0;
// 当返回1的时候表示，已经检测到静音状态了
int recorderVadArrive(void)
{
    if (gSpeedchDisapeareTMs && ((int)(DjyGetSysTime()/1000) > (gSpeedchDisapeareTMs+400))) // 750
    {
        gSpeedchDisapeareTMs = 0;
        return 1;
    }
    else
    {
        return 0;
    }
}

//
ptu32_t recorder_event_main(void)
{
    uint32_t lastKeyRecordPressDown = 0;

//    djy_linein_adc_open(ADC_BUFFER_SIZE, 1, audio_sample_rate_16000, AUD_ADC_LINEIN_DETECT_PIN);

    djy_audio_adc_open(ADC_BUFFER_SIZE, 1, audio_sample_rate_16000, AUD_ADC_LINEIN_DETECT_PIN);

    void *vadInst = WebRtcVad_Create();
    if (vadInst == NULL)
    {
        printf("[INFO] VAD instance create fail..\r\n");
        return -1;
    }

    int status = WebRtcVad_Init(vadInst);
    if (status != 0)
    {
        printf("VAD init fail\n");
        WebRtcVad_Free(vadInst);
        return -1;
    }

    // 0-Qualitymode. 优质的
    // 1-Lowbitratemode. 低比特率的
    // 2-Aggressivemode. 侵略性的，激进的
    // 3-Veryaggressivemode.
    status = WebRtcVad_set_mode(vadInst, 3);
    if (status != 0)
    {
        printf("VAD mode setup fail\n");
        WebRtcVad_Free(vadInst);
        return -1;
    }

    while (1)
    {
        if (translation_real_start||translation_start)
        {
            if (0 == lastKeyRecordPressDown)
            {
                lastKeyRecordPressDown = 1;
                // 1. 准备发送（建立连接）
                wsClientRequestCommand(WS_CLIENT_REQ_TYPE_PREPARE);
                Djy_EventDelay(500*mS);
                audio_adc_clear(); //清除DMA数据
#if (1 == USE_REC_BUFFER)
                recorderBufferInit();
#endif
                continue;
            }

#if (1 == USE_REC_BUFFER)
            if (1 == wsClientIsConnected()) // 连接成功之后（判断状态）
            {
                if (0 == gSpeedchDisapeareTMs)
                {
                    gSpeedchDisapeareTMs = (int)(DjyGetSysTime()/1000) + 3000; // 3秒延迟
                }

                // 5MS读取128Bytes；10MS读取256B；1S读取25600B
                // 20MS读取了512B数据
                uint32_t shouldRd = REC_BUFFER_A_SIZE-rbIdx;
                if (shouldRd > 256)
                {
                    shouldRd = 256;
                }
                uint32_t rdLength = djy_audio_adc_read(&tempReadBuffer[rbIdx], shouldRd);
                rbIdx += rdLength;

                if (rbIdx >= REC_BUFFER_A_SIZE)
                {
                    if (REC_BUFFER_A_SIZE != recorderBufferWrite(tempReadBuffer))
                    {
                        Djy_EventDelay(100*mS);
                    }
                    else // 已经积攒了 512B （20MS）数据了
                    {
                        // 这儿20MS，如果16kHz的话，应该有320个数据
                        // 而实际上这儿只有 512B（即256个）数据。
                        // 如果采样率是 8kHz 的，则有 160 个数据
                        // 我们要将这 256 个数据映射成 160 个数据（1.6:1的关系）
                        uint32_t ssIdx = 0;
                        for (uint32_t i=0; (i+1)<sizeof(tempReadBuffer); i+=2)
                        {
                            int16_t tmpValue = (int16_t)(tempReadBuffer[i+0] + (tempReadBuffer[i+1]<<8));

                            if (i/2 *10/16 >= ssIdx)
                            {
                                speechSeg[ssIdx++] = tmpValue;

                                if (ssIdx >= sizeof(speechSeg))
                                {
                                    break;
                                }
                            }
                        }

                        int nVadRet = WebRtcVad_Process(vadInst, 8000, speechSeg, sizeof(speechSeg)/sizeof(int16_t), 0);
//                        printf("nVadRet is %d\r\n", nVadRet);
                               if (nVadRet > 0)
                               {
                                   gSpeedchDisapeareTMs = (int)(DjyGetSysTime()/1000);
                               }
                               rbIdx = 0;
                           }
                           ssIdx = 0;
                       }

                Djy_EventDelay(10*mS);
            }
#endif
            else
            {
                Djy_EventDelay(10*mS);
            }
        }
        else
        {
            if (1 == lastKeyRecordPressDown)
            {
                lastKeyRecordPressDown = 0;
                // 2. 结束录音了，发送完缓冲区中的数据即可了
                wsClientRequestCommand(WS_CLIENT_REQ_TYPE_COMPLETE);
                gSpeedchDisapeareTMs = 0;
            }

            Djy_EventDelay(100*mS);
        }
    }

    WebRtcVad_Free(vadInst);
    return 0;
}

void recorderEventInit(void)
{
    gRecorderEvtt = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_REAL, 0, 0,
            recorder_event_main, NULL, 0x1000, "recorder function");

    if (CN_EVTT_ID_INVALID != gRecorderEvtt)
    {
        Djy_EventPop(gRecorderEvtt, NULL, 0, 0, 0, 0);
    }
    else
    {
        printf("[DEBUG] Djy_EvttRegist-recorder_event_main error\r\n");
    }
}

