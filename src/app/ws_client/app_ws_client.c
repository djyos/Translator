#include "stdint.h"
#include "stddef.h"
#include "cpu_peri.h"
#include "stdint.h"
#include "stddef.h"
#include "cpu_peri.h"
#include "app_recorder.h"
#include "app_ws_client.h"
#include "WebSocket_Client.h"
#include "app_recorder.h"
#include <sys/socket.h>
#include "music_control.h"
#include "../Gui/inc/WinSwitch.h"
#include "cJSON.h"

#define KEY_MSG_MAX_CNT 1                               // 按键消息的个数
u16     gWsClientEvtt   = CN_EVTT_ID_INVALID;           // wsClient事件类型
struct MsgQueue *pSMsgQueueReq  = NULL;                 // 请求消息队列
static int  sWsStage    = 0;                            // 0表示没有联网状态；1表示已经发送了JSCON，可以发送BIN数据了；2表示发送了空包了
unsigned char   word_storage[1024];                     // 存储录音文字
unsigned char   word_storage_real[1024];                // 实时存储录音文字
int     recvlen     = 0;
int     real_temp   = 0;                                // 实时翻译开关

extern char recognize_language[16];                     // 识别的语言
extern char ws_recognize_url[512];
extern int  translation_real_start;
extern u32  reverse__result;                            // 逆向翻译保存结果开关 0代表关闭 1代表开启
extern char LANGUAGE1[16];
extern char LANGUAGE2[16];
extern unsigned char    word_storage_result_to[512];    // 翻译结果
extern unsigned char    word_storage_result_from[512];  // 录音结果
extern char ent[12];
extern u32  playing_status;
extern char recognize_date[32];                         // 当前时间戳，RFC1123格式

void wsClientRequestCommand( WsClientReqType type )
{
    if ( pSMsgQueueReq )
    {
        uint8_t temp = type;
        MsgQ_Send( pSMsgQueueReq, &temp, 1, 1000, CN_MSGQ_PRIO_NORMAL );
    }
}


int wsClientIsConnected( void )
{
    if ( 1 == sWsStage )
    {
        return(1);
    }else {
        return(0);
    }
}


ptu32_t ws_client_event_main( void )
{
    uint8_t         msg;
    int         wsfd            = -1;
    int         Reconnect       = 0;
    static uint8_t      tempReadBuffer[512 * 4] = { 0 };
    char            recognize_data[3200]    = { 0 };
    int         ret         = 0;
    WebsocketData_Type  recvWsDataType;
    /* int wsStage = 0; // 0表示没有联网状态；1表示已经发送了JSCON，可以发送BIN数据了；2表示发送了空包了 */
    int wsStartTs = 0, wsSentCount = 0;

    while ( 1 )
    {
        if ( 0 == Get_Wifi_Connectedflag() )
        {
            Djy_EventDelay( 50 * mS );
            continue;
        }

        if ( MsgQ_Receive( pSMsgQueueReq, &msg, 1, 200 ) )
        {
            /* 1. 连接到WS服务器 */
            if ( WS_CLIENT_REQ_TYPE_PREPARE == msg )
            {
                printf( "WS_CLIENT_REQ_TYPE_PREPARE start a ws message.\r\n" );
                if ( wsfd <= 0 )
                {
                    /* 获得URL */
                    if ( (wsfd = WebSocket_LinkServer( ws_recognize_url ) ) <= 0 )
                    {
                        wsfd = -1;
                        printf( "222WebSocket_LinkServer fail!\r\n" );
                        getSuNingTime();
                        Recognize_Authorization(recognize_date);        // 语言转写初始化
                        continue;
                    }
                    printf( "\r\n TESTTTTTT WebSocket_LinkServer s is %d\r\n", wsfd );
                    printf( "\r\n WebSocket_LinkServer ok!\r\n" );
                }
                int rdLength    = 0;
                int rdTotalLength   = 0;
                do /* 读取最大 tempReadBuffer 数据，越大速度越快 */
                {
                    rdLength    = recorderBufferRead( &tempReadBuffer[rdTotalLength] );
                    rdTotalLength   += rdLength;
                }
                while ( (rdLength > 0) && (rdTotalLength < sizeof(tempReadBuffer) ) );
                char base64_tempReadBuffer[rdTotalLength * 4 / 3];
                base64_encode( tempReadBuffer, rdTotalLength, base64_tempReadBuffer );
                if( translation_real_start == 1)
                {
                    strcpy(recognize_language,"zh_cn");
                }
#if 0
                printf( "[DEBUG translation_real_start] %d\r\n", translation_real_start );
                printf( "[DEBUG recognize_language] %s\r\n", recognize_language );
#endif
                sprintf( recognize_data, "{\"common\":{\"app_id\":\"5e572f0f\"},\"business\":{\"language\":\"%s\",\"domain\":\"iat\",\"accent\":\"mandarin\"},\"data\":{\"status\":0,\"format\":\"audio/L16;rate=16000\",\"encoding\":\"raw\",\"audio\":\"%s\"}}", recognize_language, base64_tempReadBuffer );
                size_t payload_length = strlen( recognize_data );
/*                printf( "111===payload_length:%d\r\n", payload_length ); */
                ret = WebSocket_send( wsfd, recognize_data, payload_length, true, WDT_TXTDATA );

                if ( ret <= 0 )
                {
                    close( wsfd );
                    wsfd = -1;
                    continue;
                }

                if ( !Reconnect )
                {
/*                  Djy_EventDelay(500*mS); */
                    mp3_stream_reset();
                }

                sWsStage = 1;
                audio_adc_clear();                                                      /* 清除DMA数据 */
            }
            /* 3. WS数据发送完成 */
            else if ( WS_CLIENT_REQ_TYPE_COMPLETE == msg )
            {
                if ( real_temp )
                {
                    memset( word_storage_real, 0, strlen( word_storage_real ) );    /* 清空实时翻译结果 */
                    memset( word_storage, 0, strlen( word_storage ) );              /* 清空翻译结果 */
                }
                Reconnect   = 0;
                sWsStage    = 2;
                printf( "WS_CLIENT_REQ_TYPE_COMPLETE complete a ws message.\r\n" );

                if ( wsfd <= 0 )
                {
                    continue;
                }

                /* 再发送一次数据过去 */
                while ( 1 )
                {
                    int rdLength    = 0;
                    int rdTotalLength   = 0;
                    do /* 读取最大 tempReadBuffer 数据，越大速度越快 */
                    {
                        rdLength    = recorderBufferRead( &tempReadBuffer[rdTotalLength] );
                        rdTotalLength   += rdLength;
                    }
                    while ( (rdLength > 0) && (rdTotalLength < sizeof(tempReadBuffer) ) );

                    if ( rdTotalLength > 0 )
                    {
                        char base64_tempReadBuffer[rdTotalLength * 4 / 3];
                        base64_encode( tempReadBuffer, rdTotalLength, base64_tempReadBuffer );
                        memset( recognize_data, 0, strlen( recognize_data ) );
                        sprintf( recognize_data, "{\"data\":{\"status\":1,\"format\":\"audio/L16;rate=16000\",\"encoding\":\"raw\",\"audio\":\"%s\"}}", base64_tempReadBuffer );
/*                        printf( "333===payload_length:%d\r\n", strlen( recognize_data ) ); */
                        ret = WebSocket_send( wsfd, recognize_data, strlen( recognize_data ), true, WDT_BINDATA );
                        printf( "[%d]WebSocket_send(%d) ret is %d\r\n", (int) (DjyGetSysTime() / 1000), rdTotalLength, ret );
                        if ( ret <= 0 )
                        {
                            close( wsfd );
                            wsfd = -1;
                            break;
                        }
                        else
                        {
                            wsSentCount += ret;
                        }
                    }
                    else
                    {
                        break;
                    }
                }


                if ( -1 == wsfd )
                {
                    continue;
                }

                printf( "WS_CLIENT_REQ_TYPE_COMPLETE complete a ws message.\r\n" );
                printf( "Send Speed is %d Bytes/Sec (%d/%d)\r\n", wsSentCount * 1000 / ( (int) ( (DjyGetSysTime() / 1000 - wsStartTs) ) ), wsSentCount, (int) ( (DjyGetSysTime() / 1000 - wsStartTs) ) );
                wsStartTs = 0, wsSentCount = 0;


/* ***************************************************单次翻译模块********************************************************** */

                if ( real_temp != 1 )
                {
                    char *regognize_end_sign = "{\"data\":{\"status\":2}}";
                    ret = WebSocket_send( wsfd, regognize_end_sign, strlen( regognize_end_sign ), true, WDT_BINDATA );

                    mp3_stream_reset();
                    if ( ret < 0 )
                    {
                        printf( "\r\nError in ws_send: %d\r\n", ret );
                        close( wsfd );
                        wsfd = -1;
                        continue;
                    }
                    /* 4. 接收WS数据  单次按键翻译 */
                    printf( "Ready to exec WebSocket_recv\r\n" );
                    uint8_t *DataRecvBuf;
                    uint8_t *wsBuf = malloc( 4 );
                    memset( wsBuf, 0, 4 );
                    ret = WebSocket_recv( wsfd, wsBuf, 4, &recvWsDataType );
                    DataRecvBuf = malloc( recvlen );
                    memset( DataRecvBuf, 0, recvlen );
                    getrecvbuf( DataRecvBuf, wsfd, recvlen );
//                    printf( "DataRecvBuf Result:\r\n%s\r\n", DataRecvBuf ); /* 获取返回结果 */
                    if ( DataRecvBuf )
                    {
                        cJSON   * cjson     = cJSON_Parse( DataRecvBuf );
                        cJSON   * data      = cJSON_GetObjectItem( cjson, "data" );
                        cJSON   * result    = cJSON_GetObjectItem( data, "result" );
                        cJSON   * ws        = cJSON_GetObjectItem( result, "ws" );
                        cJSON   * ssss      = cJSON_GetArraySize( ws );
                        for ( int i = 0; i < ssss; i++ )
                        {
                            cJSON   * cww   = cJSON_GetArrayItem( ws, i );
                            cJSON   * cw    = cJSON_GetObjectItem( cww, "cw" );
                            cJSON   * dddd  = cJSON_GetArraySize( cw );
                            for ( int i = 0; i < dddd; i++ )
                            {
                                cJSON   * ww    = cJSON_GetArrayItem( cw, i );
                                cJSON   * w = cJSON_GetObjectItem( ww, "w" );
//                        printf( "[INFO w->valuestring] %s\r\n", w->valuestring );
                                strcat( word_storage, w->valuestring );
                            }
                        }
                        cJSON_Delete( cjson );
                    }
#if 1
                    printf( "\r\n[DEBUG word_storage] %s\r\n",word_storage);
//                    printf( "[DEBUG reverse__result] %d\r\n",reverse__result);
//                    printf( "[DEBUG playing_status] %d\r\n", playing_status );
#endif
                    getTranslationResult(word_storage, strlen(word_storage));
                    if( reverse__result == 0)
                    {
                        if ( strcmp( LANGUAGE1, "中文" ) == 0 )
                        {
                            sprintf( ent, "intp65" );
                        }
                        else if ( strcmp( LANGUAGE1, "英语" ) == 0 )
                        {
                            sprintf( ent, "intp65_en" );
                        }
                        else  {
                            sprintf( ent, "xtts" );
                        }
#if 1
                        printf( "\r\n[DEBUG ent] %s\r\n", ent );
                        printf( "\r\n[DEBUG strlen(word_storage_result_to)] %s\r\n", strlen(word_storage_result_to) );
#endif
                        if( strlen(word_storage_result_to) != 0)
                        {
                            playing_status = 1;
                            tts_play( word_storage_result_to, ent );
                        }
                    }
                    else if( reverse__result == 1)
                    {
                        if ( strcmp( LANGUAGE2, "中文" ) == 0 )
                        {
                            sprintf( ent, "intp65" );
                        }
                        else if ( strcmp( LANGUAGE2, "英语" ) == 0 )
                        {
                            sprintf( ent, "intp65_en" );
                        }
                        else  {
                            sprintf( ent, "xtts" );
                        }
#if 1
                        printf( "\r\n[DEBUG ent] %s\r\n", ent );
                        printf( "\r\n[DEBUG strlen(word_storage_result_to)] %s\r\n", strlen(word_storage_result_from) );
#endif
                        if( strlen(word_storage_result_from) != 0)
                        {
                            playing_status = 1;
                            tts_play( word_storage_result_from, ent );
                        }
                    }
                    reverse__result = 0;
                    memset( word_storage, 0, strlen( word_storage ) );
                    if ( wsfd > 0 )
                    {
                        close( wsfd );
                        wsfd = -1;
                    }

                    if ( wsBuf )
                    {
                        free( wsBuf );
                    }

                    if ( DataRecvBuf )
                    {
                        free( DataRecvBuf );
                    }
                }
                real_temp = 0; /* 录音速译开关复位 */
            }
        }
/* ***************************************************单次翻译模块********************************************************** */
        else{
            /* 2. 发送数据到WS服务器 TODO 检车是否在录音中 */
            if ( 1 == sWsStage )
            {
                if ( wsfd > 0 )
                {
                    int rdLength    = 0;
                    int rdTotalLength   = 0;
                    do /* 读取最大 tempReadBuffer 数据，越大速度越快 */
                    {
                        rdLength    = recorderBufferRead( &tempReadBuffer[rdTotalLength] );
                        rdTotalLength   += rdLength;
                    }
                    while ( (rdLength > 0) && (rdTotalLength < sizeof(tempReadBuffer) ) );
                    if ( rdTotalLength > 0 )
                    {
                        if ( 0 == wsStartTs )
                        {
                            wsStartTs = (int) (DjyGetSysTime() / 1000); /* ws运行时间 */
                        }
                        memset( recognize_data, 0, strlen( recognize_data ) );
/*                        printf("sizeof(tempReadBuffer)=============%d\r\n",sizeof(tempReadBuffer)); */
                        char base64_tempReadBuffer[rdTotalLength * 4 / 3];
                        base64_encode( tempReadBuffer, rdTotalLength, base64_tempReadBuffer );
                        sprintf( recognize_data, "{\"data\":{\"status\":1,\"format\":\"audio/L16;rate=16000\",\"encoding\":\"raw\",\"audio\":\"%s\"}}", base64_tempReadBuffer );
/*                        printf( "222===payload_length:%d\r\n", strlen( recognize_data ) ); */
                        ret = WebSocket_send( wsfd, recognize_data, strlen( recognize_data ), true, WDT_BINDATA );
/*                        printf( "[%d]WebSocket_send(%d) ret is %d,tempReadBuffer is %d\r\n", (int) (DjyGetSysTime() / 1000), rdTotalLength, ret, strlen( tempReadBuffer ) ); */


/* ***************************************************实时翻译模块********************************************************** */

                        if ( ( (DjyGetSysTime() / 1000) - wsStartTs > 2000) && (translation_real_start) )
                        {
                            real_temp = 1;
                            char *regognize_end_sign = "{\"data\":{\"status\":2}}";
                            ret = WebSocket_send( wsfd, regognize_end_sign, strlen( regognize_end_sign ), true, WDT_BINDATA );
                            uint8_t *DataRecvBuf;
                            uint8_t *wsBuf = malloc( 4 );
                            memset( wsBuf, 0, 4 );
                            ret     = WebSocket_recv( wsfd, wsBuf, 4, &recvWsDataType );
                            DataRecvBuf = malloc( recvlen );
                            memset( DataRecvBuf, 0, recvlen );
                            getrecvbuf( DataRecvBuf, wsfd, recvlen );
                            if ( DataRecvBuf )
                            {
                                cJSON   * cjson     = cJSON_Parse( DataRecvBuf );
                                cJSON   * data      = cJSON_GetObjectItem( cjson, "data" );
                                cJSON   * result    = cJSON_GetObjectItem( data, "result" );
                                cJSON   * ws        = cJSON_GetObjectItem( result, "ws" );
                                cJSON   * ssss      = cJSON_GetArraySize( ws );
                                for ( int i = 0; i < ssss; i++ )
                                {
                                    cJSON   * cww   = cJSON_GetArrayItem( ws, i );
                                    cJSON   * cw    = cJSON_GetObjectItem( cww, "cw" );
                                    cJSON   * dddd  = cJSON_GetArraySize( cw );
                                    for ( int i = 0; i < dddd; i++ )
                                    {
                                        cJSON   * ww    = cJSON_GetArrayItem( cw, i );
                                        cJSON   * w = cJSON_GetObjectItem( ww, "w" );
/*                                         printf( "[INFO REAL w->valuestring] %s\r\n", w->valuestring ); */
                                        strcat( word_storage_real, w->valuestring );
                                    }
                                }
                                cJSON_Delete( cjson );
                            }
#if 0
                            printf( "[INFO REAL word_storage_real] %s\r\n", word_storage_real );
#endif
                            getTranslationResult( word_storage_real ,strlen(word_storage_real));
                            if ( wsBuf )
                            {
                                free( wsBuf );
                            }

                            if ( DataRecvBuf )
                            {
                                free( DataRecvBuf );
                            }
                            wsStartTs = 0, wsSentCount = 0;
                        }

/* ***************************************************实时翻译模块********************************************************** */


                        if ( ret <= 0 )
                        {
                            close( wsfd );
                            wsfd        = -1;
                            Reconnect   = 1;
                            wsClientRequestCommand( WS_CLIENT_REQ_TYPE_PREPARE );
                        }
                        else
                        {
                            wsSentCount += ret;
                        }
                    }else
                    {
                        Djy_EventDelay( 100 * mS );
                    }
                }
            }
        }
    }
    return(0);
}


void wsClientEventInit( void )
{
    pSMsgQueueReq = MsgQ_Create( KEY_MSG_MAX_CNT, 1, CN_MSGQ_TYPE_FIFO );
    if ( NULL == pSMsgQueueReq )
    {
        printf( "[INFO] wsClientEventInit MsgQ_Create fail, return.\r\n" );
        return;
    }

    gWsClientEvtt = Djy_EvttRegist( EN_CORRELATIVE, CN_PRIO_REAL, 0, 0,
                    ws_client_event_main, NULL, 0x1800, "wsClient function" );

    if ( CN_EVTT_ID_INVALID != gWsClientEvtt )
    {
        Djy_EventPop( gWsClientEvtt, NULL, 0, 0, 0, 0 );
    }
    else
    {
        printf( "[DEBUG] Djy_EvttRegist-ws_client_event_main error\r\n" );
    }
}


