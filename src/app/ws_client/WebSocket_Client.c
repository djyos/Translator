
//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * WebSocket_Client.c
 *
 *  Created on: 2019-4-16
 *  czz
 */

#include "WebSocket_Client.h"

#include <stdint.h>
#include <endian.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include "net/netdb.h"

// 连接服务器
#define WEBSOCKET_LOGIN_CONNECT_TIMEOUT  1000            // 登录连接超时设置 1000ms
#define WEBSOCKET_LOGIN_RESPOND_TIMEOUT  (1000 + WEBSOCKET_LOGIN_CONNECT_TIMEOUT) // 登录等待回应超时设置 1000ms
#define WEBSOCKET_SHAKE_KEY_LEN     16   // 生成握手key的长度
#define WS_MAX_HOSTNAME_LENGTH                          80
#define WS_MAX_PATH_AND_QUERY_LENGTH                    500
typedef struct {
    char hostname[WS_MAX_HOSTNAME_LENGTH+1];
    unsigned short port;
    char path_and_query[WS_MAX_PATH_AND_QUERY_LENGTH+1];
    bool is_ssl;
} ws_endpoint;

static int socket_recv(int fd, char *buf, int len, unsigned int timeout_ms, int *err)
{
    int recved = 0;
    int cnt = 0;
    int error = 0;
    unsigned int time_spend = 0;
    s64 mark=DjyGetSysTime();
    if (err==NULL) err = &error;
    *err = 0;
    while (recved < len) {
        cnt = recv(fd , &buf[recved] , len-recved, 0);
        if (cnt > 0) {
            recved += cnt;
        }
        if (errno == ESHUTDOWN) {
            *err = -1;
            break;
        }
        time_spend = (unsigned int)((DjyGetSysTime()-mark)/1000);
        if (time_spend > timeout_ms) {
            *err = -2;
            break;
        }
        if (cnt == 0) {
            Djy_EventDelay(1000);
        }
    }
    return recved;

}
static int socket_recv2(int fd, char *buf, int len, unsigned int timeout_ms, int *err)
{
    u32 to_send = 1*1000*mS;
    u32 to_recv = 500*mS;
    #define TO_RECV_COUNT_MAXT  3
    int to_recv_cnt = TO_RECV_COUNT_MAXT;
    setsockopt(fd, SOL_SOCKET, SO_SNDTIMEO, &to_send, sizeof(to_send));
    setsockopt(fd, SOL_SOCKET, SO_RCVTIMEO, &to_recv, sizeof(to_recv));
    int recved = 0;
    int cnt = 0;
    int error = 0;
    unsigned int time_spend = 0;
    s64 mark=DjyGetSysTime();
    if (err==NULL) err = &error;
    *err = 0;
    while (1) {
//        printf("[DEBUG DEBUG]\r\n");
        cnt = recv(fd , &buf[recved] , len-recved, 0);
//        printf("[DEBUG cnt] %d\r\n",cnt);
        if (cnt > 0) {
            to_recv_cnt = TO_RECV_COUNT_MAXT;
            recved += cnt;
//            printf("[DEBUG recved] %d\r\n",recved);
        }

        if (errno == ESHUTDOWN) {
            *err = -1;
            break;
        }
        if (cnt <=0) {
           /**
            * 不等待 直接跳出
            */
            buf[recved]='\0';
            break;
           /**
            printf("socket_recv is -1\r\n");
            if (to_recv_cnt-- <= 0)
            {
                buf[recved]='\0';
                break;
            }
            */

        }
    }
    return recved;

}


static int socket_send(int fd, char *buf, int len, unsigned int timeout_ms, int *err)
{
    int sended = 0;
    int cnt = 0;
    int error = 0;
    unsigned int time_spend = 0;
    s64 mark=DjyGetSysTime();
    if (err==NULL) err = &error;
    *err = 0;
    while (sended < len) {
        cnt = send(fd, &buf[sended], len-sended, 0);
        if (cnt > 0) {
            sended += cnt;
        }
        if (errno == ESHUTDOWN) {
            *err = -1;
            break;
        }
        time_spend = (unsigned int)((DjyGetSysTime()-mark)/1000);
        if (time_spend > timeout_ms) {
            *err = -2;
            break;
        }
        if (cnt == 0) {
            Djy_EventDelay(1000);
        }
    }
    return sended;
}

//==================== delay ms ====================
//void WebSocket_delayms(unsigned int ms)
//{
//    struct timeval tim;
//    tim.tv_sec = ms/1000;
//    tim.tv_usec = (ms%1000)*1000;
//    select(0, NULL, NULL, NULL, &tim);
//}
//==================== 加密方法BASE64 ====================






//base64编/解码用的基础字符集
static const char websocket_base64char[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/*******************************************************************************
 * 名称: websocket_base64_encode
 * 功能: ascii编码为base64格式
 * 形参: bindata : ascii字符串输入
 *            base64 : base64字符串输出
 *          binlength : bindata的长度
 * 返回: base64字符串长度
 * 说明: 无
 ******************************************************************************/
static int websocket_base64_encode( const unsigned char *bindata, char *base64, int binlength)
{
    int i, j;
    unsigned char current;
    for ( i = 0, j = 0 ; i < binlength ; i += 3 )
    {
        current = (bindata[i] >> 2) ;
        current &= (unsigned char)0x3F;
        base64[j++] = websocket_base64char[(int)current];
        current = ( (unsigned char)(bindata[i] << 4 ) ) & ( (unsigned char)0x30 ) ;
        if ( i + 1 >= binlength )
        {
            base64[j++] = websocket_base64char[(int)current];
            base64[j++] = '=';
            base64[j++] = '=';
            break;
        }
        current |= ( (unsigned char)(bindata[i+1] >> 4) ) & ( (unsigned char) 0x0F );
        base64[j++] = websocket_base64char[(int)current];
        current = ( (unsigned char)(bindata[i+1] << 2) ) & ( (unsigned char)0x3C ) ;
        if ( i + 2 >= binlength )
        {
            base64[j++] = websocket_base64char[(int)current];
            base64[j++] = '=';
            break;
        }
        current |= ( (unsigned char)(bindata[i+2] >> 6) ) & ( (unsigned char) 0x03 );
        base64[j++] = websocket_base64char[(int)current];
        current = ( (unsigned char)bindata[i+2] ) & ( (unsigned char)0x3F ) ;
        base64[j++] = websocket_base64char[(int)current];
    }
    base64[j] = '\0';
    return j;
}

//==================== 加密方法 sha1哈希 ====================

typedef struct SHA1Context{
    unsigned Message_Digest[5];
    unsigned Length_Low;
    unsigned Length_High;
    unsigned char Message_Block[64];
    int Message_Block_Index;
    int Computed;
    int Corrupted;
} SHA1Context;

#define SHA1CircularShift(bits,word) ((((word) << (bits)) & 0xFFFFFFFF) | ((word) >> (32-(bits))))

static void SHA1ProcessMessageBlock(SHA1Context *context)
{
    const unsigned K[] = {0x5A827999, 0x6ED9EBA1, 0x8F1BBCDC, 0xCA62C1D6 };
    int         t;
    unsigned    temp;
    unsigned    W[80];
    unsigned    A, B, C, D, E;

    for(t = 0; t < 16; t++)
    {
        W[t] = ((unsigned) context->Message_Block[t * 4]) << 24;
        W[t] |= ((unsigned) context->Message_Block[t * 4 + 1]) << 16;
        W[t] |= ((unsigned) context->Message_Block[t * 4 + 2]) << 8;
        W[t] |= ((unsigned) context->Message_Block[t * 4 + 3]);
    }

    for(t = 16; t < 80; t++)
        W[t] = SHA1CircularShift(1,W[t-3] ^ W[t-8] ^ W[t-14] ^ W[t-16]);

    A = context->Message_Digest[0];
    B = context->Message_Digest[1];
    C = context->Message_Digest[2];
    D = context->Message_Digest[3];
    E = context->Message_Digest[4];

    for(t = 0; t < 20; t++)
    {
        temp =  SHA1CircularShift(5,A) + ((B & C) | ((~B) & D)) + E + W[t] + K[0];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = SHA1CircularShift(30,B);
        B = A;
        A = temp;
    }
    for(t = 20; t < 40; t++)
    {
        temp = SHA1CircularShift(5,A) + (B ^ C ^ D) + E + W[t] + K[1];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = SHA1CircularShift(30,B);
        B = A;
        A = temp;
    }
    for(t = 40; t < 60; t++)
    {
        temp = SHA1CircularShift(5,A) + ((B & C) | (B & D) | (C & D)) + E + W[t] + K[2];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = SHA1CircularShift(30,B);
        B = A;
        A = temp;
    }
    for(t = 60; t < 80; t++)
    {
        temp = SHA1CircularShift(5,A) + (B ^ C ^ D) + E + W[t] + K[3];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = SHA1CircularShift(30,B);
        B = A;
        A = temp;
    }
    context->Message_Digest[0] = (context->Message_Digest[0] + A) & 0xFFFFFFFF;
    context->Message_Digest[1] = (context->Message_Digest[1] + B) & 0xFFFFFFFF;
    context->Message_Digest[2] = (context->Message_Digest[2] + C) & 0xFFFFFFFF;
    context->Message_Digest[3] = (context->Message_Digest[3] + D) & 0xFFFFFFFF;
    context->Message_Digest[4] = (context->Message_Digest[4] + E) & 0xFFFFFFFF;
    context->Message_Block_Index = 0;
}

static void SHA1Reset(SHA1Context *context)
{
    context->Length_Low             = 0;
    context->Length_High            = 0;
    context->Message_Block_Index    = 0;

    context->Message_Digest[0]      = 0x67452301;
    context->Message_Digest[1]      = 0xEFCDAB89;
    context->Message_Digest[2]      = 0x98BADCFE;
    context->Message_Digest[3]      = 0x10325476;
    context->Message_Digest[4]      = 0xC3D2E1F0;

    context->Computed   = 0;
    context->Corrupted  = 0;
}

static void SHA1PadMessage(SHA1Context *context)
{
    if (context->Message_Block_Index > 55)
    {
        context->Message_Block[context->Message_Block_Index++] = 0x80;
        while(context->Message_Block_Index < 64)  context->Message_Block[context->Message_Block_Index++] = 0;
        SHA1ProcessMessageBlock(context);
        while(context->Message_Block_Index < 56) context->Message_Block[context->Message_Block_Index++] = 0;
    }
    else
    {
        context->Message_Block[context->Message_Block_Index++] = 0x80;
        while(context->Message_Block_Index < 56) context->Message_Block[context->Message_Block_Index++] = 0;
    }
    context->Message_Block[56] = (context->Length_High >> 24 ) & 0xFF;
    context->Message_Block[57] = (context->Length_High >> 16 ) & 0xFF;
    context->Message_Block[58] = (context->Length_High >> 8 ) & 0xFF;
    context->Message_Block[59] = (context->Length_High) & 0xFF;
    context->Message_Block[60] = (context->Length_Low >> 24 ) & 0xFF;
    context->Message_Block[61] = (context->Length_Low >> 16 ) & 0xFF;
    context->Message_Block[62] = (context->Length_Low >> 8 ) & 0xFF;
    context->Message_Block[63] = (context->Length_Low) & 0xFF;

    SHA1ProcessMessageBlock(context);
}

static int SHA1Result(SHA1Context *context)
{
    if (context->Corrupted)
    {
        return 0;
    }
    if (!context->Computed)
    {
        SHA1PadMessage(context);
        context->Computed = 1;
    }
    return 1;
}


static void SHA1Input(SHA1Context *context,const char *message_array,unsigned length){
    if (!length)
        return;

    if (context->Computed || context->Corrupted)
    {
        context->Corrupted = 1;
        return;
    }

    while(length-- && !context->Corrupted)
    {
        context->Message_Block[context->Message_Block_Index++] = (*message_array & 0xFF);

        context->Length_Low += 8;

        context->Length_Low &= 0xFFFFFFFF;
        if (context->Length_Low == 0)
        {
            context->Length_High++;
            context->Length_High &= 0xFFFFFFFF;
            if (context->Length_High == 0) context->Corrupted = 1;
        }

        if (context->Message_Block_Index == 64)
        {
            SHA1ProcessMessageBlock(context);
        }
        message_array++;
    }
}


static char * sha1_hash(const char *source){   // Main
    SHA1Context sha;
    char *buf;//[128];

    SHA1Reset(&sha);
    SHA1Input(&sha, source, strlen(source));

    if (!SHA1Result(&sha))
    {
        printf("SHA1 ERROR: Could not compute message digest");
        return NULL;
    }
    else
    {
        buf = (char *)malloc(128);
        memset(buf, 0, 128);
        sprintf(buf, "%08X%08X%08X%08X%08X", sha.Message_Digest[0],sha.Message_Digest[1],
        sha.Message_Digest[2],sha.Message_Digest[3],sha.Message_Digest[4]);
        //lr_save_string(buf, lrvar);

        //return strlen(buf);
        return buf;
    }
}

static int tolower(int c)
{
    if (c >= 'A' && c <= 'Z')
    {
        return c + 'a' - 'A';
    }
    else
    {
        return c;
    }
}

static int htoi(const char s[], int start, int len)
{
    int i, j;
    int n = 0;
    if (s[0] == '0' && (s[1]=='x' || s[1]=='X')) //判断是否有前导0x或者0X
    {
        i = 2;
    }
    else
    {
        i = 0;
    }
    i+=start;
    j=0;
    for (; (s[i] >= '0' && s[i] <= '9')
       || (s[i] >= 'a' && s[i] <= 'f') || (s[i] >='A' && s[i] <= 'F');++i)
    {
        if(j>=len)
        {
            break;
        }
        if (tolower(s[i]) > '9')
        {
            n = 16 * n + (10 + tolower(s[i]) - 'a');
        }
        else
        {
            n = 16 * n + (tolower(s[i]) - '0');
        }
        j++;
    }
    return n;
}

//==============================================================================================
//======================================== websocket部分 =======================================
//==============================================================================================

// websocket根据data[0]判别数据包类型
// typedef enum{
//     WDT_MINDATA = -20,      // 0x0：标识一个中间数据包
//     WDT_TXTDATA = -19,      // 0x1：标识一个text类型数据包
//     WDT_BINDATA = -18,      // 0x2：标识一个binary类型数据包
//     WDT_DISCONN = -17,      // 0x8：标识一个断开连接类型数据包
//     WDT_PING = -16,     // 0x8：标识一个断开连接类型数据包
//     WDT_PONG = -15,     // 0xA：表示一个pong类型数据包
//     WDT_ERR = -1,
//     WDT_NULL = 0
// }WebsocketData_Type;
/*******************************************************************************
 * 名称: WebSocket_getRandomString
 * 功能: 生成随机字符串
 * 形参: *buf：随机字符串存储到
 *              len : 生成随机字符串长度
 * 返回: 无
 * 说明: 无
 ******************************************************************************/
static void WebSocket_getRandomString(unsigned char *buf, unsigned int len)
{
    unsigned int i;
    unsigned char temp;
//    srand((int)time(0));
    for(i = 0; i < len; i++)
    {
        temp = (unsigned char)(rand()%256);
        if(temp == 0)   // 随机数不要0, 0 会干扰对字符串长度的判断
            temp = 128;
        buf[i] = temp;
    }
}
/*******************************************************************************
 * 名称: WebSocket_buildShakeKey
 * 功能: client端使用随机数构建握手用的key
 * 形参: *key：随机生成的握手key
 * 返回: key的长度
 * 说明: 无
 ******************************************************************************/
static int WebSocket_buildShakeKey(unsigned char *key)
{
    unsigned char tempKey[WEBSOCKET_SHAKE_KEY_LEN] = {0};
    WebSocket_getRandomString(tempKey, WEBSOCKET_SHAKE_KEY_LEN);
    return websocket_base64_encode((const unsigned char *)tempKey, (char *)key, WEBSOCKET_SHAKE_KEY_LEN);
}
/*******************************************************************************
 * 名称: WebSocket_buildRespondShakeKey
 * 功能: server端在接收client端的key后,构建回应用的key
 * 形参: *acceptKey：来自客户端的key字符串
 *         acceptKeyLen : 长度
 *          *respondKey :  在 acceptKey 之后加上 GUID, 再sha1哈希, 再转成base64得到 respondKey
 * 返回: respondKey的长度(肯定比acceptKey要长)
 * 说明: 无
 ******************************************************************************/
static int WebSocket_buildRespondShakeKey(unsigned char *acceptKey, unsigned int acceptKeyLen, unsigned char *respondKey)
{
    char *clientKey;
    char *sha1DataTemp;
    char *sha1Data;
    int i, n;
    const char GUID[] = "258EAFA5-E914-47DA-95CA-C5AB0DC85B11";
    unsigned int GUIDLEN;

    if(acceptKey == NULL)
        return 0;
    GUIDLEN = sizeof(GUID);
    clientKey = (char *)calloc(acceptKeyLen + GUIDLEN + 10, sizeof(char));
    if (!clientKey) goto END_FUNC;
    memset(clientKey, 0, (acceptKeyLen + GUIDLEN + 10));
    //
    memcpy(clientKey, acceptKey, acceptKeyLen);
    memcpy(&clientKey[acceptKeyLen], GUID, GUIDLEN);
    clientKey[acceptKeyLen + GUIDLEN] = '\0';
    //
    sha1DataTemp = sha1_hash(clientKey);
    n = strlen((const char *)sha1DataTemp);
    sha1Data = (char *)calloc(n / 2 + 1, sizeof(char));
    if (!sha1Data) goto END_FUNC;
    memset(sha1Data, 0, n / 2 + 1);
   //
    for(i = 0; i < n; i += 2)
        sha1Data[ i / 2 ] = htoi(sha1DataTemp, i, 2);
    n = websocket_base64_encode((const unsigned char *)sha1Data, (char *)respondKey, (n / 2));
    //
END_FUNC:
    free(sha1DataTemp);
    free(sha1Data);
    free(clientKey);
    return n;
}
/*******************************************************************************
 * 名称: WebSocket_matchShakeKey
 * 功能: client端收到来自服务器回应的key后进行匹配,以验证握手成功
 * 形参: *myKey：client端请求握手时发给服务器的key
 *            myKeyLen : 长度
 *          *acceptKey : 服务器回应的key
 *           acceptKeyLen : 长度
 * 返回: 0 成功  -1 失败
 * 说明: 无
 ******************************************************************************/
static int WebSocket_matchShakeKey(unsigned char *myKey, unsigned int myKeyLen, unsigned char *acceptKey, unsigned int acceptKeyLen)
{
    int retLen;
    unsigned char tempKey[256] = {0};
    //
    retLen = WebSocket_buildRespondShakeKey(myKey, myKeyLen, tempKey);
    //printf("WebSocket_matchShakeKey :\r\n%d : %s\r\n%d : %s\r\n", acceptKeyLen, acceptKey, retLen, tempKey);
    //
    if(retLen != (int)acceptKeyLen)
    {
        printf("WebSocket_matchShakeKey : len err\r\n%s\r\n%s\r\n%s\r\n", myKey, tempKey, acceptKey);
        return -1;
    }
    else if(strcmp((const char *)tempKey, (const char *)acceptKey) != 0)
    {
        printf("WebSocket_matchShakeKey : str err\r\n%s\r\n%s\r\n", tempKey, acceptKey);
        return -1;
    }
    return 0;
}
/*******************************************************************************
 * 名称: WebSocket_buildHttpHead
 * 功能: 构建client端连接服务器时的http协议头, 注意websocket是GET形式的
 * 形参: *ip：要连接的服务器ip字符串
 *          port : 服务器端口
 *    *interfacePath : 要连接的端口地址
 *      *shakeKey : 握手key, 可以由任意的16位字符串打包成base64后得到
 *      *package : 存储最后打包好的内容
 * 返回: 无
 * 说明: 无
 ******************************************************************************/
static void WebSocket_buildHttpHead(char *ip, int port, char *interfacePath, unsigned char *shakeKey, char *package)
{
    const char httpDemo[] = "GET %s HTTP/1.1\r\n"
                            "Connection: Upgrade\r\n"
                            "Host: %s:%d\r\n"
                            "Sec-WebSocket-Key: %s\r\n"
                            "Sec-WebSocket-Version: 13\r\n"
                            "Upgrade: websocket\r\n\r\n";
    sprintf(package, httpDemo, interfacePath, ip, port, shakeKey);
}

/*******************************************************************************
 * 名称: WebSocket_enPackage
 * 功能: websocket数据收发阶段的数据打包, 通常client发server的数据都要isMask(掩码)处理, 反之server到client却不用
 * 形参: *data：准备发出的数据
 *          dataLen : 长度
 *        *package : 打包后存储地址
 *        packageMaxLen : 存储地址可用长度
 *          isMask : 是否使用掩码     1要   0 不要
 *          type : 数据类型, 由打包后第一个字节决定, 这里默认是数据传输, 即0x81
 * 返回: 打包后的长度(会比原数据长2~16个字节不等)      <=0 打包失败
 * 说明: 无
 ******************************************************************************/
static int WebSocket_enPackage(unsigned char *data, unsigned int dataLen, unsigned char *package, unsigned int packageMaxLen, bool isMask, WebsocketData_Type type)
{
    unsigned char maskKey[4] = {0};    // 掩码
    unsigned char temp1, temp2;
    int count;
    unsigned int i, len = 0;

    if(packageMaxLen < 2)
        return -1;

    if(type == WDT_MINDATA)
        *package++ = 0x00;
    else if(type == WDT_TXTDATA)
        *package++ = 0x81;
    else if(type == WDT_BINDATA)
        *package++ = 0x82;
    else if(type == WDT_DISCONN)
        *package++ = 0x88;
    else if(type == WDT_PING)
        *package++ = 0x89;
    else if(type == WDT_PONG)
        *package++ = 0x8A;
    else
//        return -1;
        *package++ = type; // merlin add
    //
    if(isMask)
        *package = 0x80;
    len += 1;
    //
    if(dataLen < 126)
    {
        *package++ |= (dataLen&0x7F);
        len += 1;
    }
    else if(dataLen < 65536)
    {
        if(packageMaxLen < 4)
            return -1;
        *package++ |= 0x7E;
        *package++ = (char)((dataLen >> 8) & 0xFF);
        *package++ = (unsigned char)((dataLen >> 0) & 0xFF);
        len += 3;
    }
    else if(dataLen < 0xFFFFFFFF)
    {
        if(packageMaxLen < 10)
            return -1;
        *package++ |= 0x7F;
        *package++ = 0; //(char)((dataLen >> 56) & 0xFF);   // 数据长度变量是 unsigned int dataLen, 暂时没有那么多数据
        *package++ = 0; //(char)((dataLen >> 48) & 0xFF);
        *package++ = 0; //(char)((dataLen >> 40) & 0xFF);
        *package++ = 0; //(char)((dataLen >> 32) & 0xFF);
        *package++ = (char)((dataLen >> 24) & 0xFF);        // 到这里就够传4GB数据了
        *package++ = (char)((dataLen >> 16) & 0xFF);
        *package++ = (char)((dataLen >> 8) & 0xFF);
        *package++ = (char)((dataLen >> 0) & 0xFF);
        len += 9;
    }
    //
    if(isMask)    // 数据使用掩码时, 使用异或解码, maskKey[4]依次和数据异或运算, 逻辑如下
    {
        if(packageMaxLen < len + dataLen + 4)
            return -1;
        WebSocket_getRandomString(maskKey, sizeof(maskKey));    // 随机生成掩码
        *package++ = maskKey[0];
        *package++ = maskKey[1];
        *package++ = maskKey[2];
        *package++ = maskKey[3];
        len += 4;
        for(i = 0, count = 0; i < dataLen; i++)
        {
            temp1 = maskKey[count];
            temp2 = data[i];
            *package++ = (char)(((~temp1)&temp2) | (temp1&(~temp2)));  // 异或运算后得到数据
            count += 1;
            if(count >= (int)sizeof(maskKey))    // maskKey[4]循环使用
                count = 0;
        }
        len += i;
        *package = '\0';
    }
    else    // 数据没使用掩码, 直接复制数据段
    {
        if(packageMaxLen < len + dataLen)
            return -1;
        memcpy(package, data, dataLen);
        package[dataLen] = '\0';
        len += dataLen;
    }
    //
    return len;
}
/*******************************************************************************
 * 名称: WebSocket_dePackage
 * 功能: websocket数据收发阶段的数据解包, 通常client发server的数据都要isMask(掩码)处理, 反之server到client却不用
 * 形参: *data：解包的数据
 *      dataLen : 长度
 *      *package : 解包后存储地址
 *      packageMaxLen : 存储地址可用长度
 *      *packageLen : 解包所得长度
 * 返回: 解包识别的数据类型 如 : txt数据, bin数据, ping, pong等
 * 说明: 无
 ******************************************************************************/
static int WebSocket_dePackage(unsigned char *data, unsigned int dataLen, unsigned char *package, unsigned int packageMaxLen, unsigned int *packageLen, unsigned int *packageHeadLen)
{
    unsigned char maskKey[4] = {0};    // 掩码
    unsigned char temp1, temp2;
    char Mask = 0, type;
    int count, ret;
    unsigned int i, len = 0, dataStart = 2;
    if(dataLen < 2){printf("WebSocket_dePackage  1 return WDT_ERR\r\n");
        return WDT_ERR;}

    type = data[0]&0x0F;

    if((data[0]&0x80) == 0x80)
    {
        if(type == 0x01)
            ret = WDT_TXTDATA;
        else if(type == 0x02)
            ret = WDT_BINDATA;
        else if(type == 0x08)
            ret = WDT_DISCONN;
        else if(type == 0x09)
            ret = WDT_PING;
        else if(type == 0x0A)
            ret = WDT_PONG;
        else{printf("WebSocket_dePackage 2  return WDT_ERR\r\n");
            return WDT_ERR;}
    }
    else if(type == 0x00)
        ret = WDT_MINDATA;
    else{printf("WebSocket_dePackage 3  return WDT_ERR\r\n");
        return WDT_ERR;}
    //
    if((data[1] & 0x80) == 0x80)
    {
        Mask = 1;
        count = 4;
    }
    else
    {
        Mask = 0;
        count = 0;
    }
    //
    len = data[1] & 0x7F;
    //
    if(len == 126)
    {
        if(dataLen < 4){printf("WebSocket_dePackage  4 return WDT_ERR\r\n");
            return WDT_ERR;}
        len = data[2];
        len = (len << 8) + data[3];
        if(packageLen) *packageLen = len;//转储包长度
        if(packageHeadLen) *packageHeadLen = 4 + count;
        //
        if(dataLen < len + 4 + count){printf("WebSocket_dePackage 5  return WDT_ERR\r\n");
            return WDT_ERR;}
        if(Mask)
        {
            maskKey[0] = data[4];
            maskKey[1] = data[5];
            maskKey[2] = data[6];
            maskKey[3] = data[7];
            dataStart = 8;
        }
        else
            dataStart = 4;
    }
    else if(len == 127)
    {
        if(dataLen < 10){printf("WebSocket_dePackage  6 return WDT_ERR\r\n");
            return WDT_ERR;}
        if(data[2] != 0 || data[3] != 0 || data[4] != 0 || data[5] != 0)    //使用8个字节存储长度时,前4位必须为0,装不下那么多数据...
            {printf("WebSocket_dePackage   return WDT_ERR\r\n");
            return WDT_ERR;}
        len = data[6];
        len = (len << 8) + data[7];
        len = (len << 8) + data[8];
        len = (len << 8) + data[9];
        if(packageLen) *packageLen = len;//转储包长度
        if(packageHeadLen) *packageHeadLen = 10 + count;
        //
        if(dataLen < len + 10 + count){printf("WebSocket_dePackage 7  return WDT_ERR\r\n");
            return WDT_ERR;}
        if(Mask)
        {
            maskKey[0] = data[10];
            maskKey[1] = data[11];
            maskKey[2] = data[12];
            maskKey[3] = data[13];
            dataStart = 14;
        }
        else
            dataStart = 10;
    }
    else
    {
        if(packageLen) *packageLen = len;//转储包长度
        if(packageHeadLen) *packageHeadLen = 2 + count;
        //
        if(dataLen < len + 2 + count){printf("WebSocket_dePackage  8 return WDT_ERR\r\n");
            return WDT_ERR;}
        if(Mask)
        {
            maskKey[0] = data[2];
            maskKey[1] = data[3];
            maskKey[2] = data[4];
            maskKey[3] = data[5];
            dataStart = 6;
        }
        else
            dataStart = 2;
    }
    //
    if(dataLen < len + dataStart){printf("WebSocket_dePackage 9  return WDT_ERR\r\n");
        return WDT_ERR;}
    //
    if(packageMaxLen < len + 1){printf("WebSocket_dePackage 10  return WDT_ERR\r\n");
        return WDT_ERR;}
    //
    if(Mask)    // 解包数据使用掩码时, 使用异或解码, maskKey[4]依次和数据异或运算, 逻辑如下
    {
        for(i = 0, count = 0; i < len; i++)
        {
            temp1 = maskKey[count];
            temp2 = data[i + dataStart];
            *package++ =  (char)(((~temp1)&temp2) | (temp1&(~temp2)));  // 异或运算后得到数据
            count += 1;
            if(count >= (int)sizeof(maskKey))    // maskKey[4]循环使用
                count = 0;
        }
        *package = '\0';
    }
    else    // 解包数据没使用掩码, 直接复制数据段
    {
        memcpy(package, &data[dataStart], len);
        package[len] = '\0';
    }
    //
    return ret;
}

/*******************************************************************************
 * 名称:WebSocket_parse_url
 * 功能:解析websocket URL 到endpoint结构体
 * 形参:URL
 * 返回:endpoint url对应的参数
 *      0正确 其他对应错误代码宏如WS_ERROR_INVALID_URL_SCHEME
 * 说明: 无
 ******************************************************************************/
static char* const URL_SCHEME_SEPARATOR = "://";
static int WebSocket_parse_url(const char* url, ws_endpoint* endpoint, const bool allow_relative)
{
    char* pos = (char*) url;
    char* scheme_separator = strstr(pos, URL_SCHEME_SEPARATOR);
    if (scheme_separator == NULL)
    {
        endpoint->is_ssl = false;
    }
    else
    {
        size_t scheme_length = scheme_separator - pos;
        if ((strncmp("https", pos, scheme_length) == 0 && strncmp("https", pos, 5) == 0) ||\
            (strncmp("wss", pos, scheme_length) == 0 && strncmp("wss", pos, 3) == 0))
        {
            endpoint->is_ssl = true;
        }
        else if (strncmp("http", pos, scheme_length) == 0 || strncmp("ws", pos, scheme_length) == 0)
            {
                endpoint->is_ssl = false;
            } else {
                return WS_ERROR_INVALID_URL_SCHEME;
            }
        pos = scheme_separator + strlen(URL_SCHEME_SEPARATOR);
    }

    char* hostname = pos;
    while (*pos && !strchr(":/?#", *pos))
    {

        if (pos - hostname > WS_MAX_HOSTNAME_LENGTH)
            return WS_ERROR_HOSTNAME_TOO_LONG;
        else
            pos++;
    }

    memset(endpoint->hostname, 0, sizeof(endpoint->hostname));

    bool is_relative;

    if (pos - hostname == 0)
    { // hostname is empty
        if (scheme_separator != NULL)
        {  // has scheme
            return WS_ERROR_EMPTY_HOSTNAME;
        } else if (!allow_relative)
        {
            return WS_ERROR_RELATIVE_URL_NOT_ALLOWED;
        }
        is_relative = true;
    }
    else
    {
        is_relative = false;
    }

    memcpy(endpoint->hostname, hostname, pos - hostname);

    if (*pos == ':')
    {
        if (is_relative)
        {
            return WS_ERROR_INVALID_URL;
        }
        pos++;
        uint32_t port = 0;
        while ('0' <= *pos && *pos <= '9')
        {
            port = port * 10 + *pos - '0';
            pos++;
            if (port > UINT16_MAX)
            {
                return WS_ERROR_INVALID_PORT;
            }
        }
        if (port == 0)
        {
            return WS_ERROR_INVALID_PORT;
        }
        endpoint->port = (unsigned short) port;
    }
    else
    {
        endpoint->port = (unsigned short) (endpoint->is_ssl ? 443 : 80);
    }

    memset(endpoint->path_and_query, 0, sizeof(endpoint->path_and_query));

    if (*pos == '/')
    {
        char* path = pos;
        while (*pos && *pos != '#')
        {
            if (pos - path > WS_MAX_PATH_AND_QUERY_LENGTH)
                return WS_ERROR_PATH_AND_QUERY_TOO_LONG;
            else
                pos++;
        }

        memcpy(endpoint->path_and_query, path, pos - path);
    }
    else if (!*hostname)
    {
        return WS_ERROR_INVALID_URL;
    }
    else
    {
        endpoint->path_and_query[0] = '/';
    }

    return 0;
}

/*******************************************************************************
 * 名称: WebSocket_clientLinkToServer
 * 功能: 向websocket服务器发送http(携带握手key), 以和服务器构建连接, 非阻塞模式
 * 形参: *ip：服务器ip
 *          port : 服务器端口
 *       *interface_path : 接口地址
 * 返回: >0 返回连接句柄      <= 0 连接失败或超时, 所花费的时间 ms
 * 说明: 无
 ******************************************************************************/
int WebSocket_LinkServer(char *url)
{
    int err=0;
    int opt=0;
    int ret, fd;
    int i;
    int flage;
    struct hostent* he;
    struct hostent_ext *pnew;
    unsigned char loginBuf[512] = {0}, recBuf[512] = {0}, shakeKey[128] = {0}, *p;
    //服务器端网络地址结构体
    struct sockaddr_in report_addr;
    ws_endpoint  Endpoint;
    int port;

    flage = WebSocket_parse_url(url,&Endpoint,false);
    if(flage != 0)
    {
        printf("111111111111111\r\n");
        return flage;
    }
    if((fd = socket(AF_INET,SOCK_STREAM, 0)) < 0)
    {
        printf("2222222222222222\r\n");
        return WS_ERROR_CREATING_SOCKET;
    }

    opt = 3*1000*mS;  //time out time
    if(0 != setsockopt(fd,SOL_SOCKET,SO_RCVTIMEO,&opt,sizeof(opt)))
    {
        printf("warning: setsockopt->SO_RCVTIMEO[%d(ms)] failed!\r\n", opt);
    }
    opt = 1*1000*mS;  //time out time
    if(0 != setsockopt(fd,SOL_SOCKET,SO_SNDTIMEO,&opt,sizeof(opt)))
    {
        printf("warning: setsockopt->SO_SNDTIMEO[%d(ms)] failed!\r\n", opt);
    }

#if 0
    // 接收缓冲区
    int nRecvBuf=4*1024;         //设置为32K
    err = setsockopt(fd,SOL_SOCKET,SO_RCVBUF,(const char*)&nRecvBuf,sizeof(int));
    if(err<0){
        printf("\r\n设置接收缓冲区大小错误\r\n");
    }
#endif

#if 1
    //发送缓冲区
    int nSendBuf=4*1024+64;//设置为32K
    err = setsockopt(fd,SOL_SOCKET,SO_SNDBUF,(const char*)&nSendBuf,sizeof(int));
    if(err<0){
        printf("\r\n设置发送缓冲区大小错误\r\n");
    }
#endif

#if 0
    if ((he = gethostbyname(Endpoint.hostname)) == NULL)
    {
        //close(fd);
        //return WS_ERROR_RESOLVING_HOSTNAME;
        goto END_FUNC;
    }
    port = (int)Endpoint.port;
    memcpy(&report_addr.sin_addr, he->h_addr_list[0], he->h_length);
    report_addr.sin_port = htons(port);                             // 服务器端口号
    report_addr.sin_family = AF_INET;
#else
    pnew = (struct hostent_ext*)malloc(sizeof(struct hostent_ext));
    if (pnew == NULL)
    {
        printf("33333333333333333333\r\n");
        goto END_FUNC;
    }

    if ((he = gethostbyname_r(Endpoint.hostname,pnew)) == NULL)
    {
        //close(fd);
        //return WS_ERROR_RESOLVING_HOSTNAME;
        printf("444444444444444444444\r\n");
        free(pnew);
        goto END_FUNC;
    }
    port = (int)Endpoint.port;
    memcpy(&report_addr.sin_addr, he->h_addr_list[0], he->h_length);
    free(pnew);
    he = NULL;
    report_addr.sin_port = htons(port);                             // 服务器端口号
    report_addr.sin_family = AF_INET;
#endif


#if 0
    //非阻塞
    ret = fcntl(fd , F_GETFL , 0);
    fcntl(fd , F_SETFL , ret | O_NONBLOCK);

    //connect
    timeOut = 0;
    while(connect(fd , (struct sockaddr *)&report_addr,sizeof(struct sockaddr)) == -1)
    {
        if(++timeOut > WEBSOCKET_LOGIN_CONNECT_TIMEOUT)
        {
            close(fd);
            return WS_ERROR_TIMEOUT;
        }
        //WebSocket_delayms(1);  //1ms
        Djy_EventDelay(1000);
    }
#endif
    if (connect(fd , (struct sockaddr *)&report_addr,sizeof(struct sockaddr)) == -1) {
        printf("55555555555555555\r\n");

        goto END_FUNC;
//        close(fd);
//        return WS_ERROR_TIMEOUT;
    }

//   //非阻塞 // TODO merlin comment 3 lines
//   ret = fcntl(fd , F_GETFL , 0);
//   fcntl(fd , F_SETFL , ret | O_NONBLOCK);

    //发送http协议头
    memset(shakeKey, 0, sizeof(shakeKey));
    WebSocket_buildShakeKey(shakeKey);  // 创建握手key

    memset(loginBuf, 0, sizeof(loginBuf));  // 创建协议包
    WebSocket_buildHttpHead(Endpoint.hostname, port, Endpoint.path_and_query, shakeKey, (char *)loginBuf);
    //发出协议包
    //ret = send(fd , loginBuf , strlen((const char*)loginBuf) , MSG_NOSIGNAL);
    ret = socket_send(fd ,(char *)loginBuf , strlen((const char*)loginBuf), 5, 0);
    //显示http请求
#ifdef WEBSOCKET_DEBUG
    printf("\r\nconnect : %dms\r\nlogin_send:\r\n%s\r\n" , timeOut, loginBuf);
#endif
    //
    while(1)
    {
        memset(recBuf , 0 , sizeof(recBuf));
        //ret = recv(fd , recBuf , sizeof(recBuf) ,  MSG_NOSIGNAL);
        ret = socket_recv(fd, (char *)recBuf , sizeof(recBuf), 5, &err);
        printf("swj debug:%s------------\r\n\r\n",recBuf);
        if(ret > 0)
        {
            if(strstr(recBuf,"HTTP/1.1 401 Unauthorized"))
            {
                printf("6666666666666666\r\n");
                return WS_ERROR_UNAUTHORIZED;
            }
            if(strncmp((const char *)recBuf, (const char *)"HTTP", strlen((const char *)"HTTP")) == 0)    // 返回的是http回应信息
            {

#ifdef WEBSOCKET_DEBUG
                printf("\r\nlogin_recv : %d / %dms\r\n%s\r\n" , ret, timeOut, recBuf);//显示http返回
#endif

                if((p = (unsigned char *)strstr((const char *)recBuf, (const char *)"Sec-WebSocket-Accept: ")) != NULL) // 定位到握手字符串
                {
                    p += strlen((const char *)"Sec-WebSocket-Accept: ");
                    sscanf((const char *)p, "%s\r\n", p);
                    if(WebSocket_matchShakeKey(shakeKey, strlen((const char *)shakeKey), p, strlen((const char *)p)) == 0) // 比对握手信息
                        return fd; // 连接成功, 返回连接句柄fd
                    else {
                        ret = socket_send(fd , (char *)loginBuf , strlen((const char*)loginBuf), 5, &err);
                        if (err == -1) goto END_FUNC;
                        //ret = send(fd , loginBuf , strlen((const char*)loginBuf) , MSG_NOSIGNAL);    // 握手信号不对, 重发协议包
                    }
                }
                else {
                    //ret = send(fd , loginBuf , strlen((const char*)loginBuf) , MSG_NOSIGNAL);     // 重发协议包
                    ret = socket_send(fd , (char *)loginBuf , strlen((const char*)loginBuf), 5, &err);
                    if (err == -1) goto END_FUNC;
                }

            }
            else  // 显示异常返回数据
            {
                if(recBuf[0] >= ' ' && recBuf[0] <= '~')
                    printf("\r\nlogin_recv : %d\r\n%s\r\n" , ret, recBuf);
                else
                {
                    printf("\r\nlogin_recv : %d\r\n" , ret);
                    for(i = 0; i < ret; i++)
                        printf("%.2X ", recBuf[i]);
                    printf("\r\n");
                }
            }
        }

        if (err == -1) goto END_FUNC;
#if 0
        if(++timeOut > WEBSOCKET_LOGIN_RESPOND_TIMEOUT)
        {
            close(fd);
            return WS_ERROR_TIMEOUT;
        }
        //WebSocket_delayms(1);  //1ms
        Djy_EventDelay(1000);
#endif
    }

END_FUNC:

    close(fd);
    return WS_ERROR_TIMEOUT;
}

/*******************************************************************************
 * 名称: WebSocket_send
 * 功能: websocket数据基本打包和发送
 * 形参: fd：连接句柄
 *          *data : 数据
 *          dataLen : 长度
 *          isMask : 数据是否使用掩码, 客户端到服务器必须使用掩码模式
 *          type : 数据要要以什么识别头类型发送(txt, bin, ping, pong ...)
 * 返回: 调用send的返回
 * 说明: 无
 ******************************************************************************/
int WebSocket_send(int fd, char *data, int dataLen, bool isMask, WebsocketData_Type type)
{
    unsigned char *webSocketPackage = NULL;
    int retLen, ret;
#ifdef WEBSOCKET_DEBUG
    int i;
    printf("WebSocket_send : %d\r\n", dataLen);
#endif
    //---------- websocket数据打包 ----------
    webSocketPackage = (unsigned char *)calloc(dataLen + 128, sizeof(char));
    retLen = WebSocket_enPackage((unsigned char *)data, dataLen, webSocketPackage, (dataLen + 128), isMask, type);
    //显示数据
#ifdef WEBSOCKET_DEBUG
    printf("WebSocket_send : %d\r\n" , retLen);
    for(i = 0; i < retLen; i ++)
        printf("%.2X ", webSocketPackage[i]);
    printf("\r\n");
#endif
    ret = socket_send(fd, (char *)webSocketPackage, retLen, 1*1000, 0);
    //ret = send(fd, webSocketPackage, retLen, MSG_NOSIGNAL);
    free(webSocketPackage);
    return ret;
}
/*******************************************************************************
 * 名称: WebSocket_recv
 * 功能: websocket数据接收和基本解包
 * 形参: fd：连接句柄
 *          *data : 数据接收地址
 *          dataMaxLen : 接收区可用最大长度
 * 返回: = 0 没有收到有效数据 > 0 成功接收并解包数据 < 0 非包数据的长度
 * 说明: 无
 ******************************************************************************/

int  getrecvbuf(char *s,int fd,int dataMaxLen)
{
      int ret;
      int err=0;
      ret = socket_recv2(fd,s, dataMaxLen, 10, &err);

      return ret;
}

extern int recvlen;
int WebSocket_recv(int fd, char *data, int dataMaxLen, WebsocketData_Type *dataType)
{
    unsigned char *webSocketPackage = NULL, *recvBuf = NULL;
    int ret, dpRet = WDT_NULL, retTemp, retFinal = 0;
    int retLen = 0, retHeadLen = 0;
    int err=0;
    s64 recvTimeoutTs = 0;
    //

    recvBuf = (unsigned char *)calloc(dataMaxLen, sizeof(char));
    ret = socket_recv(fd, (char *)recvBuf, dataMaxLen, 10, &err);
    //数据可能超出了范围限制
    if(ret == dataMaxLen)
        printf("WebSocket_recv : warning !! recv buff too large !! (recv/%d)\r\n", ret);
    //
    if(ret > 0)
    {
        //---------- websocket数据解包 ----------
        webSocketPackage = (unsigned char *)calloc(ret + 128, sizeof(char));
        dpRet = WebSocket_dePackage(recvBuf, ret, webSocketPackage, (ret + 128), (unsigned int *)&retLen, (unsigned int *)&retHeadLen);
printf("dpRet=%d;  retLen = %d;   ret = %d ;   dataMaxLen = %d ;  retHeadLen=%d\r\n", dpRet, retLen, ret, dataMaxLen, retHeadLen);

       if(recvBuf) free(recvBuf);
       if(webSocketPackage) free(webSocketPackage);
       recvlen=retLen;
       return 0;

        if(dpRet == WDT_ERR && retLen == 0) //非包数据
        {
            memset(data, 0, dataMaxLen);
            if(ret < dataMaxLen)
            {
                memcpy(data, recvBuf, ret);
                retFinal = -ret;
            }
            else
            {
                memcpy(data, recvBuf, dataMaxLen);
                retFinal = -dataMaxLen;
            }
        }
        else //正常收包
        {
            //数据可能超出了范围限制
            if(retLen > dataMaxLen)
            {
                int cpLen = ret>dataMaxLen?dataMaxLen:ret;
                int opt = 1000;

                printf("WebSocket_recv : warning !! recv package too large !! (recvPackage/%d)\r\n", retLen);
                memcpy(data, &recvBuf[retHeadLen], cpLen);

                opt = 300*mS;  //time out time
                setsockopt(fd,SOL_SOCKET, SO_RCVTIMEO, &opt, sizeof(opt));

                do
                {
                    ret = socket_recv(fd, (char *)recvBuf, dataMaxLen, 10, &err);
                    printf("socket_recv ret is %d\r\n", ret);
                } while (ret > 0);

                data[cpLen-1] = '\0';
                retFinal = cpLen;
                goto recv_return_normal;
            }
            //显示数据包的头10个字节
#ifdef WEBSOCKET_DEBUG
            if(ret > 10)
                printf("WebSocket_recv : ret/%d, dpRet/%d, retLen/%d, head/%d : %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X\r\n",
                    ret, dpRet, retLen, retHeadLen,
                    recvBuf[0], recvBuf[1], recvBuf[2], recvBuf[3], recvBuf[4],
                    recvBuf[5], recvBuf[6], recvBuf[7], recvBuf[8], recvBuf[9]);
#endif

            //续传? 检查数据包的头10个字节发现recv()时并没有把一包数据接收完,继续接收..
            if(ret < retHeadLen + retLen)
            {
                while (ret < retHeadLen + retLen)
                {
                    retTemp = socket_recv(fd, &recvBuf[ret], retHeadLen+retLen - ret, 10, &err);
                    printf("ret=%d; retTemp=%d\r\n", ret, retTemp);
                    if (retTemp > 0)
                    {
                        ret += retTemp;
                        recvTimeoutTs = 0;
                    }
                    else if (0 == retTemp)
                    {
                        if (0 == recvTimeoutTs)
                        {
                            recvTimeoutTs = DjyGetSysTime();
                        }
                        else
                        {
                            if (DjyGetSysTime()-recvTimeoutTs >= 1000*1000)
                            {
                                printf("Recv timeout, recv_return_null;\r\n");
                                goto recv_return_null;;
                            }
                        }
                    }
                    else
                    {
                        printf("Recv -1, recv_return_null;\r\n");
                        goto recv_return_null;;
                    }
                }
                //再解包一次
                free(webSocketPackage); webSocketPackage=NULL;
                webSocketPackage = (unsigned char *)calloc(ret + 128, sizeof(char));
                dpRet = WebSocket_dePackage(recvBuf, ret, webSocketPackage, (ret + 128), (unsigned int *)&retLen, (unsigned int *)&retHeadLen);
                //
                if(ret > 10)
                    printf("WebSocket_recv : ret/%d, dpRet/%d, retLen/%d, head/%d : %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X %.2X\r\n",
                        ret, dpRet, retLen, retHeadLen,
                        recvBuf[0], recvBuf[1], recvBuf[2], recvBuf[3], recvBuf[4],
                        recvBuf[5], recvBuf[6], recvBuf[7], recvBuf[8], recvBuf[9]);
            }
            //
            if(retLen > 0)
            {
                if(dpRet == WDT_PING)
                {
                    WebSocket_send(fd, (char *)webSocketPackage, retLen, true, WDT_PONG);//自动 ping-pong
                    // 显示数据
#ifdef WEBSOCKET_DEBUG
                    printf("WebSocket_recv : PING %d\r\n%s\r\n" , retLen, webSocketPackage);
#endif
                }
                else if(dpRet == WDT_PONG)
                {
#ifdef WEBSOCKET_DEBUG
                    printf("WebSocket_recv : PONG %d\r\n%s\r\n" , retLen, webSocketPackage);
#endif
                }
                else //if(dpRet == WDT_TXTDATA || dpRet == WDT_BINDATA || dpRet == WDT_MINDATA)
                {
                    memcpy(data, webSocketPackage, retLen);
                    // 显示数据
#ifdef WEBSOCKET_DEBUG
                    if(webSocketPackage[0] >= ' ' && webSocketPackage[0] <= '~')
                        printf("\r\nWebSocket_recv : New Package StrFile dpRet:%d/retLen:%d\r\n%s\r\n" , dpRet, retLen, webSocketPackage);
                    else
                    {
                        printf("\r\nWebSocket_recv : New Package BinFile dpRet:%d/retLen:%d\r\n" , dpRet, retLen);
                        int i;
                        for(i = 0; i < retLen; i++)
                            printf("%.2X ", webSocketPackage[i]);
                        printf("\r\n");
                    }
#endif
                }
                //
                retFinal = retLen;
            }
#ifdef WEBSOCKET_DEBUG
            else
            {
                // 显示数据
                if(recvBuf[0] >= ' ' && recvBuf[0] <= '~')
                    printf("\r\nWebSocket_recv : ret:%d/dpRet:%d/retLen:%d\r\n%s\r\n" , ret, dpRet, retLen, recvBuf);
                else
                {
                    printf("\r\nWebSocket_recv : ret:%d/dpRet:%d/retLen:%d\r\n%s\r\n" , ret, dpRet, retLen, recvBuf);
                    int i;
                    for(i = 0; i < ret; i++)
                        printf("%.2X ", recvBuf[i]);
                    printf("\r\n");
                }
            }
#endif
        }
    }
recv_return_normal:
    if(recvBuf) free(recvBuf);
    if(webSocketPackage) free(webSocketPackage);
    if(dataType) *dataType = dpRet;
    return retFinal;

recv_return_null:

    if(recvBuf) free(recvBuf);
    if(webSocketPackage) free(webSocketPackage);
    if(dataType) *dataType = dpRet;
    return 0;
}
