#ifndef __PLAY_MP3__
#define __PLAY_MP3__

#define MP3_DEBUG 1

unsigned int Mp3Framebitrate();

int Mp3PlayData(unsigned char *buf, int len, unsigned int timeout);

#endif
