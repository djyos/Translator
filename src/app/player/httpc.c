#include "httpc.h"
#include <sys/socket.h>
#include <netdb.h>
#include "music_player.h"
#include "chain_table.h"
//#include "buf_queue.h"
#include "LoopBytes.h"
#include "errno.h"

#define debug //printf

int music_delete = 0;
int speakend=0;

extern int mp3_loopbytes_is_empty();
extern int mp3_loopbytes_len();
extern struct player_ctrl_info PlayerCtrlInfo;

const char HTTP_GET_MP3[] =
        "GET /%s HTTP/1.1\r\n\
Host: %s\r\n\
Connection: keep-alive\r\n\
Upgrade-Insecure-Requests: 1\r\n\
User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Safari/537.36\r\n\
Accept: text/html;image/audio;\r\n\
Accept-Encoding: gzip, deflate\r\n\
Accept-Language: zh-CN,zh;q=0.9,en;q=0.8 \r\n\r\n";

const char HTTP1_GET_MP3[] =
        "GET /%s HTTP/1.1\r\n\
Host: %s\r\n\
Connection: keep-alive\r\n\r\n";

const char HTTP2_GET_MP3[] =
        "GET /%s HTTP/1.1\r\n\
Host: %s\r\n\
Range: bytes=%d-%d\r\n\
Connection: keep-alive\r\n\r\n";


#define DB_DOWNLOAD_ONLY 0
#define BUFSZ  2048//1024
#define CN_RCVBUF_LEN (4*1024)
#define CN_SNDBUF_LEN (4*1024)

extern int mp3_stream_reset();
struct tagSocket *testsock;
int http_test(char *ip_addr, unsigned short port, char *url)
{

    int mark = 0;
    int len = 0;
    int err = 0;
    int s = -1;
    struct sockaddr_in address;
    int ret = 0;
    char buf_domain[100] = { 0 };

    memset(&address, 0, sizeof(address));

    char ip_buf[20] = { 0 };
    memset(ip_buf, 0, sizeof(ip_buf));

    int ip[4] = { 0 };
    int is_ipaddr = 0;
    ret = sscanf(ip_addr, "%d.%d.%d.%d", &ip[0], &ip[1], &ip[2], &ip[3]);
    is_ipaddr = (ret == 4 && ip[0] < 255 && ip[0] > 0 && ip[1] < 255 && ip[1] > 0 && ip[2] < 255 && ip[2] > 0 && ip[3] < 255 && ip[3] > 0);
    struct hostent *phost = 0;
    if (!is_ipaddr)  //domain parser
    {
        phost = gethostbyname(ip_addr);
        if (phost)
        {
            sprintf(ip_buf, "%u.%u.%u.%u", (u8) phost->h_addr_list[0][0],
                    (u8) phost->h_addr_list[0][1],
                    (u8) phost->h_addr_list[0][2],
                    (u8) phost->h_addr_list[0][3]);
            printf("info: domain=\"%s\", ip=%s!\r\n", ip_addr, ip_buf);
            strcpy(buf_domain, ip_addr);
            ip_addr = ip_buf;
        }
    }

    if (!is_ipaddr && phost == NULL)
    {
        printf("this is is_ipaddr and phost is null\r\n");
        return 0;
    }

    if ((s = socket(AF_INET, SOCK_STREAM, 0)) > 0)
    {
        printf("\r\n TESTTTTTT httpc s is %d\r\n", s);

        u32 to_send = 1 * 1000 * mS;
        u32 to_recv = 1 * 1000 * mS;
#define TO_RECV_COUNT_MAXT  2
        int to_recv_cnt = TO_RECV_COUNT_MAXT;
        setsockopt(s, SOL_SOCKET, SO_SNDTIMEO, &to_send, sizeof(to_send));
        setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, &to_recv, sizeof(to_recv));
        //���ý��ջ�����
        int sndopt = CN_RCVBUF_LEN;
        if (0 != setsockopt(s, SOL_SOCKET, SO_RCVBUF, &sndopt, 4))
        {
            debug("Client:set client sndbuf error!\n\r");
        }
        sndopt = CN_SNDBUF_LEN;
        if (0 != setsockopt(s, SOL_SOCKET, SO_SNDBUF, &sndopt, 4))
        {
            debug("Client:set client sndbuf failed!\n\r");
        }
        sndopt = 1;
        if (0 != setsockopt(s, IPPROTO_TCP, TCP_NODELAY, &sndopt, 4))
        {
            debug("tcp set socket opt 'TCP_NODELAY' failed!\r\n");
        }
        address.sin_family = AF_INET;
        address.sin_port = htons(port);
        address.sin_addr.s_addr = inet_addr(ip_addr);

        if (connect(s, (struct sockaddr*) &address, sizeof(address)) == -1)
        {
            printf("info: connect to server error!\r\n");
            closesocket(s);
            Djy_EventDelay(100*1000);
            return -1;
        }
        //=====================================================
        char *new_head_buf = (char*) malloc(2048);
        memset(new_head_buf, 0, 2048);


            if (buf_domain[0])
            {
                sprintf(new_head_buf, HTTP1_GET_MP3, url, buf_domain);
            }
            else
            {
                sprintf(new_head_buf, HTTP1_GET_MP3, url, ip_addr);
            }

        mark = 0;
        while (1)
        {
            ret = send(s, new_head_buf + mark, strlen(new_head_buf) - mark, 0);
            mark += ret;
            if (mark >= strlen(new_head_buf))
            {
                break;
            }
        }
        if (new_head_buf)
        {
            free(new_head_buf);
        }
        //=====================================================

        char *new_buf = (char*) malloc(BUFSZ);
        if (!new_buf)
        {
            printf("this is new_buf is error\r\n");
            goto ERR_RET;
        }


        while (1)
        {

            memset(new_buf, 0, BUFSZ);
            ret = recv(s, new_buf, BUFSZ - 1, 0);

            if (music_delete)
            {
                printf("-----------------------------!\r\n");
                goto ERR_RET;
            }

            for (int i = 0; i < ret; i++)
            {
                if (new_buf[i] == '0' && new_buf[i + 1] == '\r'
                && new_buf[i + 2] == '\n' && new_buf[i + 3] == '\r'
                && new_buf[i + 4] == '\n')
                {
                    speakend = 1;
                }
            }
            if (ret > 0)
            {
                if (strstr(new_buf, "HTTP/1.1 403 Forbidde"))
                {
                    printf("error: Forbidde !\r\n");
                    free(new_buf);
                    closesocket(s);
                    Djy_EventDelay(100*1000);
                    return -1;
                }

                if (strstr(new_buf, "HTTP/1.1 206 Partial Content") || strstr(new_buf, "HTTP/1.1 200 OK"))
                {
                    char *p1 = NULL, *p2 = NULL, *p3 = NULL;
                    if (p3 = strstr(new_buf, "\r\n\r\n"))
                    {
                        p3 = p3 + 4;
                    }

                    if (p1 = strstr(new_buf, "Content-Length:"))
                    {
                        p1 = p1 + strlen("Content-Length:");

                        if (NULL == p3)
                        {
                            printf("no \\r\\n\\r\\n found.\r\n");
                            break;
                        }
                        mark = 0;
                        while (1)
                        {
                            if (mark >= ret)
                            break;
                            int ret2 = Mp3PlayData(new_buf + mark, ret - mark, 5000);
                            if (ret2 > 0)
                            {
                                mark += ret2;
                            }
                            else
                            {
                                printf("mp3_play_from_file->Mp3PlayData, ret=%d!\r\n", ret);
                                Djy_EventDelay(300*1000);
                            }
                        }
                    }
                    else if (p1 = strstr(new_buf, "chunked"))
                    {
                        char *p3 = NULL;
                        if (p3 = strstr(new_buf, "\r\n\r\n"))
                        {
                            p3 = p3 + 4;
                        }

                        if (NULL == p3)
                        {
                            continue;
                        }
                        mark = 0;
                        while (1)
                        {
                            if (mark >= ret)
                            break;
                            int ret2 = Mp3PlayData(new_buf + mark, ret - mark, 5000);
                            if (ret2 > 0)
                            {
                                mark += ret2;
                            }
                            else
                            {
                                printf("mp3_play_from_file->Mp3PlayData, ret=%d!\r\n", ret);
                                Djy_EventDelay(300*1000);
                            }
                        }
                    }
                }
                else
                {
                    mark = 0;
                    while (1)
                    {
                        if (mark >= ret)
                            break;
                        int ret2 = Mp3PlayData(new_buf + mark, ret - mark, 5000);
                        if (ret2 > 0)
                        {
                            mark += ret2;
                        }
                        else
                        {
                            printf("mp3_play_from_file->Mp3PlayData, ret=%d!\r\n", ret);
                            Djy_EventDelay(300*1000);
                        }
                    }
                }
                to_recv_cnt = TO_RECV_COUNT_MAXT;
                if (speakend)
                {
                    printf("-------------0------------------!\r\n");
                    break;
                }
                Djy_EventDelay(1*1000);
            }
            else if (ret < 0) //timeout
            {
                if (errno == ESHUTDOWN)
                {
                    printf("-------------1------------------!\r\n");
                    break;
                }
                printf("to_recv_cnt :%d\r\n",to_recv_cnt);

                if ((to_recv_cnt-- <= 0)) //(to_recv_cnt-- <= 0)
                {
                    break;
                }
                Djy_EventDelay(100*1000);
            }
            else // ret == 0
            {
                printf("-------------2------------------!\r\n");
                break;
            }
        }

ERR_RET:

        mp3_stream_reset();
        free(new_buf);
        closesocket(s);
        Djy_EventDelay(100*1000);
        return 0;
    }
    else
    {
        printf("socket is failed %d\r\n",s);
        Djy_EventDelay(3000*1000);
    }
    return 0;
}



