#ifndef __MP3_COMMON_H__
#define __MP3_COMMON_H__

#ifdef   __cplusplus
extern "C" {
#endif

#define AUDIO_DMA_BUFF_SIZE (8*1024)

#define GET_TIME_MS() (unsigned int)((unsigned int)DjyGetSysTime()/1000)
#define GET_TIME_US() (unsigned int)DjyGetSysTime()


int ipdumphex(const unsigned char *buf, int size, int flag);

int ProgressBar(int passed, int total, int speed, char *url);

int dumphex16(const unsigned char *buf, int size);

#ifdef   __cplusplus
}
#endif

#endif
