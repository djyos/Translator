#ifndef __APP_RECORDER_H__
#define __APP_RECORDER_H__

#include "ring.h"

#define USE_REC_BUFFER  1
#define REC_BUFFER_CNT  100
#define REC_BUFFER_A_SIZE 512

extern struct RingBuf gAdcRingBuffer;

extern int recorderVadArrive(void);
extern void recorderEventInit(void);

#endif
