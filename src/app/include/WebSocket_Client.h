
//----------------------------------------------------
// Copyright (c) 2014, SHENZHEN PENGRUI SOFT CO LTD. All rights reserved.

// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2014 著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下二条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、此三条件表列，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、此三条件表列，以及下述
//    的免责声明。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
/*
 * WebSocket_Client.h
 *
 *  Created on: 2019-4-16
 *      Author: czz
 */

#ifndef _WEBSOCKET_CLIENT_H_
#define _WEBSOCKET_CLIENT_H_

#include <stdbool.h>
//#define   WEBSOCKET_DEBUG

#define WS_ERROR_TIMEOUT                                -1000
#define WS_ERROR_CREATING_SOCKET                        -1001
#define WS_ERROR_RESOLVING_HOSTNAME                     -1002
#define WS_ERROR_CONNECT_FAILED                         -1003
#define WS_ERROR_BUFFER_TOO_SHORT                       -1004
#define WS_ERROR_WRITING_TO_SOCKET                      -1005
#define WS_ERROR_READING_FROM_SOCKET                    -1006
#define WS_ERROR_HTTP_HANDSHAKE_PROTOCOL_ERROR          -1007
#define WS_ERROR_HTTP_HANDSHAKE_HTTP_ERROR              -1008
#define WS_ERROR_HTTP_REDIRECT_MISSING_LOCATION_HEADER  -1009
#define WS_ERROR_CONTINUATION_NOT_SUPPORTED             -1010
#define WS_ERROR_UNSUPPORTED_OPCODE                     -1011
#define WS_ERROR_PAYLOAD_EXCEEDED_MAX_LENGTH            -1012
#define WS_ERROR_INVALID_PONG_PAYLOAD                   -1013
#define WS_ERROR_TOO_MANY_REDIRECTS                     -1014
#define WS_ERROR_INVALID_REDIRECT_URL                   -1015
#define WS_ERROR_REMOTE_SOCKET_CLOSED                   -1101
#define WS_ERROR_INVALID_URL_SCHEME                     -1201
#define WS_ERROR_RELATIVE_URL_NOT_ALLOWED               -1202
#define WS_ERROR_EMPTY_HOSTNAME                         -1203
#define WS_ERROR_INVALID_URL                            -1204
#define WS_ERROR_HOSTNAME_TOO_LONG                      -1205
#define WS_ERROR_INVALID_PORT                           -1206
#define WS_ERROR_PATH_AND_QUERY_TOO_LONG                -1207
#define WS_ERROR_UNAUTHORIZED                           -1208//认证没通过

// websocket根据data[0]判别数据包类型    比如0x81 = 0x80 | 0x1 为一个txt类型数据包
typedef enum{
    WDT_MINDATA = -20,      // 0x0：标识一个中间数据包
    WDT_TXTDATA = -19,      // 0x1：标识一个txt类型数据包
    WDT_BINDATA = -18,      // 0x2：标识一个bin类型数据包
    WDT_DISCONN = -17,      // 0x8：标识一个断开连接类型数据包
    WDT_PING = -16,     // 0x8：标识一个断开连接类型数据包
    WDT_PONG = -15,     // 0xA：表示一个pong类型数据包
    WDT_ERR = -1,
    WDT_NULL = 0
}WebsocketData_Type;

int WebSocket_LinkServer(char *url);
int WebSocket_send(int fd, char *data, int dataLen, bool isMask, WebsocketData_Type type);
int WebSocket_recv(int fd, char *data, int dataMaxLen, WebsocketData_Type *dataType);


#endif
