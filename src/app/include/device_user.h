
/*
* file:	mqttsn.h
* author:	vincent.cws2008@gmail.com
* history:
* @2019-04-29: initial - circle_buf
*
*/

#ifndef __DEVICE_USER_H_
#define __DEVICE_USER_H_
#ifdef   __cplusplus
extern "C" {
#endif

#include "stdlib.h"
typedef struct StHWorkTopic {
    int topic_id;
    char url[500];
}StHWorkTopic;

int DevGetPastHWork(int id, char *out_json, int len);
int DevGetTodayHWork(int cid, char *out_json, int len);
int DevGetTodayHWorkList(int cid, char *out_json, int len);
int DevGetPastHWorkList(int cid, int page_num, int page_size,  char *out_json, int len);

int DevGetUnfinishedHWork(int qid, int hid, char *out_json, int len);
int DevGetFinishedHWork(int qid, int hid, int uid, char *out_json, int len);
int ClassList(char *out_json, int len);
char *gen_json_homework(int id, struct StHWorkTopic *arr_topic, int num, char *output, int len);
int DevGetHWorkInfo(int id, char *out_json, int len);
int GetTopicList(int hid, char *out_json, int len);
int DevGetNewsList(int pos_start, int pos_end, char *out_json, int len);
int DevGetALLInfo(int news_start, int news_end, int phw_end, char *out_json, int len);
//#define ANALOG_DATA_DEBUG



#ifdef   __cplusplus
}
#endif
#endif


