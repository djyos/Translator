#ifndef __MUSIC_PLAYER_H__
#define __MUSIC_PLAYER_H__

#include <osarch.h>
#include "chain_table.h"
// 接口对播放控制器的消息定义
#define PLAYER_MSG_TYPE_PREV        0x10    // 上一曲
#define PLAYER_MSG_TYPE_NEXT        0x11    // 下一曲
#define PLAYER_MSG_TYPE_VOL_ADD     0x12    // 音量加
#define PLAYER_MSG_TYPE_VOL_MINUS   0x13    // 音量减
#define PLAYER_MSG_TYPE_PAUSE       0x14    // 暂停
#define PLAYER_MSG_TYPE_RESUME_PLAY      0x15    // 播放（恢复）
#define PLAYER_MSG_TYPE_STOP        0x16    // 停止
#define PLAYER_MSG_TYPE_FF          0x17    // 快进
#define PLAYER_MSG_TYPE_FB          0x18    // 快退
#define PLAYER_MSG_TYPE_SHUFFLE     0x19    // 随机播放
#define PLAYER_MSG_TYPE_SINGLE      0x1A    // 单曲循环
#define PLAYER_MSG_TYPE_REALL       0x1B    // 列表循环
#define PLAYER_MSG_TYPE_RENONE      0x1C    // 不循环
#define PLAYER_MSG_TYPE_RESUMETHIS      0x1D    // 重新播放

//#define LOCK_TYPE mutex_t
//#define LOCK_INIT(x)  x = mutex_init(NULL);
//#define LOCK(x) mutex_lock(x)
//#define UNLOCK(x) mutex_unlock(x)
//#define LOCK_DEINIT(x) mutex_del(x)

// 当前播放的资源信息结构定义
struct player_ctrl_info
{
    int FilePlayTo;         // 当前播放歌曲文件位置
    int FileTotal;          // 文件总大小
    int TimePlayTo;         // 当前播放到的时间（秒钟）
    int TimeTotal;          // 资源总长
    int ListPlayTo;         // 当前播放到列表歌曲的位置

    int PlayState;           // 播放控制
    int RepeatMode;         // 循环模式（单曲循环、列表循环、不循环）
//    LOCK_TYPE    lock;

};

// 初始化列表
void pllsInit(void);

//从第pos首开始播放
void pllsWhere(int pos);

//获得歌曲数目
int getLengthList(void);

//删除一首歌
void pllsDelete(int pos);

// 清除播放列表 、将会停止所有音乐的播放
void pllsClear(void);

// 添加一首歌到播放列表中、返回0正常
int pllsAppendItem(int data,char *url,char *song,char *singer);

//列表播放
int playlistPlay(void);

// 播放控制器初始化
void playerInit(void);

//播放控制响应
int parseMsgBuf(void);

// 上一首
// 返回值：
//0列表为空；1正常播放
int playerPrev(void);

// 下一首
// 返回值：
//0列表为空；1正常播放
int playerNext(void);

// 音量+
// 返回值：
// 设置完成之后的音量值
int playerVolumeAdd(void);

// 音量-
// 返回值：
// 设置完成之后的音量值
int playerVolumeMinus(void);

// 暂停
void playerPause(void);

// 恢复播放
// 如果之前是暂停，那么从暂停的位置开始播放
// 如果之前是停止，那么从停止的那首歌重新播放
void playerResume(void);

// 选择第idx开始播放
void playerStartFrom(int idx);

// 停止播放
void playerStop(void);

// 重新播放播放
void playerRest(void);

// 快进 secs 秒
void playerFastForward(void);

// 快退 secs 秒
void playerFastBack(void);

// 随机播放
void playerShuffle(void);

// 单曲循环
void playerSingleCircu(void);

// 列表循环
void playerRepeatAll(void);

#endif
