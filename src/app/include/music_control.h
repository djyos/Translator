/*
 * VoicePrompt.h
 *
 *  Created on: 2019年5月13日
 *      Author: c
 */

#ifndef MUSCICONTROL_H_
#define MUSCICONTROL_H_

#include <stddef.h>

//AI返回状态情况
typedef enum{
    AI_ALL = 1,
    AI_SPEAK = 2,
    AI_LINK = 3,
    AI_NONE = 4,
    AI_EXIT = 5,
}GetAi;

//AI请求类型
typedef enum{
    AI_TYPE_ALL = 0,
    AI_TYPE_SPEAK = 1,
    AI_TYPE_LINK = 2,
    AI_TYPE_NONE = 3,
    AI_TYPE_EXIT = 4,
}RequestAiType;



#endif /* MUSCICONTROL_H_ */
