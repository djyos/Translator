#ifndef __APP_WS_CLIENT_H__
#define __APP_WS_CLIENT_H__

typedef enum __WsClientReqType
{
    WS_CLIENT_REQ_TYPE_NULL = 0,
    WS_CLIENT_REQ_TYPE_PREPARE = 1,     // 准备，即连接到WS服务器
    WS_CLIENT_REQ_TYPE_COMPLETE = 2,    // 可以断开WS连接了
    WS_CLIENT_REQ_TYPE_AGIN = 3,        //请求天气信息
} WsClientReqType;

extern void wsClientEventInit(void);
extern void wsClientRequestCommand(WsClientReqType type);

#endif
