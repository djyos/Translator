#include "LoopRec.h"

#include "LoopObj.c"

#define assert(x) if (!(x)) {printf("assert error: %s!\r\n", __FUNCTION__); while (1) {};}

#define debug //printf

struct StLoopRecMgr gLoopRecMgr = {0};

int LoopRecMgrInit()
{
    struct StLoopRecMgr *pMgr =  &gLoopRecMgr;
    if (pMgr->flag_init != 0) {
        return 0;
    }
    memset(pMgr, 0, sizeof(struct StLoopRecMgr));
    pMgr->flag_init = 1;

    pMgr->pLoopObjMgr = CreateLoopObjMgr(sizeof(struct StFrmRec), ARRAY_REC_MAX, LOOP_MODE_MOVESTEP);
    if (!pMgr->pLoopObjMgr) {
        printf("error: %s->CreateLoopObjMgr failed!\r\n", __FUNCTION__);
        return -1;
    }
    pMgr->total_len = 0;
    pMgr->total_dur = 0;
    return 0;
}
int LoopRecMgrPushTail(int frm_len, int frm_dur)
{
    int ret = 0;
    struct StFrmRec temp;
    struct StLoopRecMgr *pMgr = &gLoopRecMgr;
    assert(pMgr->pLoopObjMgr);
    temp.frm_length = frm_len;
    temp.frm_duration = frm_dur;
    pMgr->total_len += frm_len;
    pMgr->total_dur += frm_dur;
    ret = PushLoopObjTail(pMgr->pLoopObjMgr, &temp);
    return ret;
}
int LoopRecMgrPullHead(int *frm_len, int *frm_dur)
{
    int ret = 0;
    struct StFrmRec temp;
    struct StLoopRecMgr *pMgr = &gLoopRecMgr;
    assert(pMgr->pLoopObjMgr);
    ret = PullLoopObjHead(pMgr->pLoopObjMgr, &temp);
    if (ret >= 0) {
        if (frm_len) *frm_len = temp.frm_length;
        if (frm_dur) *frm_dur = temp.frm_duration;
        pMgr->total_len -= temp.frm_length;
        pMgr->total_dur -= temp.frm_duration;
    }
    return ret;
}
int LoopRecMgrDeInit()
{
    struct StLoopRecMgr *pMgr =  &gLoopRecMgr;
    if (pMgr->pLoopObjMgr)
        DeleteLoopObjMgr(pMgr->pLoopObjMgr);
    pMgr->flag_init = 0;
    return 0;
}

int LoopRecMgrReset()
{
    struct StLoopRecMgr *pMgr = &gLoopRecMgr;
    ResetLoopObj(pMgr);
    pMgr->total_len = 0;
    pMgr->total_dur = 0;
    return 0;
}

int GetFrmRecTotalLens()
{
    struct StLoopRecMgr *pMgr = &gLoopRecMgr;
    return pMgr->total_len;
}

int GetFrmRecTotalDurs()
{
    struct StLoopRecMgr *pMgr = &gLoopRecMgr;
    return pMgr->total_dur;
}


