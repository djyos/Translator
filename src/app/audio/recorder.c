//#include "djyos.h"
//#include <stdlib.h>
//#include "recorder.h"
//#include <sys/socket.h>
//#include "LoopBytes.h"
//#include "board.h"
//#include "../comm/comm_api.h"
//#include <Iboot_info.h>
//#include "../comm/recorder_cache.h"
//
//#define TRNG_BASE (0X00802480)
//#define TRNG_CTRL (TRNG_BASE + 0*4)
//#define TRNG_EN (0x01UL << 0)
//#define TRNG_DATA ((TRNG_BASE + 1*4))
//
//#define MAX_NOTIFY_SIZE 3200
//
//#define NEW_SZ (32*1024)//(16*1024)
//#define REGULAR_TIME 5000
//
//extern void sdcard_power_off(void);
//
//int RecCachePrintInfo();
//int oss_append_next(char *path, unsigned char *data, int len, int flag, int pos, int timeout_ms);
//ptu32_t TaskRecordSend();
//ptu32_t TaskRecordDo();
//bool_t TaskRecordSdInit(void);
//
////WAV录音
//UINT32 audio_adc_ctrl(UINT32 cmd, void *param);
//
//enum {
//    EKEY_STOP = 0,
//    EKEY_START,
//};
//
//typedef int (*FUN_CB2) (int code, int type, void *data);
//
//enum{
//    TYPE_RECORD_DO = 0,
//    TYPE_RECORD_SEND,
//};
//
//enum{
//    CODE_OVERFLOW = 0,
//    CODE_UPLOADED_OK,
//    CODE_UPLOADED_FAIL,
//};
//
//volatile int key_record = 0;
//
//char gRecordUrl[256];
//
////初始化WAV头.
//void recoder_wav_init(__WaveHeader* wavhead) //初始化WAV头
//{
//    wavhead->riff.ChunkID=0X46464952;   //"RIFF"
//    wavhead->riff.ChunkSize=0;          //还未确定,最后需要计算
//    wavhead->riff.Format=0X45564157;    //"WAVE"
//    wavhead->fmt.ChunkID=0X20746D66;    //"fmt "
//    wavhead->fmt.ChunkSize=16;          //大小为16个字节
//    wavhead->fmt.AudioFormat=0X01;      //0X01,表示PCM;0X01,表示IMA ADPCM
//    wavhead->fmt.NumOfChannels=0X01;        //單声道
//    wavhead->fmt.SampleRate=audio_sample_rate_8000;//设置采样速率
//    wavhead->fmt.ByteRate=wavhead->fmt.SampleRate*2;//字节速率=采样率*通道数*(ADC位数/8)
//    wavhead->fmt.BlockAlign=2;          //块大小=通道数*(ADC位数/8)
//    wavhead->fmt.BitsPerSample=16;      //16位PCM
//    wavhead->data.ChunkID=0X61746164;   //"data"
//    wavhead->data.ChunkSize=0xEFFFFFFF;          //数据大小,还需要计算
//}
//
//void deep_sleep(void)
//{
//    sdcard_power_off();
//    CloseSpeaker();
//    Djy_DelayUs(100000);
//    __LP_BSP_EntrySleepL4( );
////    LP_DeepSleep();
//}
//
////void djy_audio_init()
////{
////    audio_init();
////    djy_audio_adc_open(RX_DMA_BUF_SIZE,1,audio_sample_rate_8000/*audio_sample_rate_16000*/,AUD_ADC_LINEIN_DETECT_PIN);
////}
//
////提示信息
////mode:0,录音模式;1,放音模式
//void recoder_remindmsg_show(u8 mode)
//{
//    if(mode==0) //录音模式
//    {
//        printf("KEY0:REC/PAUSE\r\n");
//        printf("KEY2:STOP&SAVE\r\n");
//        printf("WK_UP:PLAY");
//    }else       //放音模式
//    {
//        printf("KEY0:STOP Play\r\n");
//        printf("WK_UP:PLAY/PAUSE\r\n");
//    }
//}
//
//bool_t TaskRecordDoInit(void)
//{
//    u16 event_recorder;
//    void *recorder_stack=psram_malloc (0x2000);
//    if (recorder_stack==0) return 0;
//    event_recorder = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS, 0, 0, TaskRecordDo, recorder_stack, 0x2000, "DoRecord");
//    if(event_recorder != CN_EVENT_ID_INVALID)
//        Djy_EventPop(event_recorder, NULL, 0, NULL, 0, 0);
//    else
//        printf("TaskRecordDoInit Fail!\r\n");
//
//    return true;
//}
//
//int record_set_name (char *oss_path)
//{
//    //printf("record_set_name: oss_path=\"%s\"!\r\n", oss_path==0?"":oss_path);
//    strcpy(gRecordUrl, oss_path);
//    return 0;
//}
//
//int record_ctrl(int val, char *oss_path)
//{
//    //printf("record_ctrl: val=%d, oss_path=\"%s\"!\r\n", val, oss_path==0?"":oss_path);
//    key_record = val;
//    if (oss_path) {
//        if (val) {
//            strcpy(gRecordUrl, oss_path);
//        }
//        else {
//            gRecordUrl[0] = 0;
//        }
//    }
//    return 0;
//}
//
//static int record_start()
//{
//    return key_record==EKEY_START;
//}
//
//static int record_stop()
//{
//    return key_record==EKEY_STOP;
//}
//
//int Add2RecordCache(unsigned char *url, unsigned char *data, int len, int flag, int timeout_ms)
//{
//    int ret = -1;
//    s64 timemark = DjyGetSysTime();
//    int mark = 0;
//    unsigned int times = 0;
//
//    if (url == 0 || url[0]==0 || data==0 || len <= 0) return -1;
//
//    while (1) {
//        ret = RecCacheAppendTail(url, &data[mark], len-mark, flag);
//        if (ret > 0) {
//            if (flag == E_APPEND_HEAD) { //如果append新创建，则下次需要flag = E_APPEND_TAIL;
//                flag = E_APPEND_TAIL;
//            }
//            mark += ret;
//        }
//        else if (ret == 0) {//超时
//            if (times++ == 0) printf("warning: %s,RecCacheAppendTail, timeout_ms=%d!\r\n", __FUNCTION__, timeout_ms);
//            Djy_EventDelay(20*1000);
//        }
//        else {//错误
//            if (times++ == 0) printf("warning: RecCacheAppendTail, ret=%d!\r\n", ret);
//            Djy_EventDelay(20*1000);
//        }
//        if (mark >= len){
//            ret = mark;
//            break;
//        }
//        if ((int)((DjyGetSysTime()- timemark)/1000) > timeout_ms)
//        {
//            printf("warning: %s timeout!\r\n", __FUNCTION__);
//            ret = mark;
//            break;
//        }
//    }
//    return ret;
//}
//
//
////删除指定的url缓冲。url格式例如/devtemp/4HH1HU20030001R/tmp_295_839_116.wav
//int RecCacheDelItem(char *url);
////item, 记录数组的大小，默认是10
//int RecCacheReset(int items);
////功能：检查指定某项数据是否发送完成。
////ret = 0: 缓冲里存在此项，但是没发送完成。
////ret < 0: 缓冲里不存在该项
////ret > 0: 发送完成。
//int RecCacheCheckReadStatus(char *url);
////此函数不可以阻塞
//#include "../GuiWin/inc/WinSwitch.h"
//int RecordNotifyMsg (int code, int type, void *data)
//{
//    int ret = 0;
//    printf("\r\n");
//    printf("info: code=%d, type=%d, %s\r\n", code, type, __FUNCTION__);
//
//    switch (type) {
//    case TYPE_RECORD_DO: //录音数据满，不能再录音，提示用户，返回-1表示退出当前录音，回到初始状态。
//        printf("info: TYPE_RECORD_DO->CODE_OVERFLOW\r\n");
//        Ui_StopRecord();
//        RecCacheDelItem((char*)data);
//        SwitchToFriendlyReminders(enum_buf_overflow);
//        ret = -1;
//        break;
//    case TYPE_RECORD_SEND:
//        switch(code) {
//        case CODE_UPLOADED_OK: //缓冲区每发完一个小题，就告诉上层，上层记录下来，提交的时候有哪些题没上传完可以查询
//            printf("info: url=%s, CODE_UPLOADED_OK\r\n", (char*)data);
//            int mark_TopicSendOk(char *url);
//            mark_TopicSendOk((char*)data);
//            ret = 0;
//            break;
//        case CODE_UPLOADED_FAIL: //缓冲区发送过程中发现这个wav的数据一直发送不出去（重试3次，退避5秒），最后超时就调用这个函数
//            printf("info: url=%s, CODE_UPLOADED_FAIL\r\n", (char*)data);
//            int Pop_CodeUploadedFail(char * data);
//            Pop_CodeUploadedFail((char*)data);
//            ret = -1;
//            break;
//        default:
//            ret = 0;
//            break;
//        }
//        break;
//    default:
//        ret = 0;
//        break;
//    }
//    //RecCachePrintInfo();
//    /* ret = 0: 不处理
//     * ret = -1: 断开
//     * 如果录音TYPE_RECORD_DO任何-1，则录音断开；
//     * 如果TYPE_RECORD_SEND->CODE_UPLOADED_FAIL返回-1，则需要放弃当前发送处理，当前录音标志废弃（no_used), 需要重新录音。
//     */
//    return ret;
//}
//
//ptu32_t TaskRecordDo()
//{
//    int ret=0;
//    int cache_ret = 0;
//    s64 timemark  = 0;
//    int total = 0;
//    FUN_CB2 RdNotify = 0;
//    char url[512];
//    __WaveHeader wav_header;
//    recoder_wav_init(&wav_header);
//
//    unsigned char *pnew = (unsigned char*)malloc(MAX_NOTIFY_SIZE);
//    if (pnew==0) {
//        printf("error: Recorder_EventMain, malloc, pnew==NULL!\r\n");
//    }
//    while (1) {
//        if (!record_start()) {
//            Djy_EventDelay(20*1000);
//            continue;
//        }
//
//
//        if (RecordNotifyMsg) RdNotify = RecordNotifyMsg;
//
//        strcpy(url, gRecordUrl);
//        total = 0;
//
//        printf("info: Key Press Down, Recording now ...\r\n");
//        RecCachePrintInfo();
//        cache_ret = Add2RecordCache(url, (unsigned char*)&wav_header, sizeof(__WaveHeader), E_APPEND_HEAD, 5000);
//        if (cache_ret != sizeof(__WaveHeader)){
//            printf("<1>warning: %s->Add2RecordCache, len=%d, cache_ret=%d!\r\n", __FUNCTION__, sizeof(__WaveHeader), cache_ret);
//            RecCachePrintInfo();
//            if (RdNotify && RdNotify(CODE_OVERFLOW, TYPE_RECORD_DO, url)<0) {
//               RecCacheSetItemDone(0);//设置结束标志,录音按键类型，参数为0
//               key_record = EKEY_STOP; //设置停止，防止重进。
//               continue;
//            }
//        }
//
//        timemark = DjyGetSysTime();
//
//        total += cache_ret;
//        while (1) {
//            if (record_stop()) {//录音按钮放开，把剩下DMA数据读出和填充到环形缓冲区。
//                //printf("info: Key Press Up, Recording Ended (total:%d)...\r\n", total);
//                //设置结束标志,录音按键类型，参数为0
//                RecCacheSetItemDone(0);
//                break;
//            }
//            ret = djy_audio_adc_read((char*)pnew, MAX_NOTIFY_SIZE);
//
//            if (ret > 0) {
//                cache_ret = Add2RecordCache(url, pnew, ret, E_APPEND_TAIL, 5000);
//                if (cache_ret != ret){
//                    printf("warning: %s->Add2RecordCache, len=%d, cache_ret=%d!\r\n", __FUNCTION__, ret, cache_ret);
//                    if (RdNotify && RdNotify(CODE_OVERFLOW, TYPE_RECORD_DO, url)<0) {
//                       RecCacheSetItemDone(0);//设置结束标志,录音按键类型，参数为0
//                       key_record = EKEY_STOP; //设置停止，防止重进。
//                       break;
//                    }
//                }
//                total += ret;
//                if ((int)((DjyGetSysTime()-timemark)/1000) > 500) {//500ms打印一次
//                    printf(".");//打印.代码正在录音中
//                    timemark = DjyGetSysTime();
//                }
//            }
//            else {
//                Djy_EventDelay(20*1000);
//            }
//        }
//    }
//    if (pnew) {
//        free(pnew);
//        pnew = NULL;
//    }
//    return 0;
//}
//
//bool_t TaskRecordSendInit(void)
//{
//    u16 event_send;
//    void *send_stack=psram_malloc (0x2000);
//    event_send = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS, 0, 0, TaskRecordSend, send_stack, 0x2000, "RecordSend");
//    if(event_send != CN_EVENT_ID_INVALID)
//        Djy_EventPop(event_send, NULL, 0, 0, 0, 0);
//    else
//        printf("TaskRecordSendInit Failed!\r\n");
//
//    return true;
//}
//
//int record_init()
//{
//    RecCacheInit(10);
//    OssMgrInit();
//    TaskRecordDoInit();
//    TaskRecordSendInit();
//    return 0;
//}
//
//void SetUploadMsgCB(int (*event_cb) (int state))
//{
//}
//
//
////return:
//// ret == 0: 超时，连续发送多次，都没返回正常数据。
//// ret >  0: 成功，返回值是下一次传进的追加位置。
//int RecordNextSend(unsigned char *url, unsigned char *data, int len, int is_append, int pos, int timeout_ms)
//{
//    int status = 0;
//    int next_mark = 0;
//    int times = 3;
//
//    while (len > 0) {//必须保证数据上传到服务器
//        //printf("info: %s, len=%d!\r\n", __FUNCTION__, len);
//        status = oss_append_next(url, data, len, is_append, pos, timeout_ms);
//        if (status > 0){//上传成功
//            break;
//        }
//        else if (status == 0) {//超时
//            printf("warning: oss_append_data, timeout, timeout_ms=%d!\r\n", timeout_ms);
//            timeout_ms += 10000;//加大超时
//        }
//        else {//错误
//            printf("warning: oss_append_data, status=%d!\r\n", status);
//        }
//        if (times == 0) {//重试3次，返回，上报信息
//            //上报ss
//            status = 0;
//            printf("ERROR: %s send tailed(retry:%d)!\r\n", __FUNCTION__, times);
//            break;
//        }
//        times--;
//    }
//    return status;
//}
//
//
//ptu32_t TaskRecordSend()
//{
//    int ret = 0;
//    int mark = 0;
//    int status = 0;
//    int timeout_ms = 0;
//    int next_mark = 0;
//    int is_finish = 0;
//
//    char url[256]="";
//    char url_mark[256]="";
//    unsigned int id = 0;
//    unsigned int id_mark = -1;
//    int is_append = 0;
//
//    char temp[200] = "";
//    static char sbuf[512];
//    memset(sbuf, 0, sizeof(sbuf));
//
//    FUN_CB2 RdNotify = RecordNotifyMsg;
//
//    char *pnew = malloc(NEW_SZ);
//    if (pnew == 0) {
//        printf("ERROR: TaskRecordSend, malloc=NULL!\r\n");
//        return 0;
//    }
//    mark = 0;
//
//    int totals = 0;
//    timeout_ms = 10*1000;
//    while (1) {
//        url[0] = 0;
//        ret = RecCacheReadHead(url, &id, sizeof(url), &pnew[mark], NEW_SZ-mark, &is_finish);
//
//        //RecCachePrintInfo();
//        //sprintf(sbuf, "RH[r:%d,f:%d,m=%d,u:%s],", ret, is_finish, mark, url[0]? (strlen(url) < 20 ? url : &url[strlen(url)-20]) :"-");
//
//        if (ret < 0 && url[0]==0) {
//            //RecCachePrintInfo();
//            url_mark[0] = 0;
//            mark = 0;
//            next_mark = 0;
//            totals = 0;
//            Djy_EventDelay(1000*1000);
//            continue;
//        }
//
//        if (ret > 0 && url[0]) {
//            if (strcmp(url_mark, url)==0 && id_mark == id){//必须相同url和相同id才能确认是同一个录音
//                mark += ret;
//            }
//            else {//新建
//                totals = 0;
//                next_mark = 0;
//                is_append = 0;
//                id_mark = id;
//                strncpy(url_mark, url, sizeof(url_mark));
//                printf("info: create new oss file \"%s\"!\r\n", url);
//                //有一种情况处理一下，就是中间有些no_use标志，下载到一半，没结束标志，
//                //然后直接获取到下个url,这些需要清除前面的数据
//                if (mark > 0) {
//                    printf("warning: discard(mark=%d), ret=%d!\r\n", mark, ret);
//                    memmove (pnew, &pnew[mark], ret);
//                }
//                mark = ret;
//            }
//        }
//
//        if (mark >= NEW_SZ) { //必须要满包菜发送，减少操作网络太频繁。
//            //printf("--->>> 1. info: is_append=%d, next_mark=%d <<<---\r\n", is_append, next_mark);
//            next_mark =  RecordNextSend(url, pnew, mark, is_append, next_mark, timeout_ms);
//            if (next_mark <= 0 && RdNotify) { //多次上传失败，发送通知
//                RdNotify(CODE_UPLOADED_FAIL, TYPE_RECORD_SEND, url);
//            }
//            totals += mark;
//            is_append = 1; //要必须第一次发完才能设置is_append
//            mark = 0;
//            printf("#");//发送数据打印
//        }
//        else if (is_finish && mark > 0) { //最后一包，第2个地方结束事件，发通知。
//            //printf("--->>> 2. info: is_append=%d, next_mark=%d <<<---\r\n", is_append, next_mark);
//            next_mark =  RecordNextSend(url, pnew, mark, is_append, next_mark, timeout_ms);
//            if (next_mark <= 0 && RdNotify) { //多次上传失败，发送通知
//                RdNotify(CODE_UPLOADED_FAIL, TYPE_RECORD_SEND, url);
//            }
//            is_append = 1;
//            totals += mark;
//        }
//        //sprintf(temp, "NET[sta:%08d,nm:%08d,t:%08d,],", status, next_mark, totals);
//        //strcat(sbuf, temp);
//        int rds = RecCacheCheckReadStatus(url);
//        if (rds == 1) {//结束当前url读，这里发通知事件
//
//            printf("\r\nEVENT UPLOAD ENDED, URL(%d):%s!!\r\n", totals, url);
//            if (RdNotify) RdNotify(CODE_UPLOADED_OK, TYPE_RECORD_SEND, url);
//            url_mark[0] = 0; //清空记录的，不然连续按键会一直追加。
//            mark = 0;
//            next_mark = 0;
//            totals = 0;
//        }
//        //sprintf(temp, "END[e:%d],", rds);
//        //strcat(sbuf, temp);
//
//        //strcat(sbuf, "              \r");
//        //printf("%s", sbuf);
//
//        if (mark < NEW_SZ / 2) { //mark小于NEW_SZ一半数据就释放CPU
//            Djy_EventDelay(10*1000);
//        }
//    }
//}
//
//int MediaIsUploading()
//{
//    return 0;
//}
///*
// *  return < 0: break by something or timeout
// *  return = 0: ok, done.
// *  return > 0: uploading.
// */
//int MediaGetUploadStatus()
//{
//    return 0;
//}
//
//static void trang_active(u8 enable)
//{
//    u32 value;
//    value = REG_READ(TRNG_CTRL);
//
//    if(enable)
//    {
//        value |= TRNG_EN;
//    }
//    else
//    {
//        value &= ~TRNG_EN;
//    }
//
//    REG_WRITE(TRNG_CTRL,value);
//}
//
//void random_init(void)
//{
//  u32 crc,crc2;
//  u8 i=8;
//  trang_active(1);
//  while(i--)
//  {
//      crc2 =trng_get_random();
//      crc ^= crc2;
//      //printf("\n\r\n\r random_init NUm ====== 0x%x,0x%x\n\r\n\r",crc,crc2);
//  }
//  srand(crc);
//}
//
//bool_t Sn_InfoSave(char *info ,u32 len);
//bool_t Sn_InfoLoad(char *buf ,u32 maxlen);
//
//int  Get_DeviceSn(char *buf,int buflen)
//{
////    const char *default_sn = "DJYOS0015";
//    char default_sn[16];
//    memset(default_sn, 0 ,sizeof(default_sn));
//    if(Get_APP_ProductInfo(APP_HEAD_SN, default_sn, sizeof(default_sn)))
//    {
//        char sn_tmp[30];
//        memset(sn_tmp, 0, sizeof(sn_tmp));
//        if (Sn_InfoLoad(sn_tmp , strlen(default_sn))) {
//            if (sn_tmp[0]==0) {
//                if (Sn_InfoSave(default_sn , strlen(default_sn))) {
//                    printf("info: write sn info ok!\r\n");
//                }
//                else {
//                    strcpy(sn_tmp, default_sn);
//                }
//            }
//        }
//        if (sn_tmp[0]!= 0 && (int)(strlen(sn_tmp)+1) <= buflen) {
//            strcpy(buf, sn_tmp);
//            //printf("info: sn: \"%s\"!\r\n", sn_tmp);
//            return 0;
//        }
//    }
//    return -1;
//}
//
//
//
