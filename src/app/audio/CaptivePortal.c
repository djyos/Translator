//----------------------------------------------------
// Copyright (c) 2018, Djyos Open source Development team. All rights reserved.
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:

// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. As a constituent part of djyos,do not transplant it to other software
//    without specific prior written permission.

// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.
//-----------------------------------------------------------------------------
// Copyright (c) 2018，著作权由都江堰操作系统开源开发团队所有。著作权人保留一切权利。
//
// 这份授权条款，在使用者符合以下三条件的情形下，授予使用者使用及再散播本
// 软件包装原始码及二进位可执行形式的权利，无论此包装是否经改作皆然：
//
// 1. 对于本软件源代码的再散播，必须保留上述的版权宣告、本条件列表，以
//    及下述的免责声明。
// 2. 对于本套件二进位可执行形式的再散播，必须连带以文件以及／或者其他附
//    于散播包装中的媒介方式，重制上述之版权宣告、本条件列表，以及下述
//    的免责声明。
// 3. 本软件作为都江堰操作系统的组成部分，未获事前取得书面许可，不允许移植到非
//    都江堰操作系统环境下运行。

// 免责声明：本软件是本软件版权持有人以及贡献者以现状（"as is"）提供，
// 本软件包装不负任何明示或默示之担保责任，包括但不限于就适售性以及特定目
// 的的适用性为默示性担保。版权持有人及本软件之贡献者，无论任何条件、
// 无论成因或任何责任主义、无论此责任为因合约关系、无过失责任主义或因非违
// 约之侵权（包括过失或其他原因等）而起，对于任何因使用本软件包装所产生的
// 任何直接性、间接性、偶发性、特殊性、惩罚性或任何结果的损害（包括但不限
// 于替代商品或劳务之购用、使用损失、资料损失、利益损失、业务中断等等），
// 不负任何责任，即在该种使用已获事前告知可能会造成此类损害的情形下亦然。
//-----------------------------------------------------------------------------
// =============================================================================

// 模块描述: TCP服务器发送测试
// 模块版本: V1.00
// 创建人员: Administrator
// 创建时间: 9:23:45 AM/Jul 7, 2014
// =============================================================================
// 程序修改记录(最新的放在最前面):
// <版本号> <修改日期>, <修改人员>: <修改功能概述>
// =============================================================================
#include <stdint.h>
#include <arpa/inet.h>
#include <djyos.h>
#include <errno.h>
#include <generic.h>
#include <multiplex.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <typedef.h>
#include "project_config.h"
#include "rw_pub.h"
#include "cpu_peri_wifi.h"

#define CN_DNS_SERVERPORT        53
#define CN_WEB_SERVERPORT        8080
#define CN_BUF_LEN               1400
#define CN_SOCKBUF_LEN           (CN_BUF_LEN*10)

static const char* http_req_token = "HTTP/1.";
static const char* getwifiinfo = "GET /getwifiinfo.html";
static struct MutexLCB * wifiInfo;
//const char* http_req_token = "GET / HTTP/1.1\r\n";

static const char httpnocontent[] =  "http/1.1 204 no content\r\n\r\n";
static const char* http_conf_token = "GET /conwifi.html?";//"GET /conwifi.html?ssid=vvvvv&pwd=12345678 HTTP/1.1\r\n";

static const char Successful_html[] = {
#include "WifiConfigurationSuccessful.dat"
};
#if 1
static const char index_html_1[] = {
#include "configwifi1.dat"
};
static const char index_html_2[] = {
#include "configwifi2.dat"
};
#else

const char index_html[] = \
       "<!DOCTYPE html>\r\n"\
       "<html>\r\n"\
       "<head>\r\n"\
       "<meta?charset=\"utf-8\">\r\n"\
       "<title>WIFI USERNAME AND PASSWORD</title>\r\n"\
       "</head>\r\n"\
       "<body>\r\n"\
       "<form action=\"conwifi.html\">\r\n"\
         "SSID: <input type=\"text\" name=\"ssid\" maxlength=\"32\"><br>\r\n"\
         "KEY: <input type=\"text\" name=\"pwd\" maxlength=\"32\"><br>\r\n"\
         "<input type=\"reset\" value=\"clean\">\r\n"\
         "<input type=\"submit\" value=\"submit\">\r\n"\
       "</form>\r\n"\
       "<p>config user ssid and key</p >\r\n"\
       "</body>\r\n"\
       "</html>\r\n";
#endif


static const char http_resp_header[] = "HTTP/1.%d %d OK\r\n"\
                                    "Content-type: text/html; charset=utf-8\r\n"
                                    "Content-Length: %d\r\n"\
                                    "Pragma: no-cache\r\n"\
                                    "\r\n";
///"Expires:Fri, 10 Apr 2019 14:00:00 GMT\r\n"

//static const char *conf_resp = "config ok";

static const char *ObjectMoved  = "HTTP/1.1 302 Object Moved\r\n"
                                  "Location: http://%s:82\r\n"
                                  "Connection: close\r\n"
                                  "Content-Type: text/html; charset=utf-8\r\n"
                                  "Content-Length: 0\r\n\r\n";

typedef enum{
    http_git,
    wifi_info,
    other,
}HttpType;

typedef struct
{
    u16             tid;        /* Transaction ID */
    u16             flags;      /* Flags */
    u16             nqueries;   /* Questions */
    u16             nanswers;   /* Answers */
    u16             nauth;      /* Authority PRs */
    u16             nother;     /* Other PRs */
    u8              data[0];    /* Data, variable length */
}tagDnsHeader;

static int server;
static bool_t exitserver = 0;

#define UNUSED(x) (void)(x)

u32 Delay_Time;
static int  Reset_DelayTime()
{
    Delay_Time = 120*10;
    return 0;
}

static ptu32_t Time_Server(void)
{
    while(Delay_Time!=0)
    {
        Djy_EventDelay(100*mS);
        Delay_Time--;
//        if(Delay_Time%10 == 0)
//            printf("Delay_Time: %d \n\r",Delay_Time);
    }
    void CloseScreen();
    CloseScreen();
    int Set_ShoutDown();
    Set_ShoutDown();
    return 0;
}
static char  wifiinfobuf[1024];
static void scan_ApCallBack(void *arg, uint8_t vif_idx)
{
    UNUSED(arg);
    UNUSED(vif_idx);
    u32 buflen = sizeof(wifiinfobuf);
    struct sta_scan_res *scan_result = 0;
    char apbuf[50];
    u32 len;
    uint32_t i,j;
    uint32_t cnt ;
    int Duplicatename =0;
    const char * json_head = "{\"AP\":[";
    const char * json_ap = "{\"AP\":\"%s\"},";
    const char * json_aplen = "],\"APLEN\":\"%d\",";
    const char * json_end = "\"CS\":\"0\",\"MAC\":\"AAAA0007959D\",\"TYPE\":\"WS01\",\"PASS\":\"0477B68BFEEB375018FBBEE888D23002\"}";

    if(false == Lock_MutexPend(wifiInfo,1000*mS))
        return ;
    len = strlen(json_head) + strlen(json_end) + 1;
    if(len >= buflen)
        goto error;
    strcpy(wifiinfobuf,json_head);
    cnt = DjyWifi_GetScanResult(&scan_result);
    if(NULL == scan_result)
        goto error;

    for(i=0;i<cnt;i++)
    {
        for(j=0;j<i;j++)//去掉重名
        {
            if(0 == strcmp(scan_result[i].ssid,scan_result[j].ssid))
                break;
        }
        if(i!=j)
        {
            Duplicatename++;
            continue;
        }
        len += strlen(json_ap) -2 + strlen(scan_result[i].ssid);
        if(len >= buflen)
            goto error;
        sprintf(apbuf,json_ap,scan_result[i].ssid);
        strcat(wifiinfobuf,apbuf);
    }
    wifiinfobuf[strlen(wifiinfobuf)-1] = '\0';
    sprintf(apbuf,json_aplen,cnt-Duplicatename);
    strcat(wifiinfobuf,apbuf);
    strcat(wifiinfobuf,json_end);

error:
    if(scan_result) free(scan_result);
    Lock_MutexPost(wifiInfo);
}

static bool_t Wifi_InfiPack(char *buf ,u32 buflen,u32 timeout)
{
    bool_t falg = false;
    if(false == Lock_MutexPend(wifiInfo,timeout))
        return falg;
    if(buflen > (strlen(wifiinfobuf)+1))
    {
        strcpy(buf,wifiinfobuf);
        falg = true;
    }
    Lock_MutexPost(wifiInfo);
    return falg;
}
static int send_http (int fd, const char *content, int content_len ,int type,int num)
{
    int ret = 0;
    int size = 0;
    int sended = 0;
    int cnts = 0;
    char *pnew = 0;
    if (content_len < 0 || fd < 0) {
        ret = -100;
        goto END_SEND;
    }
    if(type != 0 && type != 1){
        ret = -200;
        goto END_SEND;
    }

    size = 1000;
    pnew =  (char*)malloc(1000);
    if (!pnew) {
        ret = -200;
        goto END_SEND;
    }
    memset(pnew, 0, size);

    if (content_len==0 || content==NULL) {
        content_len = 0;
        content = "";
    }
    //send http header
    sended = 0;
    cnts = 0;
    sprintf(pnew, http_resp_header,type,num,content_len);
    while (sended < (int) strlen(pnew)) {
        cnts = send(fd, &pnew[sended], strlen(pnew)-sended, 0);
        if (cnts > 0) {
            sended += cnts;
        }
        if (errno == ESHUTDOWN) {
            ret = -1;
            goto END_SEND;
        }
    }
    //send http content
    sended = 0;
    cnts = 0;
    while (sended < content_len) {
        cnts = send(fd, &content[sended], content_len-sended, 0);
        if (cnts > 0) {
            sended += cnts;
        }
        if (errno == ESHUTDOWN) {
            ret = -1;
            goto END_SEND;
        }
    }
END_SEND:
    if (pnew) free(pnew);
    return ret;
}

static int send_http_configwifi (int fd)
{
    int ret = 0;
    int size = 0;
    int sended = 0;
    int cnts = 0;
    int len = 0;
    char *pnew = 0;
    char *apbuf = 0;
    int apbuflen = 0;
    int content_len = 0;
    char *content = NULL;

    if (fd < 0) {
        ret = -100;
        goto END_SEND;
    }
    size = 1000 + 1000;
    pnew =  (char*)malloc(size);
    if (!pnew) {
        ret = -200;
        goto END_SEND;
    }
    apbuf =  pnew+1000;
    memset(pnew, 0, size);
    if(true == Wifi_InfiPack(apbuf ,1000,10*mS))
    {
        apbuflen = strlen(apbuf);
    }
    content_len = apbuflen  + sizeof(index_html_1) + sizeof(index_html_2);
    sprintf(pnew, http_resp_header,0,200, content_len);

    for(int i = 0;i<4;i++)
    {
        switch (i) {
        case 0:
            content = pnew;
            len = (int)strlen(pnew);
            break;
        case 1:
            content = (char*)index_html_1;
            len = sizeof(index_html_1);
            break;
        case 2:
            content = apbuf;
            len = apbuflen;
            break;
        case 3:
            content =  (char*)index_html_2;
            len = sizeof(index_html_2);
            break;
        default:
                goto END_SEND;
                break;
        }
        sended = 0;
        cnts = 0;
        s64 timebak =  DjyGetSysTime();
        while (sended < len) {
            cnts = send(fd, &content[sended], len-sended, 0);
            if (cnts > 0) {
                sended += cnts;
            }
            if (errno == ESHUTDOWN) {
                ret = -1;
                goto END_SEND;
            }
            if(DjyGetSysTime()-timebak > (20*1000*mS))
            {
                printf("%s : %s timeout \n\r ",__FILE__,__func__);
                goto END_SEND;
            }
        }
    }
END_SEND:
    if (pnew) free(pnew);
    return ret;
}


static int hex2dec(char c)
{
    if ('0' <= c && c <= '9')
    {
        return c - '0';
    }
    else if ('a' <= c && c <= 'f')
    {
        return c - 'a' + 10;
    }
    else if ('A' <= c && c <= 'F')
    {
        return c - 'A' + 10;
    }
    else
    {
        return -1;
    }
}

// 解码url
static bool_t  urldecode(char *url,char*outbuf,int maxlen)
{
    int i = 0;
    int len = strlen(url);
    int res_len = 0;
    for (i = 0; i < len; ++i)
    {
        if((i-1)>=maxlen)
            return false;
        char c = url[i];
        if (c != '%')
        {
            outbuf[res_len++] = c;
        }
        else
        {
            char c1 = url[++i];
            char c0 = url[++i];
            int num = 0;
            num = hex2dec(c1) * 16 + hex2dec(c0);
            outbuf[res_len++] = num;
        }
    }
    outbuf[res_len] = '\0';
    return true;
}

static int get_conf_info(unsigned char *input, char *ssid, u32 ssidmaxlen,char *key, u32 keymaxlen)
{
    size_t min = 0;
    if (input==NULL || ssid==NULL || key==NULL) return -1;
    const char tag_ssid[]="ssid=";
    const char tag_pwd[]="pwd=";
    char *p1 = strstr((char*)input, tag_ssid);
    char *p2 = 0;
    if (p1) {
         p1 = p1+strlen(tag_ssid);
         p2 = strstr(p1, "&");
         if (p2) *p2 = 0;
         urldecode(p1,ssid,ssidmaxlen);
         printf("ssid=%s!\r\n", ssid);
         p1 = p2+1;
         p1 = strstr(p1, tag_pwd);
         if (p1) {
             p1 = p1+strlen(tag_pwd);
             p2 = strstr(p1, " ");
             if (p2) *p2 = 0;
             min = keymaxlen - 1;
             min = min < strlen(p1) ? min : strlen(p1);
             strcpy(key,p1);
             printf("pwd=%s!\r\n", p1);
             return 0;
         }
     }
     return -1;
}

static  ptu32_t  http_Server(void)
{

    int sockfd;
    int clientfd;
    int tempfd;
    int addrlen;
    struct sockaddr_in serveraddr;
    struct sockaddr_in clientaddr;
    char *httpsndbuf;
    struct MultiplexSetsCB *acceptsets;
    int sndopt;
    u32 Status;
    int ret=0;

    acceptsets = Multiplex_Create(1);
    if(NULL==acceptsets)
    {
        printf("Create acceptsets failed!\n\r");
    }
    else
    {
        printf("Create acceptsets success!\n\r");
    }

    httpsndbuf =  malloc(CN_BUF_LEN);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(-1 == sockfd)
    {
        printf("http socket failed!\n\r");
        return false;
    }
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(80);
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);

    if(-1==bind(sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)))
    {
        printf("http bind failed!\n\r");
        return false;
    }
    if(-1 == listen(sockfd, 2))
    {
        printf("http listen failed!\n\r");
        return false;
    }
    printf("http listen \n\r");

    if(Multiplex_AddObject(acceptsets,sockfd,CN_SOCKET_IOACCEPT|CN_SOCKET_IOREAD|\
            CN_SOCKET_IOWRITE|CN_SOCKET_IOERR|CN_MULTIPLEX_SENSINGBIT_OR))
    {
        printf("http add server to acceptsets success!\n\r");
    }
    else
    {
        printf("http add server to acceptsets failed!\n\r");
    }

    while(1)
    {
        Status = 0;
        tempfd = Multiplex_Wait(acceptsets, &Status, CN_TIMEOUT_FOREVER);
        if (tempfd == sockfd) {
            clientfd = accept(sockfd,(struct sockaddr *)&clientaddr, &addrlen);
            if (-1 == clientfd)
            {
                  continue;
            }
//            sndopt = 1024*4;
//            if(setsockopt(clientfd, SOL_SOCKET,SO_SNDBUF,&sndopt,4))
//            {
//                printf("warning: fd(%d) socket close SO_RCVTIMEO failed!\n\r", clientfd);
//            }
            sndopt = 0;
            if(setsockopt(clientfd, SOL_SOCKET,SO_RCVTIMEO,&sndopt,4))
            {
                printf("error: fd(%d) socket SO_RCVTIMEO failed!\n\r", clientfd);
            }
            sndopt = 1;
            if(setsockopt(clientfd, SOL_SOCKET,SO_NONBLOCK,&sndopt,4))
            {
                printf("error: fd(%d) socket SO_NOBLOCK failed!\n\r", clientfd);
            }

            if(!Multiplex_AddObject(acceptsets,clientfd,
                       CN_SOCKET_IOREAD|CN_SOCKET_IOERR|CN_MULTIPLEX_SENSINGBIT_OR))
            {
                printf("add server to acceptsets failed!\n\r");
            }
            continue;
        }

        if(Status & CN_SOCKET_IOREAD)//读
        {
            while (1) {
                memset(httpsndbuf, 0, CN_BUF_LEN);
                ret = recv(tempfd, httpsndbuf, CN_BUF_LEN-1, 0);
                Reset_DelayTime();
                httpsndbuf[ret] = '\0';
                switch (errno) {
                case ESHUTDOWN:
                case ENOTSOCK:
                    goto SOCKET_ERROR;
                    break;
                case EN_NEWLIB_NO_ERROR:

                     if (strstr(httpsndbuf, http_req_token)) {
                         sprintf(httpsndbuf,ObjectMoved,CFG_DHCPD_SERVERIP);
                         send(tempfd, httpsndbuf, strlen(httpsndbuf), 0);
                         goto SOCKET_ERROR;
                    }
                    else {
                         send(tempfd, httpnocontent, strlen(httpnocontent), 0);
                         printf("info: no content, ESHUTDOWN!\r\n");
                         goto SOCKET_ERROR;
                    }
                    break;

                default:
                    break;
                }
                if (CN_BUF_LEN-1 != ret) break;
                if (ret < 0)  {
                    Djy_EventDelay(500);
                }
            }
        }else  if(Status & CN_SOCKET_IOWRITE)//写
        {
            printf("info: CN_SOCKET_IOWRITE entry!\r\n");
        }
        else if(Status & CN_SOCKET_IOERR)//错误
        {
            printf("info: CN_SOCKET_IOERR entry!\r\n");
            goto SOCKET_ERROR;
        }
        continue;

SOCKET_ERROR:
        Multiplex_DelObject(acceptsets, tempfd);
        closesocket(tempfd);
    }
    printf("Portal_Webserver exit !!\\r\n");
    free(httpsndbuf);
    Multiplex_DelObject(acceptsets, sockfd);
    closesocket(sockfd);
    return false;

    return 0;
}
static bool_t Portal_Webserver(char *ssid,u32 ssidmaxlen,char *key,u32 keymaxlen)
{
    int sockfd;
    int clientfd;
    int tempfd;
    int addrlen;
    struct sockaddr_in serveraddr;
    struct sockaddr_in clientaddr;
    char *httpsndbuf;
    struct MultiplexSetsCB *acceptsets;
    int sndopt;
    u32 Status;
    int ret=0;

    acceptsets = Multiplex_Create(1);
    if(NULL==acceptsets)
    {
        printf("Webserver Create acceptsets failed!\n\r");
    }
    else
    {
        printf("Webserver Create acceptsets success!\n\r");
    }

    httpsndbuf =  malloc(CN_BUF_LEN);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if(-1 == sockfd)
    {
        printf("Webserver Portal_Webserver socket failed!\n\r");
        return false;
    }
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(82);
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);

    if(-1==bind(sockfd, (struct sockaddr *)&serveraddr, sizeof(serveraddr)))
    {
        printf("Webserver Portal_Webserver bind failed!\n\r");
        return false;
    }
    if(-1 == listen(sockfd, 10))
    {
        printf("Webserver Portal_Webserver listen failed!\n\r");
        return false;
    }
    printf("Webserver Portal_Webserver listen \n\r");

    if(Multiplex_AddObject(acceptsets,sockfd,CN_SOCKET_IOACCEPT|CN_SOCKET_IOREAD|\
            CN_SOCKET_IOWRITE|CN_SOCKET_IOERR|CN_MULTIPLEX_SENSINGBIT_OR))
    {
        printf("Webserver add server to acceptsets success!\n\r");
    }
    else
    {
        printf("Webserver add server to acceptsets failed!\n\r");
    }

    while(1)
    {
        Status = 0;
        tempfd = Multiplex_Wait(acceptsets, &Status, CN_TIMEOUT_FOREVER);
        if (tempfd == sockfd) {
            clientfd = accept(sockfd,(struct sockaddr *)&clientaddr, &addrlen);
            if (-1 == clientfd)
            {
                  continue;
            }
            sndopt = 1024*4;
            if(setsockopt(clientfd, SOL_SOCKET,SO_SNDBUF,&sndopt,4))
            {
                printf("warning: fd(%d) socket close SO_RCVTIMEO failed!\n\r", clientfd);
            }
            sndopt = 0;
            if(setsockopt(clientfd, SOL_SOCKET,SO_RCVTIMEO,&sndopt,4))
            {
                printf("error: fd(%d) socket SO_RCVTIMEO failed!\n\r", clientfd);
            }
            sndopt = 1;
            if(setsockopt(clientfd, SOL_SOCKET,SO_NONBLOCK,&sndopt,4))
            {
                printf("error: fd(%d) socket SO_NOBLOCK failed!\n\r", clientfd);
            }

            if(!Multiplex_AddObject(acceptsets,clientfd,
                       CN_SOCKET_IOREAD|CN_SOCKET_IOERR|CN_MULTIPLEX_SENSINGBIT_OR))
            {
                printf("add server to acceptsets failed!\n\r");
            }
            continue;
        }

        if(Status & CN_SOCKET_IOREAD)//读
        {
            while (1) {
                memset(httpsndbuf, 0, CN_BUF_LEN);
                ret = recv(tempfd, httpsndbuf, CN_BUF_LEN-1, 0);
                Reset_DelayTime();
                 switch (errno) {
                case ESHUTDOWN:
                case ENOTSOCK:
                    goto SOCKET_ERROR;
                    break;
                case EN_NEWLIB_NO_ERROR:
                    if (strstr(httpsndbuf, http_conf_token)) {
                        if (0 == get_conf_info((unsigned char *)httpsndbuf, ssid, ssidmaxlen, key, keymaxlen)) {
                            if (-1 == send_http (tempfd, Successful_html, sizeof(Successful_html),1,200))
                            {
                                printf("==============SOCKET_ERROR =============\n\r");
                                goto SOCKET_ERROR;
                            }
                            Djy_EventDelay(200*mS);
                            free(httpsndbuf);
                            Multiplex_DelObject(acceptsets, sockfd);
                            closesocket(sockfd);
                            exitserver = 1;
                            return true;
                        }
                    } else if (strstr(httpsndbuf, getwifiinfo)){//wifi 列表
                        DjyWifi_StartScan(scan_ApCallBack);
                        if(true == Wifi_InfiPack(httpsndbuf,CN_BUF_LEN,1000*mS))
                        {
                            if (-1 == send_http (tempfd, httpsndbuf, strlen(httpsndbuf),0,200))
                                goto SOCKET_ERROR;
                        }
                    } else if (strstr(httpsndbuf, http_req_token)) {
                        if (-1 == send_http_configwifi(tempfd))
                            goto SOCKET_ERROR;
                    }
                    else {
                        send(tempfd, httpnocontent, strlen(httpnocontent), 0);
                         printf("info: no content, ESHUTDOWN!\r\n");
                         goto SOCKET_ERROR;
                    }
                    break;

                default:
                    break;
                }
                if (CN_BUF_LEN-1 != ret) break;
                if (ret < 0)  {
                    Djy_EventDelay(500);
                }
            }
        }else  if(Status & CN_SOCKET_IOWRITE)//写
        {
            printf("info: CN_SOCKET_IOWRITE entry!\r\n");
        }
        else if(Status & CN_SOCKET_IOERR)//错误
        {
            printf("info: CN_SOCKET_IOERR entry!\r\n");
            goto SOCKET_ERROR;
        }
        continue;


SOCKET_ERROR:
        Multiplex_DelObject(acceptsets, tempfd);
        closesocket(tempfd);
    }
    printf("Portal_Webserver exit !!\\r\n");
    free(httpsndbuf);
    Multiplex_DelObject(acceptsets, sockfd);
    closesocket(sockfd);
    return false;
}

static  void DecodeDomainName(unsigned  char *name,int len)
{
    unsigned char *s;
    unsigned char num;

    s = name;
    while((*s != '\0')&&((ptu32_t)s < ((ptu32_t)name + len)))
    {
        num = *s+1;
        *s = '.';
        s += num;
    }

    return;
}

//=============================DNS=============================================
static bool_t Dns_PacketCheck(tagDnsHeader *packet)
{
    if(packet == NULL)
        return false;
//    tagDnsHeader PACK;
//    PACK.
//    PACK.tid     = ;        /* Transaction ID */
//    PACK.flags;      /* Flags */
//    PACK.nqueries;   /* Questions */
//    PACK.nanswers;   /* Answers */
//    PACK.nauth;      /* Authority PRs */
//    PACK.nother;     /* Other PRs */
//    PACK.data[0];    /* Data, variable length */
    return 1;
    tagDnsHeader *pdns = malloc(strlen(packet->data)+1);
    if (packet->data[0] && pdns) {
        strcpy(pdns, packet->data);
        DecodeDomainName(pdns, strlen(pdns)+1);
        if (strstr(pdns, "djyos.com") ||
            strstr(pdns, "apple.com") ||
            strstr(pdns, "connectivitycheck") ||
            strstr(pdns, "google.com")) {
            printf("domain:%s!\r\n", pdns);
            printf("------get domain-----!\r\n");
            if (pdns) free(pdns);
            return true;
        }
    }
    if (pdns) free(pdns);
    return false;
}

static int Dns_Ackpacket(tagDnsHeader *packet)
{
    char *p;
    int len;
    u16 data16;
    u32 data32;

    p = (char *)packet->data;
    packet->flags    = htons(0x8180);
    packet->nanswers = htons(1);

    len = strlen(p)+1+sizeof(u16)+sizeof(u16);
    p+=len;

    //应答包
    data16 = htons(0xc00c);//域名地址
    memcpy((void *)p, (void *)&data16,sizeof(data16));
    p+=sizeof(data16);

    data16 = htons(1);//type
    memcpy((void *)p, (void *)&data16,sizeof(data16));
    p+=sizeof(data16);

    data16 = htons(1);//class
    memcpy((void *)p, (void *)&data16,sizeof(data16));
    p+=sizeof(data16);

    data32 = htonl(4000);//ttl
    memcpy((void *)p, (void *)&data32,sizeof(data32));
    p+=sizeof(data32);

    data16 = htons(4);//datalen
    memcpy((void *)p, (void *)&data16,sizeof(data16));
    p+=sizeof(data16);

    data32 = (u32)inet_addr(CFG_DHCPD_SERVERIP);//ipaddr
    memcpy((void *)p, (void *)&data32,sizeof(data32));
    p+=sizeof(data32);

    return (int)(p-(char *)packet);
}

static ptu32_t Dns_Server(void)
{
    int addrlen;
    struct sockaddr_in serveraddr;
    struct sockaddr_in clientaddr;
    char *rcvbuf;
    int   rcvlen;
    int sndopt;
    int sendlen;

    server = socket(AF_INET, SOCK_DGRAM, 0);
    if(-1 == server)
    {
        printf("Dns_Server socket failed!\n\r");
        return false;
    }
    else
    {
        printf("Dns_Server socket success!\n\r");
    }
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_port = htons(CN_DNS_SERVERPORT);
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);

    if(-1==bind(server, (struct sockaddr *)&serveraddr, sizeof(serveraddr)))
    {
        printf("Dns_Server bind failed!\n\r");
        return false;
    }
    else
    {
        printf("bind success!\n\r");
    }
    //设置接收缓冲区
    sndopt = CN_SOCKBUF_LEN;
    if(0 == setsockopt(server,SOL_SOCKET ,SO_RCVBUF,&sndopt,4))
    {
        printf("Client:set client sndbuf success!\n\r");
    }
    else
    {
        printf("Client:set client sndbuf failed!\n\r");
    }
    //设置发送缓冲区触发水平
    rcvbuf = malloc(CN_BUF_LEN);
    if(NULL != rcvbuf)
    {
        addrlen = sizeof(struct sockaddr);
        while(1)
        {
            rcvlen = recvfrom(server,rcvbuf,CN_BUF_LEN-1,0,(struct sockaddr *)&clientaddr,&addrlen);
            if(rcvlen > 0)
            {
                rcvbuf[rcvlen] = '\0';
                Reset_DelayTime();
                //DNS包检查
                if(true == Dns_PacketCheck((tagDnsHeader *)rcvbuf))
                {
                    //封装应答DNS包
                    sendlen =  Dns_Ackpacket((tagDnsHeader *)rcvbuf);
                    //发送应答DNS包
                    sendto(server, rcvbuf, sendlen, 0, (struct sockaddr *)&clientaddr,addrlen);
                }

            }
            if(exitserver)
            {
                free(rcvbuf);
                closesocket(server);
                //删除线程
                return true;
            }
        }
    }
    free(rcvbuf);
    closesocket(server);
    return false;
}

bool_t ModuleInstall_Portal(char *ssid,u32 ssidmaxlen,char *key,u32 keymaxlen)
{
    static u16   evtt_id = CN_EVTT_ID_INVALID;
    static u16   evtt = CN_EVTT_ID_INVALID;
    static u16   evtttime = CN_EVTT_ID_INVALID;
    bool_t flag;

    Reset_DelayTime();
    wifiInfo = Lock_MutexCreate("wifi Info Semp");
    if(wifiInfo==NULL)
    {
        printf("error : wifi Info Mutex Create Error!\r\n");
        return false;
    }else{
        DjyWifi_StartScan(scan_ApCallBack);
    }
    if(evtt_id == CN_EVTT_ID_INVALID)
    {
        evtt_id = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS+1, 0, 1,
                Dns_Server,NULL, 0x1000*2, "DNS Server");
        if (evtt_id != CN_EVTT_ID_INVALID)
        {
            Djy_EventPop(evtt_id, NULL, 0, 0, 0, 0);
        }
    }
    if(evtttime == CN_EVTT_ID_INVALID)
    {
        evtttime = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS+1, 0, 1,
                Time_Server,NULL, 0x1000, "timedelay");
        if (evtttime != CN_EVTT_ID_INVALID)
        {
            Djy_EventPop(evtttime, NULL, 0, 0, 0, 0);
        }
    }
    if(evtt == CN_EVTT_ID_INVALID)
    {
        evtt = Djy_EvttRegist(EN_CORRELATIVE, CN_PRIO_RRS+1, 0, 1,
                http_Server,NULL, 0x1000*2, "http Server");
        if (evtt != CN_EVTT_ID_INVALID)
        {
            Djy_EventPop(evtt, NULL, 0, 0, 0, 0);
        }
    }

    flag = Portal_Webserver(ssid,ssidmaxlen,key,keymaxlen);
    return flag;
}





