#ifndef __CHAIN_TABLE_H__
#define __CHAIN_TABLE_H__

typedef struct Node
{
    int data;
    char url[128];
    char song[64];
    char singer[32];
    struct Node *pNext;
    struct Node *pPre;
}NODE, *pNODE;

//创建双向链表
pNODE CreateDbLinkList(void);

//打印链表
void TraverseDbLinkList(pNODE pHead);

//判断链表是否为空
int IsEmptyDbLinkList(pNODE pHead);

//计算链表长度
int GetLengthDbLinkList(pNODE pHead);

//向双向链表中顺序插入节点
int InsertSeqDbLinkList(pNODE pHead,int data,char *url,char *song,char *singer);

//向链表插入节点
int InsertEleDbLinkList(pNODE pHead, int pos, int data,char *url);

//从链表删除节点
int DeleteEleDbLinkList(pNODE pHead, int pos);

//删除整个链表，释放内存
void FreeMemory(pNODE *ppHead);

#endif

