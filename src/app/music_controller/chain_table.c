#include "project_config.h"
#include "stdint.h"
#include "stddef.h"
#include "chain_table.h"

//创建双向链表
pNODE CreateDbLinkList(void)
{
    int i, length = 0, data = 0;
    pNODE pTail = NULL, p_new = NULL;
    pNODE pHead = (pNODE)malloc(sizeof(NODE));

    if (NULL == pHead)
    {
        printf("内存分配失败！\n\r");
    }

    pHead->data = 0;
    memcpy((p_new->url),NULL,strlen(NULL)+1);
    memcpy((p_new->song),NULL,strlen(NULL)+1);
    memcpy((p_new->singer),NULL,strlen(NULL)+1);
    pHead->pPre = NULL;
    pHead->pNext = NULL;
    pTail = pHead;

    return pHead;
}

//打印链表
void TraverseDbLinkList(pNODE pHead)
{
    pNODE pt = pHead->pNext;

    printf("打印链表如：");
    while (pt != NULL)
    {
        printf("%d ", pt->data);
        printf("%s ", pt->url);
        printf("%s ", pt->song);
        printf("%s ", pt->singer);
        pt = pt->pNext;
    }
    printf("\n\r");
}

//判断链表是否为空
int IsEmptyDbLinkList(pNODE pHead)
{

    if (pHead == NULL)
    {
        return 1;
    }
    else
    {
        return 0;
    }

}

//计算链表的长度
int GetLengthDbLinkList(pNODE pHead)
{
    int length = 0;
    pNODE pt = pHead->pNext;

    while (pt != NULL)
    {
        length++;
        pt = pt->pNext;
    }
    return length;
}

//向双向链表中顺序插入节点
int InsertSeqDbLinkList(pNODE pHead,int data,char *url,char *song,char *singer)
{
    pNODE pt = NULL, p_new = NULL;

    int pos=GetLengthDbLinkList(pHead)+1;

    if (pos > 0 && pos < GetLengthDbLinkList(pHead)+2)
    {
        p_new = (pNODE)malloc(sizeof(NODE));

        if (NULL == p_new)
        {
            printf("内存分配失败！\n\r");
            return 0;
        }

        while (1)
        {
            pos--;
            if (0 == pos)
            {
                break;
            }
            pHead = pHead->pNext;
        }

        pt = pHead->pNext;
        p_new->data = data;
        memcpy((p_new->url),url,strlen(url)+1);
        memcpy((p_new->song),song,strlen(song)+1);
        memcpy((p_new->singer),singer,strlen(singer)+1);
        p_new->pNext = pt;
        if (NULL != pt)
        {
            pt->pPre = pHead;
        }
        p_new->pPre = pHead;
        pHead->pNext = p_new;

        return 1;
    }
    else
    {
        return 0;
    }
}

//向双向链表中插入节点
int InsertEleDbLinkList(pNODE pHead, int pos, int data,char *url)
{
//    if(IsEmptyDbLinkList(pHead))
//    {
//        printf("双向链表为空！\r\n");
//        return 0;
//    }

    pNODE pt = NULL, p_new = NULL;

    if (pos > 0 && pos < GetLengthDbLinkList(pHead)+2)
    {
        p_new = (pNODE)malloc(sizeof(NODE));

        if (NULL == p_new)
        {
            printf("内存分配失败！\n\r");
        }

        while (1)
        {
            pos--;
            if (0 == pos)
            {
                break;
            }
            pHead = pHead->pNext;
        }

        pt = pHead->pNext;
        p_new->data = data;
        memcpy((p_new->url),url,strlen(url)+1);
        p_new->pNext = pt;
        if (NULL != pt)
        {
            pt->pPre = pHead;
        }
        p_new->pPre = pHead;
        pHead->pNext = p_new;

        return 1;
    }
    else
    {
        return 0;
    }
}


//从链表中删除节点
int DeleteEleDbLinkList(pNODE pHead, int pos)
{
    pNODE pt = NULL;

    if (pos > 0 && pos < GetLengthDbLinkList(pHead) + 1)
    {
        while (1)
        {
            pos--;
            if (0 == pos)
            {
                break;
            }
            pHead = pHead->pNext;
        }

        pt = pHead->pNext->pNext;
        free(pHead->pNext);
        pHead->pNext = pt;
        if (NULL != pt)
        {
            pt->pPre = pHead;
        }
        return 1;
    }
    else
    {
        return 0;
    }
}

//删除整个链表，释放内存
void FreeMemory(pNODE *ppHead)
{
    pNODE pt = NULL;

    while (*ppHead != NULL)
    {
        pt = (*ppHead)->pNext;
        free(*ppHead);
        if (NULL != pt)
        {
            pt->pPre = NULL;
        }
        *ppHead = pt;
    }
}

