#include "project_config.h"
#include "stdint.h"
#include "stddef.h"
#include "music_player.h"
#include "chain_table.h"

extern struct player_ctrl_info PlayerCtrlInfo;
void test(void)
{
    int flag = 0, length = 0;
    pNODE head = NULL;

    head = CreateDbLinkList();

    flag = IsEmptyDbLinkList(head);
    if (flag)
    {
        printf("双向链表为空！\r\n");
    }
    else
    {
        length = GetLengthDbLinkList(head);
        printf("双向链表的长度为：%d\r\n", length);
        TraverseDbLinkList(head);
    }

    for(int i=0;i<5;i++)
    {
        flag = InsertSeqDbLinkList(head, i,"https","hh","dsa");
        if (flag)
        {
            printf("插入节点成功！\r\n");
            TraverseDbLinkList(head);
        }
        else
        {
            printf("插入节点失败！\r\n");
        }
    }

    flag = IsEmptyDbLinkList(head);
    if (flag)
    {
        printf("双向链表为空，不能进行删除操作！\r\n");
    }

    else
    {
        printf("请输入要删除节点的位置：2\r\n");
        flag = DeleteEleDbLinkList(head, 2);
        if (flag)
        {
            printf("删除节点成功！\r\n");
            TraverseDbLinkList(head);
        }
        else
        {
            printf("删除节点失败！\r\n");
        }
        printf("请输入要删除节点的位置：22\r\n");
         flag = DeleteEleDbLinkList(head, 2);
         if (flag)
         {
             printf("删除节点成功！\r\n");
             TraverseDbLinkList(head);
         }
         else
         {
             printf("删除节点失败！\r\n");
         }
    }

    FreeMemory(&head);
    if (NULL == head)
    {
        printf("已成功删除双向链表，释放内存完成！\r\n");
    }
    else
    {
        printf("删除双向链表失败，释放内存未完成！\r\n");
    }

//    head = CreateDbLinkList();

        flag = IsEmptyDbLinkList(head);
        if (flag)
        {
            printf("双向链表为空！\r\n");
        }
        else
        {
            length = GetLengthDbLinkList(head);
            printf("双向链表的长度为：%d\r\n", length);
            TraverseDbLinkList(head);
        }

        for(int i=0;i<5;i++)
        {
            flag = InsertSeqDbLinkList(head, i,"https","hh","dsa");
            if (flag)
            {
                printf("插入节点成功！\r\n");
                TraverseDbLinkList(head);
            }
            else
            {
                printf("插入节点失败！\r\n");
            }
        }

        flag = IsEmptyDbLinkList(head);
        if (flag)
        {
            printf("双向链表为空，不能进行删除操作！\r\n");
        }

        else
        {
            printf("请输入要删除节点的位置：2\r\n");
            flag = DeleteEleDbLinkList(head, 2);
            if (flag)
            {
                printf("删除节点成功！\r\n");
                TraverseDbLinkList(head);
            }
            else
            {
                printf("删除节点失败！\r\n");
            }
            printf("请输入要删除节点的位置：22\r\n");
             flag = DeleteEleDbLinkList(head, 2);
             if (flag)
             {
                 printf("删除节点成功！\r\n");
                 TraverseDbLinkList(head);
             }
             else
             {
                 printf("删除节点失败！\r\n");
             }
        }

        FreeMemory(&head);
        if (NULL == head)
        {
            printf("已成功删除双向链表，释放内存完成！\r\n");
        }
        else
        {
            printf("删除双向链表失败，释放内存未完成！\r\n");
        }

}

void test2(void)
{
    printf("++++++===========:%d\r\n",PlayerCtrlInfo.FilePlayTo);
    printf("测试初始化\r\n");
    playerInit();

    printf("测试添加歌曲\r\n");
    for(int i=1;i<5;i++)
    {
        pllsAppendItem(i,"https","hh","dsa");
    }
    printf("测试开始播放\r\n");
    PlayerCtrlInfo.FilePlayTo=1000;
    for(int i=0;i<6;i++)
    {
        if(3==i)
        {
            playerNext();
        }
        playlistPlay();
        printf("\r\n");
    }
    printf("测试删除第二首歌\r\n");
    pllsDelete(2);
    printf("测试清空列表\r\n");
    pllsClear();

}
